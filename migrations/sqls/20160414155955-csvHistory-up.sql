CREATE TABLE IF NOT EXISTS `csvUploadHistories` (
`id` INTEGER auto_increment,
`name` VARCHAR(255),
`url`  VARCHAR(255),
`admin_id` INTEGER,
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME,
PRIMARY KEY (`id`),
FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB;

ALTER TABLE `itemByShops` ADD `from` BIGINT;
ALTER TABLE `itemByShops` ADD `to` BIGINT;