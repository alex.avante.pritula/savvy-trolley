
INSERT INTO `permissions`
(`id`, `permission`, `createdAt`, `updatedAt`)
VALUES
(35, 'BLOCK_PERMISSIONS_TO_SHOP', now(), now()),
(36, 'UPLOAD_FROM_CSV', now(), now());

INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT id, 36,now(), now() FROM admins WHERE role  = 'SUPER_ADMIN' OR role = 'ENTERPRISE_SAVVY_SHOP';

INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT id, 35,now(), now() FROM `admins` WHERE `admins`.`role` =  'SUPER_ADMIN' OR `admins`.`role` = 'SHOP';

INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT id, 28, now(), now() FROM `admins` WHERE `admins`.`role` NOT IN ('SUPER_ADMIN', 'USER', 'STATISTIC');

INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT id, 29, now(), now() FROM `admins` WHERE `admins`.`role` NOT IN ('SUPER_ADMIN', 'USER', 'STATISTIC');

INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT id, 30, now(), now() FROM `admins` WHERE `admins`.`role` NOT IN ('SUPER_ADMIN', 'USER', 'STATISTIC');

INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT id, 31, now(), now() FROM `admins` WHERE `admins`.`role` NOT IN ('SUPER_ADMIN', 'USER', 'STATISTIC');