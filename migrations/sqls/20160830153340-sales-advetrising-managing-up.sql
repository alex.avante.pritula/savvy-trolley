CREATE TABLE IF NOT EXISTS `shopManagingPermissions` (
`id` INTEGER auto_increment,
`message` VARCHAR(255),
`admin_id` INTEGER,
`shop_id`  INTEGER,
`type`  VARCHAR(20),
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
PRIMARY KEY (`id`),
FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`shop_id`)  REFERENCES `shops`  (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB;

ALTER TABLE `shops` ADD `hasSales` TINYINT(1) DEFAULT true;
ALTER TABLE `shops` ADD `hasAdvertising` TINYINT(1) DEFAULT true;