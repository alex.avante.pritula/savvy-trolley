CREATE TABLE IF NOT EXISTS `settings` (
`id` INTEGER auto_increment ,
`hasAdverts` TINYINT(1) DEFAULT TRUE,
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
PRIMARY KEY (`id`)) ENGINE=InnoDB;

INSERT INTO `settings`
(`hasAdverts`, `createdAt`, `updatedAt`)
VALUES
(TRUE, now(), now());