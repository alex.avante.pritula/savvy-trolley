ALTER TABLE `devices`
CHANGE `deviceId` `device_id` VARCHAR(190),
ADD CONSTRAINT `uc_device` UNIQUE `device_id_user_id_unique` (`device_id`, `user_id`);