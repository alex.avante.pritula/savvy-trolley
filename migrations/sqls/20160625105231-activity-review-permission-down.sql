DELETE FROM `permissions` WHERE `id` IN (34);

ALTER TABLE `lastEditLogs`
ADD `version` INTEGER DEFAULT 1,
ADD CONSTRAINT `entity_type_entity_id_unique` UNIQUE (`entity_type`, `entity_id`)