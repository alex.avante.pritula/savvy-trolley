INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT id, 18, now(), now()
FROM admins WHERE role = 'SHOP';