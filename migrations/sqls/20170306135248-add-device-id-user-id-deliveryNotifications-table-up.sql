ALTER TABLE `deliveryNotifications`
ADD COLUMN `device_id` VARCHAR(190) DEFAULT NULL,
ADD COLUMN `user_id` INT(11),
ADD CONSTRAINT `delivery_notification_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);