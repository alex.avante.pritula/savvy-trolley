INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT
id, 18, now(), now()
FROM admins WHERE role = 'ENTERPRISE_SAVVY_SHOP';

INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT
id, 20, now(), now()
FROM admins WHERE role = 'ENTERPRISE_SAVVY_SHOP';