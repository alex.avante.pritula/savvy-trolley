ALTER TABLE `itemByShops`
MODIFY `regularPrice` FLOAT,
MODIFY `salePrice` FLOAT,
MODIFY `unitPrice` FLOAT;