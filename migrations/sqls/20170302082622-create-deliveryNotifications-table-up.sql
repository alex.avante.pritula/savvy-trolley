CREATE TABLE `deliveryNotifications` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `entity_id` INT(11) NOT NULL,
  `entity_type` TINYINT(4) UNSIGNED DEFAULT 1,
  `is_sent` TINYINT(1) DEFAULT false,
  `push_type` TINYINT(4) UNSIGNED DEFAULT 1,
  `data` TEXT COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_date_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdAt` DATETIME NOT NULL,
  `updatedAt` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;