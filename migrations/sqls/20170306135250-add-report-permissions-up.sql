INSERT INTO `permissions` (id, permission, description, createdAt, updatedAt)
VALUES (37,'REVIEW_REPORT', NULL, now(), now()),
	   (38,'EDIT_REPORT', NULL, now(), now()),
	   (39,'REPORT', NULL, now(), now());

INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT
id, 39, now(), now()
FROM `admins` WHERE role IN ("SUPER_ADMIN", "SHOP", "ENTERPRISE_SAVVY_SHOP", "SAVVY_SHOP");

INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT
id, 37, now(), now()
FROM `admins` WHERE `role` IN ("SUPER_ADMIN", "SHOP", "ENTERPRISE_SAVVY_SHOP", "SAVVY_SHOP");

INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT
id, 38, now(), now()
FROM `admins` WHERE `role` IN ("SUPER_ADMIN", "SHOP", "ENTERPRISE_SAVVY_SHOP");