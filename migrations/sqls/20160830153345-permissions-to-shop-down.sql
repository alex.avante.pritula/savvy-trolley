DELETE FROM `permissionByAdmins` WHERE permission_id IN (35, 36);
DELETE FROM `permissions` WHERE id IN (35, 36);

DELETE FROM `permissionByAdmins` WHERE  permission_id IN (28, 29, 30, 31) AND
admin_id IN (SELECT id FROM admins WHERE role NOT IN ('SUPER_ADMIN', 'USER', 'STATISTIC')) ;