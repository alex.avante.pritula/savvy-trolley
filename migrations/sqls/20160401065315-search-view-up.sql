CREATE OR REPLACE VIEW `pricesForEnableItem` AS
 (SELECT `ibs`.`item_id`,`ibs`.`shop_id` , MIN(`ibs`.`salePrice`) as `bestPrice`
  FROM `itemByShops` as `ibs`
  WHERE `ibs`.`enabled` = true AND `ibs`.`isSale` = true
  GROUP BY `ibs`.`item_id`, `ibs`.`shop_id`)
UNION
 (SELECT `ibs`.`item_id`, `ibs`.`shop_id`, MIN(`ibs`.`regularPrice`) as `bestPrice`
  FROM `itemByShops` as `ibs`
  WHERE `ibs`.`enabled` = true AND `ibs`.`isSale` = false
  GROUP BY `ibs`.`item_id`, `ibs`.`shop_id`)
ORDER BY bestPrice;


CREATE OR REPLACE VIEW `bestPricesForEnableItem` AS
 SELECT   `item_id`, MIN(`bestPrice`) AS `price`, `shop_id`
 FROM `pricesForEnableItem`
 GROUP BY `item_id`;