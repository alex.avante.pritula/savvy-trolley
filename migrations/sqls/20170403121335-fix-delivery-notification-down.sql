ALTER TABLE `deliveryNotifications` DROP FOREIGN KEY `delivery_notification_user`;
ALTER TABLE `deliveryNotifications` ADD CONSTRAINT `delivery_notification_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
