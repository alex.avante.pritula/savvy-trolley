INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT
admin.id, permission.id, now(), now()
FROM admins AS admin, permissions AS permission
WHERE admin.role like '%SAVVY_SHOP' AND permission.id in (16,17,18,19,20,21);