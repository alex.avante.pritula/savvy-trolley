CREATE TABLE IF NOT EXISTS `advertising` (
`id` INTEGER auto_increment,
`item_id` INTEGER,
`product_id` INTEGER,
`post_code_id` INTEGER,
`from` BIGINT,
`to` BIGINT,
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
UNIQUE `advertising_product_id_item_id_unique` (`item_id`, `product_id`),
PRIMARY KEY (`id`),
FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`product_id`) REFERENCES `itemByShops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB;