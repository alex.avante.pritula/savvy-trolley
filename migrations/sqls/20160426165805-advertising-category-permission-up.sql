INSERT INTO `permissions`
(`id`, `permission`, `createdAt`, `updatedAt`)
VALUES
(24, 'CATEGORY', now(), now()),
(25, 'CREATE_CATEGORY', now(), now()),
(26, 'REVIEW_CATEGORY', now(), now()),
(27, 'EDIT_CATEGORY', now(), now()),

(28, 'ADVERTISING', now(), now()),
(29, 'CREATE_ADVERTISING', now(), now()),
(30, 'REVIEW_ADVERTISING', now(), now()),
(31, 'EDIT_ADVERTISING', now(), now());

INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT
admin.id, permission.id, now(), now()
FROM admins as admin CROSS JOIN permissions as permission
WHERE admin.role = 'SUPER_ADMIN' AND permission.permission in ('CATEGORY', 'CREATE_CATEGORY', 'REVIEW_CATEGORY',
 'EDIT_CATEGORY', 'ADVERTISING', 'CREATE_ADVERTISING', 'REVIEW_ADVERTISING', 'EDIT_ADVERTISING')
GROUP BY admin.id, permission.id