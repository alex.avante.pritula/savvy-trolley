ALTER TABLE `itemByShops`
MODIFY `regularPrice` FLOAT(9,2),
MODIFY `salePrice` FLOAT(9,2),
MODIFY `unitPrice` FLOAT(9,2);