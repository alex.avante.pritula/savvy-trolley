CREATE TABLE IF NOT EXISTS `histories` (
`id` INTEGER auto_increment,
`user_id` INTEGER,
`item_id` INTEGER,
`regularPrice` FLOAT(9,2),
`salePrice` FLOAT(9,2),
`unitPrice` FLOAT(9,2),
`isSale` VARCHAR(255),
`purchaseDate` BIGINT,
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
UNIQUE `histories_item_id_user_id_for_purchase_date_unique` (`user_id`, `item_id`, `purchaseDate`),
PRIMARY KEY (`id`),
FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB;