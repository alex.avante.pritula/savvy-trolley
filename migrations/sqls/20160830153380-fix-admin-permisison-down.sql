DELETE FROM `permissionByAdmins`
WHERE `permission_id` = 26 AND `admin_id` IN (SELECT id FROM admins WHERE role = 'ENTERPRISE_SAVVY_SHOP');

DELETE FROM `permissionByAdmins`
WHERE `permission_id` = 20 AND `admin_id` IN (SELECT id FROM admins WHERE role = 'SHOP');