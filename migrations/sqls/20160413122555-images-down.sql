ALTER TABLE `categories`
CHANGE `image_id` `image` VARCHAR(255),
DROP FOREIGN KEY `category_image`;

ALTER TABLE `shops`
CHANGE `image_id` `image` VARCHAR(255),
DROP FOREIGN KEY `shop_image`;

ALTER TABLE `items`
CHANGE `image_id` `image` VARCHAR(255),
DROP FOREIGN KEY `item_image`;

DROP TABLE IF EXISTS `images`;