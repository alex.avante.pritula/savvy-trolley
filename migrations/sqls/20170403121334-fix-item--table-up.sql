ALTER TABLE `items` DROP COLUMN units;
ALTER TABLE `items` CHANGE itemSize `itemSizeOld` VARCHAR(255);
ALTER TABLE `items` ADD itemSize FLOAT;
UPDATE `items` SET `itemSize` = CAST(`items`.`itemSizeOld` AS DECIMAL(10,2)) WHERE `items`.`itemSizeOld` IS NOT NULL;
ALTER TABLE `items` DROP COLUMN itemSizeOld; 

