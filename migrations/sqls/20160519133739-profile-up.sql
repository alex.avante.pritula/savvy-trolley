INSERT INTO `permissions`
(`id`, `permission`, `createdAt`, `updatedAt`)
VALUES
(32, 'PROFILE', now(), now());

INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT
id, 32, now(), now()
FROM admins;