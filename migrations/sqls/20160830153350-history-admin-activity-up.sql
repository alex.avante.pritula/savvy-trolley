CREATE TABLE IF NOT EXISTS `salesActivityByAdmins` (
`id` INTEGER auto_increment ,
`admin_id` INTEGER,
`product_id` INTEGER,
`from` BIGINT,
`to` BIGINT,
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
PRIMARY KEY (`id`),
FOREIGN KEY (`admin_id`)   REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`product_id`) REFERENCES `itemByShops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `advActivityByAdmins` (
`id` INTEGER auto_increment ,
`admin_id` INTEGER,
`advertising_id` INTEGER,
`from` BIGINT,
`to` BIGINT,
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
PRIMARY KEY (`id`),
FOREIGN KEY (`admin_id`)   REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`advertising_id`) REFERENCES `advertising` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB;