INSERT INTO `permissions` (id, permission, description, createdAt, updatedAt)
VALUES (40,'EDIT_ADWORDS', NULL, now(), now());

INSERT INTO `permissionByAdmins` (`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT id, 40, now(), now()
FROM `admins` WHERE `role`  = "SUPER_ADMIN";