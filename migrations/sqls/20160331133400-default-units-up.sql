INSERT INTO `units` (`id`, `type`, `createdAt`, `updatedAt`) VALUES
(1, 'ltr', now(), now()),
(2, 'ml', now(), now()),
(3, 'kg', now(), now()),
(4, 'gr', now(), now()),
(5, 'pc', now(), now());