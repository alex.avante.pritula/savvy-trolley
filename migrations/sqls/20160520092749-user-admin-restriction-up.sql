INSERT INTO `permissions`
(`id`, `permission`, `createdAt`, `updatedAt`)
VALUES
(33, 'EXCEPT_SUPER_ADMIN', now(), now());

INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT
id, 33, now(), now()
FROM admins WHERE role = 'USER';