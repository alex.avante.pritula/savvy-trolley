CREATE TABLE IF NOT EXISTS `users` (
`id` INTEGER auto_increment ,
`email` VARCHAR(190) UNIQUE,
`password` VARCHAR(255),
`verified` TINYINT(1) DEFAULT false,
`accountType` VARCHAR(50) DEFAULT 'email',
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
UNIQUE `users_email_unique` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `postCodes` (
`id` INTEGER auto_increment ,
`code` VARCHAR(4) UNIQUE,
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
UNIQUE `postCodes_code_unique` (`code`),
PRIMARY KEY (`id`)) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `postCodeByUsers` (
`id` INTEGER auto_increment,
`user_id` INTEGER,
`post_code_id` INTEGER,
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
UNIQUE `postCodeByUsers_post_code_id_user_id_unique` (`user_id`, `post_code_id`),
PRIMARY KEY (`id`),
FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`post_code_id`) REFERENCES `postCodes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `confirmCodes` (
`id` INTEGER auto_increment,
`code` VARCHAR(255),
`isActive` TINYINT(1) DEFAULT false,
`user_id` INTEGER,
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
PRIMARY KEY (`id`),
FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `categories` (
`id` INTEGER auto_increment ,
`title` VARCHAR(255),
`image` VARCHAR(255),
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
`parent_id` INTEGER,
PRIMARY KEY (`id`),
FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `units` (
`id` INTEGER auto_increment ,
`type` VARCHAR(255),
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
PRIMARY KEY (`id`)) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `items` (
`id` INTEGER auto_increment ,
`barCode` VARCHAR(36) UNIQUE,
`name` VARCHAR(255),
`brand` VARCHAR(255),
`image` VARCHAR(255),
`units` INTEGER,
`servingPerPack` INTEGER,
`servingSize` INTEGER,
`enabled` TINYINT(1) DEFAULT true,
`unit_id` INTEGER,
`serving_unit_id` INTEGER,
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
UNIQUE `items_barCode_unique` (`barCode`),
PRIMARY KEY (`id`),
FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
FOREIGN KEY (`serving_unit_id`) REFERENCES `units` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `itemByCategories` (
`id` INTEGER auto_increment ,
`item_id` INTEGER, `category_id`
INTEGER, `createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
UNIQUE `itemByCategories_item_id_category_id_unique` (`item_id`, `category_id`),
PRIMARY KEY (`id`),
FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `shops` (
`id` INTEGER auto_increment ,
`name` VARCHAR(255),
`image` VARCHAR(255),
`owner` VARCHAR(255),
`abn` VARCHAR(11),
`acn` VARCHAR(9),
`address` VARCHAR(255),
`contactName` VARCHAR(255),
`contactPhone` VARCHAR(255),
`contactEmail` VARCHAR(255),
`postCode` VARCHAR(255),
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
PRIMARY KEY (`id`)) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `itemByShops` (
`id` INTEGER auto_increment,
`regularPrice` FLOAT,
`salePrice` FLOAT,
`unitPrice` FLOAT,
`isSale` VARCHAR(255),
`enabled` TINYINT(1) DEFAULT true,
`item_id` INTEGER,
`shop_id` INTEGER,
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
UNIQUE `itemByShops_shop_id_item_id_unique` (`item_id`, `shop_id`),
PRIMARY KEY (`id`),
FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`shop_id`) REFERENCES `shops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB;
