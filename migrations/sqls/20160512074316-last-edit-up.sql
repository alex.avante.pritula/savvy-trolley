CREATE TABLE IF NOT EXISTS `lastEditLogs` (
`id` INTEGER auto_increment ,
`admin_id` INTEGER,
`entity_type` INTEGER NOT NULL,
`entity_id` INTEGER NOT NULL,
`operation_type` ENUM('CREATE', 'EDIT', 'DELETE'),
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
UNIQUE `entity_type_entity_id_unique` (`entity_type`, `entity_id`),
PRIMARY KEY (`id`),
FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE SET NULL ON UPDATE CASCADE) ENGINE=InnoDB;
