ALTER TABLE `lastEditLogs`
DROP `version`,
DROP INDEX `entity_type_entity_id_unique`;

INSERT INTO `permissions`
(`id`, `permission`, `createdAt`, `updatedAt`)
VALUES
(34, 'ACTIVITY_REVIEW', now(), now());

INSERT INTO `permissionByAdmins`
(`admin_id`, `permission_id`, `createdAt`, `updatedAt`)
SELECT
id, 34, now(), now()
FROM admins WHERE role not like '%SAVVY_SHOP';