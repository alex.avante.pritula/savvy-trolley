CREATE TABLE IF NOT EXISTS `shopByAdmins` (
`id` INTEGER auto_increment,
`admin_id` INTEGER,
`shop_id` INTEGER,
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
UNIQUE `shopByAdmins_shop_id_admin_id_unique` (`admin_id`, `shop_id`),
PRIMARY KEY (`id`),
FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`shop_id`) REFERENCES `shops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB;