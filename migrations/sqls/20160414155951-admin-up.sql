CREATE TABLE IF NOT EXISTS `admins` (
`id` INTEGER auto_increment ,
`email` VARCHAR(190) UNIQUE,
`password` VARCHAR(255),
`role` VARCHAR(255),
`type` VARCHAR(255),
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
UNIQUE `admins_email_unique` (`email`),
PRIMARY KEY (`id`)) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `permissions` (
`id` INTEGER auto_increment ,
`permission` VARCHAR(190) UNIQUE,
`description` TEXT,
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
UNIQUE `roles_role_unique` (`permission`),
PRIMARY KEY (`id`)) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `permissionByAdmins` (`id` INTEGER auto_increment ,
`admin_id` INTEGER,
`permission_id` INTEGER,
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
UNIQUE `roleByAdmins_role_id_admin_id_unique` (`admin_id`, `permission_id`),
PRIMARY KEY (`id`),
FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB;


INSERT INTO `permissions`
(`id`, `permission`, `createdAt`, `updatedAt`)
VALUES
(1, 'SHOP', now(), now()),
(2, 'CREATE_SHOP', now(), now()),
(3, 'REVIEW_SHOP', now(), now()),
(4, 'EDIT_SHOP', now(), now()),
(5, 'ONLY_OWN_SHOP', now(), now()),
(6, 'HAS_ONE_SHOP', now(), now()),
(7, 'NOT_HAS_SHOP', now(), now()),

(8, 'ITEM', now(), now()),
(9, 'CREATE_ITEM', now(), now()),
(10, 'REVIEW_ITEM', now(), now()),
(11, 'EDIT_ITEM', now(), now()),

(12, 'USER', now(), now()),
(13, 'CREATE_USER', now(), now()),
(14, 'REVIEW_USER', now(), now()),
(15, 'EDIT_USER', now(), now()),

(16, 'ADMIN', now(), now()),
(17, 'CREATE_ADMIN', now(), now()),
(18, 'REVIEW_ADMIN', now(), now()),
(19, 'EDIT_ADMIN', now(), now()),
(20, 'ASSIGN_SHOP', now(), now()),
(21, 'ONLY_ENTERPRISE_SAVVY_SHOP_ADMIN', now(), now()),

(22, 'STATISTIC', now(), now()),
(23, 'REVIEW_STATISTIC', now(), now());


INSERT INTO `admins`
(`id`, `email`, `password`, `type`, `role`, `createdAt`, `updatedAt`)
VALUES
(1, 'savvyreformations@gmail.com', 'rRnnRgIGl0lzN/K4/4A8WJ6WNJprBCzj1eXIB2hoOGc=', 'Super admin', 'SUPER_ADMIN', now(), now());

INSERT INTO `permissionByAdmins`
(`id`, `admin_id`, `permission_id`, `createdAt`, `updatedAt`)
VALUES
(1, 1, 1, now(), now()),
(2, 1, 2, now(), now()),
(3, 1, 3, now(), now()),
(4, 1, 4, now(), now()),
(5, 1, 7, now(), now()),
(6, 1, 8, now(), now()),
(7, 1, 9, now(), now()),
(8, 1, 10, now(), now()),
(9, 1, 11, now(), now()),
(10, 1, 12, now(), now()),
(11, 1, 13, now(), now()),
(12, 1, 14, now(), now()),
(13, 1, 15, now(), now()),
(14, 1, 16, now(), now()),
(15, 1, 17, now(), now()),
(16, 1, 18, now(), now()),
(17, 1, 19, now(), now()),
(18, 1, 20, now(), now()),
(19, 1, 22, now(), now()),
(20, 1, 23, now(), now());