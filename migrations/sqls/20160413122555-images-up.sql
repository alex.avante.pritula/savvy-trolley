CREATE TABLE IF NOT EXISTS `images` (
`id` INTEGER auto_increment,
`url` TEXT,
`name` VARCHAR(255),
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
PRIMARY KEY (`id`)) ENGINE=InnoDB;

ALTER TABLE `categories`
CHANGE `image` `image_id` INTEGER,
ADD CONSTRAINT `category_image` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`);

ALTER TABLE `shops`
CHANGE `image` `image_id` INTEGER,
ADD CONSTRAINT `shop_image` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`);

ALTER TABLE `items`
CHANGE `image` `image_id` INTEGER,
ADD CONSTRAINT `item_image` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`);

INSERT INTO `images` (`url`, `name`, `createdAt`, `updatedAt`)
VALUES 
('http://lorempixel.com/640/480/', 'lorempixel', now(), now());