CREATE TABLE IF NOT EXISTS `wishes` (
`id` INTEGER auto_increment ,
`user_id` INTEGER,
`item_id` INTEGER,
`createdAt` DATETIME NOT NULL,
`updatedAt` DATETIME NOT NULL,
UNIQUE `wishes_item_id_user_id_unique` (`user_id`, `item_id`),
PRIMARY KEY (`id`),
FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB;