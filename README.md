# Savvy_Trolley



### Install dependencies <br>
```bash
    npm install
```
### Create logs and uploads folders
```bash
    mkdir logs/ logs/export 
```

### Deploy database
```bash
    npm run create-db
    npm run migrate up
    npm run generate-data postcodes
```

### Run app: 
```bash
    npm start
```

### Generate random data: 
```bash
    npm run generate-data {items | shops | categories | connections | all} {number of resources}
```