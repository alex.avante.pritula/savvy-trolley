const services = require('../server/services');
const config = require('../config');
const kue = require('kue');
const Queue = kue.createQueue();
const Promise = require('bluebird');
const pushTypes = config.constants['PUSH_TYPES'];
var models = require('../server/models');

var notifications = [];
services.deliveryNotification.getPushNotifaicationsData()
  .then(function (existNotifications) {
      notifications = existNotifications;
      return Promise.mapSeries(notifications, (data) =>{
          if (data.push_type === pushTypes['SHOP_ADDED']) {
              return  'New shop was added!';
          }

          var itemId = JSON.parse(data['data']).itemId;
          return models.items.findById(itemId)
              .then((item) => {
                 if (item){
                    if (data.push_type === pushTypes['REGULAR_PRICE_REDUCED']){
                        return  item.name + '. Price reduced go and check it out!';
                    }

                    if (data.push_type === pushTypes['ITEM_ENABLED_MODE_CHANGED'] ||
                      data.push_type === pushTypes['CONCRETE_ITEM_ENABLED_MODE_CHANGED'] ||
                      data.push_type === pushTypes['ITEM_OUT_OF_STOCK']) {
                          return 'Uh oh... ' + item.name + ' in your Shopping List is now out of stock. Please review your Shopping List';
                    }

                    if (data.push_type === pushTypes['SALE_START']){
                        return  item.name + '. Sales start today';
                    }

                    if (data.push_type === pushTypes['SALE_FINISH']){
                        return item.name + '. Sales finishes today';
                    }
                 }
                 return '';
              });
      });
    })
    .then((titles)=>{
       var users = {};
       notifications.forEach((notification, i)=>{
          if (typeof (users[notifications[i]['token']]) === 'undefined') {
            users[notifications[i]['token']] = {
              token: notifications[i]['token'],
              ids  : [],
              title: ''
            };
          }

          if (titles[i].length) { 
            users[notifications[i]['token']].ids.push(notifications[i].id); 
            users[notifications[i]['token']].title += titles[i] + '\n';
          }
       });
       var tokens = Object.keys(users);
       return Promise.mapSeries(tokens, (token)=>{
          var options = users[token];
          if (!options['ids'].length || !options['title'].length){
              return;
          }
          return new Promise((resolve) => Queue.create('deliveryNotificationWorker', options).save(resolve));
       });
    })
    .then(() => {
      process.exit();
    })
    .catch(function (err) {
        console.warn(err);
        process.exit();
    });