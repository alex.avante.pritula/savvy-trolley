'use strict';

/**
 * Module dependencies.
 * @private
 */
var mysql = require('mysql');
var uuid = require('uuid');
var util = require('util');
var async = require('async');
var fs = require('fs');
var Promise = require('promise');
var Storage = require('./Storage');
var storageUtil = require('./storageUtil');
var constants = require('./constants.json');

/**
 * Logger storage by means of MySQL
 *
 * @constructor MySqlStorage
 * @extends Storage
 *
 * @public
 *
 * @param {Object} options
 * @param {Object} options.config database config
 * @param {String} [options.model] table name
 * @param {Object} [options.export] cron settings
 * @param {String} [options.export.path] backup folder
 * @param {String|Date} [options.export.time] cron time property
 * @param {String} [options.export.timeZone] cron time zone property
 * @param {Boolean} [options.export.enableAutoLogExport]
 * @param {Boolean} [options.export.enableHandLogExport]
 *
 * @throws  Will throw an error if the arguments `options.config` is null.
 */
function MySqlStorage(options) {
    if (!options) {
        throw new Error('Invalid options');
    }

    Storage.apply(this, arguments);
    if (options.config) {
        var config = options.config;
        this.pool = mysql.createPool({
            host           : config.host,
            user           : config.user,
            password       : config.password,
            database       : config.name,
            connectionLimit: 100
        });
    } else {
        throw new Error('Invalid database information');
    }

    this._logsModelName = options.model || constants.DEFAULT_TABLE_NAME;
    _initDB.apply(this);
}
util.inherits(MySqlStorage, Storage);

/**
 * Logging request information into a database by means of MySQL
 *
 * @public
 *
 * @this MySqlStorage
 *
 * @param {Object} level - type log information
 * @param {Object} info - log information
 */
MySqlStorage.prototype.addRequestLog = function (level, info) {
    info.level = level;
    info.date = Date.now();
    info.logType = constants.logtypes.AUTOLOG;
    var self = this;

    this.pool.getConnection(function (err, connection) {
        if (err) {
            return self.throwIfError(err);
        }
        connection.query(`INSERT INTO ${self._logsModelName} SET ?`, info, function (err, result) {
            self.throwIfError(err);
            connection.release();
        });
    });
};

// TODO: bug with update request log, it can not be complete in some cases(if addRequestLog is not finished yet)
MySqlStorage.prototype.updateRequestLog = function (level, info) {
    info.level = level;
    var self = this;

    this.pool.getConnection(function (err, connection) {
        if (err) {
            return self.throwIfError(err);
        }

        connection.query(`UPDATE ${self._logsModelName} SET ? WHERE id = "${info.id}"`, info, function (err, result) {
            self.throwIfError(err);
            connection.release();
        });
    });
};

/**
 * Hand-log in database by means of MySQL
 *
 * @param {String} level
 * @param {String} value
 * @returns {Promise}
 */
MySqlStorage.prototype.log = function (level, value) {
    var self = this;
    value = storageUtil.manualLogFormatter.apply(null, arguments);

    return new Promise(function (fulfill, reject) {
        self.pool.getConnection(function (err, connection) {
            if (err) {
                return reject(err);
            }

            connection.query(`INSERT INTO ${self._logsModelName} SET ?`, {
                id     : uuid.v1(),
                level  : level,
                date   : Date.now(),
                value  : value,
                logType: constants.logtypes.HANDLOG
            }, function (err, result) {
                if (err) {
                    return reject(err);
                }

                    connection.release();
                    fulfill(result);
                });
        });
    });
};

/**
 * Export logs to a file and delete them from the table
 *
 * @private
 *
 * @this MySqlStorage
 *
 * @param {Number|Date} date
 * @param {String} type
 */
MySqlStorage.prototype.export = function (date) {
    if (date instanceof Date) {
        date = date.getTime();
    }

    if (typeof date != 'number') {
        throw new TypeError('The expected value of Number');
    }

    if (!this.exportPath) {
        throw new Error('Invalid export path');
    }

    var tableName = this._logsModelName;

    var where = [date];

    var self = this;
    this.pool.getConnection(function (err, connection) {
        if (err) return self.throwIfError(err);
        connection.query(`SELECT COUNT(id) AS count FROM ${tableName} WHERE date < ?`, where, function (err, data) {
            var count = data[0].count;
            if (err) {
                connection.close();
                return self.throwIfError(err);
            }
            if (!count) return connection.release();

            var filename = `${self.exportPath}${tableName}_${date}.log`;
            var stream = fs.createWriteStream(filename, {
                flags: 'a'
            });
            var iterationCount = Math.ceil(count / constants.DEFAULT_EXPORT_LIMIT);
            async.timesSeries(iterationCount, exportData(stream, connection, tableName, where), function (err) {
                self.throwIfError(err);
                connection.release();
                stream.close();
            });
        });
    });
};

function exportData(stream, connection, tableName, where) {
    return function (iteration, next) {
        var ids = [];
        async.waterfall([
            _getLegacyLog,
            _saveLogInFile,
            _deleteLegacyLog
        ], next);

        function _getLegacyLog(cb) {
            connection.query(`SELECT * FROM ${tableName} WHERE date < ? ORDER BY date LIMIT ?`,
                [].concat(where, constants.DEFAULT_EXPORT_LIMIT),
                cb);
        }

        function _saveLogInFile(logs, fields, cb) {
            ids = storageUtil.logsToIds(logs);
            var textLog = storageUtil.logsToString(logs);
            stream.write(textLog, 'utf8', cb);
        }

        function _deleteLegacyLog(cb) {
            connection.query(`DELETE FROM ${tableName} WHERE id in (?)`, [ids], cb);
        }
    }
}

/**
 * create table for logging
 *
 * @private
 */
function _initDB() {
    var createLogTableQuery = `CREATE TABLE IF NOT EXISTS ${this._logsModelName} (
        id char(36) NOT NULL PRIMARY KEY,
		date bigint NULL,
		headers text NULL,
        level varchar(255) NULL,
        url varchar(256),
        status int(11) NULL,
        type enum('GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'HEAD') NULL,
        body text NULL,
        start int(11) NULL,
        time int(11) NULL,
        response text NULL,
        value text NULL,
        logType int(11),
        error text NULL)`;


    var self = this;
    this.pool.getConnection(function (err, connection) {
        if (err) {
            return self.throwIfError(err);
        }

        async.parallel([
            _createLogTable
        ], _errorHandler);

        function _createLogTable(cb) {
            connection.query(createLogTableQuery, function (err) {
                if (err) {
                    return cb(err);
                }

                return cb(null);
            });
        }

        function _errorHandler(err) {
            if (err) {
                return self.throwIfError(err);
            }

            connection.release();
        }
    });
}

/**
 * Module exports.
 * @public
 */
module.exports = MySqlStorage;