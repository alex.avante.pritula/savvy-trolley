'use strict';

/**
 * Module dependencies.
 * @private
 */
var uuid = require('uuid');
var fs = require('fs');
var Promise = require('promise');
var util = require('util');
var storageUtil = require('./storageUtil');
var levelMapper = require('./levelMapper');
var Storage = require('./Storage');
var SequelizeMySqlStorage = require('./SequelizeMySqlStorage');
var MySqlStorage = require('./MySqlStorage');
var MongoStorage = require('./MongoStorage');
var MongooseStorage = require('./MongooseStorage');

/**
 * Default value for logger.
 * @private
 */
var defaultProperty = {
    type    : 1,
    start   : 1,
    time    : 1,
    body    : 1,
    response: 1,
    error   : 1,
    headers : 1
};
var defaultStorage = {
    addRequestLog   : function (level, value) {
        value = storageUtil.logToString(value);
        level = level == 'info'  ? level : 'error';
        console[level](new Date(), level, value);
    },
    updateRequestLog: function (level, value) {
        value = storageUtil.logToString(value);
        level = level == 'info'  ? level : 'error';
        console[level](new Date(), level, value);
    },
    log             : function (level, value) {
        var log = new Date() + ' ' + level + ': ';
        log += storageUtil.manualLogFormatter.apply(null, arguments);
        return new Promise(function (resolve, reject) {
            if(console[level]) {
                console[level](log);
                return resolve(log);
            }

            console.log(log);
            resolve(log);
        });
    }
};


/**
 * Create logger with options
 * @public
 *
 * @param {Object} [options]
 * @param {Object} [options.storage] - Log storage.
 * @param {Object} [options.fileName] - The path to the log file (for logging logger exceptions).
 * @param {Object} [options.properties] - Map with properties for logging.
 * @param {Boolean} [options.enable] - Enable/disable value.
 * @param {Boolean} [options.isActive] - Enable/disable value.
 *
 * @returns {Object} logger - logger object. <br />
 * logger.requestHandler - express middleware request handler. <br />
 * function({Object} req,{Object} res, {Function} next)
 * logger.errorHandler - express middleware error handler. <br />
 * function({Object} req,{Object} res, {Function} next)
 */
function logger(options) {
    var settings = options || {};
    var properties = settings.properties || defaultProperty;
    var storage = settings.storage || defaultStorage;
    var enable = settings.enable || settings.isActive || false;

    if (storage instanceof Storage && enable) {
        var fileName = settings.fileName || 'logs/logger.log';
        _initStorage(storage, fileName);
    }

    var logger = {
        log           : _log(storage, enable),
        requestHandler: _requestHandler(storage, properties, enable),
        errorHandler  : _errorHandler(enable)
    };
    levelMapper(logger);

    return logger;
}

function _log(storage, enable) {
    return function (level, value) {
        if (!enable) {
            return;
        }
        return storage.log.apply(storage, arguments);
    }
}

/**
 * Catch a request and log him
 * @private
 * @param {Object} storage
 * @param {Object} properties
 * @param {Boolean} enable
 * @return {Function} middleware function <br />
 * function({Object} req,{Object} res, {Function} next)
 */
function _requestHandler(storage, properties, enable) {
    return function (req, res, next) {
        if (!enable) {
            return next();
        }

        var result = {};
        var start = new Date();

        _catchRequest(req, properties, result);
        _createResponseBody(res);

        res.on('finish', function () {
            _catchResponse(res, properties, result);
            var level = result.error ? 'error' : 'info';
            storage.updateRequestLog(level, result);
        });
        next();

        function _catchRequest(req, properties, result) {
            result.id = uuid.v1();
            result.url = req.path;

            if (properties['type']) {
                result.type = req.method;
            }

            if (properties['start']) {
                result.start = start;
            }

            if (properties['headers']) {
                result.headers = JSON.stringify(req.headers);
            }

            if (properties['body']) {
                result.body = JSON.stringify(_getRequestBody(req));
            }

            storage.addRequestLog('info', result);
        }

        function _catchResponse(res, properties, result) {
            if (properties['time']) {
                result.time = new Date().getTime() - start.getTime();
            }

            if (properties['response']) {
                result.response = res.body;
            }

            if (res.error && properties['error']) {
                result.error = res.error.stack.split("\n").join(', ');
            }

            result.status = res.statusCode;
        }
    }
}

/**
 * Catch error and populate 'res.error'
 * @private
 * @param {Boolean} enable
 * @return {Function} middleware function <br />
 * function({Object} err,{Object} req, {Object} res, {Function} next)
 */
function _errorHandler(enable) {
    return function (err, req, res, next) {
        if (err && enable) {
            res.error = err;
        }

        return next(err);
    }
}

/**
 * Get request body
 * @private
 * @param {Object} req
 * @return {Object} request body
 */
function _getRequestBody(req) {
    switch (req.method) {
        case 'GET':
            return req.query || {};

        case 'POST':
        case 'PUT':
        case 'PATCH':
        case 'DELETE':
            return req.body || {};
    }

    return false;
}

/**
 * Catch Response body and populate 'res.body'
 * @private
 * @param {Object} res
 */
function _createResponseBody(res) {
    var oldSend = res.send;
    var oldEnd = res.end;
    var body = '';

    res.send = function (chunk) {
        if (typeof chunk == 'string') {
            body += chunk;
        }

        oldSend.apply(res, arguments);
    };

    res.end = function (chunk) {
        res.body = body;
        oldEnd.apply(res, arguments);
    };
}

/**
 * Init storage object - configure error handler and open.
 * @private
 * @param {Storage} storage
 * @param {String} fileName
 */
function _initStorage(storage, fileName) {

    var stream = fs.createWriteStream(fileName, {
        flags          : 'a',
        defaultEncoding: 'utf8'
    });
    storage.on('error', function (err) {
        if (err) {
            var result = (new Date() + ' : ');
            for (var key in err) {
                if (err.hasOwnProperty(key)) {
                    result += key + ' = ' + err[key] + ', '
                }
            }
            if (err.stack) {
                result += err.stack.split("\n").join(',');
            }

            result += ';\n';
            stream.write(result);
        }
    });
}

/**
 * Static property
 * SequelizeMySqlStorage - Logger adapter for sequelize
 * @public
 */
logger.SequelizeMySqlStorage = SequelizeMySqlStorage;

/**
 * Static property
 * logger.MySqlStorage - Logger adapter for mysql
 * @public
 */
logger.MySqlStorage = MySqlStorage;

/**
 * Static property
 * logger.MongoStorage - Logger adapter for mongodb
 * @public
 */
logger.MongoStorage = MongoStorage;

/**
 * Static property
 * logger.MongooseStorage - Logger adapter for mongoose
 * @public
 */
logger.MongooseStorage = MongooseStorage;

/**
 * Module exports.
 * @public
 */
module.exports = logger;