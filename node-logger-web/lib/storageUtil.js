'use strict';

/**
 * Module dependencies.
 * @private
 */
var fs = require('fs');

/**
 * Convert log to string
 *
 * @private
 *
 * @param {Object} log
 * @returns {String}
 */
function logToString(log) {
    var result = `${log.date} - ${log.level}: `;
    delete log.date;
    delete log.level;

    var fields = Object.keys(log)
        .filter(function (key){ return log[key];})
        .map(function (key) { return `${key} = ${log[key]}`;})
        .join(', ');
    return result + fields;
}

/**
 * Convert logs to string
 *
 * @private
 *
 * @param {String} filename
 * @param {Object} logs
 * @returns {String}
 */
function logsToString(logs) {
    return logs.map(function (log) {
        return logToString(log);
    }).join('\n') + '\n';
}

/**
 * Convert logs to ids collection
 *
 * @private
 *
 * @param {Object} logs
 * @returns {Array<Number>}
 */
function logsToIds(logs) {
    return logs.map(function (log) {
        return log.id;
    });
}

function stringify(value) {
    if (typeof value == 'object') {
        return JSON.stringify(value);
    }
    return value;
}

function manualLogFormatter() {
    var log = [];
    Array.prototype.splice.call(arguments, 0, 1);
    Array.prototype.forEach.call(arguments, function (argument) {
        log.push(stringify(argument));
    });

    return log.join(', ');
}

module.exports = {
    logsToIds         : logsToIds,
    logToString       : logToString,
    logsToString      : logsToString,
    manualLogFormatter: manualLogFormatter,
    stringify         : stringify
};
