'use strict';

/**
 * Module dependencies.
 * @private
 */
var Sequelize = require('sequelize');
var uuid = require('uuid');
var util = require('util');
var async = require('async');
var fs = require('fs');
var Storage = require('./Storage');
var storageUtil = require('./storageUtil');
var constants = require('./constants.json');

/**
 * table scheme for sequelize
 * @private
 */
var logScheme = {
    level   : {
        type     : Sequelize.STRING,
        allowNull: true
    },
    id      : {
        type      : Sequelize.UUID,
        primaryKey: true
    },
    headers : {
        type     : Sequelize.TEXT,
        allowNull: true
    },
    date    : {
        type     : Sequelize.BIGINT,
        allowNull: true
    },
    url     : {
        type     : Sequelize.STRING,
        allowNull: true
    },
    status  : {
        type     : Sequelize.INTEGER,
        allowNull: true
    },
    type    : {
        type     : Sequelize.ENUM('GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'HEAD'),
        allowNull: true
    },
    start   : {
        type     : Sequelize.DATE,
        allowNull: true
    },
    body    : {
        type     : Sequelize.TEXT,
        allowNull: true
    },
    time    : {
        type     : Sequelize.INTEGER,
        allowNull: true
    },
    response: {
        type     : Sequelize.TEXT,
        allowNull: true
    },
    error   : {
        type     : Sequelize.TEXT,
        allowNull: true
    },
    value   : {
        type     : Sequelize.TEXT,
        allowNull: true
    },
    logType : {
        type     : Sequelize.INTEGER,
        allowNull: false
    }
};

var setCurrentTimeStampHook = {
    beforeCreate: function (request) {
        request.date = Date.now();
    }
};

/**
 * Logger storage by means of Sequelize (MySQL)
 *
 * @constructor SequelizeMySqlStorage
 * @extends Storage
 *
 * @public
 *
 * @param {Object} options
 * @param {Object} [options.db] sequelize instance
 * @param {Object} [options.config] database config
 * @param {String} [options.model] table name
 * @param {Object} [options.export] cron settings
 * @param {String} [options.export.path] backup folder
 * @param {String|Date} [options.export.time] cron time property
 * @param {String} [options.export.timeZone] cron time zone property
 * @param {Boolean} [options.export.enableLogExport]
 *
 * @throws  Will throw an error if the arguments `options.db` and `options.config` is null.
 */
function SequelizeMySqlStorage(options) {
    if (!options) {
        throw new Error('Invalid options');
    }

    Storage.apply(this, arguments);
    if (options.db) {
        this._db = options.db;
    } else if (options.config) {
        var config = options.config;
        this._db = new Sequelize(config.name, config.user, config.password, {
            host   : config.host,
            dialect: 'mysql',
            pool   : {
                max : 100,
                min : 5,
                idle: 10000
            },
            logging: false
        });
    } else {
        throw new Error('Invalid database information');
    }

    this._logsModelName = options.model || constants.DEFAULT_TABLE_NAME;

    this._logModel = this._db.define(this._logsModelName, logScheme, {
        hooks: setCurrentTimeStampHook
    });

    _initDB.apply(this);
}
util.inherits(SequelizeMySqlStorage, Storage);

/**
 * Logging request information into a database by means of Sequelize (MySQL)
 *
 * @public
 *
 * @this SequelizeMySqlStorage
 *
 * @param {Object} level - type log information
 * @param {Object} requestLog - log information
 */
SequelizeMySqlStorage.prototype.addRequestLog = function (level, requestLog) {
    requestLog.level = level;
    requestLog.logType = constants.logtypes.AUTOLOG;

    var self = this;
    this._logModel
        .create(requestLog)
        .catch(function (err) {
            self.throwIfError(err);
        });
};

SequelizeMySqlStorage.prototype.updateRequestLog = function (level, responseLog) {
    responseLog.level = level;
    var self = this;
    var found;

    this._logModel.findById(responseLog.id)
        .then(function (data) {

            // if log is not found - check it one more time, probably it do not finished yet
            if (!data) {
                if (found) {
                    return;
                }

                process.nextTick(function () {
                    found = true;
                    self.updateRequestLog.apply(self, [level, responseLog]);
                });
                return;
            }

            data.update(responseLog)
                .catch(function (err) {
                    self.throwIfError(err);
                });

        })
        .catch(function (err) {
            self.throwIfError(err);
        });
};

/**
 * Hand-log in database by means of Sequelize (MySQL)
 *
 * @param {String} level
 * @param {String} value
 * @returns {Promise}
 */
SequelizeMySqlStorage.prototype.log = function (level, value) {
    value = storageUtil.manualLogFormatter.apply(null, arguments);
    return this._logModel.create({
        id     : uuid.v1(),
        level  : level,
        value  : value,
        date   : Date.now(),
        logType: constants.logtypes.HANDLOG
    });
};

/**
 * Export logs to a file and delete them from the table
 *
 * @private
 *
 * @this SequelizeMySqlStorage
 *
 * @param {Number|Date} date
 */
SequelizeMySqlStorage.prototype.export = function (date) {
    var model = this._logModel;
    var name = this._logsModelName;

    var where = {
        date: {
            $lt: date
        }
    };

    var self = this;
    model.count({where: where})
        .then(function (count) {
            if (!count) return;

            var filename = `${self.exportPath}${name}_${date}.log`;
            var stream = fs.createWriteStream(filename, {
                flags: 'a'
            });
            var iterationCount = Math.ceil(count / constants.DEFAULT_EXPORT_LIMIT);
            async.timesSeries(iterationCount, exportData(stream, model, where), function (err) {
                self.throwIfError(err);
                stream.close();
            })
        });
};

/**
 * Export iteration
 * @param stream
 * @param model
 * @param where
 * @returns {Function}
 */
function exportData(stream, model, where) {
    return function (iteration, done) {
        var ids = [];
        var limit = constants.DEFAULT_EXPORT_LIMIT;
        model.findAll({
            where : where,
            raw   : true,
            order : 'date',
            limit : limit
        })
            .then(function (logs) {
                ids = storageUtil.logsToIds(logs);
                var textLog = storageUtil.logsToString(logs);
                return stream.write(textLog);
            })
            .then(function () {
                return model.destroy({where: {id: {$in: ids}}});
            })
            .then(function () {
                done();
            })
            .catch(done);
    }
}

function _initDB() {
    var self = this;
    this._db
        .sync()
        .catch(function (err) {
            self.throwIfError(err);
        });
}

/**
 * Module exports.
 * @public
 */
module.exports = SequelizeMySqlStorage;