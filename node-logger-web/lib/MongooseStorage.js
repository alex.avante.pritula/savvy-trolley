'use strict';

/**
 * Module dependencies.
 * @private
 */
var uuid = require('uuid');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var util = require('util');
var async = require('async');
var fs = require('fs');
var Promise = require('promise');
var Storage = require('./Storage');
var storageUtil = require('./storageUtil');
var constants = require('./constants.json');

function getId(){
    return uuid.v1();
}

function getDate(){
    return new Date();
}

var logStruct = {
    level   : String,
    id      : {type: String, default: getId},
    date    : {type: Number, default: Date.now},
    url     : String,
    status  : Number,
    type    : String,
    createAt: {type: Date , default: getDate},
    body    : String,
    time    : Number,
    response: String,
    error   : String,
    value   : String,
    logType : Number
};

/**
 * Logger storage by means of Mongodb
 *
 * @constructor MongooseStorage
 * @extends Storage
 *
 * @public
 *
 * @param {Object} options
 * @param {String} [options.url]
 * @param {String} [options.model] table name
 * @param {Object} [options.export] cron settings
 * @param {String} [options.export.path] backup folder
 * @param {String|Date} [options.export.time] cron time property
 * @param {String} [options.export.timeZone] cron time zone property
 * @param {Boolean} [options.export.enableAutoLogExport]
 * @param {Boolean} [options.export.enableHandLogExport]
 *
 * @throws  Will throw an error if the arguments `options.connection` and `options.url` is null.
 */
function MongooseStorage(options) {
    if (!options) {
        throw new Error('Invalid options');
    }

    Storage.apply(this, arguments);
    if (!mongoose.connection.name) {
        if (options.url) {
            var url = options.url;
            var options = {
                db    : {native_parser: true},
                server: {poolSize: 100}
            };

            mongoose.connect(url, options);
        } else {
            throw new Error('Invalid URL value');
        }
    }
    this._logsModelName = options.model || constants.DEFAULT_TABLE_NAME;

    var logsSchema = new Schema(logStruct, {collection: this._logsModelName});

    this._logsModel = mongoose.model(this._logsModelName, logsSchema);
}
util.inherits(MongooseStorage, Storage);

/**
 * Logging request information into a database by means of mongoose (mongodb)
 *
 * @public
 *
 * @this MongooseStorage
 *
 * @param {Object} level - type log information
 * @param {Object} info - log information
 */
MongooseStorage.prototype.addRequestLog = function (level, info) {
    info.level = level;
    info.date = new Date().getTime();
    info.logType = constants.logtypes.AUTOLOG;
    var self = this;

    var log = new this._logsModel(info);

    log.save(function (err) {
        self.throwIfError(err);
    });
};

MongooseStorage.prototype.updateRequestLog = function (level, info) {
    info.level = level;
    info.date = new Date().getTime();
    var self = this;

    this._logsModel
        .update({id: info.id}, info, {multi: false})
        .exec(function (err) {
            self.throwIfError(err);
        });
};

/**
 * Hand-log in database by means of mongoose (mongodb)
 *
 * @param {String} level
 * @param {String} value
 * @returns {Promise}
 */
MongooseStorage.prototype.log = function (level, value) {
    var self = this;
    value = storageUtil.manualLogFormatter.apply(null, arguments);

    return new Promise(function (fulfill, reject) {
        var log = new self._logsModel({
            level  : level,
            value  : value,
            logType: constants.logtypes.HANDLOG
        });

        log.save(function (err, log) {
            if (err) return reject(err);
            fulfill(log);
        });
    });
};

/**
 * Export logs to a file and delete them from the table
 *
 * @private
 *
 * @this MongooseStorage
 *
 * @param {Number|Date} date
 * @param {String} type
 */
MongooseStorage.prototype.export = function (date) {
    if (date instanceof Date) {
        date = date.getTime();
    }

    if (typeof date != 'number') {
        throw new TypeError('The expected value of Number');
    }

    if (!this.exportPath) {
        throw new Error('Invalid export path');
    }

    var name = this._logsModelName;
    var model = this._logsModel;

    var where = {
        date: {$lte: date}
    };

    var self = this;
    model.count(where, function (err, count) {
        if (err) {
            return self.throwIfError(err);
        }
        if (!count) return;
        
        var filename = `${self.exportPath}${name}_${date}.log`;
        var stream = fs.createWriteStream(filename, {
            flags: 'a'
        });
        var iterationCount = Math.ceil(count / constants.DEFAULT_EXPORT_LIMIT);
        async.timesSeries(iterationCount, exportData(stream, model, where), function (err) {
            self.throwIfError(err);
            stream.close();
        });
    });
};

/**
 * Export iteration
 * @param stream
 * @param model
 * @param where
 * @returns {Function}
 */
function exportData(stream, model, where) {
    return function (iteration, done) {
        var ids = [];
        async.waterfall([
            _getLegacyLog,
            _saveLogInFile,
            _deleteLegacyLog
        ], done);

        /**
         * Read old logs
         * @param cb
         * @private
         */
        function _getLegacyLog(cb) {
            model
                .find(where)
                .sort('createAt')
                .limit(constants.DEFAULT_EXPORT_LIMIT)
                .lean()
                .exec(cb);
        }

        /**
         * Save old logs into file
         * @param logs
         * @param cb
         * @private
         */
        function _saveLogInFile(logs, cb) {
            ids = storageUtil.logsToIds(logs);
            var textLog = storageUtil.logsToString(logs);
            stream.write(textLog, 'utf8', cb);
        }

        /**
         * Delete old logs
         * @param cb
         * @private
         */
        function _deleteLegacyLog(cb) {
            model.find({id: {$in: ids}})
                .remove()
                .exec(cb);
        }
    }
}

/**
 * Module exports.
 * @public
 */
module.exports = MongooseStorage;
