/**
 * Levels list.
 */
var levels = ['info', 'warn', 'error'];

/**
 * Mapping log level methods in manual logging
 * @param target
 */
function setLevels (target){
    levels.forEach(function (level){
        target[level] = function () {
            var args = [level].concat(Array.prototype.slice.call(arguments));
            target.log.apply(target, args);
        }
    })
}

module.exports = setLevels;