var mysql = require('mysql');
var fs = require('fs');

var config = require('../config/index').db;

var connection = mysql.createConnection({
    host     : config.host,
    user     : config.user,
    password : config.password,
    port : config.port
});

fs.readFile(__dirname + '/schema.sql', {encoding: 'utf-8'}, function(err,data) {
    if (err) {
        fail(err);
    }

    connection.query(data, function (err, rows) {
        if (err) {
            fail(err);
        }

        ok();
    });
});

function ok(){
    console.error('build success!');
    process.exit(0);
}

function fail(err){
    console.error('error connecting: ' + err.stack);
    process.exit(1);
}