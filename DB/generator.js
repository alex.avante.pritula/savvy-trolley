var models = require('../server/models');
var Chance = require('chance');
var async = require('async');
var chance = new Chance();

setTimeout(function () {
    var type = process.argv[2];
    var count = process.argv[3] || 10;

    count = parseInt(count, 10);
    create(type, count);
}, 1000);

function create(type, count) {
    console.log('Start generate for ', count, type);
    switch (type) {
        case 'advert':
            createAdvertising(handler(type));
            break;
        case 'items':
            createItem(count, handler(type, count));
            break;
        case 'categories':
            createCategories(count, handler(type, count));
            break;
        case 'shops':
            createShops(count, handler(type, count));
            break;
        case 'connections':
            createConnections(count, handler(type, count));
            break;
        case 'all':
            createAll(count, handler(type, count));
            break;
        case 'postcodes':
            generateAllPostcodes(handler(type));
            break;

        default:
            createAll(10, handler('default'));
    }
}

function generateAllPostcodes(done) {
    async.timesSeries(10, function (first, next) {
        async.timesSeries(10, function (second, next) {
            async.timesSeries(10, function (third, next) {
                var postcode = [];
                async.timesSeries(10, function (fourth, next) {
                    postcode.push(generatePostcode(first, second, third, fourth));
                    next();
                }, writePostcodes(postcode, next));
            }, next);
        }, next);
    }, done);
}

function writePostcodes(postcode, next) {
    return function (err) {
        if (err) return next(err);
        models
            .postCodes
            .bulkCreate(postcode, {ignoreDuplicates: true})
            .then(function () {
                next();
            })
            .catch(next);
    }
}

function generatePostcode(first, second, third, fourth) {
    var postcode = '' + first + '' + second + '' + third + '' + fourth;
    return {code: postcode};
}

function createAdvertising(done) {
    async.parallel({
        items: function (cb) {
            models.items.all().then(function () {
                cb();
            }).catch(cb);
        },
        shops: function (cb) {
            var options = {
                include: [{
                    model: models.items,
                    as: 'items'
                }]
            };
            models.shops.all(options).then(function () {
                cb();
            }).catch(cb);
        },
        postCodes: function (cb) {
            models.postCodes.all().then(function () {
                cb();
            }).catch(cb);
        }
    }, function (err, result) {
        if (err) return done(err);
        var items = result.items;
        var shops = result.shops;
        var postCodes = result.postCodes;
        console.log('items', items.length);
        console.log('shops', shops.length);
        console.log('postCodes', postCodes.length);
        async.eachSeries(items, function (item, nextItem) {
            async.eachSeries(shops, function (shop, nextShop) {
                var tmpItems = shop.items;
                var itemByShop = tmpItems[Math.round(Math.random() * (tmpItems.length - 1))].itemByShop;
                var postCode = find(postCodes, shop.postCode);
                if (postCode) {
                    postCode = postCode.id;
                    itemByShop = itemByShop.id;
                    console.log('postCode', postCode);
                    console.log('itemByShop', itemByShop);
                    addAdvertising(item, postCode, itemByShop, nextShop);
                } else {
                    nextShop();
                }
            }, nextItem);
        }, done)
    });

    function find(postCodes, code) {
        return postCodes.find(function (postCode) {
            return postCode.code == code;
        })
    }
}

function addAdvertising(item, postCodeId, productId, next) {
    var advertisingData = {
        post_code_id: postCodeId,
        from: Date.now(),
        to: Date.now() + 1000 * 60 * 60 * 24 * 356
    };

    models.advertising.checkMaximum(item.id, postCodeId)
        .then(function () {
            return item.addRecommends(productId, advertisingData);
        })
        .then(function () {
            next();
        })
        .catch(function (err) {
            next();
        });
}

function createCategories(count, cb) {
    async.timesSeries(count, function (count, next) {
        createCategory(null)(count, function (err, result) {
            if (err) return next(err);

            var count = chance.natural({min: 1, max: 5});
            createSybcategories(result.id, count, next)
        });
    }, cb);
}

function createSybcategories(parent, count, cb) {
    async.timesSeries(count, createCategory(parent), cb);
}

function createCategory(parent) {
    return function (count, next) {
        models.categories.create({
            title: chance.word({length: 5}) + ':' + count,
            image_id: 1,
            parent_id: parent
        }).then(function (category) {
            next(null, category);
        }).catch(function (err) {
            next(err)
        });
    }
}

function createShops(count, cb) {
    async.timesSeries(count, function (count, next) {
        var pool = '1234567890';
        models.shops.create({
            name: chance.word({length: 3}) + ':' + count,
            image_id: 1,
            owner: chance.name(),
            abn: chance.string({length: 11, pool: pool}),
            acn: chance.string({length: 9, pool: pool}),
            address: chance.address(),
            contactName: chance.name(),
            contactPhone: chance.phone(),
            contactEmail: chance.email(),
            postCode: chance.string({length: 4, pool: pool}),
            longitude: chance.floating({min: -180, max: 180}),
            latitude: chance.floating({min: -85.05112878, max: 85.05112878})
        }).then(function () {
            next();
        }).catch(function (err) {
            next(err)
        });
    }, cb);
}

function createItem(count, cb) {
    var names = ['apple', 'pineapple', 'milk', 'oil'];
    models.units.all()
        .then(function (units) {
            async.timesSeries(count, function (count, next) {

                var pool = '1234567890';
                var unitsCounter = chance.natural({min: 10, max: 1000});
                var itemSize = chance.natural({min: 1, max: 10});
                var servingPerPack = chance.natural({min: 1, max: 10});
                var unit = units[count % units.length];

                models.items.create({
                    barCode: chance.string({length: 14, pool: pool}),
                    name: names[count % names.length] + " " + chance.word({length: 7}),
                    brand: chance.name(),
                    image_id: 1,
                    units: unitsCounter,
                    unit_id: unit.id,
                    itemSize: itemSize,
                    servingPerPack: servingPerPack,
                    servingSize: unitsCounter * servingPerPack,
                    serving_unit_id: unit.id
                }).then(function () {
                    next();
                }).catch(function (err) {
                    next(err)
                });
            }, cb);
        }).catch(function (err) {
        cb(err)
    });
}


function createCategoriesItems(max, done) {
    var where = {parent_id: null};
    models.categories.count({where: where})
        .then(function (count) {
            async.timesSeries(count, function (iterate, next) {
                models.categories.all({limit: 1, offset: iterate, where: where})
                    .then(function (categories) {
                        createCategoryItems(categories[0], max, next);
                    });
            }, done);
        })
        .catch(done);
}

function createCategoryItems(category, max, done) {
    models.items.count()
        .then(function (count) {
            max = max < count ? max : count;
            var start = chance.natural({min: 0, max: count - max - 1});
            async.timesSeries(max, function (iterate, next) {
                models.items.all({limit: 1, offset: start + iterate})
                    .then(function (items) {
                        createCategoryItem(items[0], category, next);
                    });
            }, done);
        })
        .catch(done);
}

function createCategoryItem(item, category, done) {
    category.addItem(item)
        .then(function () {
            done();
        })
        .catch(function (err) {
            done(err);
        });
}

function createShopsItems(max, done) {
    models.shops.count()
        .then(function (count) {
            async.timesSeries(count, function (iterate, next) {
                models.shops.all({limit: 1, offset: iterate})
                    .then(function (shops) {
                        createShopItems(shops[0], max, next);
                    });
            }, done);
        })
        .catch(done);
}

function createShopItems(shop, max, done) {
    models.items.count()
        .then(function (count) {
            max = max < count ? max : count;
            var start = chance.natural({min: 0, max: count - max - 1});
            async.timesSeries(max, function (iterate, next) {
                models.items.all({limit: 1, offset: start + iterate})
                    .then(function (items) {
                        createShopItem(items[0], shop, next);
                    });
            }, done);
        })
        .catch(done);
}

function createShopItem(item, shop, done) {
    var price = chance.natural({min: 10, max: 1000});
    var units = item.units || 1;
    var data = {
        regularPrice: price,
        unitPrice: ((price / units)),
        salePrice: price * 0.9,
        isSale: chance.bool(),
        from: Date.now(),
        to:   Date.now() + 1000 * 60 * 60 * 24 * 356
    };
    shop.addItem(item, data)
        .then(function () {
            done();
        })
        .catch(function (err) {
            done(err);
        });
}

function createConnections(max, done) {
    async.parallel([
        async.apply(createCategoriesItems, max),
        async.apply(createShopsItems, max)
    ], done)
}

function createAll(count, done) {
    async.series([
        async.apply(createCategories, count),
        async.apply(createShops, count),
        async.apply(createItem, count * 10),
        async.apply(createConnections, count)
    ], done);
}

function handler(type, count) {
    return function (err) {
        if (err) throw err;
        console.log('Create', count ? count : '', type);
        console.log('Generate finish');
        process.exit(0);
    }
}
