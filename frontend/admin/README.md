# Savvy Trolley Admin Panel



## Getting started

Install dependencies:

    $ cd frontend/admin
    $ npm install
    
Run development web-server:

    $ gulp serve
    
Build distribution version:
    
    $ gulp build