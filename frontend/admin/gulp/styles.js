'use strict';

var gulp = require('gulp');

var paths = gulp.paths;

var $ = require('gulp-load-plugins')();

gulp.task('styles', function () {

  var injectFiles = gulp.src([
    paths.src + '/{app,components}/**/*.css',
    '!' + paths.src + '/app/index.css',
    '!' + paths.src + '/app/vendor.css'
  ], { read: false });

  var indexFilter = $.filter('index.css');

  return gulp.src([
    paths.src + '/app/index.css',
    paths.src + '/app/vendor.css'
  ])
    .pipe(indexFilter)
    .pipe($.inject(injectFiles))
    .pipe(indexFilter.restore())

  .pipe($.autoprefixer())
    .on('error', function handleError(err) {
      console.error(err.toString());
      this.emit('end');
    })
    .pipe(gulp.dest(paths.tmp + '/serve/app/'));
});
