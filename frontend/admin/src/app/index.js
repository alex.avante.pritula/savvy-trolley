'use strict';

angular.module('st-admin', ['ngResource', 'ngAnimate', 'ngCookies', 'ngSanitize', 'ngMessages', 'toaster', 'ui.bootstrap', 'ui.grid', 'ui.grid.autoResize', 'ui.router', 'ui.select', 'app', 'LocalStorageModule', 'angular-cache', 'ngFileUpload', 'uiGmapgoogle-maps'])

    .config(function ($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider, $logProvider, $resourceProvider, localStorageServiceProvider, CacheFactoryProvider, uiGmapGoogleMapApiProvider) {
        $logProvider.debugEnabled(true);

        uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyDyrbYdlDm09icTZgYsaCcT-UzV92hYDRE',
            libraries: 'places'
        });

        angular.extend(CacheFactoryProvider.defaults, { maxAge: 15 * 60 * 1000 });

        localStorageServiceProvider
            .setPrefix('st')
            .setStorageType('localStorage')
            .setNotify(true, true);

        $stateProvider
            .state('auth', {
                url: '',
                controller: 'AuthController',
                controllerAs: 'auth',
                template: '<ui-view/>',
                abstract: true
            })
            .state('auth.login', {
                url: '/login',
                templateUrl: 'app/components/auth/login.html',
                data: { title: 'Login' }
            })
            //home
            .state('home', {
                url: '',
                templateUrl: 'app/views/layout.html',
                controller: 'MainController',
                controllerAs: 'vm',
                abstract: true
            })
            .state('home.dashboard', {
                parent: '',
                url: '/dashboard',
                templateUrl: 'app/components/dashboard/view.html',
                controller: 'DashboardController',
                controllerAs: 'vm',
                data: { title: 'Dashboard' }
            })

            //shops
            .state('home.shops', {
                url: '/shops',
                abstract: true,
                template: '<ui-view/>',
                data: { title: 'Shops' }
            })
            .state('home.shops.list', {
                url: '/list/:pageNum?:query&:postcodes&:limit&:sortField&:sortDirection',
                templateUrl: 'app/components/shops/list.html',
                controller: 'ShopsController',
                controllerAs: 'vm',
                params: {
                    query: {value: '', squash: true},
                    postcodes: {value: '[]', squash: true},
                    pageNum: {value: '1', squash: true},
                    limit: {value: '10', squash: true},
                    sortField: {value: 'createdAt', squash: true},
                    sortDirection: {value: 'desc', squash: true}
                },
                reloadOnSearch: false
            })
            .state('home.shops.create', {
                url: '/create',
                templateUrl: 'app/components/shops/create.html',
                controller: 'ShopController',
                controllerAs: 'vm',
                resolve: {
                    isCreated: function ($q) {
                        return $q.resolve(false);
                    }
                }
            })
            .state('home.shops.view', {
                url: '/view/:id?:pageNum:query:limit:sortField:sortDirection',
                templateUrl: 'app/components/shops/view.html',
                controller: 'ShopController',
                controllerAs: 'vm',
                params: {
                    pageNum: {value: '1', squash: true},
                    limit: {value: '10', squash: true},
                    sortField: {value: 'createdAt', squash: true},
                    sortDirection: {value: 'desc', squash: true}
                },
                resolve: {
                    isCreated: function ($q) {
                        return $q.resolve(true);
                    }
                },
                reloadOnSearch: false
            })
            .state('home.shops.itemCreate', {
                url: '/:shopId/items/create',
                views: {
                    "": {
                        templateUrl : 'app/components/items/shop/main.html',
                        controller  : 'ItemTabsController',
                        controllerAs: 'vm'
                    },
                    "block__item@home.shops.itemCreate": {
                        templateUrl: 'app/components/items/item/view.html',
                        controller: 'ItemController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('home.shops.itemView', {
                url: '/:shopId/items/:itemId/activeTab/:tabIndex/view',
                views: {
                    "": {
                        templateUrl : 'app/components/items/shop/main.html',
                        controller  : 'ItemTabsController',
                        controllerAs: 'vm'
                    },
                    "block__item@home.shops.itemView": {
                        templateUrl : 'app/components/items/item/view.html',
                        controller  : 'ItemController',
                        controllerAs: 'vm'
                    },
                    "block__connection@home.shops.itemView": {
                        templateUrl : 'app/components/items/shop/view.html',
                        controller  :  'ItemByShopController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('home.users', {
                url: '/users',
                abstract: true,
                template: '<ui-view/>'
            })
            .state('home.users.list', {
                url: '/list?:pageNum:limit:sortField:sortDirection',
                templateUrl: 'app/components/users/list.html',
                controller: 'UsersController',
                controllerAs: 'vm',
                params: {
                    pageNum: {value: '1'},
                    limit: {value: '10'},
                    sortField: {value: 'createdAt'},
                    sortDirection: {value: 'desc'}
                },
                reloadOnSearch: false
            })
            .state('home.users.view', {
                url: '/view/:id',
                templateUrl: 'app/components/users/view.html',
                controller: 'UserController',
                controllerAs: 'vm'
            })

            //admins
            .state('home.admins', {
                url: '/admins?:pageNum&:limit:sortField:sortDirection',
                templateUrl: 'app/components/admins/list.html',
                controller: 'AdminsController',
                controllerAs: 'vm',
                params: {
                    pageNum: {value: '1'},
                    limit: {value: '10'},
                    sortField: {value: 'createdAt'},
                    sortDirection: {value: 'desc'}
                },
                reloadOnSearch: false
            })
            .state('home.reports', {
                url: '/reports',
                templateUrl: 'app/components/reports/view.html',
                controller:  'ReportsController',
                controllerAs: 'vm',
                params: {
                    pageNum: {value: '1'},
                    limit: {value: '10'},
                    sortField: {value: 'createdAt'},
                    sortDirection: {value: 'desc'},
                    query: {value: ''},
                    type: { value: null}
                },
                reloadOnSearch: false
            })
            //items
            .state('home.items', {
                url: '/items',
                abstract: true,
                template: '<ui-view/>'
            })
            .state('home.items.list', {
                url: '/list?:pageNum&:limit:sortField:sortDirection:query',
                templateUrl: 'app/components/items/list.html',
                controller: 'ItemsController',
                controllerAs: 'vm',
                params: {
                    pageNum: {value: '1'},
                    limit: {value: '10'},
                    sortField: {value: 'createdAt'},
                    sortDirection: {value: 'desc'},
                    query: {value: ''}
                },
                reloadOnSearch: false
            })
            .state('home.items.create', {
                url: '/create',
                views: {
                    "": {
                        templateUrl: 'app/components/items/item/main.html'
                    },
                    "block__item@home.items.create": {
                        templateUrl: 'app/components/items/item/view.html',
                        controller: 'ItemController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('home.items.view', {
                url: '/view/:itemId',
                views: {
                    "": {
                        templateUrl: 'app/components/items/item/main.html'
                    },
                    "block__item@home.items.view": {
                        templateUrl: 'app/components/items/item/view.html',
                        controller: 'ItemController',
                        controllerAs: 'vm'
                    }
                }
            })
            //categories
            .state('home.categories', {
                url: '/categories',
                templateUrl: 'app/components/categories/list.html',
                controller: 'CategoriesController',
                controllerAs: 'vm'

            })
            .state('home.categories.list', {
                url: '/:pageNum',
                templateUrl: 'app/components/categories/list.html',
                controller: 'CategoriesController',
                controllerAs: 'vm'
            })

            //advertising
            .state('home.advertising', {
                url: '/recommendations',
                abstract: true,
                template: '<ui-view/>'
            })
            .state('home.advertising.list', {
                url: '/list?:pageNum:limit:sortField:sortDirection',
                templateUrl: 'app/components/advertising/list.html',
                controller: 'AdvertisementsController',
                controllerAs: 'vm',
                params: {
                    pageNum: {value: '1'},
                    limit: {value: '10'},
                    sortField: {value: 'createdAt'},
                    sortDirection: {value: 'desc'}
                },
                reloadOnSearch: false
            })
            .state('home.advertising.view', {
                url: '/view/:id',
                templateUrl: 'app/components/advertising/view.html',
                controller: 'AdvertisementController',
                controllerAs: 'vm'
            })
            .state('home.advertising.create', {
                url: '/create',
                templateUrl: 'app/components/advertising/create.html',
                controller: 'AdvertisementController',
                controllerAs: 'vm'
            })

            //statistics
            .state('home.statistics', {
                url: '/statistics',
                templateUrl: 'app/components/statistics/view.html',
                controller: 'StatisticsController',
                controllerAs: 'vm'
            })

            //error states
            .state('403', {
                url: '/403/',
                templateUrl: "views/error/403.html"
            })
            .state('404', {
                url: '/404/',
                templateUrl: "views/error/404.html"
            })
            .state('500', {
                url: '/500/',
                templateUrl: "views/error/500.html"
            });

        //redirection
        $urlRouterProvider.when('', '/');
        $urlRouterProvider.when('/', '/dashboard');
        //base route
        $urlRouterProvider.otherwise('/dashboard');
        //html5 mode | TODO: enable
        $locationProvider.html5Mode({
            enabled: false,
            requireBase: false
        });
    })
    //constants
    .constant('_', window._) //lodash namespace
    .constant('MONTH_DATE_RANGE_LIMITS',{
        SALES: 3,
        ADVERTISING: 3
    })
    .constant('REPORT_TYPES', [{
        type:  null,
        title: "All"
    },{
        type: "MONTH",
        title: "Month reports"
    },{
        type: "BULK",
        title: "Bulk upload reports"
    }])
    .constant('APP_LOGO', 'SAVVY TROLLEY') //logo
    .constant('SITE_NAME', 'Savvy Trolley')
    .constant('LANDING_URL','13.54.138.55') 
    .constant('BASE_URL', window.location.origin) //base path
    .constant('DEFAULT_IMAGE', '/admin/images/no-image.jpg')
    .constant('API_PATH', '/admin/api/v1') 
    .constant('BULK_TEMPLATE_PATH', '/api/templates/')
    .constant('LIMITS', [10, 20, 50, 100])
    .constant('UNIT_TYPES', {"": null, ltr: 1, ml: 2, kg: 3, gr: 4, pc: 5})
    .constant('ENTITY_TYPES', {
        SHOP: 1,
        ITEM: 2,
        ITEM_BY_SHOP: 3,
        CATEGORY: 4,
        USER: 5,
        ADMIN: 6,
        ADVERTISING: 7,
        REPORT: 8
    })
    .constant('PUSH_TYPES_MESSAGES',{
        SALE_START: "Start of sale for item '%'",
        SALE_FINISH: "Finish of sale for item '%'",
        REGULAR_PRICE_REDUCED: "Regular price reduced for item  '%'",
        ITEM_ENABLED_MODE_CHANGED: "Disabled item '%' for all shops",
        CONCRETE_ITEM_ENABLED_MODE_CHANGED: "Disabled item '%' in your shop",
        ITEM_OUT_OF_STOCK: "Item '%' is out of stock",
        SHOP_ADDED: "Shop '%' is created"
    })
    .constant('DELIVERY_PUSH_HOURS_LIMITS',{
        START: 10,
        END:   18,
        STEP:  4
    })
    .constant('ROLES', {
        SAVVY_SHOP: {
            permissions: [
                'SHOP', 'REVIEW_SHOP', 'EDIT_SHOP', 'ONLY_OWN_SHOP', 'HAS_ONE_SHOP', // SHOP
                'ITEM', 'CREATE_ITEM', 'REVIEW_ITEM', 'EDIT_ITEM', // ITEM
                'ADVERTISING', 'CREATE_ADVERTISING', 'REVIEW_ADVERTISING', 'EDIT_ADVERTISING', // ADVERTISING
                'REPORT','REVIEW_REPORT', // REPORT
                'BULK_UPLOAD', // BULK_UPLOAD
                'PROFILE', // PROFILE
                'REVIEW_CATEGORY' // CATEGORY
            ],
            type: 'Savvy admin'
        },
        ENTERPRISE_SAVVY_SHOP: {
            permissions: [
                'SHOP', 'CREATE_SHOP', 'REVIEW_SHOP', 'EDIT_SHOP', 'ONLY_OWN_SHOP', // SHOP
                'ITEM', 'CREATE_ITEM', 'REVIEW_ITEM', 'EDIT_ITEM', // ITEM
                'ADVERTISING', 'CREATE_ADVERTISING', 'REVIEW_ADVERTISING', 'EDIT_ADVERTISING', // ADVERTISING
                'REVIEW_ADMIN', 'ASSIGN_SHOP', 'REVIEW_CATEGORY', // CATEGORY
                'REPORT','REVIEW_REPORT', 'EDIT_REPORT', // REPORT
                'BULK_UPLOAD', // BULK_UPLOAD
                'ACTIVITY_REVIEW', // ACTIVITY
                'PROFILE' // PROFILE
            ],
            type: 'Enterprise admin'
        },
        SHOP: {
            permissions: [
                'SHOP', 'CREATE_SHOP', 'REVIEW_SHOP', 'EDIT_SHOP', 'NOT_HAS_SHOP', 'ASSIGN_SHOP', 'BLOCK_PERMISSIONS_TO_SHOP', // SHOP
                'ITEM', 'CREATE_ITEM', 'REVIEW_ITEM', 'EDIT_ITEM',  // ITEM
                'ADVERTISING', 'CREATE_ADVERTISING', 'REVIEW_ADVERTISING', 'EDIT_ADVERTISING', // ADVERTISING
                'ACTIVITY_REVIEW','REVIEW_ADMIN', // ADMIN
                'REPORT','REVIEW_REPORT', 'EDIT_REPORT', // REPORT
                'PROFILE', // PROFILE
                'REVIEW_CATEGORY' // CATEGORY
            ],
            type: 'Shop admin'
        },
        USER: {
            permissions: [
                'USER', 'CREATE_USER', 'REVIEW_USER', 'EDIT_USER', // USER
                'ADMIN', 'CREATE_ADMIN', 'REVIEW_ADMIN', 'EDIT_ADMIN', 'ASSIGN_SHOP', 'EXCEPT_SUPER_ADMIN', // ADMIN
                'ACTIVITY_REVIEW', // ACTIVITY
                'PROFILE' // PROFILE
            ],
            type: 'User admin'
        },
        STATISTIC: {
            permissions: [
                'STATISTIC', 'REVIEW_STATISTIC', // STATISTIC
                'ACTIVITY_REVIEW', // ACTIVITY
                'PROFILE' // PROFILE
            ],
            type: 'Statistics admin'
        },
        SUPER_ADMIN: {
            permissions: [
                'SHOP', 'CREATE_SHOP', 'REVIEW_SHOP', 'EDIT_SHOP', 'HAS_ONE_SHOP', 'BLOCK_PERMISSIONS_TO_SHOP', // SHOP
                'ITEM', 'CREATE_ITEM', 'REVIEW_ITEM', 'EDIT_ITEM',  // ITEM
                'ADMIN', 'CREATE_ADMIN', 'REVIEW_ADMIN', 'EDIT_ADMIN', 'ASSIGN_SHOP', // ADMIN
                'USER', 'CREATE_USER', 'REVIEW_USER', 'EDIT_USER', // USER
                'STATISTIC', 'REVIEW_STATISTIC', // STATISTIC
                'CATEGORY', 'CREATE_CATEGORY', 'REVIEW_CATEGORY', 'EDIT_CATEGORY', // CATEGORY
                'ADVERTISING', 'CREATE_ADVERTISING', 'REVIEW_ADVERTISING', 'EDIT_ADVERTISING', 'EDIT_ADWORDS', // ADVETISING
                'ACTIVITY_REVIEW', 'UPDATE_ADMIN_PASSWORD', // ACTIVITY
                'REPORT','REVIEW_REPORT', 'EDIT_REPORT', // REPORT
                'BULK_UPLOAD', // BULK_UPLOAD
                'PROFILE' // PROFILE
            ],
            type: 'Super admin'
        }
    })
    //run app
    .run(function ($rootScope, $state, $http, CacheFactory, $location) {
        $rootScope.$state = $state;
        $rootScope._ = window._;

        $http.defaults.cache = CacheFactory('AppCache', {
            maxAge: 15 * 60 * 1000, // Items added to this cache expire after 15 minutes
            cacheFlushInterval: 60 * 60 * 1000, // This cache will clear itself every hour
            deleteOnExpire: 'aggressive' // Items will be deleted from this cache when they expire
        });

        var oldPath = $location.path();
        $rootScope.$on('$locationChangeSuccess', function () {
            var newPath = $location.path();
            if(newPath != oldPath){
                oldPath = newPath;
                var scrollStep = -window.scrollY / 10,
                scrollInterval = setInterval(function(){
                    if (window.scrollY != 0) window.scrollBy( 0, scrollStep );
                    else clearInterval(scrollInterval);
                },10);
            }
        });
    });
