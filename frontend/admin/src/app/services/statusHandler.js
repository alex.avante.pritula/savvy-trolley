angular.module('app').service('StatusHandler', ['toaster', '$log', '$state', 'Storage',
    StatusHandler
]);

/**
 * Response status notifier
 * @param toaster
 * @param $log
 * @param $state
 * @param Storage
 * @returns {Function}
 * @constructor
 */
function StatusHandler(toaster, $log, $state, Storage) {

    /**
     * Response status handler
     * @param status
     * @param msg
     */
    return function _statusHandler(status, msg, params) {
        //casting message to string
        msg = msg ? msg.toString() : 'No Message';

        switch (status) {
            case 400:
                //Bad request
                _addNotice(msg, params);
                break;
            case 401:
                //Unauthorized
                // _addNotice(msg, params);
                Storage.clearAll();
                $state.transitionTo('auth.login');
                break;
            case 403:
                //Forbidden
                _addNotice(msg, params);
                $state.transitionTo('403');
                break;
            case 404:
                //Not found
                _addNotice(msg, params);
                $state.transitionTo('404');
                break;
            case 405:
                //Method not allowed
                _addNotice(msg, params);
                break;
            case 417:
                //Expectation Failed
                _addNotice(msg, params);
                break;
            case 422:
                //Unprocessable Entity
                _addNotice(msg, params);
                break;
            case 500:
                //Server error
                _addNotice(msg, params);
                Storage.clearAll();
                $state.transitionTo('500');
                break;
            case 200:
                //OK
                $log.debug('status OK: ', status);
                _addNotice(msg, params);
                break;
            default:
                //otherwise
                $log.info('Response message: ', status, msg);
                break;
        }
    };

    /**
     * Emit notification event
     * @param msg
     * @param params
     * @private
     */
    function _addNotice(msg, params) {

        /* toaster options
         {
         "closeButton": false,
         "debug": false,
         "newestOnTop": false,
         "progressBar": true,
         "positionClass": "toast-bottom-left",
         "preventDuplicates": false,
         "onclick": null,
         "showDuration": "300",
         "hideDuration": "1000",
         "timeOut": "5000",
         "extendedTimeOut": "1000",
         "showEasing": "swing",
         "hideEasing": "linear",
         "showMethod": "fadeIn",
         "hideMethod": "fadeOut"
         }
         * */


        var params = _.extend({
            type   : 'error',
            title  : 'Error message',
            body   : msg,
            timeout: 2000
        }, params);

        toaster.pop(params);
    }

}