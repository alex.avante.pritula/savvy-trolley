angular.module('app').service('Download', ['BASE_URL', 'API_PATH',
    DownloadService
]);

function DownloadService(BASE_URL, API_PATH) {
    var _url = BASE_URL + API_PATH;

    return {
        get: download
    };

    function download(options) {
        var url = _url + buildQuery(options);
        window.open(url, '_blank');
    }

    function buildQuery(options) {
        var url = replacer(options.url, options.params);
        var queryBlock = [];

        _.forOwn(options.query, function (value, key) {
            queryBlock.push(key + '=' + value);
        });

        return url + '?' + queryBlock.join('&');
    }

    function replacer(string, fields) {
        var result = string;
        fields = fields || {};
        _.forOwn(fields, function (value, key) {
            result = result.replace('{{' + key + '}}', value);
        });
        return result;
    }
}