angular.module('app').service('Statistics', ['$resource', '$rootScope', 'BASE_URL', 'API_PATH', 'CacheFactory',
    StatisticsService
]);

function StatisticsService($resource, $rootScope, BASE_URL, API_PATH, CacheFactory) {

    /**
     * Statistics cache impl
     */
    var cache = CacheFactory('Statistics', {
        storageMode       : 'localStorage',
        storagePrefix     : 'st.',
        maxAge            : 15 * 60 * 1000,
        cacheFlushInterval: 60 * 60 * 1000,
        deleteOnExpire    : 'aggressive'
    });

    cache.disable();

    var service = {
        read: read
    };

    var url = BASE_URL + API_PATH + '/statistics/:options';

    var params = {
        options: '@options'
    };

    var options = {};

    var Resource = $resource(url, params, {
        read: {
            method: 'GET',
            cache : cache
        }
    }, {cancellable: true});

    function read(data) {
        var request = Resource.read(data);
        $rootScope.requests.push(request);
        return request.$promise;
    }


    return service;
}