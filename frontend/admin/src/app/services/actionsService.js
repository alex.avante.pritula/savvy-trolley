angular.module('app').service('Actions', ['$rootScope', '$resource', 'BASE_URL', 'API_PATH', '$log', 'StatusHandler',
    ActionsService
]);

/**
 * Actions service
 * @description Read and write last admin activity for resource by id|type
 * @param $rootScope
 * @param $resource
 * @param BASE_URL
 * @param API_PATH
 * @param $log
 * @param StatusHandler
 * @returns {{read: read, write: write}}
 * @constructor
 */
function ActionsService($rootScope, $resource, BASE_URL, API_PATH, $log, StatusHandler) {
    var service = {
        read : read,
        write: write
    };

    $rootScope.lastActivity = {};

    var url = BASE_URL + API_PATH + '/log';
    var params = {};
    var options = {};

    var Resource = $resource(url, params, {
        read : {
            method : 'GET',
            isArray: false,
            cache  : false
        },
        write: {
            method: 'POST'
        }
    });

    function read(data) {
        var fname = 'Actions read';
        return Resource.read(data).$promise.then(_onSuccess(fname), _onError(fname));
    }

    function write(data) {
        var fname = 'Actions write';
        return Resource.write(data).$promise.then(_onSuccess(fname), _onError(fname));
    }


    $rootScope.$on('actions.read', function (e, data) {
        var params = {
            entity_type: data.entityType,
            entity_id  : data.entityId
        };

        read(params);
    });

    $rootScope.$on('actions.write', function (e, data) {
        var params = {
            entityType   : data.entityType,
            entityId     : data.entityId,
            adminId      : $rootScope.currentAdmin.id,
            operationType: data.operationType,
            payload      : data.payload ? data.payload : null
        };

        write(params);
    });


    //default on success handler
    function _onSuccess(fname) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);

            var options = {
                  year:   'numeric',
                  month:  'short',
                  day:    'numeric',
                  hour:   '2-digit',
                  minute: '2-digit',
                  seconds:'2-digit'
            };

            if(res.log) {
                res['log'].createdAt = new Date(res['log'].createdAt).toLocaleString('en-GB', options);
                res['log'].updatedAt = new Date(res['log'].updatedAt).toLocaleString('en-GB', options);
            }
            $rootScope.lastActivity = res;
        }
    }

    //default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            $rootScope.lastActivity = {};
            // StatusHandler(res.status, res.data.message);
        }
    }


    return service;
}
