angular.module('app').service('Uploads', ['BASE_URL', 'API_PATH', 'Upload',
    UploadService
]);

/**
 * Upload data service
 * @param BASE_URL
 * @param API_PATH
 * @param Upload
 * @returns {{image: uploadImage, isUploadInProgress: isUploadInProgress}}
 * @constructor
 */
function UploadService(BASE_URL, API_PATH, Upload) {

    var service = {
        image             : uploadImage,
        isUploadInProgress: isUploadInProgress
    };

    var url = BASE_URL + API_PATH + '/upload';

    /* Set the default values for ngf-select and ngf-drop directives*/
    Upload.setDefaults({
        // ngfMinSize: 10 * 1024, //10KiB
        // ngfMaxSize: 2 * 1024 * 1024//2MiB
        /* ...*/
    });


    function uploadImage(file) {
        return Upload.upload({
            url   : url + '/images',
            method: 'POST',
            data  : {images: file}
        });
    }

    function isUploadInProgress() {
        return Upload.isUploadInProgress();
    }

    return service;
}