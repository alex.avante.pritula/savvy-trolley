angular.module('app').service('ReportsService', ['$resource', 'BASE_URL', 'API_PATH', 'CacheFactory',
    ReportsService
]);

function ReportsService($resource, BASE_URL, API_PATH, CacheFactory) {

    var cache = CacheFactory('ReportsService', {
        storageMode: 'localStorage',
        storagePrefix: 'st.',
        maxAge: 15 * 60 * 1000,
        cacheFlushInterval: 60 * 60 * 1000,
        deleteOnExpire: 'aggressive'
    });

    cache.disable();

    var url = BASE_URL + API_PATH + '/report';

    var service = {
        read  : read,
        remove: remove
    };

    var Resource = $resource(url, {}, {
        read      : {
            url: url + 's',
            method : 'GET',
            isArray: false,
            cache  : cache
        },
        remove    : {
            url: url + '/:id',
            method: 'DELETE',
            params : { id: '@id'}
        }
    }, {cancellable: true});

    function read(data) {
        var request = Resource.read(data);
        return request.$promise;
    }

    function remove(data) {
        cache.removeAll();
        return Resource.remove(data).$promise;
    }

    return service;
}
