angular.module('app').service('Auth', ['$resource', '$state', 'BASE_URL', 'API_PATH', 'Storage',
    AuthService
]);

/**
 * Authentication service
 * @param $resource
 * @param BASE_URL
 * @param API_PATH
 * @param Storage
 * @returns {{login: login, logout: logout, current: current}}
 * @constructor
 */
function AuthService($resource, $state, BASE_URL, API_PATH, Storage) {
    var service = {
        login  : login,
        logout : logout,
        current: current
    };

    var url = BASE_URL + API_PATH + '/auth';
    var params = {};
    var options = {};

    var Auth = $resource(url, params, {
        login  : {
            method : 'POST',
            body   : {
                email   : ':email',
                password: ':password'
            },
            isArray: false,
            cache: false
        },
        logout : {
            method : 'GET',
            url    : url + '/logout',
            isArray: true,
            cache: false
        },
        current: {
            method : 'GET',
            url    : url + '/current',
            isArray: false,
            cache: false
        }
    });

    //login
    function login(data) {
        return Auth.login(data).$promise;
    }

    //logout
    function logout() {
        return Auth.logout()
            .$promise
            .finally(function () {
                Storage.clearAll();
                $state.reload();
            });
    }

    //receive current admin
    function current() {
        return Auth.current().$promise;
    }

    return service;
}