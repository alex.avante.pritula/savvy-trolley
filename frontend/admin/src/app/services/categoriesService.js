angular.module('app').service('Categories', ['$resource', '$rootScope', 'BASE_URL', 'API_PATH', 'CacheFactory',
    CategoriesService
]);

/**
 * Categories REST service
 * @param $resource
 * @param BASE_URL
 * @param API_PATH
 * @param CacheFactory
 * @returns {{create: create, read: read, update: update, remove: remove, subcategories: subcategories, items: items, addItem: addItem, removeItem: removeItem}}
 * @constructor
 */
function CategoriesService($resource, $rootScope, BASE_URL, API_PATH, CacheFactory) {

    /**
     * Categories cache impl
     */
    var cache = CacheFactory('Categories', {
        storageMode       : 'localStorage',
        storagePrefix     : 'st.',
        maxAge            : 15 * 60 * 1000,
        cacheFlushInterval: 60 * 60 * 1000,
        deleteOnExpire    : 'aggressive'
    });

    cache.disable();

    var service = {
        create       : create,
        read         : read,
        update       : update,
        remove       : remove,
        subcategories: subcategories
    };

    var url = BASE_URL + API_PATH + '/categories/:id';
    var params = {id: '@id'};
    var options = {};

    var Resource = $resource(url, params, {
        read         : {
            method : 'GET',
            isArray: false,
            cache  : cache
        },
        create       : {
            method: 'POST'
        },
        update       : {
            method: 'PUT'
        },
        remove       : {
            method: 'DELETE'
        },
        subcategories: {
            method : 'GET',
            url    : url + '/subcategories',
            isArray: false,
            cache  : cache
        }
    }, {cancellable: true});

    //categories
    function create(data) {
        cache.removeAll();

        return Resource.create(data).$promise;
    }

    function read(data) {
        var request = Resource.read(data);
        $rootScope.requests.push(request);
        return request.$promise;
    }

    function update(data) {
        cache.removeAll();

        return Resource.update(data).$promise;
    }

    function remove(data) {
        cache.removeAll();

        return Resource.remove(data).$promise;
    }

    //subcategories
    function subcategories(data) {
        var request = Resource.subcategories(data);
        $rootScope.requests.push(request);
        return request.$promise;
    }


    return service;
}