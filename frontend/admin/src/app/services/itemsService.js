angular.module('app').service('Items', ['$resource', '$rootScope', 'BASE_URL', 'API_PATH', 'CacheFactory',
    ItemsService
]);

function ItemsService($resource, $rootScope, BASE_URL, API_PATH, CacheFactory) {

    /**
     * Items cache impl
     */
    var cache = CacheFactory('Items', {
        storageMode       : 'localStorage',
        storagePrefix     : 'st.',
        maxAge            : 15 * 60 * 1000,
        cacheFlushInterval: 60 * 60 * 1000,
        deleteOnExpire    : 'aggressive'
    });

    cache.disable();

    var service = {
        create: create,
        read  : read,
        update: update,
        remove: remove,
        enable: enable
    };

    var url     = BASE_URL + API_PATH + '/items/:id';
    var params  = {id: '@id'};
    var options = {};

    var Resource = $resource(url, params, {
        read      : {
            method : 'GET',
            isArray: false,
            cache  : cache
        },
        create    : {
            method: 'POST'
        },
        update    : {
            method: 'PUT'
        },
        remove    : {
            method: 'DELETE'
        },
        enable    : {
            method: 'PATCH'
        }
    }, {cancellable: true});

    function create(data) {
        cache.removeAll();

        return Resource.create(data).$promise;
    }

    function read(data) {
        var request = Resource.read(data);
        $rootScope.requests.push(request);
        return request.$promise;
    }

    function update(data) {
        cache.removeAll();

        return Resource.update(data).$promise;
    }

    function remove(data) {
        cache.removeAll();

        return Resource.remove(data).$promise;
    }

    function enable(data) {
        cache.removeAll();

        return Resource.enable(data).$promise;
    }

    return service;
}