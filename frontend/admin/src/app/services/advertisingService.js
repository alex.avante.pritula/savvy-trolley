angular.module('app').service('Advertising', ['$resource', '$rootScope', 'BASE_URL', 'API_PATH', 'CacheFactory',
    AdvertisingService
]);

function AdvertisingService($resource, $rootScope, BASE_URL, API_PATH, CacheFactory) {

    /**
     * Items cache impl
     */
    var cache = CacheFactory('Advertising', {
        storageMode       : 'localStorage',
        storagePrefix     : 'st.',
        maxAge            : 15 * 60 * 1000,
        cacheFlushInterval: 60 * 60 * 1000,
        deleteOnExpire    : 'aggressive'
    });

    cache.disable();

    var service = {
        read  : read,
        create: create,
        update: update,
        remove: remove
    };
    var url = BASE_URL + API_PATH + '/items/:id/recommends';
    var params = {
        id: '@id'
    };
    var options = {};

    var Resource = $resource(url, params, {
        read  : {
            method: 'GET',
            //isArray: false,
            cache : cache
        },
        create: {
            method: 'POST'
        },
        update: {
            method: 'PUT',
            url   : url + '/:productId',
            params: {productId: '@productId'},
            cache : cache
        },
        remove : {
            method: 'DELETE',
            url   : url + '/:productId',
            params: {productId: '@productId'},
            cache : cache
        }
    }, {cancellable: true});

    function read(data) {
        var request = Resource.read(data);
        $rootScope.requests.push(request);
        return request.$promise;
    }

    function create(data) {
        cache.removeAll();

        return Resource.create(data).$promise;
    }

    function update(data) {
        cache.removeAll();

        return Resource.update(data).$promise;
    }

    function remove(data) {
        cache.removeAll();

        return Resource.remove(data).$promise;
    }

    return service;
}