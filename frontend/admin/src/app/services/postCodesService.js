angular.module('app').service('PostCodes', ['$resource', '$rootScope', 'BASE_URL', 'API_PATH', 'CacheFactory',
    PostCodesService
]);

function PostCodesService($resource, $rootScope, BASE_URL, API_PATH, CacheFactory) {

    /**
     * Items cache impl
     */
    var cache = CacheFactory('PostCodes', {
        storageMode       : 'localStorage',
        storagePrefix     : 'st.',
        maxAge            : 15 * 60 * 1000,
        cacheFlushInterval: 60 * 60 * 1000,
        deleteOnExpire    : 'aggressive'
    });

    cache.disable();

    var service = {
        read  : read
    };

    var url = BASE_URL + API_PATH + '/postcodes';


    var Resource = $resource(url, {}, {
        read  : {
            method: 'GET',
            cache : cache
        }
    }, {cancellable: true});

    function read(data) {
        var request = Resource.read(data);
        $rootScope.requests.push(request);
        return request.$promise;
    }


    return service;
}