angular.module('app').service('Shops', ['$resource', '$rootScope', 'BASE_URL', 'API_PATH', 'CacheFactory',
    ShopsService
]);

/**
 * Shops REST service
 * @param $resource
 * @param BASE_URL
 * @param API_PATH
 * @param CacheFactory
 * @constructor
 */

function ShopsService($resource, $rootScope, BASE_URL, API_PATH, CacheFactory) {

    /**
     * Shops cache impl
     */
    var cache = CacheFactory('Shops', {
        storageMode       : 'localStorage',
        storagePrefix     : 'st.',
        maxAge            : 15 * 60 * 1000,
        cacheFlushInterval: 60 * 60 * 1000,
        deleteOnExpire    : 'aggressive'
    });

    cache.disable();

    var service = {
        //shops manage
        create: create,
        read  : read,
        update: update,
        remove: remove,

        //items manage
        items              : items,
        itemsAdd           : itemsAdd,
        itemsUpdate        : itemsUpdate,
        itemsUpdateEnabling: itemsUpdateEnabling,
        itemsRemove        : itemsRemove,
        itemsReccomends    : itemsReccomends, 

        //owners manage
        owners      : owners,
        ownersAdd   : ownersAdd,
        ownersRemove: ownersRemove,

        //blocking manage
        changeShopPermissionByType: changeShopPermissionByType,
        getShopPermissionByType: getShopPermissionByType,

        blockingHistoryByType: blockingHistoryByType,
        addBlockingHistoryItem:  addBlockingHistoryItem,
        blockingHistoryCommentsCount : blockingHistoryCommentsCount
    };

    var url = BASE_URL + API_PATH + '/shops/:id';
    var params = {id: '@id'};

    var Resource = $resource(url, params, {
        read               : {
            method : 'GET',
            isArray: false,
            cache  : cache
        },
        create             : {
            method: 'POST'
        },
        update             : {
            method: 'PUT'
        },
        remove             : {
            method: 'DELETE'
        },
        //items by shop
        items              : {
            method: 'GET',
            url   : url + '/items/:itemId',
            params: {itemId: '@itemId'},
            cache : cache
        },
        itemsAdd           : {
            method : 'POST',
            url    : url + '/items',
            isArray: false,
            cache  : cache
        },
        itemsUpdate        : {
            method: 'PUT',
            url   : url + '/items/:itemId',
            params: {itemId: '@itemId'},
            cache : cache
        },
        itemsUpdateEnabling: {
            method: 'PATCH',
            url   : url + '/items/:itemId',
            params: {itemId: '@itemId'}
        },
        itemsRemove        : {
            method: 'DELETE',
            url   : url + '/items/:itemId',
            params: {itemId: '@itemId'},
            cache : cache
        },
        itemsReccomends     :{
            method: 'GET',
            url   : url + '/reccomends',
            cache : cache
        },
        //shop owners assigning
        owners             : {
            method: 'GET',
            url   : url + '/owners/:ownerId',
            params: {ownerId: '@ownerId'},
            cache : cache

        },
        ownersAdd          : {
            method : 'POST',
            url    : url + '/owners/:ownerId',
            params : {ownerId: '@ownerId'},
            isArray: false,
            cache  : cache
        },
        ownersRemove       : {
            method: 'DELETE',
            url   : url + '/owners/:ownerId',
            params: {ownerId: '@ownerId'},
            cache : cache
        },
        //blocking history
        blockingHistoryCommentsCount : {
            method: 'GET',
            url: url + '/blockingHistoryCountToShop',
            cache  : cache
        },
        blockingHistoryByType  : {
            method: 'GET',
            url: url + '/blockingHistoryToShop',
            cache  : cache
        },
        addBlockingHistoryItem  : {
            method: 'POST',
            url: url + '/blockingHistoryToShop',
            cache  : cache
        },
        changeShopPermissionByType : {
            method: 'PUT',
            url   : url + '/permissions/:type',
            params: {type: '@type'},
            cache : cache
        },
        getShopPermissionByType : {
            method: 'GET',
            url: url + '/permissions/:type',
            params: {type: '@type'},
            cache  : cache
        }

    }, {cancellable: true});

    function getShopPermissionByType(data){
        cache.removeAll();
        return Resource.getShopPermissionByType(data).$promise;
    }

    function changeShopPermissionByType(data){
        cache.removeAll();
        return Resource.changeShopPermissionByType(data).$promise;
    }

    function blockingHistoryByType (data){
        cache.removeAll();
        return Resource.blockingHistoryByType(data).$promise;
    }

    function blockingHistoryCommentsCount (data){
        cache.removeAll();
        return Resource.blockingHistoryCommentsCount(data).$promise;
    }

    function addBlockingHistoryItem(data){
        cache.removeAll();
        return Resource.addBlockingHistoryItem(data).$promise;
    }

    function create(data) {
        cache.removeAll();
        return Resource.create(data).$promise;
    }

    function read(data) {
        var request = Resource.read(data);
        $rootScope.requests.push(request);
        return request.$promise;
    }

    function update(data) {
        cache.removeAll();
        return Resource.update(data).$promise;
    }

    function remove(data) {
        cache.removeAll();
        return Resource.remove(data).$promise;
    }

    function items(data) {
        var request = Resource.items(data);
        $rootScope.requests.push(request);
        return request.$promise;
    }

    function itemsAdd(data) {
        cache.removeAll();
        return Resource.itemsAdd(data).$promise;
    }

    function itemsUpdate(data) {
        cache.removeAll();
        return Resource.itemsUpdate(data).$promise;
    }

    function itemsUpdateEnabling(data) {
        cache.removeAll();
        return Resource.itemsUpdateEnabling(data).$promise;
    }

    function itemsRemove(data) {
        cache.removeAll();
        return Resource.itemsRemove(data).$promise;
    }

    function owners(data) {
        var request = Resource.owners(data);
        $rootScope.requests.push(request);
        return request.$promise;
    }

    function ownersAdd(data) {
        cache.removeAll();
        return Resource.ownersAdd(data).$promise;
    }

    function ownersRemove(data) {
        cache.removeAll();
        return Resource.ownersRemove(data).$promise;
    }

    function itemsReccomends(data){
        return Resource.itemsReccomends(data).$promise;
    }

    return service;
}