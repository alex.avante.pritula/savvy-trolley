/**
 * LocalStorageService wrapper
 */
angular.module('app')
    .service('Storage', ['localStorageService', function (localStorageService) {

        if (localStorageService.isSupported) {
            console.log('localStorageService.isSupported', localStorageService.isSupported);
            return localStorageService;
        } else {
            throw Error('Local Storage is not supported!');
        }

    }]);
