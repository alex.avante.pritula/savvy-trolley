angular.module('app').service('Settings', ['$resource', '$rootScope', 'BASE_URL', 'API_PATH', 'CacheFactory',
    SettingsService
]);

function SettingsService($resource, $rootScope, BASE_URL, API_PATH, CacheFactory) {

    /**
     * Items cache impl
     */
    var cache = CacheFactory('Settings', {
        storageMode       : 'localStorage',
        storagePrefix     : 'st.',
        maxAge            : 15 * 60 * 1000,
        cacheFlushInterval: 60 * 60 * 1000,
        deleteOnExpire    : 'aggressive'
    });

    cache.disable();

    var service = {
        read  : read
    };

    var url = BASE_URL + API_PATH + '/settings';


    var Resource = $resource(url, {}, {
        read  : {
            method: 'GET',
            cache : cache
        }
    }, {cancellable: true});

    function read() {
        var request = Resource.read();
        $rootScope.requests.push(request);
        return request.$promise;
    }


    return service;
}