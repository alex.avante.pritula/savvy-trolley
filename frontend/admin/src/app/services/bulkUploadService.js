angular.module('app').service('BulkUploadService', ['$resource', 'BASE_URL', 'API_PATH', 'Upload', '$q',
    BulkUploadService
]);

/**
 * Upload data service
 * @param BASE_URL
 * @param API_PATH
 * @param Upload
 * @returns {{image: uploadImage, isUploadInProgress: isUploadInProgress}}
 * @constructor
 */

function BulkUploadService($resource, BASE_URL, API_PATH, Upload, $q){

    var service = {
        upload : upload,
        isUploadInProgress: isUploadInProgress
    };

   var url = BASE_URL + API_PATH + '/bulkUpload';

    /*Upload.setDefaults({
      ngfMinSize: 10 * 1024, //10KiB
      ngfMaxSize: 2 * 1024 * 1024//2MiB
    });*/

    function  upload(file, type, shopId) {
        return Upload.upload({
            url   : url + '/' + type,
            method: 'POST',
            data  :  {
                bulk  : file,
                shopId: shopId 
            }
        });
    }

    function isUploadInProgress() {
        return Upload.isUploadInProgress();
    }

    return service;
}