angular.module('app').service('Admins', ['$resource', '$rootScope', 'BASE_URL', 'API_PATH', 'CacheFactory',
    AdminsService
]);

/**
 * Admins REST service
 * @param $resource
 * @param BASE_URL
 * @param API_PATH
 * @param CacheFactory
 * @returns {{create: create, read: read, update: update, remove: remove, updateAdmin: updateAdmin, changePassword: changePassword}}
 * @constructor
 */
function AdminsService($resource, $rootScope, BASE_URL, API_PATH, CacheFactory) {

    /**
     * Admins cache impl
     */
    var cache = CacheFactory('Admins', {
        storageMode       : 'localStorage',
        storagePrefix     : 'st.',
        maxAge            : 15 * 60 * 1000, // Items added to this cache expire after 15 minutes
        cacheFlushInterval: 60 * 60 * 1000, // This cache will clear itself every hour
        deleteOnExpire    : 'aggressive' // Items will be deleted from this cache when they expire
    });

    cache.disable();

    var service = {
        create        : create,
        read          : read,
        disable       : disable,
        remove        : remove,
        updateAdmin   : updateAdmin,
        changePassword: changePassword,
        freeAdmin     : freeAdmin
    };
//role
    var url = BASE_URL + API_PATH + '/admins/:id';
    var params = {id: '@id'};
    var options = {};

    var Resource = $resource(url, params, {
        read          : {
            method : 'GET',
            isArray: false,
            cache  : cache
        },
        create        : {
            method: 'POST'
        },
        disable       : {
            method: 'PATCH'
        },
        updateAdmin    : {
            method: 'PATCH',
            url   : url + '/updateAdmin'
        },
        changePassword: {
            method: 'PATCH',
            url   : url + '/password'
        },
        remove        : {
            method: 'DELETE'
        },
        freeAdmin     : {
            method: 'GET',
            url   : BASE_URL + API_PATH + '/admins/free/savvy',
            cache : false
        }
    }, {cancellable: true});

    function create(data) {
        cache.removeAll();

        return Resource.create(data).$promise;
    }

    function read(data) {
        var request = Resource.read(data);
        $rootScope.requests.push(request);
        return request.$promise;
    }

    function disable(data) {
        cache.removeAll();

        return Resource.disable(data).$promise;
    }

    function updateAdmin(data) {
        cache.removeAll();

        return Resource.updateAdmin(data).$promise;
    }

    function changePassword(data) {
        cache.removeAll();

        return Resource.changePassword(data).$promise;
    }

    function remove(data) {
        cache.removeAll();

        return Resource.remove(data).$promise;
    }

    function freeAdmin(data) {
        var request = Resource.freeAdmin(data);
        $rootScope.requests.push(request);
        return request.$promise;
    }

    return service;
}