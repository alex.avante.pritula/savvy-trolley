angular.module('app').service('Users', ['$resource', '$rootScope', 'BASE_URL', 'API_PATH', 'CacheFactory',
    UsersService
]);

/**
 * Users REST service
 * @param $resource
 * @param BASE_URL
 * @param API_PATH
 * @param CacheFactory
 * @returns {{create: create, read: read, update: update, remove: remove, changeRole: changeRole, changePassword: changePassword}}
 * @constructor
 */
function UsersService($resource, $rootScope, BASE_URL, API_PATH, CacheFactory) {

    /**
     * Admins cache impl
     */
    var cache = CacheFactory('Users', {
        storageMode       : 'localStorage',
        storagePrefix     : 'st.',
        maxAge            : 15 * 60 * 1000, // Items added to this cache expire after 15 minutes
        cacheFlushInterval: 60 * 60 * 1000, // This cache will clear itself every hour
        deleteOnExpire    : 'aggressive' // Items will be deleted from this cache when they expire
    });

    cache.disable();

    var service = {
        read           : read,
        remove         : remove,
        changePostCodes: changePostCodes,
        changeAdvert   : changeAdvert,
        changeAdverts  : changeAdverts,
        getWishes      : getWishes,
        removeWishes   : removeWishes,
        getTrolley     : getTrolley,
        removeTrolley  : removeTrolley,
        getHistory     : getHistory,
        updateUser     : updateUser 
    };

    var url = BASE_URL + API_PATH + '/users/:id';
    var params = {id: '@id'};

    var Resource = $resource(url, params, {
        read           : {
            method : 'GET',
            isArray: false,
            cache  : cache
        },
        updateUser     : {
            method: 'patch',
            url   : url + '/credentials'
        },
        changePostCodes: {
            method: 'PUT',
            url   : url + '/postcodes'
        },
        changeAdverts  : {
            method: 'patch'
        },
        remove         : {
            method: 'DELETE'
        },
        getWishes      : {
            method: 'GET',
            url   : url + '/wishes',
            cache : cache
        },
        removeWishes   : {
            method: 'PUT',
            url   : url + '/wishes'
        },
        getTrolley     : {
            method: 'GET',
            url   : url + '/shopping',
            cache : cache
        },
        removeTrolley  : {
            method: 'PUT',
            url   : url + '/shopping'
        },
        getHistory     : {
            method: 'GET',
            url   : url + '/history',
            cache : cache
        }
    }, {cancellable: true});

    function read(data) {
        var request = Resource.read(data);
        $rootScope.requests.push(request);
        return request.$promise;
    }

    function getWishes(data) {
        var request = Resource.getWishes(data);
        $rootScope.requests.push(request);
        return request.$promise;
    }

    function removeWishes(data) {
        cache.removeAll();
        var request = Resource.removeWishes(data);
        return request.$promise;
        // return Resource.removeWishes(data).$promise;
    }

    function getTrolley(data) {
        var request = Resource.getTrolley(data);
        $rootScope.requests.push(request);
        return request.$promise;
        // return Resource.getTrolley(data).$promise;
    }

    function removeTrolley(data) {
        cache.removeAll();
        var request = Resource.removeTrolley(data);
        return request.$promise;
        // return Resource.removeTrolley(data).$promise;
    }

    function getHistory(data) {
        var request = Resource.getHistory(data);
        $rootScope.requests.push(request);
        return request.$promise;
        // return Resource.getHistory(data).$promise;
    }

    function changePostCodes(data) {
        cache.removeAll();

        var request = Resource.changePostCodes(data);
        return request.$promise;
        // return Resource.changePostCodes(data).$promise;
    }

    function changeAdvert(data) {
        cache.removeAll();

        var request = Resource.changeAdverts(data);
        return request.$promise;
        // return Resource.changeAdverts(data).$promise;
    }

    function changeAdverts(data) {
        cache.removeAll();

        var request = Resource.changeAdverts(data);
        return request.$promise;
        // return Resource.changeAdverts(data).$promise;
    }

    function remove(data) {
        cache.removeAll();

        var request = Resource.remove(data);
        return request.$promise;
        // return Resource.remove(data).$promise;
    }

    function updateUser(data){
        cache.removeAll();

        var request = Resource.updateUser(data);
        return request.$promise;
        // return Resource.updateUser(data).$promise;
    }

    return service;
}