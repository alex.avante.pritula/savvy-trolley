angular.module('app').controller('UserController', ['$scope', '$rootScope', '$state', '$log', 'toaster', 'Users', 'StatusHandler',
    UserController
]);

function UserController($scope, $rootScope, $state, $log, toaster, Users, StatusHandler) {
    var vm = angular.extend(this, {
        read             : read,
        readWishes       : readWishes,
        readTrolley      : readTrolley,
        readHistory      : readHistory,
        savePostCodes    : savePostCodes,
        addPostCode      : addPostCode,
        changePostCode   : changePostCode,
        removePostCode   : removePostCode,
        removeWish       : removeWish,
        removeFromTrolley: removeFromTrolley,
        updateUser: updateUser
    });

    //meta dummy
    vm.meta = {
        isPostCodesChanged    : false,
        hasNotMaximumPostCodes: true
    };
    vm.wishes_meta = {
        limit     : 5,
        page      : parseInt($state.params.pageNum, 10) || 1,
        totalCount: 0
    };
    vm.shopping_meta = {
        limit     : 5,
        page      : parseInt($state.params.pageNum, 10) || 1,
        totalCount: 0
    };
    vm.history_meta = {
        limit     : 5,
        page      : parseInt($state.params.pageNum, 10) || 1,
        totalCount: 0
    };
    //current user id
    var _id = parseInt($state.params.id, 10) || null;


    /**
     * Receive user model
     */
    function read() {
        var fname = 'Read user';
        var params = {id: _id || null};

        Users.read(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);

                vm.user = res;
                vm.meta.hasNotMaximumPostCodes = res.postCodes.length < 5;

                readWishes();
                readTrolley();
                readHistory();
            }
        }

        function _onError(fname) {
            return function _onError(res) {
                $log.info(fname + ' error: ', arguments);
                StatusHandler(res.status, res.data.message, {type: 'error'});
                  $state.go('home.users.list');
            }
        }
    }

    /**
     * Receive users wishlist
     */
    function readWishes() {
        var fname = 'Read wishes';
        var params = angular.extend(vm.wishes_meta, {
            id: _id || null
        });
        Users.getWishes(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.wishes = res.wishes;
                angular.extend(vm.wishes_meta, res.meta);
            }
        }
    }


    function updateUser(isValid, data){

        $scope.submitted = true;

        if (isValid) {
            var fname = 'Update user';
            var confirmed = confirm('Are you sure you want to update this user data?');
                
            if(confirmed) {
                var params = {
                    id       : _id || null,
                    email    :  data.email,
                    password :  data.password 
                };
                Users.updateUser(params).then(_onSuccess(fname), _onError(fname));
            }
        }

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);

                $rootScope.$broadcast('actions.write', {
                    entityId     : _id,
                    entityType   : 'USER',
                    operationType: 'EDIT'
                });

                $scope.submitted = false;
            }
        }
    }
    /**
     * Receive users shopping list
     */
    function readTrolley() {
        var fname = 'Read shopping list';
        var params = angular.extend(vm.shopping_meta, {
            id: _id || null
        });
        Users.getTrolley(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);

                vm.trolley = res.shopping;
                angular.extend(vm.shopping_meta, res.meta);
            }
        }
    }

    /**
     * Receive users history list
     */
    function readHistory() {
        var fname = 'Read history';
        var params = angular.extend(vm.history_meta, {
            id: _id || null
        });
        Users.getHistory(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);

                vm.histories = res.histories;
                angular.extend(vm.history_meta, res.meta);
            }
        }
    }

    /**
     * Change postcodes for user
     * @param isValid
     * @param data
     */
    function savePostCodes(isValid, data) {
        var fname = 'Change post codes for user';
        var params = {
            id       : _id || null,
            postCodes: data
        };

        if (isValid) {
            Users.changePostCodes(params).then(_onSuccess(fname), _onError(fname));
        }

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);
                vm.meta.isPostCodesChanged = false;

                $rootScope.$broadcast('actions.write', {
                    entityId     : _id,
                    entityType   : 'USER',
                    operationType: 'EDIT'
                });
            }
        }
    }

    /**
     * Add postcode
     */
    function addPostCode() {
        var postCodes = vm.user.postCodes;
        if (vm.meta.hasNotMaximumPostCodes) {
            vm.meta.isPostCodesChanged = true;
            postCodes.push({code: ''});
        }
        vm.meta.hasNotMaximumPostCodes = postCodes.length < 5
    }

    /**
     * Remove postcode
     * @param index
     */
    function removePostCode(index) {

        var postCodes = vm.user.postCodes;
        if (index >= 0 && index < 5) {
            var confirmed = confirm('Are you sure you want to delete this post code?');
            if (confirmed) {  
                postCodes.splice(index, 1);
                vm.meta.isPostCodesChanged = true;
                vm.meta.hasNotMaximumPostCodes = postCodes.length < 5
            }
        }
    }

    /**
     * Postcodes changing flag
     */
    function changePostCode() {
        vm.meta.isPostCodesChanged = true;
    }

    /**
     * Remove item from users wishlist
     * @param index
     */
    function removeWish(index) {
        var confirmed = confirm('Are you sure you want to delete this wish?');
        if (confirmed) {  
            var fname = 'Remove wish item';
            var wishItem = vm.wishes[index];
            var params = {
                id          : _id || null,
                removedItems: [wishItem.id]
            };

            Users.removeWishes(params).then(_onSuccess(fname), _onError(fname));
        }

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);

                $rootScope.$broadcast('actions.write', {
                    entityId     : _id,
                    entityType   : 'USER',
                    operationType: 'EDIT'
                });

                readWishes();
            }
        }
    }

    /**
     * Remove item from users shopping list
     * @param index
     */
    function removeFromTrolley(index) {
        var confirmed = confirm('Are you sure you want to delete this shopping list item?');
        if (confirmed) {
            var fname = 'Remove trolley item';
            var trolleyItem = vm.trolley[index];
            var params = {
                id          : _id || null,
                removedItems: [{itemId: trolleyItem.id, shopId: trolleyItem.choice.shopId}]
            };

            Users.removeTrolley(params).then(_onSuccess(fname), _onError(fname));
        }
        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);

                $rootScope.$broadcast('actions.write', {
                    entityId     : _id,
                    entityType   : 'USER',
                    operationType: 'EDIT'
                });

                readTrolley();
            }
        }
    }

    //helpers

    //default on success handler
    function _onSuccess(fname) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);
            $scope.submitted = false;

            $state.go('home.users.list');
        }
    }

    //default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.data.message, {type: 'error'});
        }
    }


    //run view
    if (_id) {
        vm.read();
        $rootScope.$broadcast('actions.read', {
            entityId     : _id,
            entityType   : 'USER'
        });
    }

}
