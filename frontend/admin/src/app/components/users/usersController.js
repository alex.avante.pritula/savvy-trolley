angular.module('app').controller('UsersController', ['$scope', '$rootScope', '$state', '$log', 'toaster', 'Users', 'Settings', 'LIMITS',
    UsersController
]);

/**
 * UsersController
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param $log
 * @param toaster
 * @param Users
 * @constructor
 */
function UsersController($scope, $rootScope, $state, $log, toaster, Users, Settings, LIMITS, uiGridConstants) {
    var vm = angular.extend(this, {
        read: read,
        remove: remove,
        changeAdvert: changeAdvert,
        changeAdverts: changeAdverts,
        readSettings: readSettings
    });

    //meta dummy
    vm.meta = {
        limit: $state.params.limit || 10,
        page: parseInt($state.params.pageNum, 10) || 1,
        totalCount: 0,
        query: $state.params.query || ''
    };
    vm.sorting = {
        by: $state.params.sortField || 'createdAt',
        at: $state.params.sortDirection || 'desc'
    };

    vm.hasAdverts = false;

    vm.limits = LIMITS;

    vm.gridOptions = {
        enableHorizontalScrollbar: 0,
        enableVerticalScrollbar: 0,
        minRowsToShow: 1,
        paginationPageSize: vm.meta.limit,
        paginationCurrentPage: vm.meta.page,
        useExternalSorting: true,
        rowHeight: 34,
        enableMinHeightCheck: true,
        columnDefs: [
            // default
            {
                field: 'id',
                displayName: 'Id',
                width: '5%',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'id' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'email',
                displayName: 'Email',
                width: '*',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'email' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'accountType',
                displayName: 'Account Type',
                width: '*',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'accountType' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'createdAt',
                displayName: 'Created At',
                width: '*',
                type: 'date',
                cellFilter: 'date: "dd MMM yyyy, hh:mm:ss a"',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'createdAt' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'verified',
                displayName: 'Verify',
                width: '*',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'verified' ? {
                    direction: $state.params.sortDirection || uiGridConstants.ASC,
                    priority: 0
                } : undefined,
                cellTemplate: [
                    '<i class="fa" ng-class="row.entity.verified ? \'fa-check\' : \'fa-remove\'"></i>',
                    '<span>{{row.entity.verified ? "Yes" : "No"}}</span>'
                ].join(' ')
            },
            {
                name: 'hasAdverts',
                displayName: 'Adwords',
                cellClass: 'text-center',
                width: '8%',
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                visible: false,
                cellTemplate: [
                    '<a class="btn btn-sm btn-warning"',
                    'ng-click="vm.changeAdvert(item.id, item.hasAdverts)">',
                    '<i class="fa" ng-class="row.entity.hasAdverts ? \'fa-check\' : \'fa-remove\'"></i>',
                    '{{row.entity.hasAdverts ? "Yes" : "No"}}</a>'
                ].join(' ')
            },
            {
                name: 'details',
                displayName: "",
                cellClass: 'text-center grid-nav-button',
                width: "8%",
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                visible: $rootScope.checkPermission['EDIT_USER'] || false,
                cellTemplate: [
                    '<a class="btn btn-sm btn-info"',
                    'ui-sref="home.users.view({id: row.entity.id})">',
                    '<i class="fa fa-info"></i>',
                    '<span>Manage</span>',
                    '</a>'
                ].join(' ')
            },
            {
                name: 'delete',
                displayName: "",
                cellClass: 'text-center grid-nav-button',
                width: "8%",
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                visible: $rootScope.checkPermission['EDIT_USER'] || false,
                cellTemplate: [
                    '<button class="btn btn-sm btn-danger" ng-click="grid.appScope.vm.remove(row.entity.id)">',
                    '<i class="fa fa-trash"></i>',
                    '<span>Delete</span>',
                    '</button>'
                ].join(' ')
            }
        ],
        onRegisterApi: function (gridApi) {
            vm.gridApi = gridApi;
            vm.gridApi.core.on.sortChanged($scope, sort);
        }
    };

    function read(options) {
        var fname = 'Users read';
        var params = angular.extend({}, vm.sorting, vm.meta, options, {
            id: null
        });

        Users.read(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                setUsers(res.users);
                angular.extend(vm.meta, res.meta);

                $state.go('home.users.list', {
                    pageNum: vm.meta.page,
                    limit: vm.meta.limit,
                    query: vm.meta.query,
                    sortField: vm.sorting.by,
                    sortDirection: vm.sorting.at
                });
            }
        }
    }

    function remove(id) {
        var fname = 'Delete user';
        $log.debug(fname, arguments);
        var confirmed = confirm('Are you sure you want to delete this item?');

        if (confirmed) {
            Users.remove({id: id}).then(_onSuccess(fname, id), _onError(fname));
        }
    }

    function changeAdvert(id, hasAdverts) {
        var fname = 'Edit advert to user';
        var params = {
            id: id,
            hasAdverts: !hasAdverts
        };

        Users.changeAdvert(params).then(_onSuccess(fname), _onError(fname));
    }

    function changeAdverts() {
        var fname = 'Edit advert to user';
        var params = {
            hasAdverts: !vm.hasAdverts
        };

        Users.changeAdverts(params).then(_onSuccess(fname), _onError(fname));
    }

    function readSettings() {
        var fname = 'Read advert settings to user';
        Settings.read().then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.hasAdverts = res.hasAdverts;
            }
        }
    }

    //helpers

    function setUsers(users) {
        vm.gridOptions.data = users;
        vm.gridOptions.minRowsToShow = users.length;
        $scope.height = (vm.gridOptions.data ? vm.gridOptions.data.length + 1 : 1) * 34;
    }

    function sort(grid, sortColumns) {
        while (sortColumns.length > 1) {
            sortColumns[0].unsort();
            sortColumns.shift();
        }
        var sorting = {by: 'id', at: 'asc'};
        if (sortColumns.length != 0 && sortColumns[0].sort.direction) {
            var sortColumn = sortColumns[0];
            sorting.by = sortColumn.field;
            sorting.at = sortColumn.sort.direction;
        }
        vm.sorting = sorting;
        vm.read();
    }

    //default on success handler
    function _onSuccess(fname, id) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);

            vm.read();
            vm.readSettings();

            $rootScope.$broadcast('actions.write', {
                entityId: res.id || id,
                entityType: 'USER',
                operationType: fname.split(' ')[0].toUpperCase()
            });
        }
    }

    //default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }
    }

    //run
    vm.read();
    vm.readSettings();
    $rootScope.$broadcast('actions.read', {
        entityType: 'USER'
    });
}
