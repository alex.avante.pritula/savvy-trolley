angular.module('app').controller('StatisticsController', ['$scope', '$rootScope', '$state', '$log', 'toaster', 'Statistics', 'PostCodes', 'Download', 'LIMITS',
    StatisticsController
]);

/**
 * StatisticsController
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param $log
 * @param toaster
 * @param Statistics
 * @param PostCodes
 * @constructor
 */
function StatisticsController($scope, $rootScope, $state, $log, toaster, Statistics, PostCodes, Download, LIMITS) {

    var ONE_DAY = 24 * 60 * 60 * 1000;
    var rowHeight = 36;
    var defaultDate = new Date();
    var defaultFrom = new Date(defaultDate.getFullYear(), defaultDate.getMonth(), defaultDate.getDate());
    var defaultTo = new Date(defaultDate.getFullYear(), defaultDate.getMonth(), defaultDate.getDate() + 1);

    var vm = angular.extend(this, {
        read         : read,
        changeOptions: changeOptions,
        refreshCodes : refreshCodes,
        exportToExcel: exportToExcel
    });

    //meta dummy
    vm.meta = {
        limit     : 25,
        page      : parseInt($state.params.pageNum, 10) || 1,
        totalCount: 0

    };

    // Datepicker options
    vm.configModals = {
        opened         : {},
        options        : {
            showWeeks: true
        },
        open           : function (num) {
            this.opened[num] = true;
        },
        fromOptions    : {
            minDate: null
        },
        toOptions      : {
            minDate: new Date()
        },
        format         : 'dd.MM.yyyy',
        altInputFormats: 'M!/d!/yyyy'
    };

    // set default filters
    vm.filters = {
        options : 'users',
        from    : defaultFrom,
        to      : defaultTo,
        postcode: []
    };

    function changeOptions() {
        vm.sorting = {};
        vm.gridOptions.columnDefs = getColumnDefs();
        read();
    }

    //default sorting
    // vm.sorting = {by: 'postcode', at: 'asc'};

    vm.limits = LIMITS;

    vm.gridOptions = {
        data                     : [],
        enableHorizontalScrollbar: 0,
        enableVerticalScrollbar  : 0,
        //paginationPageSizes      : vm.meta.totalCount,
        paginationPageSize       : vm.meta.limit,
        paginationCurrentPage    : vm.meta.page,
        useExternalSorting       : true,
        rowHeight                : rowHeight,
        enableMinHeightCheck     : true,
        columnDefs               : getColumnDefs(),
        onRegisterApi            : function (gridApi) {
            vm.gridApi = gridApi;
            vm.gridApi.core.on.sortChanged($scope, sort);
        }
    };

    function getColumnDefs() {
        return [
            {
                field          : 'postCode',
                displayName    : 'Post Code',
                width          : '*',
                visible        : vm.filters.options === 'users',
                enableFiltering: false,
                enableHiding   : false
            },
            {
                field          : 'numberOfUsers',
                displayName    : 'Number Of Users',
                width          : '*',
                visible        : vm.filters.options === 'users',
                enableFiltering: false,
                enableHiding   : false
            },
            {
                field          : 'name',
                displayName    : 'Item Name',
                width          : '*',
                visible        : vm.filters.options === 'wishes' || vm.filters.options === 'history',
                enableFiltering: false,
                enableHiding   : false
            },
            {
                field          : 'barCode',
                displayName    : 'Barcode',
                width          : '*',
                visible        : vm.filters.options === 'wishes' || vm.filters.options === 'history',
                enableFiltering: false,
                enableHiding   : false
            },
            {
                field          : 'itemsInWishList',
                displayName    : 'Items In Wish List',
                width          : '*',
                visible        : vm.filters.options === 'wishes',
                enableFiltering: false,
                enableHiding   : false
            },
            {
                field          : 'purchasedItems',
                displayName    : 'Purchased Items',
                width          : '*',
                visible        : vm.filters.options === 'history',
                enableFiltering: false,
                enableHiding   : false
            }];
    }

    vm.columns = [
        {option: 'users', label: 'Number Of Users'},
        {option: 'wishes', label: 'Items In Wish List'},
        {option: 'history', label: 'Purchased Items'}
    ];

    $scope.ready = false;
    $scope.height = rowHeight;

    /**
     * Update table data
     */
    function read() {
        var fname = 'Refresh statistics';
        if (vm.filters.postcode.length && vm.filters.options) {
            vm.meta.locked = false;
            var params = angular.extend({}, vm.meta, vm.filters, vm.sorting);
            params.postcode = params.postcode.join(',');
            params.from = params.from.getTime();
            params.to = params.to.getTime();
            $scope.ready = false;

            if (params.from == params.to) params.to += ONE_DAY;

            Statistics.read(params).then(_onSuccess(fname), _onError(fname));
        } else {
            vm.gridOptions.data = [];
            vm.gridOptions.minRowsToShow = 0;
            $scope.height = rowHeight;
            vm.meta.totalCount = 0;
            $scope.ready = false;
        }

        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);

                $scope.ready = res.statistics.length > 0;

                vm.gridOptions.data = res.statistics;
                vm.gridOptions.minRowsToShow = res.statistics.length;
                $scope.height = (vm.gridOptions.data ? vm.gridOptions.data.length + 1 : 1) * rowHeight;
                angular.extend(vm.meta, res.meta);
            }
        }
    }

    /**
     * Receive list of post codes
     */
    function refreshCodes(postcode) {
        var fname = 'Refresh postcodes';
        var params = {postcode: postcode || null, limit: 100};
        PostCodes.read(params).then(_onSuccess(fname), _onError(fname));

        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);

                vm.codes = res.postCodes;
            }
        }
    }


    function exportToExcel() {
        if ($scope.ready) {
            $log.debug('Start export', arguments);

            var params = angular.extend({}, vm.meta, vm.filters, vm.sorting);
            params.postcode = params.postcode.join(',');
            params.from = params.from.getTime();
            params.to = params.to.getTime();

            if (params.from == params.to) params.to += ONE_DAY;

            var options = {
                url   : '/statistics/{{type}}/export',
                params: {type: params.options},
                query : params
            };
            Download.get(options);
        }
    }

//helpers

    function sort(grid, sortColumns) {
        sortColumns = _.sortBy(sortColumns, function (v) {
            return v.sort.priority;
        });
        if (sortColumns.length > 1) {
            sortColumns[0].unsort();
            sortColumns.shift();
        }
        else {
            vm.sorting = {};
            if (sortColumns.length != 0 && sortColumns[0].sort.direction) {
                var sortColumn = sortColumns[0];
                vm.sorting.by = sortColumn.field;
                vm.sorting.at = sortColumn.sort.direction;
            }

            vm.read();
        }
    }

    /**
     * Compare from/to dates and set min
     * @param from
     */
    $scope.setDateRange = function (from) {
        var fromDate = new Date(from);
        var toDate = new Date(vm.filters.to);
        if (toDate <= fromDate) {
            vm.filters.to = fromDate;
        }
        vm.configModals.toOptions.minDate = fromDate;
    };

//default on success handler
    function _onSuccess(fname) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);
            // update table
        }
    }

//default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            vm.results = [];
            vm.meta.totalCount = 0;
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }
    }
}