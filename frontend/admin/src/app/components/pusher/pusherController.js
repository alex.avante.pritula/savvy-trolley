angular.module('app').controller('pusherModalCtrl', ['$scope', '$uibModalInstance', 'DELIVERY_PUSH_HOURS_LIMITS','PUSH_TYPES_MESSAGES', 'items', pusherModalCtrl ]);

function pusherModalCtrl($scope, $uibModalInstance, DELIVERY_PUSH_HOURS_LIMITS, PUSH_TYPES_MESSAGES, items) {
    $scope.hourStep = DELIVERY_PUSH_HOURS_LIMITS['STEP'];

    $scope.items = items.map(function(item){
        var data = Object.assign({
            ignoreOffset: true,
            toSend: true,
            onlyTime: true,
            message: PUSH_TYPES_MESSAGES[item['type']],
            dateTime: new Date()
        }, item);


        var date = new Date(data.dateTime);
        var controllDate = new Date();

        if (controllDate < date){
            date.setHours(DELIVERY_PUSH_HOURS_LIMITS['START'],0,0,0);
        } else {
            var hours = DELIVERY_PUSH_HOURS_LIMITS['START'] + Math.ceil(parseFloat(date.getHours() + 1 - DELIVERY_PUSH_HOURS_LIMITS['START']) / DELIVERY_PUSH_HOURS_LIMITS['STEP']) * DELIVERY_PUSH_HOURS_LIMITS['STEP'];
            if (hours > DELIVERY_PUSH_HOURS_LIMITS['END']) {
               hours = DELIVERY_PUSH_HOURS_LIMITS['START'];
               date.setDate(date.getDate() + 1);
            }

            if (hours < DELIVERY_PUSH_HOURS_LIMITS['START']){
              hours = DELIVERY_PUSH_HOURS_LIMITS['START'];
            }
            date.setHours(hours, 0,0,0);
        }

        data.dateTime = date;

        data.pickerOptions = {
            opened: false,
            open: function () {
                this.opened = true;
            },
            options: {
                hoursStep: DELIVERY_PUSH_HOURS_LIMITS['STEP'],
                minDate: new Date(date),
                showWeeks: true
            },
            format: 'dd.MM.yyyy',
            altInputFormats: 'M!/d!/yyyy'
        };

        return data;
    });

    $scope.ok = function () {
        var pusherItems = [];
        $scope.items.forEach(function(item){
            if(item.toSend){
                var data = {
                    type: item.type,
                    dateTime: item.dateTime
                };

                if(item.ignoreOffset) {
                   var offset = data.dateTime.getTimezoneOffset() / 60;
                   var dateTime = data.dateTime;
                   dateTime.setHours(dateTime.getHours() - offset);
                   data.dateTime = dateTime;
                }

                pusherItems.push(data);
            }
        });
        $uibModalInstance.close(pusherItems);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.timeChanged = function(index){
       var tempDate = new Date($scope.items[index].dateTime);
       var hours = tempDate.getHours();
       var current = new Date();
       if (tempDate < current){
           hours = DELIVERY_PUSH_HOURS_LIMITS['START'] + Math.ceil(parseFloat(current.getHours() - DELIVERY_PUSH_HOURS_LIMITS['START'] - 1) / DELIVERY_PUSH_HOURS_LIMITS['STEP']) * DELIVERY_PUSH_HOURS_LIMITS['STEP'];
       }
       
       if (hours < DELIVERY_PUSH_HOURS_LIMITS['START']){
          hours = DELIVERY_PUSH_HOURS_LIMITS['START'];
       }

       if (hours > DELIVERY_PUSH_HOURS_LIMITS['END']){
          hours = DELIVERY_PUSH_HOURS_LIMITS['END'];
       }
       
       tempDate.setHours(hours);
       $scope.items[index].dateTime = tempDate;
    }
}
