angular
    .module('app')
    .controller('MainController', ['$scope', '$rootScope', '$state', 'SITE_NAME', 'APP_LOGO', 'ROLES', 'LANDING_URL' , 'Storage', 'Actions',
        MainController
    ]);

/**
 *
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param SITE_NAME
 * @param APP_LOGO
 * @param ROLES
 * @param Storage
 * @constructor
 */
function MainController($scope, $rootScope, $state, SITE_NAME, APP_LOGO, ROLES, LANDING_URL, Storage, Actions) {
    var vm = this;

    vm.year = new Date().getFullYear();
    vm.siteName = SITE_NAME;
    vm.APP_LOGO = APP_LOGO;

    //menu
    vm.pages = [
        {
            title     : 'Dashboard',
            state     : 'home.dashboard',
            class     : 'dashboard',
            icon      : 'dashboard',
            permission: 'PROFILE'
        },
        {
            title     : 'Shops',
            state     : 'home.shops.list',
            class     : 'shops',
            icon      : 'shopping-cart',
            permission: 'SHOP'
        },
        {
            title     : 'Users',
            state     : 'home.users.list',
            class     : 'users',
            icon      : 'users',
            permission: 'USER'
        },
        {
            title     : 'Categories',
            state     : 'home.categories',
            class     : 'categories',
            icon      : 'database',
            permission: 'CATEGORY'
        },
        {
            title     : 'Items',
            state     : 'home.items.list',
            class     : 'items',
            icon      : 'barcode',
            permission: 'ITEM'
        },
        {
            title     : 'Admins',
            state     : 'home.admins',
            class     : 'admins',
            icon      : 'user-secret',
            permission: 'ADMIN'
        },
        {
            title     : 'Recommendations',
            state     : 'home.advertising.list',
            class     : 'advertising',
            icon      : 'info',
            permission: 'ADVERTISING'
        },
        {
            title     : 'Statistics',
            state     : 'home.statistics',
            class     : 'statistics',
            icon      : 'line-chart',
            permission: 'STATISTIC'
        },
        {
            title     : 'Reports',
            state     : 'home.reports',
            class     : 'reports',
            icon      : 'file-text-o',
            permission: 'REPORT'
        }
    ];
    $rootScope.requests = [];

    //set current page
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        _.forEach($rootScope.requests, function (request) {
            request.$cancelRequest();
            console.info('request#$cancelRequest');
        });
        $rootScope.requests = [];

        $rootScope.lastActivity = {};
    });

    //set current page
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        $rootScope.page = _.find(vm.pages, function (p, t) {
            return $state.includes(p.state);
        });
    });

    //checkout current authorized admin
    if (!Storage.get('currentAdmin'))  $state.transitionTo('auth.login');
    else {
        /**
         * Build permissions collection for admin actions restriction
         * @returns {object}
         */
        $rootScope.checkPermission = (function () {
            if (!$rootScope.currentAdmin) {
                $rootScope.currentAdmin = Storage.get('currentAdmin');
            }

            var result = {};

            if ($rootScope.currentAdmin.role) {
                _.forEach(ROLES[$rootScope.currentAdmin.role].permissions, function (permission) {
                    result[permission] = true;
                });

            }
            return result;
        })();
    }
}
