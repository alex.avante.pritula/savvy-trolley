angular.module('app').controller('AuthController', ['$scope', '$rootScope', '$state', '$log', 'Auth', 'Storage', 'toaster','StatusHandler', 'LANDING_URL',
    AuthController
]);

/**
 * AdvertisingController
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param $log
 * @param Auth
 * @param Storage
 * @param StatusHandler
 * @constructor
 */
function AuthController($scope, $rootScope, $state, $log, Auth, Storage,toaster, StatusHandler, LANDING_URL) {

    //bind currentAdmin to rootScope
    Storage.bind($rootScope, 'currentAdmin', null, 'currentAdmin');

    this.registrationUrl = "http://"+ LANDING_URL + "/for-business/#registration";

    this.login = function (isValid, data) {
        $scope.submitted = true;

        if (isValid) {
            Auth.login(data).then(_onSuccess, _onError);
        }

        function _onSuccess(res) {
            StatusHandler(null, 'Authorized!');
            $scope.submitted = false;
            Storage.set('currentAdmin', res);
            toaster.pop({
                title: 'Welcome to Savvy Trolley',
                body : 'Admin Panel'
            });
            $state.go('home.dashboard');
        }

        function _onError(err) {
            $log.info('err', err.data);
            StatusHandler(403, 'Incorrect login or password');
        }
    };

    this.logout = function () {
        Auth.logout();
    };



}
