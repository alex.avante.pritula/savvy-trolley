angular.module('app').controller('ProfileController', ['$scope', '$rootScope', '$state', '$log', 'toaster', 'Admins', 'StatusHandler',
    ProfileController
]);

/**
 * ProfileController
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param $log
 * @param toaster
 * @param Admins
 * @constructor
 */
function ProfileController($scope, $rootScope, $state, $log, toaster, Admins, StatusHandler) {
    var vm = angular.extend(this, {
        changePassword: changePassword
    });
    vm.oldPasswordCorrectStatus = true;
    vm.passForm = {};
    vm.pass = {};
    vm.passSubmitted = false;

    /**
     * Update admin password
     * @param isValid
     * @param data
     */
    function changePassword(isValid, data) {
        var fname = 'Change password';
        $log.debug('changePassword: ', arguments);
        vm.passSubmitted = true;
        if (isValid) {
            var query = {id: 'me', oldPassword: data.oldPassword, newPassword: data.newPassword};
            Admins.changePassword(query).then(_onSuccess(fname), _onError(fname));
        }

        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);
                vm.passSubmitted = false;
                vm.passForm.$setPristine();
                vm.pass = {};
            }
        }

        function _onError(fname) {
            return function _onError(res) {
                vm.oldPasswordCorrectStatus = res.data.message !== 'Old password incorrect.';
                $log.info(fname + ' error: ', arguments);
                StatusHandler(res.status, res.data.message || res.statusText, {type: 'error'});
            }
        }
    }


    //helpers

    //default on success handler
    function _onSuccess(fname) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);
            $scope.submitted = false;
        }
    }

    //default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }
    }

}