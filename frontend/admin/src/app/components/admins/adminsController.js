angular.module('app').controller('AdminsController',
    ['$scope', '$rootScope', '$state', '$log', 'toaster', '$uibModal', 'StatusHandler', 'Admins', 'ROLES', 'LIMITS',
        AdminsController
    ]);

/**
 * AdminsController
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param $log
 * @param toaster
 * @param $uibModal
 * @param StatusHandler
 * @param Admins
 * @param ROLES
 * @constructor
 */
function AdminsController($scope, $rootScope, $state, $log, toaster, $uibModal, StatusHandler, Admins, ROLES, LIMITS) {
    //var vm = this;
    var vm = angular.extend(this, {
        read      : read,
        create    : create,
        disable   : disable,
        remove    : remove,
        updateAdmin : updateAdmin,
        openModal:    openModal
    });

    //meta dummy
    vm.meta = {
        limit: $state.params.limit || 10,
        page: parseInt($state.params.pageNum, 10) || 1,
        totalCount: 0
    };

    vm.sorting = {
        by: $state.params.sortField || 'createdAt',
        at: $state.params.sortDirection || 'desc'
    };

    vm.roles = buildRoleList();
    $scope.submitted = false;
    $scope.editSubmitted = false;
    var _id = $state.params.id || null;


    vm.limits = LIMITS;

    vm.gridOptions = {
        enableHorizontalScrollbar: 0,
        enableVerticalScrollbar  : 0,
        paginationPageSize       : vm.meta.limit,
        paginationCurrentPage    : vm.meta.page,
        useExternalSorting       : true,
        rowHeight                : 34,
        enableMinHeightCheck     : true,
        columnDefs               : [
            {
                field          : 'id',
                displayName    : 'Id',
                width          : '5%',
                enableFiltering: false,
                enableHiding   : false,
                sort: $state.params.sortField == 'id' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field          : 'email',
                displayName    : 'Email',
                width          : '*',
                enableFiltering: false,
                enableHiding   : false,
                sort: $state.params.sortField == 'email' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field          : 'type',
                displayName    : 'Role',
                width          : '*',
                enableFiltering: false,
                enableHiding   : false,
                sort: $state.params.sortField == 'type' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field          : 'createdAt',
                displayName    : 'Created At',
                width          : '*',
                type           : 'date',
                cellFilter     : 'date: "dd MMM yyyy, hh:mm:ss a"',
                enableFiltering: false,
                enableHiding   : false,
                sort: $state.params.sortField == 'createdAt' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                name            : 'edit',
                displayName     : '',
                cellClass: 'text-center grid-nav-button',
                width           : "8%",
                enableFiltering : false,
                enableSorting   : false,
                enableColumnMenu: false,
                visible         : $rootScope.checkPermission['EDIT_ADMIN'] || false,
                cellTemplate    : [
                    '<button class="btn btn-sm btn-info" ng-click="grid.appScope.vm.openModal(row.entity)">',
                    '<i class="fa fa-edit"></i>',
                    '<span>Manage</span>',
                    '</button>'
                ].join(' ')
            },
            {
                name            : 'delete',
                displayName     : '',
                cellClass       : 'text-center grid-nav-button',
                width           : "8%",
                enableFiltering : false,
                enableSorting   : false,
                enableColumnMenu: false,
                visible         : $rootScope.checkPermission['EDIT_ADMIN'] || false,
                cellTemplate    : [
                    '<button class="btn btn-sm btn-danger" ng-click="grid.appScope.vm.remove(row.entity.id)">',
                    '<i class="fa fa-trash"></i>',
                    '<span>Delete</span>',
                    '</button>'
                ].join(' ')
            },
            {
                name            : 'enabled',
                displayName     : 'Enabled',
                headerCellClass : 'text-center',
                cellClass       : 'text-center grid-nav-button',
                width           : "8%",
                enableFiltering : false,
                enableSorting   : false,
                enableColumnMenu: false,
                visible         : $rootScope.checkPermission['EDIT_ADMIN'] || false,
                cellTemplate    : [
                    '<button class="btn btn-sm btn-warning"',
                    'ng-click="grid.appScope.vm.disable(row.entity)">',
                    '<i class="fa" ng-class="row.entity.enabled ? \'fa-check\' : \'fa-remove\'"></i>',
                    '<span>{{row.entity.enabled ? "Yes" : "No"}}</span>',
                    '</button>'
                ].join(' ')
            }
        ],
        onRegisterApi            : function (gridApi) {
            vm.gridApi = gridApi;
            vm.gridApi.core.on.sortChanged($scope, sort);
        }
    };

    /**
     * Receive one or list of admins
     */
    function read() {
        var fname = 'Admins read';
        var params = angular.extend({}, vm.sorting, vm.meta, {
            id: _id || null
        });

        Admins.read(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.gridOptions.data = res.admins;
                vm.gridOptions.minRowsToShow = res.admins.length;
                $scope.height = (vm.gridOptions.data ? vm.gridOptions.data.length + 1 : 1) * 34;
                angular.extend(vm.meta, res.meta);

                $state.go('home.admins', {
                    pageNum: vm.meta.page,
                    limit: vm.meta.limit,
                    sortField: vm.sorting.by,
                    sortDirection: vm.sorting.at
                });
            }
        }
    }

    /**
     * Create admin model
     * @param isValid
     * @param data
     */
    function create(isValid, data) {
        var fname = 'Admin create';
        $log.debug('create: ', arguments);
        $scope.submitted = true;
        if (isValid) {
            if (data.password !== data.verifyPassword) {
                return;
            }

            Admins.create(data).then(_onSuccess(fname), _onError(fname));
        }

        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);

                $scope.submitted = false;
                $scope.modalInstance.close();

                $rootScope.$broadcast('actions.write', {
                    entityId     : res.id,
                    entityType   : 'ADMIN',
                    operationType: 'CREATE'
                });

                _clearForms();
                vm.read();
            }
        }

        function _onError(fname) {
            return function _onError(res) {
                $log.info(fname + ' error: ', arguments);
                if(res.status == 422) toaster.pop('error', "Email address already exists in database. Please enter a unique email address");
                else StatusHandler(res.status, res.statusText, {type: 'error'});
            }
        }
    }

    /**
     * Disable admin
     * @param index
     */
    function disable(admin) {
        var fname = 'Admin disable';
        $log.debug('disable: ', admin);
        $scope.submitted = true;
        if (admin) {
            var params = {
                id     : admin.id,
                enabled: !admin.enabled
            };
            Admins.disable(params).then(_onSuccess(fname), _onError(fname));
        }

        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);
                $scope.submitted = false;

                $rootScope.$broadcast('actions.write', {
                    entityId     : res.id,
                    entityType   : 'ADMIN',
                    operationType: 'EDIT'
                });

                $state.reload('home.admins');
            }
        }
    }

    /**
     * Update admin role
     * @param isValid
     * @param id
     * @param role
     */
    function updateAdmin(isValid, admin) {
        var fname = 'Update admin';
        $log.debug('updateAdmin: ', arguments);
        $scope.editSubmitted = true;

        if (isValid) {
            var query = {
                id: admin.id,
                role: admin.role
            };
            if ($rootScope.checkPermission['UPDATE_ADMIN_PASSWORD'] && admin.isPasswordChangable){
                if(!admin.password || !admin.verifyPassword ||
                    admin.password != admin.verifyPassword) return;
                query.password = admin.password;
            }
            Admins.updateAdmin(query).then(_onSuccess(fname), _onError(fname));
        }

        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);
                $scope.editSubmitted = false;
                $scope.modalInstance.close();

                $rootScope.$broadcast('actions.write', {
                    entityId     : res.id,
                    entityType   : 'ADMIN',
                    operationType: 'EDIT'
                });

                vm.read();
            }
        }
    }

    /**
     * Remove admin model
     * @param id
     */
    function remove(id) {
        var confirmed = confirm('Are you sure you want to delete this admin?');
        if (confirmed) {
            var fname = 'Admin remove';
            $log.debug('remove: ', arguments);
            Admins.remove({id: id}).then(_onSuccess(fname), _onError(fname));
        };

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);
                $scope.submitted = false;

                $rootScope.$broadcast('actions.write', {
                    entityId     : id,
                    entityType   : 'ADMIN',
                    operationType: 'DELETE'
                });

                // update list
                read();
            }
        }
    }

    function openModal (data) {
        var tmpl = 'app/components/admins/create.html';
        if(data) {
            tmpl = 'app/components/admins/edit.html';
            if($rootScope.checkPermission['UPDATE_ADMIN_PASSWORD']) {
                data.password = "";
                data.verifyPassword = "";
                data.isPasswordChangable = false;
            }
        }
        $scope.modalInstance = $uibModal.open({
            templateUrl: tmpl,
            controller : 'adminModalCtrl',
            scope      : $scope,
            size       : 'lg',
            resolve    : {
                item: data || {}
            }
        });

        $scope.modalInstance.result.then(function (results) {
            $log.info('modalInstance submit', arguments);
            _clearForms();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
            _clearForms();
        });
    };

    //helpers

    function sort(grid, sortColumns) {
        sortColumns = _.sortBy(sortColumns, function (v) {
            return v.sort.priority;
        });
        if (sortColumns.length > 1) {
            sortColumns[0].unsort();
            sortColumns.shift();
        }
        else {
            var sorting = {by: 'id', at: 'asc'};
            if (sortColumns.length != 0 && sortColumns[0].sort.direction) {
                var sortColumn = sortColumns[0];
                sorting.by = sortColumn.field;
                sorting.at = sortColumn.sort.direction;
            }

            vm.sorting = sorting;
            vm.read();
        }
    }

    function buildRoleList() {
        var roles = {};
        if ($rootScope.checkPermission['ONLY_ENTERPRISE_SAVVY_SHOP_ADMIN']) {
            roles['ENTERPRISE_SAVVY_SHOP'] = ROLES['ENTERPRISE_SAVVY_SHOP'];
        } else if ($rootScope.checkPermission['EXCEPT_SUPER_ADMIN']) {
            _.forIn(ROLES, function (value, key) {
                if (key !== 'SUPER_ADMIN')
                    roles[key] = value;
            });
        } else {
            roles = ROLES;
        }

        return roles;
    }

    //default on success handler
    function _onSuccess(fname) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);
            $scope.submitted = false;
            $state.go('home.admins');
        }
    }

    //default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }
    }

    //clear admin forms
    function _clearForms() {
        if ($scope.adminForm) {
            $scope.adminForm.$setPristine();
        }
        if ($scope.roleForm) {
            $scope.roleForm.$setPristine();
        }

        $scope.submitted = false;
    }

    //run
    vm.read();
    $rootScope.$broadcast('actions.read', {
        entityType: 'ADMIN'
    });

}
