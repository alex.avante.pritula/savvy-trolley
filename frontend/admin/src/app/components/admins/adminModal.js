angular.module('app').controller('adminModalCtrl', ['$scope', '$uibModalInstance', 'item',
        adminModalCtrl
]);

function adminModalCtrl($scope, $uibModalInstance, item) {
    $scope.admin = item;

    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}