angular.module('app').controller('AdvertisementsController', ['$scope', '$rootScope', '$state', '$log', 'Items', 'StatusHandler', 'LIMITS', 'BASE_URL', 'DEFAULT_IMAGE',
    AdvertisementsController
]);

/**
 * AdvertisingController
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param $log
 * @param Items
 * @param StatusHandler
 * @param LIMITS
 * @constructor
 */
function AdvertisementsController($scope, $rootScope, $state, $log, Items, StatusHandler, LIMITS, BASE_URL, DEFAULT_IMAGE) {

    var vm = angular.extend(this, {
        read: read
    });

    vm.noImage = BASE_URL + DEFAULT_IMAGE;
    
    //meta dummy
    vm.meta = {
        limit: $state.params.limit || 10,
        page: parseInt($state.params.pageNum, 10) || 1,
        totalCount: 0
    };

    vm.sorting = {
        by: $state.params.sortField || 'createdAt',
        at: $state.params.sortDirection || 'desc'
    };

    vm.gridOptions = {
        enableHorizontalScrollbar: 0,
        enableVerticalScrollbar  : 0,
        paginationPageSize       : vm.meta.limit,
        paginationCurrentPage    : vm.meta.page,
        useExternalSorting       : true,
        rowHeight                : 34,
        enableMinHeightCheck     : true,
        columnDefs               : [
            {
                field           : 'image',
                displayName     : 'Image',
                width           : '5%',
                enableFiltering : false,
                enableSorting   : false,
                enableColumnMenu: false,
                cellClass       : 'text-center',
                cellTemplate: '<img src="{{(row.entity.image ?  row.entity.image : grid.appScope.vm.noImage)}}" alt="{{row.entity.name}}" width="30" height="30"/>'
            },
            {
                field          : 'name',
                displayName    : 'Name',
                width          : '*',
                enableFiltering: false,
                enableHiding   : false,
                sort: $state.params.sortField == 'name' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field          : 'brand',
                displayName    : 'Brand',
                width          : '*',
                enableFiltering: false,
                enableHiding   : false,
                sort: $state.params.sortField == 'brand' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field          : 'barCode',
                displayName    : 'BarCode',
                width          : '*',
                enableFiltering: false,
                enableHiding   : false,
                sort: $state.params.sortField == 'barCode' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field          : 'createdAt',
                displayName    : 'Created At',
                width          : '*',
                type           : 'date',
                cellFilter     : 'date: "dd MMM yyyy, hh:mm:ss a"',
                enableFiltering: false,
                enableHiding   : false,
                sort: $state.params.sortField == 'createdAt' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field          : 'updatedAt',
                displayName    : 'Updated At',
                width          : '*',
                type           : 'date',
                cellFilter     : 'date: "dd MMM yyyy, hh:mm:ss a"',
                enableFiltering: false,
                enableHiding   : false,
                sort: $state.params.sortField == 'updatedAt' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                name            : 'details',
                displayName     : '',
                cellClass       : 'text-center grid-nav-button',
                width           : "8%",
                enableFiltering : false,
                enableSorting   : false,
                enableColumnMenu: false,
                visible         : $rootScope.checkPermission['REVIEW_ADVERTISING'] || false,
                cellTemplate    : [
                    '<a class="btn btn-sm btn-info"',
                    'ui-sref="home.advertising.view({id: row.entity.id})">',
                    '<i class="fa fa-info"></i>',
                    '<span>Manage</span>',
                    '</a>'
                ].join(' ')
            }
        ],
        onRegisterApi          : function (gridApi) {
            vm.gridApi = gridApi;
            vm.gridApi.core.on.sortChanged($scope, sort);
        }
    };

    vm.limits = LIMITS;

    var _id = $state.params.id || null;
    var page = $state.params.pageNum || 1;

    /**
     * Receive one or list of items
     * @param options
     */
    function read(options) {
        var fname = 'Read items';
        var params = angular.extend({}, vm.sorting, vm.meta, options || {
                id: _id || null
            });

        Items.read(params).then(_onSuccess(fname), _onError(fname));
    }

    //helpers

    function sort(grid, sortColumns) {
        sortColumns = _.sortBy(sortColumns, function (v) {
            return v.sort.priority;
        });
        if (sortColumns.length > 1) {
            sortColumns[0].unsort();
            sortColumns.shift();
        }
        else {
            var sorting = {by: 'id', at: 'asc'};
            if (sortColumns.length != 0 && sortColumns[0].sort.direction) {
                var sortColumn = sortColumns[0];
                sorting.by = sortColumn.field;
                sorting.at = sortColumn.sort.direction;
            }

            vm.sorting = sorting;
            vm.read();
        }
    }

    //default on success handler
    function _onSuccess(fname) {
        return function _onSuccess(res) {
            $log.debug(fname + ' success: ', arguments);
            vm.gridOptions.data = res.items;
            vm.gridOptions.minRowsToShow = res.items.length;
            $scope.height = (vm.gridOptions.data ? vm.gridOptions.data.length + 1 : 1) * 34;
            angular.extend(vm.meta, res.meta);

            $state.go('home.advertising.list', {
                pageNum: vm.meta.page,
                limit: vm.meta.limit,
                sortField: vm.sorting.by,
                sortDirection: vm.sorting.at
            });
        }
    }

    //default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }
    }

    //run
    vm.read();
    $rootScope.$broadcast('actions.read', {
        entityType: 'ADVERTISING'
    });
}
