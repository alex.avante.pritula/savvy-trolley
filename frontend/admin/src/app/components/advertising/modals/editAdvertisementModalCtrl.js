angular.module('app').controller('EditAdvertisementModalCtrl',
    ['$scope', '$rootScope', '$state', '$uibModalInstance', 'toaster', '$log', 'item', 'Advertising', 'StatusHandler', 'Shops', 'MONTH_DATE_RANGE_LIMITS',
        EditAdvertisementModalCtrl
    ]);

/**
 * AdvertisingController
 * @param $scope
 * @param $state
 * @param $uibModalInstance
 * @param toaster
 * @param $log
 * @param item
 * @param Advertising
 * @param StatusHandler
 * @constructor
 */
function EditAdvertisementModalCtrl($scope, $rootScope, $state, $uibModalInstance, toaster, $log, item, Advertising, StatusHandler, Shops, MONTH_DATE_RANGE_LIMITS) {

    var vm = angular.extend(this, {
        dateRangeLimit      : dateRangeLimit,
        setDateRangeByTo    : setDateRangeByTo,
        setDateRangeByFrom  : setDateRangeByFrom,
        updateAdvertisement : updateAdvertisement
    });

    vm.item = item.choice;
    vm.hasDateRangeLimit = false;
    vm.item.id = parseInt($state.params.id, 10);

    /**
     *  From/to date time picker options
     */
    vm.configModals = {
        opened         : {},
        options        : {
            showWeeks: true
        },
        open           : function (num) {
            this.opened[num] = true;
        },
        fromOptions    : {
            minDate: new Date()
        },
        toOptions      : {
            minDate: new Date()
        },
        format         : 'dd.MM.yyyy',
        altInputFormats: 'M!/d!/yyyy'
    };

    /** Setting date min/max range
     *
     */
    function setDateRangeByFrom(from) {
        var fromDate = new Date(from);
        var toDate = new Date(vm.item.to);
        if(toDate < fromDate) vm.item.to = fromDate;

        if(vm.hasDateRangeLimit){
            var maxDate = new Date(fromDate);
            maxDate.setMonth(maxDate.getMonth() + MONTH_DATE_RANGE_LIMITS['ADVERTISING']);
            vm.configModals.toOptions._maxDate = maxDate;
            if(maxDate < vm.item.to) vm.item.to = maxDate;
        }

        vm.configModals.toOptions.minDate = fromDate;
    };

    function setDateRangeByTo(to) {
        if(vm.hasDateRangeLimit) {
            var toDate = new Date(to);
            var maxDate = new Date(vm.configModals.toOptions._maxDate);
            if (maxDate < toDate) {
                vm.item.to = maxDate;
                var msg = "You can only set a recommendation for a maximum of 3 months from the start date. Please ensure the end date is 3 months or less than the start date";
                toaster.pop('error', msg);
            }
        }
    };

    /**
     * Update advertisement model
     * @param isValid
     * @param data
     */
    function updateAdvertisement(isValid, data) {
        var fname = 'Update recommendation';
        $log.debug(fname, arguments);
        vm.submitted = true;

        if (isValid) {
            Advertising.update(formatAdvertisementData(data)).then(_onSuccess(fname), _onError(fname));
        }
    }


    //helpers

    //adv date range formatter
    function formatAdvertisementData(data) {
        data.from = new Date(data.from).getTime();
        data.to = new Date(data.to).getTime();
        return data;
    }

    function dateRangeLimit() {
        var fname = 'Shop Owners read';
        var params =  { id: vm.item.shopId || null };

        Shops.owners(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);

                vm.hasDateRangeLimit = res['owners'].findIndex(function(el){
                    return el.role === 'ENTERPRISE_SAVVY_SHOP'
                }) == -1;

                if(vm.hasDateRangeLimit){
                    var maxDate = new Date();
                    var fromDate = new Date(vm.item.from);
                    if(fromDate > maxDate) maxDate = fromDate;
                    maxDate.setMonth(maxDate.getMonth() + MONTH_DATE_RANGE_LIMITS['ADVERTISING']);
                    vm['configModals'].toOptions['_maxDate'] = maxDate;
                }
            }
        }
    }

    //default on success handler
    function _onSuccess(fname) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);

            $rootScope.$broadcast('actions.write', {
                entityId     : vm.item.id,
                entityType   : 'ADVERTISING',
                operationType: 'EDIT'
            });

            $uibModalInstance.close();
        }
    }

    //default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }
    }


    if($rootScope['currentAdmin'].role == "SAVVY_SHOP") vm.dateRangeLimit();
}