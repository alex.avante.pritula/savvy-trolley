angular.module('app').controller('AdvertisementModalCtrl', ['$scope', '$rootScope', '$state', '$uibModalInstance', 'toaster', '$log', 'Shops', 'Advertising', 'PostCodes', 'StatusHandler', 'MONTH_DATE_RANGE_LIMITS',
    AdvertisementModalCtrl
]);

/**
 * AdvertisingController
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param toaster
 * @param $log
 * @param Shops
 * @param Advertising
 * @param PostCodes
 * @constructor
 */
function AdvertisementModalCtrl($scope, $rootScope, $state, $uibModalInstance, toaster, $log, Shops, Advertising, PostCodes, StatusHandler, MONTH_DATE_RANGE_LIMITS) {

    var vm = angular.extend(this, {
        readShops                 : readShops,
        readShopItems             : readShopItems,
        createAdvertisement       : createAdvertisement,
        selectShop                : selectShop,
        selectItem                : selectItem,
        refreshCodes              : refreshCodes,
        setDateRangeByFrom        :  setDateRangeByFrom,
        setDateRangeByTo          :  setDateRangeByTo,
        advertisingDateRangeLimit : advertisingDateRangeLimit
    });

    var _id = parseInt($state.params.id, 10) || null;

    //meta dummy
    vm.meta = {
        limit     : 10,
        page      : parseInt($state.params.pageNum, 10) || 1,
        totalCount: 0,
        postCode  : []
    };

    vm.shops_meta = angular.extend({}, vm.meta);
    vm.items_meta = angular.extend({}, vm.meta);

    vm.shops = [];
    vm.items = [];

    vm.itemAdvertising = {
        from: new Date(),
        to: new Date()
    };

    // here we collect selected shop and item
    vm.selected = {};

    // Datepicker options
    vm.configModals = {
        opened         : {},
        options        : {
            showWeeks: true
        },
        open           : function (num) {
            this.opened[num] = true;
        },
        fromOptions    : {
            minDate    : new Date()
        },
        toOptions      : {
          minDate      : new Date()
        },
        format         : 'dd.MM.yyyy',
        altInputFormats: 'M!/d!/yyyy'
    };

    /** Setting date min/max range
     *
     */
    function setDateRangeByFrom(from) {
        var fromDate = new Date(from);
        var toDate = new Date(vm.itemAdvertising.to);
        if(toDate < fromDate)  vm.itemAdvertising.to = fromDate;

        if(vm.selected.hasDateRangeLimit){
            var maxDate = new Date(fromDate);
            maxDate.setMonth(maxDate.getMonth() + MONTH_DATE_RANGE_LIMITS['ADVERTISING']);
            vm.configModals.toOptions._maxDate = maxDate;
            if(maxDate < vm.itemAdvertising.to) vm.itemAdvertising.to = maxDate;
        }

        vm.configModals.toOptions.minDate = fromDate;
    };

    function setDateRangeByTo(to) {
        if(vm.selected.hasDateRangeLimit) {
            var toDate = new Date(to);
            var maxDate = new Date(vm.configModals.toOptions._maxDate);
            if (maxDate < toDate) {
                vm.itemAdvertising.to = maxDate;
                var msg = "You can only set a recommendation for a maximum of 3 months from the start date. Please ensure the end date is 3 months or less than the start date";
                toaster.pop('error', msg);
            }
        }
    };

    /** Receive list of shops
     * @param type
     * @param options
     */
    function readShops(type, options) {
        var fname = 'Read shops';
        var params = angular.extend(vm.shops_meta, options);

        // if we change filter data -> deselect all item and shop
        if (type === 'update') {
            vm.selected.shop = null;
            vm.selected.item = null;
            vm.selected.hasDateRangeLimit = false;
            vm.items = [];
            vm.items_meta = angular.extend({}, vm.meta);
            //params = angular.extend(vm.shops_meta, vm.meta);
            params.postcode = vm.meta.postCode.join(',');
        }

        Shops.read(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.shops = res.shops;
                angular.extend(vm.shops_meta, res.meta);
            }
        }
    }

    /**
     * Receive one or list of items for selected shop
     */
    function readShopItems() {
        var fname = 'Read items';
        var params = angular.extend(vm.items_meta, {
            id: vm.selected.shop.id || null
        });

        Shops.itemsReccomends(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.items = res.items;
                angular.extend(vm.items_meta, res.meta);
            }
        }
    }

    /**
     * Create advertisement model
     * @param isValid
     * @param data
     */
    function createAdvertisement(isValid, data) {
        var fname = 'Create recommendation';
        $log.debug(fname, arguments);
        vm.submitted = true;
        if (isValid) {
            Advertising.create(formatAdvertisementData(data)).then(_onSuccess(fname), _onError(fname));
        }
    }


    //helpers

    /**
     * Check selected current shop
     * @param shop
     */
    function selectShop(shop) {
        var isSavvyShop = ($rootScope['currentAdmin'].role == 'SAVVY_SHOP');
        if(!shop.hasAdvertising && isSavvyShop){
            var msg =  "Recommendation privileges have been disabled for your account. This typically occurs when there is an issue with your billing. For more information, please email:\n" + shop.contactEmail;
            toaster.pop('error', msg);
            return;
        }

        vm.selected.shop = shop;
        vm.selected.item = null;
        vm.selected.hasDateRangeLimit = false;
        vm.readShopItems();

        if(isSavvyShop) vm.advertisingDateRangeLimit();
    }

    function advertisingDateRangeLimit() {
        var fname = 'Shop Owners read';
        var params =  { id: vm.selected.shop.id || null };

        Shops.owners(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);

                vm.selected.hasDateRangeLimit = res['owners'].findIndex(function(el){
                    return el.role === 'ENTERPRISE_SAVVY_SHOP'
                }) == -1;

                if(vm.selected.hasDateRangeLimit){
                    var maxDate = new Date();
                    maxDate.setMonth(maxDate.getMonth() + MONTH_DATE_RANGE_LIMITS['ADVERTISING']);
                    vm['configModals'].toOptions['_maxDate'] = maxDate;
                }
            }
        }
    }

    /**
     * Check current item selected
     * @param item
     */
    function selectItem(item) {
        vm.selected.item = item;
        vm.itemAdvertising = {
            id       : _id,
            postCode : vm.selected.shop.postCode,
            productId: item.productId
        };
        vm.submitted = false;
    }

    /**
     * Receive list of post codes
     */
    function refreshCodes(postcode) {
        var fname = 'Refresh postcodes';
        var params = {postcode: postcode || null, limit: 100, type: 'shop'};
        PostCodes.read(params).then(_onSuccess(fname), _onError(fname));

        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);

                vm.codes = res.postCodes;
            }
        }
    }

    //default on success handler
    function _onSuccess(fname) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);

            vm.submitted = false;
            vm.selected.item = null;

            $uibModalInstance.dismiss();

            $rootScope.$broadcast('actions.write', {
                entityId     : _id,
                entityType   : 'ADVERTISING',
                operationType: 'CREATE'
            });
        }
    }

    //default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }
    }

    function formatAdvertisementData(data) {
        return {
            from     : new Date(data.from).getTime(),
            to       : new Date(data.to).getTime(),
            postCode : data.postCode,
            productId: data.productId,
            id       : data.id
        }
    }

    /**
     * Compare from/to dates and set min
     * @param from
     */
    $scope.setDateRange = function (from) {
        var fromDate = new Date(from);
        var toDate = new Date(vm.itemAdvertising.to);
        if(toDate <= fromDate) {
            vm.itemAdvertising.to = fromDate;
        }
        vm.configModals.toOptions.minDate = fromDate;
    };

    //run
    vm.readShops();
}