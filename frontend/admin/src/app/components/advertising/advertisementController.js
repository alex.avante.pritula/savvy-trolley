angular.module('app').controller('AdvertisementController', ['$scope', '$rootScope', '$state', '$log', 'toaster', '$uibModal', 'Items', 'Advertising','StatusHandler', 'LIMITS',
    AdvertisementController
]);

/**
 * AdvertisingController
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param $log
 * @param toaster
 * @param $uibModal
 * @param Items
 * @param Advertising
 * @param LIMITS
 * @constructor
 */
function AdvertisementController($scope, $rootScope, $state, $log, toaster, $uibModal, Items, Advertising, StatusHandler, LIMITS) {

    var vm = angular.extend(this, {
        read    : read,
        remove  : remove,
        edit    :   edit,
        readItem: readItem,
    });

    var _id = parseInt($state.params.id, 10) || null;

    //meta dummy
    vm.meta = {
        limit     : 10,
        page      : 1,
        totalCount: 0
    };

    vm.gridOptions = {
        enableHorizontalScrollbar: 0,
        enableVerticalScrollbar  : 0,
        paginationPageSize       : vm.meta.limit,
        paginationCurrentPage    : vm.meta.page,
        useExternalSorting       : true,
        rowHeight                : 34,
        enableMinHeightCheck     : true,
        columnDefs               : [
            // default
            {
                field          : 'name',
                displayName    : 'Name',
                width          : '*',
                enableFiltering: false,
                enableHiding   : false
            },
            {
                field          : 'choice.from',
                displayName    : 'From',
                width          : '15%',
                type           : 'date',
                cellFilter     : 'date: "dd MMM yyyy"',
                enableFiltering: false,
                enableHiding   : false
            },
            {
                field          : 'choice.to',
                displayName    : 'To',
                width          : '15%',
                type           : 'date',
                cellFilter     : 'date: "dd MMM yyyy"',
                enableFiltering: false,
                enableHiding   : false
            },
            {
                field          : 'choice.postCode',
                displayName    : 'Post Code',
                width          : '*',
                enableFiltering: false,
                enableHiding   : false
            },
            {
                field          : 'choice.shopName',
                displayName    : 'Shop Name',
                width          : '*',
                enableFiltering: false,
                enableHiding   : false
            },
            {
                name            : 'edit',
                displayName     : 'Edit',
                cellClass: 'text-center grid-nav-button',
                width: "8%",
                enableFiltering : false,
                enableSorting   : false,
                enableColumnMenu: false,
                visible         : $rootScope.checkPermission['EDIT_ADVERTISING'] || false,
                cellTemplate    : [
                    '<button class="btn btn-sm btn-info"',
                    'ng-click="grid.appScope.vm.edit(row.entity)"  >',
                    '<i class="fa fa-edit"></i>',
                    '<span>Edit</span>',
                    '</button>'
                ].join(' ')
            },
            {
                name            : 'delete',
                displayName     : 'Delete',
                cellClass: 'text-center grid-nav-button',
                width: "8%",
                enableFiltering : false,
                enableSorting   : false,
                enableColumnMenu: false,
                visible         : $rootScope.checkPermission['EDIT_ADVERTISING'] || false,
                cellTemplate    : [
                    '<button class="btn btn-sm btn-danger" ng-click="grid.appScope.vm.remove(row.entity)">',
                    '<i class="fa fa-trash"></i>',
                    '<span>Delete</span>',
                    '</button>'
                ].join(' ')
            }
        ],
        onRegisterApi            : function (gridApi) {
            vm.gridApi = gridApi;
            vm.gridApi.core.on.sortChanged($scope, sort);
        }
    };

    function edit(rowData){
        if(!rowData.choice.hasAdvertising && $rootScope['currentAdmin'].role == 'SAVVY_SHOP'){
            var msg =  "Recommendation privileges have been disabled for your account. This typically occurs when there is an issue with your billing. For more information, please email:\n"
                + rowData.choice.contactEmail;
            toaster.pop('error', msg);
            return;
        }
        vm.openModal('editAdvertisement', rowData, 'EditAdvertisementModalCtrl');
    }

    vm.limits = LIMITS;

    vm.advertisements = {};

    /**
     * Receive list of advertisements
     */
    function read(options) {
        var fname = 'Read recommendations';
        var params = angular.extend({}, vm.meta, options, {id: _id});

        Advertising.read(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.gridOptions.data = res.advertising;
                vm.gridOptions.minRowsToShow = res.advertising.length;
                $scope.height = (vm.gridOptions.data ? vm.gridOptions.data.length + 1 : 1) * 34;
                angular.extend(vm.meta, res.meta);
            }
        }
    }

    /**
     * Receive item
     */
    function readItem() {
        var fname = 'Read item';
        var params = {id: _id};

        Items.read(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.item = res;
                read();
            }
        }

        function _onError(fname) {
            return function _onError(res) {
                $log.info(fname + ' error: ', arguments);
                StatusHandler(res.status, res.statusText, {type: 'error'});
                if(res.status == 404) $state.go('home.advertising.list');
            }
        }
    }

    /**
     * Delete advertisement
     */
    function remove(advertisement) {
        var confirmed = confirm('Are you sure you want to delete this recommendation product item?');
        if (confirmed) {
            var fname = 'Delete recommendation';
            var params = {
                id       : _id,
                productId: advertisement.choice.productId
            };

            Advertising.remove(params).then(_onSuccess(fname), _onError(fname));
        }

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);

                // if we delete the last item on page -> change page
                if (vm.advertisements.length === 1 && vm.meta.page > 1) {
                    vm.meta.page = vm.meta.page - 1;
                }

                $rootScope.$broadcast('actions.write', {
                    entityId     : _id,
                    entityType   : 'ADVERTISING',
                    operationType: 'DELETE'
                });

                vm.read();
            }
        }

    }

    /**
     * Open modal according to type of action with optional data
     * @param type
     * @param data
     * @param controller
     */
    vm.openModal = function (type, data, controller) {
        var tmpl = 'app/components/advertising/modals/' + type + '.html';

        vm.modalInstance = $uibModal.open({
            templateUrl : tmpl,
            controller  : controller || 'AdvertisementModalCtrl',
            scope       : $scope,
            controllerAs: 'vm',
            size        : 'lg',
            resolve     : {
                item: angular.copy(data) || {}
            }
        });

        vm.modalInstance.result.then(function (results) {
            $log.info('modalInstance submit', arguments);
            vm.read();

        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
            vm.read();
        });
    };


    //helpers

    function sort(grid, sortColumns) {
        sortColumns = _.sortBy(sortColumns, function (v) {
            return v.sort.priority;
        });
        if (sortColumns.length > 1) {
            sortColumns[0].unsort();
            sortColumns.shift();
        }
        else {
            var sorting = {by: 'createdAt', at: 'desc'};
            if (sortColumns.length != 0 && sortColumns[0].sort.direction) {
                var sortColumn = sortColumns[0];
                sorting.by = sortColumn.field;
                sorting.at = sortColumn.sort.direction;
            }

            vm.meta = angular.extend({}, vm.meta, sorting);
            vm.read();
        }
    }

    //default on success handler
    function _onSuccess(fname) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);

            //close all modals on update
            $scope.modalInstance.close();
        }
    }
$rootScope['currentAdmin']
    //default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }
    }


    //run
    vm.readItem();

    $rootScope.$broadcast('actions.read', {
        entityId  : _id,
        entityType: 'ADVERTISING'
    });
}
