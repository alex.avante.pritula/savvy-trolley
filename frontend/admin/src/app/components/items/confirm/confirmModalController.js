angular.module('app').controller('confirmModalController', ['$scope', '$uibModalInstance', confirmModalController ]);

function confirmModalController($scope, $uibModalInstance) {

    $scope.ok = function(status) {
        $uibModalInstance.close(status);
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
}