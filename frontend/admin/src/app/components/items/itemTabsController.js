angular.module('app').controller('ItemTabsController', ['$scope','$rootScope', '$uibModal', '$state', '$log',
    ItemTabsController
]);

/**
 *ItemTabsController
 * @param $scope
 * @param $rootScope
 * @param $uibModa
 * @param $state
 * @param $log 
 * @constructor
 */


function ItemTabsController($scope, $rootScope, $uibModal, $state, $log ) {
    var vm = angular.extend(this, { deselect: deselect });
    var _itemId = parseInt($state.params.itemId, 10) || null;
    var _shopId = parseInt($state.params.shopId, 10) || null;

    vm.isView = $state.includes('home.shops.itemView');
    vm.activeTab = parseInt($state.params.tabIndex, 10) || 0;
    vm.isUnsaved = false;
    
    function deselect($event){
        if(vm.isUnsaved){
            $event.preventDefault();

            $scope.modalInstance = $uibModal.open({
                templateUrl: 'app/components/items/confirm/view.html',
                controller: 'confirmModalController',
                scope: $scope,
                size: 'xs'
            });

            $scope.modalInstance.result.then(function (status) {
                var newTab = (+!vm.activeTab);
                if(status){
                    $rootScope.$broadcast('item.' + (vm.activeTab ? 'shop.': '') + 'write', newTab);
                    return;  
                }
                vm.activeTab = newTab;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });

        }
    }

   $scope.$watch('vm.activeTab', function (newValue, oldValue) {
        if (newValue !== oldValue) {
            vm.isUnsaved = false;
            $state.go('^.itemView', {
                itemId: _itemId,
                shopId: _shopId,
                tabIndex: newValue
            });
          }
    });

    $rootScope.$on('item.save', function (e, data) {
        if(data['tabIndex'] === vm.activeTab){
            vm.isUnsaved = data.status;
        }
    });
}
