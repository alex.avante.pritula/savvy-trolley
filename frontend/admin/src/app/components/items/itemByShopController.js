angular.module('app').controller('ItemByShopController', ['$scope', '$rootScope', '$state', '$log', 'toaster', 'Shops', '$uibModal', 'MONTH_DATE_RANGE_LIMITS',
    ItemByShopController
]);

/**
 * ItemByShopController
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param $log
 * @param toaster
 * @constructor
 */
function ItemByShopController($scope, $rootScope, $state, $log, toaster, Shops, $uibModal, MONTH_DATE_RANGE_LIMITS) {

    var vm = angular.extend(this, {
        read: read,
        save: save,
        back: back,
        update: update,
        create: create,
        readShopAdditionalData: readShopAdditionalData,
        isSaleChanged: isSaleChanged,
        setDateRangeByFrom: setDateRangeByFrom,
        setDateRangeByTo: setDateRangeByTo,
        calculateUnitPrice: calculateUnitPrice,
        calculateSalePrice: calculateSalePrice
    });

    var _itemId   = parseInt($state.params.itemId, 10) || null;
    var _shopId   = parseInt($state.params.shopId, 10) || null;
    var _tabIndex = parseInt($state.params.tabIndex, 10) || 0;

    vm.meta = { exist: false };

    vm.shop = {
        hasSales: false,
        hasDateRangeLimit: false,
        contactEmail: ""
    };
   
    vm.isSaleToChoice = false;
    vm.pusherMarkers = {};

    vm.configModals = {
        opened: {},
        options: {
            showWeeks: true
        },
        open: function (num) {
            this.opened[num] = true;
        },
        fromOptions: {
            minDate: new Date()
        },
        toOptions: {

        },
        format: 'dd.MM.yyyy',
        altInputFormats: 'M!/d!/yyyy'
    };

    function setDateRangeByFrom(from) {
        var fromDate = new Date(from);
        var toDate = new Date(vm.item.to);
        if (toDate < fromDate){
            vm.item.to = fromDate;
        }

        if (vm.shop.hasDateRangeLimit){
            var maxDate = new Date(fromDate);
            maxDate.setMonth(maxDate.getMonth() + MONTH_DATE_RANGE_LIMITS['SALES']);
            vm.configModals.toOptions._maxDate = maxDate;
            if(maxDate < vm.item.to) {
                 vm.item.to = maxDate;
            }
        }
        vm.configModals.toOptions.minDate = fromDate;
    };

    function setDateRangeByTo(to) {
        if(vm.shop.hasDateRangeLimit) {
            var toDate = new Date(to);
            var maxDate = new Date(vm.configModals.toOptions._maxDate);

            if (maxDate < toDate) {
                vm.item.to = maxDate;
                var msg = "You can only set a sale for a maximum of 3 months from the start date. Please ensure the end date is 3 months or less than the start date";
                toaster.pop('error', msg);
            }
        }
    };

    /** Receive additional shop data
     *
     */
    function readShopAdditionalData(){
        var fname = 'Read shop additional data';
        var params =  { id: _shopId || null };

        Shops.read(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.shop.hasSales = res.hasSales;
                vm.shop.contactEmail = res.contactEmail;
                read();
            }
        }

         /**
         * Custom error handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onError(fname) {
            return function _onError(res) {
                $log.info(fname + ' error: ', arguments);
                StatusHandler(res.status, res.statusText, {type: 'error'});
                if(res.status == 404)  $state.go('home.shops.list');
            }
        }

    }

    function calculateUnitPrice() {
        var inc = 1;
        var unitPrice = null;
        var visualUnitPrice =  '';
        var type = vm.item.unitType;
        var itemSize = parseFloat(vm.item.itemSize);        
        var regularPrice = parseFloat(vm.item.visualRegularPrice);

        if(type === 'ml' || type === 'gr'){
            inc = 100;
        }

        if(type === 'ltr'){
            inc = 0.1;
            type = 'ml';
        }

        if(type === 'kg'){
            inc = 0.1;
            type = 'gr'; 
        }
        
        if (type !== 'pc') {
            type = '100' + type
       
         }

        if(regularPrice && itemSize){
            unitPrice = regularPrice / itemSize * inc;
            visualUnitPrice = (Math.ceil(unitPrice * 100) / 100).toFixed(2) + '/' +   type;
        }

        vm.item = Object.assign({}, vm.item, { 
            unitPrice: unitPrice,
            regularPrice: regularPrice,
            visualUnitPrice: visualUnitPrice
        });
    }


    function calculateSalePrice(){
        vm.item = Object.assign({}, vm.item, { 
           salePrice: parseFloat(vm.item.visualSalePrice)
        });
    }

    function saleDateRangeLimit() {
        var fname = 'Shop Owners read';
        var params =  { id: _shopId || null };

        Shops.owners(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);

                vm.shop.hasDateRangeLimit = res['owners'].findIndex(function(el){
                    return el.role === 'ENTERPRISE_SAVVY_SHOP'
                }) == -1;

                if(vm.shop.hasDateRangeLimit){
                    var maxDate = new Date();
                    var fromDate = (vm.meta.exist ? new Date(vm.item.from) : new Date());
                    if(fromDate > maxDate) maxDate = fromDate;
                    maxDate.setMonth(maxDate.getMonth() + MONTH_DATE_RANGE_LIMITS['SALES']);
                    vm['configModals'].toOptions['_maxDate'] = maxDate;
                }
            }
        }
    }


    /**
     * Receive one or list of items
     */
    function read() {
        var fname = 'Read shop item';
        var params = angular.extend(vm.meta, {
            id: _shopId || null,
            itemId: _itemId || null
        });

        vm.item = {};
        vm.meta.exist = false;

        Shops.items(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);

                var visualUnitPrice = '';
                var type = res.unitType;

                if(type === 'ltr'){
                    type = 'ml';
                }

                if(type === 'kg'){
                    type = 'gr'; 
                }

                if (type !== 'pc') {
                    type = '100' + type
                }

                if(res.unitPrice){
                    visualUnitPrice = res.unitPrice.toFixed(2) + '/' + type;
                }

                vm.item =  Object.assign({
                   visualSalePrice: (res.salePrice ? res.salePrice.toFixed(2): '') ,
                   visualRegularPrice: (res.regularPrice ? res.regularPrice.toFixed(2): '') ,
                   visualUnitPrice: visualUnitPrice
                }, res);

                vm.meta.exist = res.exist;

                if(vm.meta.exist) {
                    vm.configModals.toOptions.minDate = new Date(res.from);

                    $rootScope.$broadcast('actions.read', {
                        entityId: vm.item.productId,
                        entityType: 'ITEM_BY_SHOP'
                    });

                    var isSavvyShop = ($rootScope['currentAdmin'].role === 'SAVVY_SHOP');
                    var from = new Date(res.from);
                    var to = new Date(res.to);

                    if (isSavvyShop && !vm.shop.hasSales) {
                        vm.isSaleToChoice = false;
                    } else {
                        var controlDate = new Date();
                        var tempDate = new Date(to);
                        controlDate.setHours(0, 0, 0, 0);
                        tempDate.setHours(0, 0, 0, 0);

                        if (tempDate < controlDate) {
                            vm.isSaleToChoice = false;
                        } else {
                            vm.isSaleToChoice = vm.item.isSale;
                        }
                    }

                    vm.pusherMarkers = {
                        regularPrice: res.regularPrice,
                        isOutOfStock: res.isOutOfStock,
                        isSale: vm.isSaleToChoice,
                        from: from.getTime(),
                        to: to.getTime()
                    };

                    if (isSavvyShop) {
                        saleDateRangeLimit();
                    }
                }
            }
        }

         /**
         * Custom error handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onError(fname) {
            return function _onError(res) {
                $log.info(fname + ' error: ', arguments);
                StatusHandler(res.status, res.statusText, { type: 'error' });

                var isSavvyShop = ($rootScope['currentAdmin'].role == "SAVVY_SHOP");
                vm.isSaleToChoice = !(isSavvyShop && !vm.shop.hasSales);
                if(isSavvyShop) saleDateRangeLimit();
                //if (res.status == 404) back();
            }
        }
    }

    /**
     * Create item model
     * @param isValid
     * @param data
     */
    function create(isValid, data) {
        var fname = 'Create shop item';
        $log.debug(fname, arguments);
        $scope.submitted = true;

        if (isValid) {
            var params = {
                id:           _shopId,
                itemId:       _itemId,
                regularPrice: data.regularPrice,
                unitPrice:    data.unitPrice,
                salePrice:    data.salePrice,
                isSale:       vm.isSaleToChoice,
                isOutOfStock: data.isOutOfStock || false,
                from:         data.from ? new Date(data.from).getTime() : 0,
                to:           data.to ? new Date(data.from).getTime() : 0,
                deliveryDateTime: {}
            };

            var pusherItems = [];
            if(params.isOutOfStock){
                pusherItems.push({
                    type: "ITEM_OUT_OF_STOCK",
                    onlyTime : true,
                    name: vm.item.name
                });
            }

            if(params.isSale){
                if(vm.pusherMarkers.from != params.from){
                    pusherItems.push({
                        type: "SALE_START",
                        onlyTime: true,
                        name: vm.item.name,
                        dateTime: new Date(data.from)
                    });
                }

                if(vm.pusherMarkers.to != params.to){
                    pusherItems.push({
                        type: "SALE_FINISH",
                        onlyTime: true,
                        name: vm.item.name,
                        dateTime: new Date(data.to)
                    });
                }
            }

            if(!pusherItems.length){
                Shops.itemsAdd(params).then(_onSuccess(fname, params), _onError(fname));
                return;
            }

            $scope.modalInstance = $uibModal.open({
                templateUrl: 'app/components/pusher/view.html',
                controller : 'pusherModalCtrl',
                scope      : $scope,
                size       : 'xs',
                resolve    : {
                    items: function(){
                        return pusherItems;
                    }
                }
            });

            $scope.modalInstance.result.then(function (deliveryDateTime) {
                $log.info('modalInstance submit', arguments);

                deliveryDateTime.forEach(function(item){
                    params.deliveryDateTime[item.type] = item.dateTime;
                });
                Shops.itemsAdd(params).then(_onSuccess(fname, params), _onError(fname));
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }

    }

    function isSaleChanged(){
        if($rootScope['currentAdmin'].role == 'SAVVY_SHOP' && !vm.shop.hasSales){
            var msg =  "Sale privileges have been disabled for your account. This typically occurs when there is an issue with your billing. For more information, please email:\n" + vm.contactEmail;
            toaster.pop('error', msg);
            vm.isSaleToChoice = false;
        } else {
            vm.isSaleToChoice = !vm.isSaleToChoice;
            $scope.choiceForm.$setDirty();
            vm.item.isSale = vm.isSaleToChoice;
        }
    }

    function update(isValid, data, newTab) {

        var fname = 'Edit shop item';
        $log.debug(fname, arguments);
        $scope.submitted = true;

        if (isValid) {
            var params = {
                id: _shopId,
                itemId: _itemId,
                regularPrice: data.regularPrice,
                unitPrice: data.unitPrice,
                salePrice: data.salePrice || 0,
                isSale: data.isSale || false,
                from: data.from ? new Date(data.from).getTime() : 0,
                to:  data.to ? new Date(data.to).getTime() : 0,
                isOutOfStock: data.isOutOfStock || false,
                deliveryDateTime: {}
            };

            var pusherItems = [];

            if(!vm.pusherMarkers.isOutOfStock && params.isOutOfStock){
                pusherItems.push({
                    type: "ITEM_OUT_OF_STOCK",
                    name: vm.item.name,
                    onlyTime: true
                });
            }

            if(params.isSale){
                if(vm.pusherMarkers.from != params.from){
                    pusherItems.push({
                        type: "SALE_START",
                        onlyTime: true,
                        name: vm.item.name,
                        dateTime: new Date(data.from)
                    });
                }

                if(vm.pusherMarkers.to != params.to){
                    pusherItems.push({
                        type: "SALE_FINISH",
                        onlyTime: true,
                        name: vm.item.name,
                        dateTime: new Date(data.to)
                    });
                }
            }

            if(vm.pusherMarkers.regularPrice > params.regularPrice){
                pusherItems.push({
                    type: "REGULAR_PRICE_REDUCED",
                    onlyTime: true,
                    name: vm.item.name
                });
            }

            if(!pusherItems.length){
                Shops.itemsUpdate(params).then(_onSuccess(fname), _onError(fname));
                return;
            }

            $scope.modalInstance = $uibModal.open({
                templateUrl: 'app/components/pusher/view.html',
                controller : 'pusherModalCtrl',
                scope      : $scope,
                size       : 'xs',
                resolve    : {
                    items: function(){
                        return pusherItems;
                    }
                }
            });

            $scope.modalInstance.result.then(function (deliveryDateTime) {
                $log.info('modalInstance submit', arguments);

                deliveryDateTime.forEach(function(item){
                    params.deliveryDateTime[item.type] = item.dateTime;
                });

                Shops.itemsUpdate(params).then(_onSuccess(fname), _onError(fname));

            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });

            function _onSuccess(fname) {
                return function _onSuccess(res) {

                    var msg = fname + ' success!';
                    $log.debug(msg, arguments);
                    toaster.pop('info', msg);
                    $scope.submitted = false;

                    $rootScope.$broadcast('actions.write', {
                        entityId: res.id || params.id,
                        entityType: 'ITEM_BY_SHOP',
                        operationType: fname.split(' ')[0].toUpperCase(),
                        payload: JSON.stringify(params)
                    });

                    $state.go('^.itemView', {
                        itemId: _itemId,
                        shopId: _shopId,
                        tabIndex: ((newTab != null) ? newTab: _tabIndex)
                    },{ reload: true });
                }
            }

        }
    }

    function save(isValid, data) {
        if (vm.meta.exist) update(isValid, data);
        else create(isValid, data);
    }

    function back() {
        $state.go('home.shops.view', { id: _shopId });
    }
    
    function _onSuccess(fname, params) {
        return function _onSuccess(res) {

            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);
            $scope.submitted = false;

            $rootScope.$broadcast('actions.write', {
                entityId: res.id || params.id,
                entityType: 'ITEM_BY_SHOP',
                operationType: fname.split(' ')[0].toUpperCase(),
                payload: JSON.stringify(params)
            });

            $state.go('^.itemView', {
                itemId: _itemId,
                shopId: _shopId,
                tabIndex: _tabIndex
            },{ reload: true });
        }
    }

    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            var msg = 'Regular prices can only be updated once a month on your current plan. You can always advertise a Sale price!';
            if(status == 403) toaster.pop('error', msg);
            else StatusHandler(res.status, res.statusText, { type: 'error' });
        }
    }

    if(_tabIndex !== null){
        $scope.$watch('choiceForm.$dirty', function (newValue, oldValue) {
            if (newValue !==  oldValue) {
                $rootScope.$broadcast('item.save', {
                    tabIndex: _tabIndex,
                    status: newValue
                });
            }
        });

        $rootScope.$on('item.shop.write', function (e, newTab) {
            console.warn(choiceForm);
            vm.update($scope.choiceForm.$valid, vm.item, newTab);
        });
    }

    readShopAdditionalData();
}
