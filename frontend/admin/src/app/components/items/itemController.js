angular.module('app').controller('ItemController', ['$scope', '$rootScope', '$state', '$log', 'toaster', 'Items','Categories', 'Uploads', 'StatusHandler', 'UNIT_TYPES',
    ItemController
]);

/**
 * ItemController
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param $log
 * @param toaster
 * @param Items
 * @param Categories
 * @param Uploads
 * @param StatusHandler
 * @param UNIT_TYPES
 * @constructor
 */
function ItemController($scope, $rootScope, $state, $log, toaster, Items, Categories, Uploads, StatusHandler, UNIT_TYPES) {

    var vm = angular.extend(this, {
        create: create,
        read: read,
        update: update,
        check: check,
        save: save,
        back: back,
        changeCategory: changeCategory,
        changeSubcategory: changeSubcategory
    });

    var _itemId = parseInt($state.params.itemId, 10)   || null;
    var _shopId = parseInt($state.params.shopId, 10)   || null;
    var _tabIndex = parseInt($state.params.tabIndex, 10) || 0;

    vm.UNIT_TYPES = UNIT_TYPES;

    vm.item = { categories: [] };

    vm.category = {};
    vm.categories = [];
    vm.subcategories = [];

    vm.meta = {
        exist: !!_itemId,
        checking: false
    };

    function back() {
        if (_shopId != null) {
            return $state.go('home.shops.view', { id: _shopId });
        }
        return $state.go('home.items.list');
    }

    function changeCategory(query) {
        var fname = 'Read categories';
        var params = { query: query };
        $log.debug(fname, arguments);

        Categories.read(params).then(_onSuccess(fname), _onError(fname));

        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                vm.categories = res.categories;
            }
        }
    }

    /**
     * Receive list of subcategories by query for current category
     * @param query
     */
    function changeSubcategory(query) {
        var fname = 'Read subcategories';
        var params = {query: query, id: vm.category.id};
        $log.debug(fname, arguments);

        Categories.subcategories(params).then(_onSuccess(fname), _onError(fname));

        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                var categories = vm.item.categories;
                vm.subcategories = _.filter(res.categories, function (subcategory) {
                    return !_.find(categories, function (category) {
                        return subcategory.id == category.id;
                    });
                });
            }
        }
    }

    /**
     * Receive one or list of items
     */
    function read() {
        var fname = 'Read items';
        var params = {
            id: _itemId || null
        };

        Items.read(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.item = res;
                vm.meta.exist = true;
            }
        }

        /**
        * Custom error handler
        * @override
        * @param fname
        * @returns {Function}
        * @private
        */
        function _onError(fname) {
            return function _onError(res) {
                $log.info(fname + ' error: ', arguments);
                StatusHandler(res.status, res.statusText, {type: 'error'});
                if (res.status == 404) {
                    if(_shopId) $state.go('home.shops.view', { id: _shopId });
                    else back();
                }
            }
        }
    }

    /**
     * Create or update item model
     * @param isValid
     * @param data
     */
    function save(isValid, data, newTab) {

        var temp = angular.copy(data);

        temp.unitType = vm.UNIT_TYPES[temp.unitType];
        temp.servingType = vm.UNIT_TYPES[temp.servingType];

        _.forOwn(temp, function (value, key) {
            if (value === null) {
                delete temp[key];
            }
        });

        if (!vm.meta.exist) {
            return create(isValid, temp, newTab);
        }

        update(isValid, temp, newTab);
    }

    /**
     * Create item model
     * @param isValid
     * @param data
     */
    function create(isValid, data) {
        var fname = 'Create item';
        $log.debug(fname, arguments);
        $scope.submitted = true;

        if (isValid) {
            Items.create(data).then(_onSuccess(fname), _onError(fname));
        }

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                toaster.pop('info',fname + ' success: ');
                vm.item = res;
                $scope.submitted = false;

                $rootScope.$broadcast('actions.write', {
                    entityId: res.id,
                    entityType: 'ITEM',
                    operationType: fname.split(' ')[0].toUpperCase()
                });

                var params = { itemId   : vm.item.id || _itemId };

                if(_shopId){
                    $state.go('^.itemView', Object.assign({
                      shopId   : _shopId,
                      tabIndex : 1
                    }, params));
                } else {
                    $state.go('^.view', params);
                }
            }
        }
    }

    /**
     * Update item model
     * @param isValid
     * @param data
     */
    function update(isValid, data, newTab) {
        var fname = 'Edit item';
        $log.debug(fname, arguments);
        $scope.submitted = true;
        if (isValid) {
            Items.update(data).then(_onSuccess(fname), _onError(fname));
        }

        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);
                $scope.submitted = false;

                $rootScope.$broadcast('actions.write', {
                    entityId     : res.id,
                    entityType   : 'ITEM',
                    operationType: fname.split(' ')[0].toUpperCase()
                });

                var params = { itemId:  _itemId };

                if(_shopId){
                    console.warn(newTab, "@222222222222222222222");
                    $state.go('^.itemView', Object.assign({
                        shopId: _shopId,
                        tabIndex: ((newTab !== null) ? newTab: _tabIndex)
                    }, params), { reload: true });
                } else {
                    $state.go('^.view', params, { reload: true });
                }
            }
        }
    }

    /**
     * Check item exist
     * @param isValid
     * @param code
     */
    function check(isValid, code) {
        var fname = 'Check bar code';

        if (isValid && !vm.meta.exist) {
            var params = {
                query: code
            };

            vm.meta.exist = false;
            vm.meta.checking = true;

            Items.read(params).then(_onSuccess(fname), _onError(fname))
        }

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                
                $log.debug(msg, arguments);
                vm.meta.checking = false;

                if (res.items.length > 0) {
                    var params = { itemId: res.items[0].id };

                    if(_shopId){
                        $state.go('^.itemView', Object.assign({
                          shopId: _shopId,
                          tabIndex: 0
                        }, params),{ reload: true });
                    } else {
                        $state.go('^.view', params,{ reload: true });
                    }
                }
            }
        }
    }

    //helpers

    //default on success handler
    function _onSuccess(fname, newTab) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);
            $scope.submitted = false;

            $rootScope.$broadcast('actions.write', {
                entityId     : res.id,
                entityType   : 'ITEM',
                operationType: fname.split(' ')[0].toUpperCase()
            });

            var params = { itemId:  _itemId };

            if(_shopId){
                $state.go('^.itemView', Object.assign({
                    shopId: _shopId,
                    tabIndex: 1
                }, params),{ reload: true });
            } else {
                $state.go('^.view', params,{ reload: true });
            }
        }
    }

    //default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }
    }

    /**
     * Upload image handler
     * @returns {*}
     */
    function imageScope() {
        //Image upload watcher
        return $scope.$watch('image', function (newValue) {
            if (newValue) {
                Uploads.image($scope.image).then(_onSuccess, _onError, _onProgress);
                $scope.inProgress = Uploads.isUploadInProgress();
            }
        });

        //upload handlers
        function _onSuccess(res) {
            var msg = 'Image uploaded!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);

            vm.item.image = res.data.images[0];
            vm.item.imageId = res.data.images[0].id;
            $scope.inProgress = Uploads.isUploadInProgress();
        }

        function _onError(res) {
            $log.info('Image upload error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }

        function _onProgress(evt) {
            console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :' + evt.config.data);
        }
    }

    if (_itemId) {
        vm.read();

        $rootScope.$broadcast('actions.read', {
            entityId: _itemId,
            entityType: 'ITEM'
        });
    }
    
    if(_tabIndex !== null){
        $scope.$watch('itemForm.$dirty', function (newValue, oldValue) {
            if (newValue !== oldValue) {
                $rootScope.$broadcast('item.save', {
                    tabIndex: _tabIndex,
                    status: newValue
                });
            }
        });

       $rootScope.$on('item.write', function (e, newTab) {
          vm.save($scope.itemForm.$valid, vm.item, newTab);
       });
    }

    imageScope();
    changeCategory();
}
