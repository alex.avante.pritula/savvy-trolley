angular.module('app').controller('ItemsController', ['$scope', '$rootScope', '$state', '$log', 'toaster', 'Items', 'BulkUploadService', 'StatusHandler', '$uibModal' , '$q',  'LIMITS', 'BASE_URL', 'BULK_TEMPLATE_PATH', 'DEFAULT_IMAGE',
    ItemsController
]);

/**
 * ItemsController
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param $log
 * @param toaster
 * @param Items
 * @param BASE_URL
 * @constructor
 */
function ItemsController($scope, $rootScope, $state, $log, toaster, Items, BulkUploadService, StatusHandler, $uibModal, $q, LIMITS, BASE_URL, BULK_TEMPLATE_PATH, DEFAULT_IMAGE) {

    var vm = angular.extend(this, {
        read: read,
        remove: remove,
        bulk: bulk,
        bulkInfo: bulkInfo,
        changeEnableMode: changeEnableMode
    });

    vm.template = BASE_URL + BULK_TEMPLATE_PATH;
    vm.noImage = BASE_URL + DEFAULT_IMAGE;

    vm.file = null;
    vm.report = null;

    $scope.uploadSubmitted = false;

    //meta dummy
    vm.meta = {
        query: $state.params.query || '',
        limit: $state.params.limit || 10,
        page: parseInt($state.params.pageNum, 10) || 1,
        totalCount: 0
    };

    vm.sorting = {
        by: $state.params.sortField || 'createdAt',
        at: $state.params.sortDirection || 'desc'
    };

    var fakeI18n = function( title ){
        var deferred = $q.defer();
        return deferred.promise;
    };

    vm.gridOptions = {
        enableHorizontalScrollbar: 0,
        enableVerticalScrollbar: 0,
        paginationPageSize: vm.meta.limit,
        paginationCurrentPage: vm.meta.page,
        useExternalSorting: true,
        rowHeight: 34,
        //enableGridMenu: true,
        //gridMenuTitleFilter: fakeI18n,
        enableMinHeightCheck: true,
        columnDefs: [
            {
                field: 'image',
                displayName: 'Image',
                width: '6%',
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                cellClass: 'text-center',
                cellTemplate: '<img src="{{row.entity.image ?  row.entity.image : grid.appScope.vm.noImage}}" alt="{{row.entity.name}}" width="30" height="30"/>'
            },
            {
                field: 'name',
                displayName: 'Name',
                width: '*',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'name' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'brand',
                name: 'Brand',
                width: '*',
                enableFiltering: false,
                enableHiding: true,
                sort: $state.params.sortField == 'brand' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'barCode',
                name: 'BarCode',
                width: '*',
                enableFiltering: false,
                enableHiding: true,
                sort: $state.params.sortField == 'barCode' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'createdAt',
                displayName: 'Created At',
                width: '*',
                type: 'date',
                cellFilter: 'date: "dd MMM yyyy, hh:mm:ss a"',
                enableFiltering: false,
                enableHiding: true,
                sort: $state.params.sortField == 'createdAt' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'updatedAt',
                displayName: 'Updated At',
                width: '*',
                type: 'date',
                cellFilter: 'date: "dd MMM yyyy, hh:mm:ss a"',
                enableFiltering: false,
                enableHiding: true,
                sort: $state.params.sortField == 'updatedAt' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                name: 'details',
                displayName: '',
                cellClass: 'text-center grid-nav-button',
                width: "8%",
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                visible: $rootScope.checkPermission['REVIEW_ITEM'] || false,
                cellTemplate: [
                    '<a class="btn btn-sm btn-info"',
                    'ui-sref="home.items.view({ itemId: row.entity.id })">',
                    '<i class="fa fa-info"></i>',
                    '<span>Manage</span>',
                    '</a>'
                ].join(' ')
            },
            {
                name: 'delete',
                displayName: '',
                cellClass: 'text-center grid-nav-button',
                width: "8%",
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                visible: $rootScope.checkPermission['EDIT_ITEM'] || false,
                cellTemplate: [
                    '<button class="btn btn-sm btn-danger" ng-disabled="row.entity.viewOnly" ng-click="grid.appScope.vm.remove(row.entity.id)">',
                    '<i class="fa fa-trash"></i>',
                    '<span>Delete</span>',
                    '</button>'
                ].join(' ')
            },
            {
                name: 'enabled',
                displayName: 'Enabled',
                headerCellClass: 'text-center',
                cellClass: 'text-center grid-nav-button',
                width: "7%",
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                visible: $rootScope.checkPermission['EDIT_ITEM'] || false,
                cellTemplate: [
                    '<button class="btn btn-sm btn-warning" ng-disabled="row.entity.viewOnly" ng-click="grid.appScope.vm.changeEnableMode(row.entity)">',
                    '<i class="fa" ng-class="row.entity.enabled ? \'fa-check\' : \'fa-remove\'"></i>',
                    '<span>{{row.entity.enabled ? "Yes" : "No"}}</span>',
                    '</button>'
                ].join(' ')
            }
        ],
        onRegisterApi: function (gridApi) {
            vm.gridApi = gridApi;
            vm.gridApi.core.on.sortChanged($scope, sort);
        }
    };

    vm.limits = LIMITS;

    var _id = $state.params.id || null;
    var page = $state.params.pageNum || 1;

    /**
     * Receive one or list of items
     * @param options
     */
    function read(options) {
        var fname = 'Read items';
        var params = angular.extend({}, vm.sorting, vm.meta, options || {
            id: _id || null
        });
       
        Items.read(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.gridOptions.data = res.items;
                vm.gridOptions.minRowsToShow = res.items.length;
                $scope.height = (vm.gridOptions.data ? vm.gridOptions.data.length + 1 : 1) * 34;
                angular.extend(vm.meta, res.meta);
                $state.go('home.items.list', {
                    pageNum: vm.meta.page,
                    limit: vm.meta.limit,
                    sortField: vm.sorting.by,
                    sortDirection: vm.sorting.at,
                    query: vm.meta.query
                });
            }
        }
    }


    function bulkInfo(){
        alert([
        'A xlsx template file is located below each bulk upload option. Download,',
        'populate, and upload by using the correct choice. You can rename the file',
        'name to suit your needs'
      ].join(' '));
    }
    
    /**
     * change item enable mode
     * @param item
     */
    function changeEnableMode(item) {
        var fname = 'Edit item';
        $log.debug(fname, arguments);
        $scope.submitted = true;

        var data  = {
            id: item.id,
            enabled: !item.enabled
        };

        //if(!item.enabled){
            Items.enable(data).then(_onSuccess(fname, item.id), _onError(fname));
        //    return;
       // }

       /*$scope.modalInstance = $uibModal.open({
            templateUrl: 'app/components/pusher/view.html',
            controller : 'pusherModalCtrl',
            scope      : $scope,
            size       : 'xs',
            resolve    : {
                items: function(){
                    return [{
                        type: "ITEM_ENABLED_MODE_CHANGED",
                        onlyTime : true,
                        name: item.name
                    }];
                }
            }
        });

        $scope.modalInstance.result.then(function (deliveryDateTime) {
            $log.info('modalInstance submit', arguments);
            if(deliveryDateTime.length) data.deliveryDateTime = deliveryDateTime[0].dateTime;
            Items.enable(data).then(_onSuccess(fname, item.id), _onError(fname));
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });*/
    }

    /**
     * Remove item model
     * @param id
     */
    function remove(id) {
        var fname = 'Delete item';
        $log.debug(fname, arguments);
        var confirmed = confirm('Are you sure you want to delete this item?');

        if (confirmed) {
            Items.remove({id: id}).then(_onSuccess(fname, id), _onError(fname));
        }
    }

    /**
     * Remove items by bulk upload
     * @param file
     */

    function bulk(file, type){
        var fname = 'Upload items from file: ' + file.name;
        $log.debug(fname, arguments);
        $scope.uploadSubmitted = true;
        BulkUploadService.upload(file, 'items'+ type).then(_onSuccess(fname), _onError(fname), _onProgress);

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname){
            return function(res){
                vm.report = res.data.url;
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);
                $scope.uploadSubmitted = false;
                
                $rootScope.$broadcast('actions.write', {
                    entityId:      res.data.id,
                    entityType:    'REPORT',
                    operationType: 'CREATE'
                });

                $scope.$applyAsync(function(){  vm.read(); }); 
            }
        }

        /**
         * Custom error handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onError(fname) {
            return function(res) {
                vm.report = null;
                $scope.uploadSubmitted = false;
                $log.info(fname + ' error: ', arguments);
                StatusHandler(res.status, res.statusText, {type: 'error'});
            }
        }

        function _onProgress(evt) {
            console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :' + evt.config.data);
        }
    }

    function sort(grid, sortColumns) {
        sortColumns = _.sortBy(sortColumns, function (v) {
            return v.sort.priority;
        });

        if (sortColumns.length > 1) {
            sortColumns[0].unsort();
            sortColumns.shift();
        } else {
            var sorting = {by: 'id', at: 'asc'};
            if (sortColumns.length != 0 && sortColumns[0].sort.direction) {
                var sortColumn = sortColumns[0];
                sorting.by = sortColumn.field;
                sorting.at = sortColumn.sort.direction;
            }

            vm.sorting = sorting;
            vm.read();
        }
    }

    //default on success handler
    function _onSuccess(fname, id) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);
            $scope.submitted = false;

            $rootScope.$broadcast('actions.write', {
                entityId: res.id || id,
                entityType: 'ITEM',
                operationType: fname.split(' ')[0].toUpperCase()
            });
            vm.read();
        }
    }

    //default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }
    }

    vm.read({page: page});

    $rootScope.$broadcast('actions.read', { entityType: 'ITEM' });

    $scope.height = (vm.gridOptions.data ? vm.gridOptions.data.length + 1 : 1) * 34;

}
