angular.module('app').controller('DashboardController', ['$scope', '$rootScope', '$state', '$log', 'toaster', 'Users', 'Settings',
    DashboardController
]);

/**
 * DashboardController
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param $log
 * @param toaster
 * @constructor
 */
function DashboardController($scope, $rootScope, $state, $log, toaster, Users, Settings) {
    var vm = angular.extend(this, {
        changeAdverts: changeAdverts,
        readSettings : readSettings
    });

    $log.info('DashboardController');

    vm.hasAdverts = false;

    function changeAdverts() {
        var fname = 'Edit advert to user';
        var params = {
            hasAdverts: !vm.hasAdverts
        };
        var flag = params.hasAdverts;

        Users.changeAdverts(params).then(_onSuccess(fname, flag), _onError(fname));

        //default on success handler
        function _onSuccess(fname, flag) {
            return function _onSuccess(res) {
                //var msg = fname + ' success!';
                var msg = flag ? 'Adwords enabled!' : 'Adwords disabled!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);

                vm.readSettings();

                $rootScope.$broadcast('actions.write', {
                    entityId     : 0,
                    entityType   : 'ADVERTISING',
                    operationType: fname.split(' ')[0].toUpperCase()
                });
            };
        }
    }

    function readSettings() {
        var fname = 'Read advert settings to user';
        Settings.read().then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.hasAdverts = res.hasAdverts;
            };
        }
    }


    //default on success handler
    function _onSuccess(fname) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);

        };
    }

    //default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        };
    }

    vm.readSettings();
    $rootScope.$broadcast('actions.read', {
        entityId  : 0,
        entityType: 'ADVERTISING'
    });
}
