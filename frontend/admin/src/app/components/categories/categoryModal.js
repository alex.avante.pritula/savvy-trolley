angular.module('app').controller('categoryModalCtrl',
    ['$scope', '$log', '$uibModalInstance', 'toaster', 'Uploads', 'item', 'parent',
        categoryModalCtrl
    ]);


function categoryModalCtrl($scope, $log, $uibModalInstance, toaster, Uploads, item, parent) {
    $scope.item = item;
    $scope.parent = parent;
    $scope.item.parentId = $scope.parent ? $scope.parent.id : null;

    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    //Image upload watcher
    $scope.$watch('image', function (newValue) {
        if (newValue) {
            Uploads.image($scope.image)
                .then(_onSuccess, _onError, _onProgress);

            $scope.inProgress = Uploads.isUploadInProgress();
        }
    });

    //upload handlers
    function _onSuccess(res) {
        var msg = 'Image uploaded!';
        $log.debug(msg, arguments);
        toaster.pop('info', msg);

        $scope.item.image = res.data.images[0];
        $scope.item.imageId = res.data.images[0].id;
        $scope.inProgress = Uploads.isUploadInProgress();
    }

    function _onError(res) {
        $log.info('Image upload error: ', arguments);
        StatusHandler(res.status, res.statusText, {type: 'error'});
    }

    function _onProgress(evt) {
        console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :' + evt.config.data);
    }

}