angular.module('app').controller('CategoriesController', ['$scope', '$rootScope', '$state', '$log', '$uibModal', 'toaster', 'Categories',
    CategoriesController
]);

/**
 * CategoriesController
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param $log
 * @param $uibModal
 * @param toaster
 * @param Categories
 * @constructor
 */
function CategoriesController($scope, $rootScope, $state, $log, $uibModal, toaster, Categories) {
    //var vm = this;
    var vm = angular.extend(this, {
        create           : create,
        read             : read,
        update           : update,
        remove           : remove,
        readSubcategories: readSubcategories,
        selectCategory   : selectCategory
    });

    //meta dummy
    vm.meta = {
        limit     : 10,
        page      : parseInt($state.params.pageNum, 10) || 1,
        totalCount: 0
    };
    $scope.submitted = false;
    vm.selectedCategory = null;
    vm.categories_meta = angular.extend({}, vm.meta);
    vm.subcategories_meta = angular.extend({}, vm.meta);
    vm.showSubcutegory = false;

    var _id = $state.params.id || null;
    var page = $state.params.pageNum || 1;

    if (!$state.includes('list')) {
        $state.go('home.categories.list', {pageNum: vm.meta.page}, {notify: false});
    }

    /**
     * Receive one or list of categories
     */
    function read() {
        var fname = 'Read categories';
        var params = angular.extend(vm.categories_meta, {
            id: _id || null
        });

        Categories.read(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.categories = res.categories;
                angular.extend(vm.categories_meta, res.meta);

                $state.go('home.categories.list', {pageNum: vm.categories_meta.page});
            }
        }
    }

    /**
     * Create category model
     * @param isValid
     * @param data
     */
    function create(isValid, data) {
        var fname = 'Create category';
        $log.debug(fname, arguments);
        $scope.submitted = true;
        if (isValid) {
            Categories.create(data).then(_onSuccess(fname), _onError(fname));
        }
    }

    /**
     * Update category model
     * @param isValid
     * @param data
     */
    function update(isValid, data) {
        var fname = 'Edit category';
        $log.debug(fname, arguments);
        $scope.submitted = true;
        if (isValid) {
            Categories.update(data).then(_onSuccess(fname), _onError(fname));
        }
    }

    /**
     * Remove category model
     * @param id
     */
    function remove(id) {
        var fname = 'Delete category';
        $log.debug(fname, arguments);
        var confirmed = confirm('Are you sure you want to delete this category?');
        if (confirmed) {
            Categories.remove({id: id}).then(_onSuccess(fname, id), _onError(fname));
        }
    }

    /**
     * Receive list of subcategories by category
     */
    function readSubcategories(categoryId) {
        var fname = 'Read subcategories';
        var params = angular.extend(vm.subcategories_meta, {
            id: categoryId || vm.selectedCategory.id || null
        });
        vm.showSubcutegory = false;

        Categories.subcategories(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.subcategories = res.categories;
                angular.extend(vm.subcategories_meta, res.meta);
                vm.showSubcutegory = true;
            }
        }
    }


    /**
     * Open modal according to type for resource with optional data
     * @param type
     * @param resource
     * @param data
     */
    vm.openModal = function (type, resource, data) {
        var tmpl = 'app/components/categories/' + resource + '/' + type + '.html';

        $scope.modalInstance = $uibModal.open({
            templateUrl: tmpl,
            controller : 'categoryModalCtrl',
            scope      : $scope,
            size       : 'lg',
            resolve    : {
                item  : angular.copy(data) || {},
                parent: resource === 'subcategory' ? vm.selectedCategory : null
            }
        });

        //
        $scope.modalInstance.result.then(function (results) {
            $log.info('modalInstance submit', arguments);

            $scope.submitted = false;

        }, function () {
            $log.info('Modal dismissed at: ' + new Date());

            $scope.submitted = false;
        });
    };


    //helpers

    //select current category
    function selectCategory(category) {
        vm.selectedCategory = category;
        if(category)
            vm.readSubcategories(category.id);
    }

    //default on success handler
    function _onSuccess(fname, id) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);
            $scope.submitted = false;

            $rootScope.$broadcast('actions.write', {
                entityId     : res.id || id,
                entityType   : 'CATEGORY',
                operationType: fname.split(' ')[0].toUpperCase()
            });

            //close all modals on update
            if ($scope.modalInstance) {
                $scope.modalInstance.close();
            }

            vm.read();
            if(vm.selectedCategory) {
                vm.readSubcategories(vm.selectedCategory.id);
            }

            // $state.transitionTo($state.current, {pageNum: vm.meta.page}, {notify: false, reload: true});
        }
    }

    //default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }
    }

    //run
    vm.read();
    $rootScope.$broadcast('actions.read', {
        entityType: 'CATEGORY'
    });
}