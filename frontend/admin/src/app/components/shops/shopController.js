angular.module('app').controller('ShopController', ['$scope', '$rootScope', '$state', '$log', 'toaster', 'Shops', 'Admins', 'Uploads', 'StatusHandler', 'LIMITS', 'isCreated', 'BulkUploadService', '$uibModal', 'BASE_URL', 'REPORT_TYPES','BULK_TEMPLATE_PATH', 'DEFAULT_IMAGE',
    ShopController
]);

function ShopController($scope, $rootScope, $state, $log, toaster, Shops, Admins, Uploads, StatusHandler, LIMITS, isCreated, BulkUploadService, $uibModal, BASE_URL, REPORT_TYPES, BULK_TEMPLATE_PATH, DEFAULT_IMAGE) {
   var vm = angular.extend(this, {
        create: create,
        read: read,
        update: update,
        remove: remove,
        owners: owners,

        readAdmins: readAdmins,
        readFree: readFree,

        readItems: readItems,
        updateItemEnablingMode: updateItemEnablingMode,
        removeItem: removeItem,
        
        setDateRangeByFrom: setDateRangeByFrom,

        addBlockingComment: addBlockingComment,
        openBlockingOpenModal: openBlockingOpenModal,
        changeShopPermission: changeShopPermission,
        readBlockingHistoryItems: readBlockingHistoryItems,
        getBlockingHistoryCommentsCount: getBlockingHistoryCommentsCount,

        upload: upload,
        bulkInfo: bulkInfo,
        uploadPermissionHandler: uploadPermissionHandler
    });

    var template = BASE_URL + BULK_TEMPLATE_PATH;
    var _id = parseInt($state.params.id, 10) || null;

    $scope.uploadSubmitted = false;

    vm.limits = LIMITS;
    vm.templates = {
        ITEMS_TO_SHOP: template + 'itemsToShop',
        SALES: template + 'sales',
        ADVERTISING: template + 'recommendations'
    };

    vm.noImage = BASE_URL + DEFAULT_IMAGE;

    vm.file = null;
    vm.report = null;

    vm.blockingHistoryCommentsCount = {};
    vm.currentBlockingHistoryCommentsType = "";

    vm.meta = {
        limit: $state.params.limit || 10,
        page: parseInt($state.params.pageNum, 10) || 1,
        totalCount: 0
    };

    vm.blockingHistoryMeta = {
        limit: 10,
        page: 1,
        totalCount: 0
    };

    vm.sorting = {
        by: $state.params.sortField || 'id',
        at: $state.params.sortDirection || 'desc'
    };

    vm.isCreated = isCreated;

    vm.shop = {};
    vm.reportRedirect = {};

    vm.shopAdmins = [];
    vm.shopOwners = [];
    vm.enterShopOwners = [];

    vm.items_meta = angular.extend({}, vm.meta, { 
        query: $state.params.query,
        timeLine: $state.params.timeLine,
        to: ($state.params.from ? new Date($state.params.to) : new Date() ), 
        from: ($state.params.from ? new Date($state.params.from) : new Date())
    });

    vm.configModals = {
        opened         : {},
        options        : {
            showWeeks: true
        },
        open           : function (num) {
            this.opened[num] = true;
        },
        fromOptions    : {
           // maxDate    : new Date()
        },
        toOptions      : {
          // maxDate      : new Date()
        }, 
        format         : 'dd.MM.yyyy',
        altInputFormats: 'M!/d!/yyyy'
    };

    vm.blockingHistoryGridOptions = {
        enableHorizontalScrollbar: 0,
        enableVerticalScrollbar: 0,
        paginationPageSize: vm.meta.limit,
        paginationCurrentPage: vm.meta.page,
        useExternalSorting: true,
        enableMinHeightCheck: true,
        columnDefs: [
            {
                field: 'message',
                displayName: 'Blocking comment',
                width: '75%',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'message' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'createdAt',
                displayName: 'Date of creating',
                width: '25%',
                type: 'date',
                cellFilter: 'date: "dd MMM yyyy, hh:mm:ss a"',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'createdAt' ? {
                    direction: $state.params.sortDirection || 'desc',
                    priority: 0
                } : undefined
            }
        ],
        onRegisterApi: function (gridApi) {
            vm.blockingHistoryGridApi = gridApi;
            vm.blockingHistoryGridApi.core.on.sortChanged($scope, sortBlockingHistoryItems);
        }
    }

    vm.gridOptions = {
        enableHorizontalScrollbar: 0,
        enableVerticalScrollbar: 0,
        paginationPageSize: vm.meta.limit,
        paginationCurrentPage: vm.meta.page,
        useExternalSorting: true,
        rowHeight: 34,
        enableMinHeightCheck: true,
        columnDefs: [
            {
                field: 'image',
                displayName: 'Image',
                width: '5%',
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                cellClass: 'text-center',   
                cellTemplate: '<img src="{{(row.entity.image ?  row.entity.image : grid.appScope.vm.noImage)}}" alt="{{row.entity.name}}" width="30" height="30"/>'
            },
            {
                field: 'name',
                displayName: 'Name',
                width: '*',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'name' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'brand',
                displayName: 'Brand',
                width: '*',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'brand' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'barCode',
                displayName: 'BarCode',
                width: '*',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'barCode' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'hasSale',
                displayName: 'has\nSale',
                width: '*',
                enableFiltering: false,
                enableHiding: false,
                enableSorting: true,
                cellTemplate: '<h3><b>{{row.entity.hasSale ? "+": "-"}}</b></h3>'
            },
            {
                field: 'hasAdvertising',
                displayName: 'has\nRecommendations',
                width: '*',
                enableFiltering: false,
                enableHiding: false,
                enableSorting: true,
                cellTemplate: '<h3><b>{{row.entity.hasAdvertising ? "+": "-"}}</b></h3>'
            },
            {
                field: 'createdAt',
                displayName: 'Created At',
                width: '*',
                type: 'date',
                cellFilter: 'date: "dd MMM yyyy, hh:mm:ss a"',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'createdAt' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'updatedAt',
                displayName: 'Updated At',
                width: '*',
                type: 'date',
                cellFilter: 'date: "dd MMM yyyy, hh:mm:ss a"',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'updatedAt' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                name: 'details',
                displayName: '',
                cellClass: 'text-center grid-nav-button',
                width: "8%",
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                visible: $rootScope.checkPermission['REVIEW_ITEM'] || false,
                cellTemplate: [
                    '<a class="btn btn-sm btn-info"',
                    'ui-sref="home.shops.itemView({ shopId: grid.appScope.vm.shop.id, itemId: row.entity.id, tabIndex: 0 })">',
                    '<i class="fa fa-info"></i>',
                    '<span>Details</span>',
                    '</a>'
                ].join(' ')
            },
            {
                name: 'delete',
                displayName: '',
                cellClass: 'text-center grid-nav-button',
                width: "8%",
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                visible: $rootScope.checkPermission['EDIT_SHOP'] || false,
                cellTemplate: [
                    '<button class="btn btn-sm btn-danger" ng-click="grid.appScope.vm.removeItem(row.entity)">',
                    '<i class="fa fa-trash"></i>',
                    '<span>Delete</span>',
                    '</button>'
                ].join(' ')
            },
            {
                name: 'enabled',
                displayName: 'Enabled',
                cellClass: 'text-center grid-nav-button',
                headerCellClass: 'text-center',
                width: '10%',
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                visible: $rootScope.checkPermission['EDIT_SHOP'] || false,
                cellTemplate: [
                    '<button class="btn btn-sm btn-warning" ng-click="grid.appScope.vm.updateItemEnablingMode(row.entity)">',
                    '<i class="fa" ng-class="row.entity.enabled ? \'fa-check\' : \'fa-remove\'"></i>',
                    '<span>{{row.entity.enabled ? "Yes" : "No"}}</span>',
                    '</button>'
                ].join(' ')
            }
        ],
        onRegisterApi: function (gridApi) {
            vm.gridApi = gridApi;
            vm.gridApi.core.on.sortChanged($scope, sortItems);
        }
    };

    console.log('ShopController.isCreated', isCreated);


    function bulkInfo(){
        alert([
           'A xlsx template file is located below each bulk upload option. Download,',
           'populate, and upload by using the correct choice. You can rename the file',
           'name to suit your needs'
        ].join(' '));
    }

    function setDateRangeByFrom(from) {
        var fromDate = new Date(from);
        var toDate = new Date(vm.items_meta.to);
        if(toDate < fromDate)  vm.items_meta.to = fromDate;
        vm.configModals.toOptions.minDate = fromDate;
    };

    /**
     * Receive one or list of shops
     */
    function read() {
        var fname = 'Read shop';
        var params = {id: _id || null};

        Shops.read(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.shop = res;
                vm.reportRedirect = {
                    query: vm.shop.name,
                    type: REPORT_TYPES[2]
                };
                vm.map.address = vm.shop.address;
                vm.map.marker.coords.longitude = vm.shop.longitude;
                vm.map.marker.coords.latitude = vm.shop.latitude;
            }
        }

        /**
         * Custom error handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onError(fname) {
            return function _onError(res) {
                $log.info(fname + ' error: ', arguments);
                StatusHandler(res.status, res.statusText, { type: 'error' });
                if (res.status == 404) $scope.cancel();
            }
        }
    }

    /**
     * Create shop model
     * @param isValid
     * @param data
     */
    function create(isValid, data) {
        var fname = 'Create shop';
        $log.debug(fname, arguments);
        $scope.submitted = true;

        if (isValid) {
            $scope.modalInstance = $uibModal.open({
                templateUrl: 'app/components/pusher/view.html',
                controller : 'pusherModalCtrl',
                scope      : $scope,
                size       : 'xs',
                resolve    : {
                    items: function(){
                        return [{
                            type: "SHOP_ADDED",
                            onlyTime : true,
                            name: data.name
                        }];
                    }
                }
            });

            $scope.modalInstance.result.then(function (deliveryDateTime) {
                $log.info('modalInstance submit', arguments);

                var shop = angular.copy(data);
                if(deliveryDateTime.length) shop.deliveryDateTime = deliveryDateTime[0].dateTime;
                shop.owners = vm.enterShopOwners.map(function(item){ return item.id; });
                if(vm.shopOwner.selected) shop.owners.push(vm.shopOwner.selected.id);

                Shops.create(shop).then(_onSuccess(fname), _onError(fname));
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);
                $scope.submitted = false;

                $rootScope.$broadcast('actions.write', {
                    entityId     : res.id,
                    entityType   : 'SHOP',
                    operationType: 'CREATE'
                });

                $state.go('home.shops.view', { id: res.id });
            }
        }
    }

    /**
     * Update shop model
     * @param isValid
     * @param data
     */
    function update(isValid, data) {
        var fname = 'Edit shop';
        $log.debug(fname, arguments);
        $scope.submitted = true;

        if (isValid) {
            data.owners = vm.enterShopOwners.map(function(item){ return item.id; });
            if(vm.shopOwner.selected) data.owners.push(vm.shopOwner.selected.id);
            data.isLocal = data.isLocal ? 1 : 0;
            data.hasSales = data.hasSales ? 1 : 0;
            data.hasAdvertising = data.hasAdvertising ? 1 : 0;
            Shops.update(data).then(_onSuccess(fname, data.id), _onError(fname));
        }
    }

    /**
     * Remove shop model
     * @param id
     */
    function remove(id) {
        var fname = 'Delete shop';
        $log.debug(fname, arguments);
        var confirmed = confirm('Are you sure you want to delete this shop?');

        if (confirmed) {
            Shops.remove({id: id}).then(_onSuccess(fname, id), _onError(fname));
        }
    }

    /**
     * Receive list of all admins
     * @param role
     * @param query
     */
    function readAdmins(role, query) {
        var fname = 'Admins read';
        var params = {
            role: role,
            email: query || null,
            limit: 50
        };

        Admins.read(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.shopAdmins[role] = res.admins;
                var admins = role == 'ENTERPRISE_SAVVY_SHOP' ? vm.enterShopOwners : [vm.shopOwner.selected];

                vm.shopAdmins[role] = _.filter(res.admins, function (resAdmin) {
                    return !_.find(admins, function (admin) {
                        return resAdmin.id == admin.id;
                    });
                });
            }
        }
    }

    function readFree(role, query) {
        var fname = 'Admins free read';
        var params = {
            role: role,
            email: query || null,
            limit: 50
        };

        Admins.freeAdmin(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.shopAdmins[role] = res.admins;
            }
        }
    }

    /**
     * Receive one or list of items
     */
   function readBlockingHistoryItems(options) {
        var fname = 'Read shop blocking history items';
        var params = angular.extend({}, vm.blockingHistorySorting, vm.blockingHistoryMeta, options, {
            id: _id || null,
            type: vm.currentBlockingHistoryCommentsType
        });

        Shops.blockingHistoryByType(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */

        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.blockingHistoryGridOptions.data = res.items;
                vm.blockingHistoryGridOptions.minRowsToShow = res.items.length;
                $scope.blockingHistoryGridHeight = (vm.blockingHistoryGridOptions.data ? vm.blockingHistoryGridOptions.data.length + 1 : 1) * 34;
                angular.extend(vm.blockingHistoryMeta, res.meta);
            }
        }
    }

    /**
     * Receive one or list of items
     */
    function readItems(options) {
        var fname = 'Read Shop items';
        var params = angular.extend({}, vm.sorting, vm.items_meta, options, { 
            id: _id || null,
            timeLine: vm.items_meta.timeLine ? 1 : 0, 
            from: (vm.items_meta.timeLine ? vm.items_meta.to.getTime(): null),
            to: (vm.items_meta.timeLine ? vm.items_meta.to.getTime(): null)
        });

        Shops.items(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.gridOptions.data = res.items;
                vm.gridOptions.minRowsToShow = res.items.length;
                $scope.height = (vm.gridOptions.data ? vm.gridOptions.data.length + 1 : 1) * 34;
                angular.extend(vm.items_meta, res.meta);

                $state.go('home.shops.view', {
                    id: _id || null,
                    pageNum: params.page,
                    limit: params.limit,
                    query: params.query,
                    sortField: params.by,
                    sortDirection: params.at,
                    from: params.from,
                    to: params.to,
                    timeLine: params.timeLine
                })
            }
        }
    }
    /**
     * Update enabling mode for item into current shop.
     * @param item
     */
    function updateItemEnablingMode(item) {

        var fname = 'EDIT Shop items enabling mode';
        var data = {
            id: _id || null,
            itemId: item.id,
            enable: !item.enabled
        };

        if(!item.enabled) {
            Shops.itemsUpdateEnabling(data).then(_onSuccess(fname), _onError(fname));
            return;
        }

        $scope.modalInstance = $uibModal.open({
            templateUrl: 'app/components/pusher/view.html',
            controller : 'pusherModalCtrl',
            scope      : $scope,
            size       : 'xs',
            resolve    : {
                items: function(){
                    return [{
                        type: "CONCRETE_ITEM_ENABLED_MODE_CHANGED",
                        onlyTime : true,
                        name: item.name
                    }];
                }
            }
        });

        $scope.modalInstance.result.then(function (deliveryDateTime) {
            $log.info('modalInstance submit', arguments);

            if(deliveryDateTime.length) data.deliveryDateTime = deliveryDateTime[0].dateTime;
            Shops.itemsUpdateEnabling(data).then(_onSuccess(fname), _onError(fname));
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);

                $rootScope.$broadcast('actions.write', {
                    entityId: _id || null,
                    entityType: 'SHOP',
                    operationType: fname.split(' ')[0].toUpperCase()
                });

                readItems();
            }
        }
    }

    /**
     * Remove item from shop
     * @param item
     */
    function removeItem(item) {
        var confirmed = confirm('Are you sure you want to delete this item?');
        if(confirmed){
            var fname = 'Edit Remove item';
            var params = {
                id: _id,
                itemId: item.id
            };
            Shops.itemsRemove(params).then(_onSuccess(fname), _onError(fname));
        }

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);

                $rootScope.$broadcast('actions.write', {
                    entityId: _id,
                    entityType: 'SHOP',
                    operationType: fname.split(' ')[0].toUpperCase()
                });
                readItems();
            }
        }
    }

    /**
     * Receive shop owners (administrators)
     */
    function owners() {
        var fname = 'Shop Owners read';
        var params = {
            id: _id
        };

        Shops.owners(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.shop.owners = res.owners;

                vm.shopOwners = _.filter(res.owners, function (el) {
                    return el.role === 'SAVVY_SHOP'
                });

                vm.shopOwner = { selected: vm.shopOwners[0] };

                vm.enterShopOwners = _.filter(res.owners, function (el) {
                    return el.role === 'ENTERPRISE_SAVVY_SHOP'
                });
            }
        }
    }

    /**
     * Upload items to shop
     * @param csv file
     */

    function upload(file, type){
        var fname = 'Upload items to shop from: ' + file.name;
        $scope.uploadSubmitted = true;
        BulkUploadService.upload(file, type, _id).then(_onBulkUploadSuccess(fname), _onBulkUploadError(fname), _onBulkUploadProgress);
    }

   function uploadPermissionHandler(type){
        if($rootScope['currentAdmin'].role == 'SAVVY_SHOP'){
            var msg = " privileges have been disabled for your account. This typically occurs when there is an issue with your billing. For more information, please email:\n" + vm.shop.contactEmail;
            if(type == 'SALES' && !vm.shop.hasSales) toaster.pop('error', "Sales" + msg);
            if(type == 'ADVERTISING' && !vm.shop.hasAdvertising) toaster.pop('error', "Recommendations " + msg);
        }
    }

    /**
     * Custom success handler
     * @override
     * @param fname
     * @returns {Function}
     * @private
     */
   function _onBulkUploadSuccess(fname) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            var data = res.data;
            $log.debug(msg, arguments);
            toaster.pop('info', msg);
            $scope.uploadSubmitted = false;
            vm.report = data.url;

            $rootScope.$broadcast('actions.write', {
                entityId:     data.id,
                entityType:   'REPORT',
                operationType:'CREATE'
            });

            $scope.$applyAsync(function(){  vm.readItems({ page: 1 }); }); 
        }
    }

   function _onBulkUploadError(fname) {
        return function _onError(res) {
            vm.report = null;
            $scope.uploadSubmitted = false;
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }
   }

   function _onBulkUploadProgress(evt){
       console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :' + evt.config.data);
   }

    function changeShopPermission(type, value){
        var fname = 'Changing '+ type +' permission to shop';

        var params = {
            id: _id,
            type: type,
            value: value ? 1 : 0
        };

        Shops.changeShopPermissionByType(params).then(_onSuccess(fname), _onError(fname));
        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                $rootScope.$broadcast('actions.write', {
                    entityId     : res.id || _id,
                    entityType   : 'SHOP',
                    operationType: 'EDIT'
                });
            }
        }
    }


    function openBlockingOpenModal(modalType){
        var modalOptions;

        switch(modalType){
            case  'ADD_BLOCKING_SALES_MESSAGE':
                modalOptions = { header: 'Add new blocking sales comment' };
                vm.currentBlockingHistoryCommentsType = 'SALE_BLOCKING';
                break;
            case 'ADD_BLOCKING_ADVERTISING_MESSAGE':
                modalOptions = { header: 'Add new blocking advertising comment' };
                vm.currentBlockingHistoryCommentsType = 'ADVERTISING_BLOCKING';
                break;
            case 'SEE_BLOCKING_SALES_HISTORY':
                modalOptions = {
                    header: 'Blocking sales comments history',
                    isGrid: true
                };

                if(vm.currentBlockingHistoryCommentsType != 'SALE_BLOCKING') {
                    vm.currentBlockingHistoryCommentsType = 'SALE_BLOCKING';
                    vm.blockingHistoryMeta = {
                        limit: 10,
                        page: 1,
                        totalCount: 0
                    };
                }
                vm.readBlockingHistoryItems();
                break;
            case 'SEE_BLOCKING_ADVERISING_HISTORY':
                modalOptions = {
                    header: 'Blocking advertising comments history',
                    isGrid: true
                };

                if(vm.currentBlockingHistoryCommentsType != 'ADVERTISING_BLOCKING') {
                    vm.currentBlockingHistoryCommentsType = 'ADVERTISING_BLOCKING';
                    vm.blockingHistoryMeta = {
                        limit: 10,
                        page: 1,
                        totalCount: 0
                    };
                }
                vm.readBlockingHistoryItems();
                break;
            default:
                vm.currentBlockingHistoryCommentsType = "";
                return;
        }

        $scope.modalInstance = $uibModal.open({
            templateUrl : 'app/components/shops/shopPermissionModal/view.html',
            controller  : 'shopPermissionModalController',
            scope       : $scope,
            size        : 'lg',
            resolve     : { modalOptions: modalOptions }
        });

        $scope.modalInstance.result.then(function (results) {
            $log.info('modalInstance submit', arguments);
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addBlockingComment(isValid, message, isClose){
        var fname = 'Add sales blocking comment to shop';
        $log.debug('Update Shop: ', arguments);

        if (isValid){
            var params = {
                id: _id,
                message: message,
                type: vm.currentBlockingHistoryCommentsType
            };
            Shops.addBlockingHistoryItem(params).then(_onSuccess(fname), _onError(fname));
        }
        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);

                if(isClose) $scope.$applyAsync(vm.readBlockingHistoryItems);
                else $scope.modalInstance.close();

                $rootScope.$broadcast('actions.write', {
                    entityId     : res.id || _id,
                    entityType   : 'SHOP',
                    operationType: 'EDIT'
                });

                vm.getBlockingHistoryCommentsCount();
            }
        }

        /**
         * Custom error handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onError(fname) {
            return function _onError(res) {
                $log.info(fname + ' error: ', arguments);
                toaster.pop('error', res.statusText);
                StatusHandler(res.status, res.statusText, {type: 'error'});
            }
        }
    }

    function getBlockingHistoryCommentsCount(){
        var fname = 'Get shop blocking history comments count';
        var params = {id: _id || null};

        Shops.blockingHistoryCommentsCount(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.blockingHistoryCommentsCount = res.countByTypes;
            }
        }

        /**
         * Custom error handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onError(fname) {
            return function _onError(res) {
                $log.info(fname + ' error: ', arguments);
                StatusHandler(res.status, res.statusText, {type: 'error'});
            }
        }
    }

    function sortBlockingHistoryItems(grid, sortColumns) {
        sortColumns = _.sortBy(sortColumns, function (v) {
            return v.sort.priority;
        });
        if (sortColumns.length > 1) {
            sortColumns[0].unsort();
            sortColumns.shift();
        }
        else {
            var sorting = { by: 'createdAt', at: 'desc' };
            if (sortColumns.length != 0 && sortColumns[0].sort.direction) {
                var sortColumn = sortColumns[0];
                sorting.by = sortColumn.field;
                sorting.at = sortColumn.sort.direction;
            }

            vm.blockingHistorySorting = sorting;
            vm.readBlockingHistoryItems();
        }
    }


    //helpers
    function sortItems(grid, sortColumns) {
        sortColumns = _.sortBy(sortColumns, function (v) {
            return v.sort.priority;
        });
        if (sortColumns.length > 1) {
            sortColumns[0].unsort();
            sortColumns.shift();
        }
        else {
            var sorting = { by: 'createdAt', at: 'desc' };
            if (sortColumns.length != 0 && sortColumns[0].sort.direction) {
                var sortColumn = sortColumns[0];
                sorting.by = sortColumn.field;
                sorting.at = sortColumn.sort.direction;
            }

            vm.sorting = sorting;
            vm.readItems();
        }
    }

    //default on success handler
    function _onSuccess(fname, id) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);
            $scope.submitted = false;

            $rootScope.$broadcast('actions.write', {
                entityId: _id || res.id || id,
                entityType: 'SHOP',
                operationType: fname.split(' ')[0].toUpperCase()
            });
            $state.reload();
        }
    }

    //default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }
    }

    /**
     * Upload image handler
     * @returns {*}
     */
    function imageScope() {
        //Image upload watcher
        return $scope.$watch('image', function (newValue) {
            if (newValue) {
                Uploads.image($scope.image).then(_onSuccess, _onError, _onProgress);
                $scope.inProgress = Uploads.isUploadInProgress();
            }
        });

        //upload handlers
        function _onSuccess(res) {
            var msg = 'Image uploaded!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);

            vm.shop.image = res.data.images[0];
            vm.shop.imageId = res.data.images[0].id;
            $scope.inProgress = Uploads.isUploadInProgress();
        }

        function _onError(res) {
            $log.info('Image upload error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }

        function _onProgress(evt) {
            console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :' + evt.config.data);
        }
    }

    angular.extend(vm, {
        map: {
            center: {
                latitude: -28.0316313,
                longitude: 135.2187349
            },
            zoom: 3,
            options: {
                scrollwheel: true
            },
            control: {},
            address: '',
            events: {
                click: function (mapModel, eventName, originalEventArgs) {
                    $scope.$apply(function () {
                        var place = {
                            latitude: originalEventArgs[0].latLng.lat(),
                            longitude: originalEventArgs[0].latLng.lng()
                        };

                        setPlacePosition(place);
                    });
                }
            },
            marker: {
                idKey: 0,
                events: {
                    dragend: function (marker, eventName, args) {
                        $scope.$apply(function () {
                            var place = {
                                latitude: marker.getPosition().lat(),
                                longitude: marker.getPosition().lng()
                            };

                            setPlacePosition(place);
                        });
                    }

                },
                control: {},
                options: {draggable: true},
                coords: {
                    //latitude : -33.7966068,
                    //longitude: 150.6422513
                }
            }
        }
    });

    /**
     * Setup map position according to place and find address
     * @param place
     */
    function setPlacePosition(place) {
        var geocoder = new google.maps.Geocoder();
        var latLng = new google.maps.LatLng(place.latitude, place.longitude);
        vm.map.zoom = vm.map.zoom < 14 ? 14 : vm.map.zoom;
        vm.map.center = angular.copy(place);
        vm.map.marker.coords = angular.copy(place);

        geocoder.geocode({'latLng': latLng}, function (result, status) {
            if (status == google.maps.GeocoderStatus.OK && result) {
                vm.map.address = result[0].formatted_address;

            } else {
                console.log('Geocoder failed due to: ' + status);
                $log.info('Location not found! Try another one!');
                toaster.pop('info', 'Location not found! Try another one!');
            }
        });
    }

    /**
     * Setup map position and address according to place
     */
    $rootScope.$on('google.place', function (e, data) {
        $scope.$apply(function () {
            vm.map.zoom = vm.map.zoom < 14 ? 14 : vm.map.zoom;
            vm.map.center = angular.copy(data.place);
            vm.map.marker.coords = angular.copy(data.place);
            vm.map.address = data.address;
        });
    });


    $scope.$watch('vm.map.address', function (nv, ov) {
        if (nv && nv !== ov) {
            vm.shop.address = vm.map.address;
            vm.shop.longitude = vm.map.marker.coords.longitude;
            vm.shop.latitude = vm.map.marker.coords.latitude;
        }
    });

    if (_id) {
        vm.read();
        vm.owners();
        vm.readItems();
        vm.getBlockingHistoryCommentsCount();

        $rootScope.$broadcast('actions.read', {
            entityId: _id,
            entityType: 'SHOP'
        });
    }

    if ($rootScope.checkPermission['ASSIGN_SHOP']) {
        readFree('SAVVY_SHOP');
        readAdmins('ENTERPRISE_SAVVY_SHOP');
    }

    imageScope();

    $scope.cancel = function () {
        $state.go('home.shops.list');
    };

}
