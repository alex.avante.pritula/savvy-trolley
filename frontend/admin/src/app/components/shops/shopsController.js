angular.module('app').controller('ShopsController', ['$scope', '$rootScope', '$state', '$log', 'toaster', 'Shops', 'PostCodes', 'LIMITS', 'BASE_URL', 'DEFAULT_IMAGE',
    ShopsController
]);

/**
 * ShopsController
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param $log
 * @param toaster
 * @param Shops
 * @param PostCodes
 * @constructor
 */
function ShopsController($scope, $rootScope, $state, $log, toaster, Shops, PostCodes, LIMITS, BASE_URL, DEFAULT_IMAGE) {
    //var vm = this;
    var vm = angular.extend(this, {
        create: create,
        read: read,
        update: update,
        remove: remove,
        refreshCodes: refreshCodes
    });

    //meta dummy
    vm.meta = {
        limit: $state.params.limit || 10,
        page: parseInt($state.params.pageNum, 10) || 1,
        totalCount: 0
    };
    vm.sorting = {
        by: $state.params.sortField || 'createdAt',
        at: $state.params.sortDirection || 'desc'
    };

    vm.filter = {
        query:     $state.params.query || '',
        adminEmail:$state.params.adminEmail || '',
        postcodes: $state.params.postcodes ? JSON.parse($state.params.postcodes) : []
    };

    vm.limits = LIMITS;
    vm.noImage = BASE_URL + DEFAULT_IMAGE;

    vm.gridOptions = {
        enableHorizontalScrollbar: 0,
        enableVerticalScrollbar: 0,
        paginationPageSize: vm.meta.limit,
        paginationCurrentPage: vm.meta.page,
        useExternalSorting: true,
        rowHeight: 34,
        enableMinHeightCheck: true,
        columnDefs: [
            {
                field: 'image',
                displayName: 'Image',
                width: '5%',
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                cellClass: 'text-center',
                cellTemplate: '<img src="{{(row.entity.image ?  row.entity.image : grid.appScope.vm.noImage)}}" alt="{{row.entity.name}}" width="30" height="30"/>'
            },
            {
                field: 'name',
                displayName: 'Name',
                width: '*',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'name' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'owner',
                displayName: 'Owner',
                width: '*',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'owner' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'postCode',
                displayName: 'Post Code',
                width: '*',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'postCode' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'createdAt',
                displayName: 'Created At',
                width: '*',
                type: 'date',
                cellFilter: 'date: "dd MMM yyyy, hh:mm:ss a"',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'createdAt' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                field: 'updatedAt',
                displayName: 'Updated At',
                width: '*',
                type: 'date',
                cellFilter: 'date: "dd MMM yyyy, hh:mm:ss a"',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'updatedAt' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            },
            {
                name: 'details',
                displayName: "",
                width: "8%",
                cellClass: 'text-center grid-nav-button',
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                visible: $rootScope.checkPermission['REVIEW_ITEM'] || false,
                cellTemplate: [
                    '<a class="btn btn-sm btn-info"',
                    'ui-sref="home.shops.view({id: row.entity.id})">',
                    '<i class="fa fa-info"></i>',
                    '<span>Manage</span>',
                    '</a>'
                ].join(' ')
            },
            {
                name: 'delete',
                displayName: "",
                width: "8%",
                cellClass: 'text-center grid-nav-button',
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                visible: $rootScope.checkPermission['EDIT_ITEM'] || false,
                cellTemplate: [
                    '<button class="btn btn-sm btn-danger" ng-click="grid.appScope.vm.remove(row.entity.id)">',
                    '<i class="fa fa-trash"></i>',
                    '<span>Delete</span>',
                    '</button>'
                ].join(' ')
            }
        ],
        onRegisterApi: function (gridApi) {
            vm.gridApi = gridApi;
            vm.gridApi.core.on.sortChanged($scope, sort);
        }
    };

    var _id = $state.params.shopId || null;

    /**
     * Receive one or list of shops
     */
    function read(options) {
        var fname = 'Read shops';
        var params = angular.extend({}, vm.filter, vm.sorting, vm.meta, options || { id: _id || null });

        params.postcodes = params.postcodes.join(',');

        Shops.read(params).then(_onSuccess(fname), _onError(fname));

        /**
         * Custom success handler
         * @override
         * @param fname
         * @returns {Function}
         * @private
         */
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);

                vm.gridOptions.data = res.shops;
                vm.gridOptions.minRowsToShow = res.shops.length;
                $scope.height = (vm.gridOptions.data ? vm.gridOptions.data.length + 1 : 1) * 34;
                angular.extend(vm.meta, res.meta);

                // postCodes =
                $state.go('home.shops.list', {
                    pageNum:       params.page,
                    limit:         params.limit,
                    query:         params.query,
                    postcodes:     JSON.stringify(vm.filter.postcodes),
                    sortField:     params.by,
                    sortDirection: params.at,
                    adminEmail:    params.adminEmail
                });
            }
        }
    }

    /**
     * Create shop model
     * @param isValid
     * @param data
     */
    function create(isValid, data) {
        var fname = 'Create shop';
        $log.debug(fname, arguments);
        $scope.submitted = true;

        if (isValid) {

            $scope.modalInstance = $uibModal.open({
                templateUrl: 'app/components/pusher/view.html',
                controller : 'pusherModalCtrl',
                scope      : $scope,
                size       : 'xs',
                resolve    : {
                    items: function(){
                        return [{
                            onlyTime : true,
                            type: "SHOP_ADDED"
                        }];
                    }
                }
            });

            $scope.modalInstance.result.then(function (deliveryDateTime) {
                $log.info('modalInstance submit', arguments);
                if(deliveryDateTime.length) data.deliveryDateTime = deliveryDateTime[0].dateTime;
                Shops.create(data).then(_onSuccess(fname), _onError(fname));
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });

        }
    }

    /**
     * Update shop model
     * @param isValid
     * @param data
     */
    function update(isValid, data) {
        var fname = 'Edit shop';
        $log.debug(fname, arguments);
        $scope.submitted = true;
        if (isValid) {
            Shops.update(data).then(_onSuccess(fname, data.id), _onError(fname));
        }
    }

    /**
     * Remove shop model
     * @param id
     */
    function remove(id) {
        var fname = 'Delete shop';
        $log.debug(fname, arguments);
        var confirmed = confirm('Are you sure you want to delete this shop?');
        if (confirmed) {
            Shops.remove({id: id}).then(_onSuccess(fname, id), _onError(fname));
        }

        //default on success handler
        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);
                toaster.pop('info', msg);
                $scope.submitted = false;

                $rootScope.$broadcast('actions.write', {
                    entityId: id,
                    entityType: 'SHOP',
                    operationType: 'DELETE'
                });

                vm.read();
            }
        }
    }


    /**
     * Open modal according to type of action with optional data
     * @param type
     * @param data
     */
    vm.openModal = function (type, data) {
        var tmpl = 'app/components/shops/' + type + '.html';

        $scope.modalInstance = $uibModal.open({
            templateUrl: tmpl,
            controller: 'shopModalCtrl',
            scope: $scope,
            size: 'lg',
            resolve: {
                item: angular.copy(data) || {}
            }
        });

        //
        $scope.modalInstance.result.then(function (results) {
            $log.info('modalInstance submit', arguments);

        }, function () {
            $log.info('Modal dismissed at: ' + new Date());

        });
    };

    function sort(grid, sortColumns) {
        sortColumns = _.sortBy(sortColumns, function (v) {
            return v.sort.priority;
        });
        if (sortColumns.length > 1) {
            sortColumns[0].unsort();
            sortColumns.shift();
        }
        else {
            var sorting = {by: 'id', at: 'asc'};
            if (sortColumns.length != 0 && sortColumns[0].sort.direction) {
                var sortColumn = sortColumns[0];
                sorting.by = sortColumn.field;
                sorting.at = sortColumn.sort.direction;
            }

            vm.sorting = sorting;
            vm.read();
        }
    }

    //helpers
    /**
     * Receive list of post codes
     */
    function refreshCodes(postcode) {
        var fname = 'Refresh postcodes';
        var params = {postcode: postcode || null, limit: 100, type: 'shop'};
        PostCodes.read(params).then(_onSuccess(fname), _onError(fname));

        function _onSuccess(fname) {
            return function _onSuccess(res) {
                var msg = fname + ' success!';
                $log.debug(msg, arguments);

                vm.codes = res.postCodes;
            }
        }
    }

    //default on success handler
    function _onSuccess(fname, id) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);
            $scope.submitted = false;

            $rootScope.$broadcast('actions.write', {
                entityId: res.id || id,
                entityType: 'SHOP',
                operationType: fname.split(' ')[0].toUpperCase()
            });

            $state.reload('home.shops.list.paginated', {
                pageNum: vm.meta.page,
                limit: vm.meta.limit,
                query: vm.filter.query,
                postcodes: JSON.stringify(vm.filter.postcodes),
                sortField: vm.sorting.by,
                sortDirection: vm.sorting.at
            });
        }
    }

    //default on error handler
    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }
    }

    //run
    vm.read();
    vm.refreshCodes();
    $rootScope.$broadcast('actions.read', {
        entityType: 'SHOP'
    });
}
