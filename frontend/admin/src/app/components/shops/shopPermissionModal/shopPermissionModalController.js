angular.module('app').controller('shopPermissionModalController', ['$scope', '$uibModalInstance', 'modalOptions', shopPermissionModalController ]);

function shopPermissionModalController($scope, $uibModalInstance, modalOptions) {

    $scope.blockingMessage = '';
    $scope.modalOptions = modalOptions;

    $scope.addBlockingComment = function (valid, blockingMessage,isGrid){
        $scope.blockingMessage = '';
        $scope['vm'].addBlockingComment(valid, blockingMessage,isGrid);
    };

    $scope.cancel = function () {
        $scope.blockingMessage = '';
        $uibModalInstance.dismiss('cancel');
    };
}