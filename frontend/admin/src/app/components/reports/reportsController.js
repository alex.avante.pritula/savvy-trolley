angular.module('app').controller('ReportsController', ['$rootScope','$scope', '$state', '$log', 'toaster', 'ReportsService', 'LIMITS', 'REPORT_TYPES',
    ReportsController
]);

/**
 * ReportsController
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param $log
 * @param toaster
 * @param ReportsService
 * @param LIMITS
 * @constructor
 */
function ReportsController($rootScope, $scope, $state, $log, toaster, ReportsService,  LIMITS,  REPORT_TYPES) {

    var vm = angular.extend(this, {
        read: read,
        remove: remove,
        setDateRangeByFrom: setDateRangeByFrom,
        setDateRangeByTo: setDateRangeByTo
    });

    vm.limits = LIMITS;
    vm.types = REPORT_TYPES;

    vm.configModals = {
        opened         : {},
        options        : {
            showWeeks: true
        },
        open           : function (num) {
            this.opened[num] = true;
        },
        fromOptions    : {
            maxDate    : new Date()
        },
        toOptions      : {
            maxDate    : new Date()
        },
        format         : 'dd.MM.yyyy',
        altInputFormats: 'M!/d!/yyyy'
    };

    //meta dummy
    vm.meta = {
        limit     : $state.params.limit || 10,
        page      : parseInt($state.params.pageNum, 10) || 1,
        totalCount: $state.params.totalCount || 0
    };

    vm.sorting = {
        by: $state.params.sortField || 'createdAt',
        at: $state.params.sortDirection || 'desc'
    };

    vm.filter = {
        query: $state.params.query || '',
        email: $state.params.email || '',
        type: $state.params.type || REPORT_TYPES[0],
        from: ($state.params.from ? new Date($state.params.from) : new Date(new Date().setDate(1))),
        to: ($state.params.to   ? new Date($state.params.to)   : new Date())
    };

    function setDateRangeByFrom(from) {
        var fromDate = new Date(from);
        var toDate = new Date(vm.filter.to);
        if(fromDate > toDate) vm.filter.to = fromDate;
        vm.read({ page: 1});
    }

    function setDateRangeByTo(to) {
        var fromDate = new Date(vm.filter.from);
        var toDate = new Date(to);
        if(fromDate > toDate) vm.filter.to = fromDate;
        vm.read({ page: 1});
    }

    vm.gridOptions = {
        enableHorizontalScrollbar: 0,
        enableVerticalScrollbar: 0,
        paginationPageSize: vm.meta.limit,
        paginationCurrentPage: vm.meta.page,
        useExternalSorting: true,
        rowHeight: 34,
        enableMinHeightCheck: true,
        columnDefs: [
            {
                field: 'name',
                displayName: 'Name',
                width: '*',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'name' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            }, {
                field: 'createdAt',
                displayName: 'Date',
                width: '20%',
                type: 'date',
                cellFilter: 'date: "dd MMM yyyy, hh:mm:ss a"',
                enableFiltering: false,
                enableHiding: false,
                sort: $state.params.sortField == 'createdAt' ? {
                    direction: $state.params.sortDirection || 'asc',
                    priority: 0
                } : undefined
            }, {
                name: 'download',
                displayName: '',
                cellClass: 'text-center grid-nav-button',
                width: '12%',
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                cellTemplate: [
                    '<a ng-href="{{ row.entity[\'url\'] }} " >',
                    '<button class="btn btn-sm btn-success" >',
                    '<i class="fa fa-download"></i>',
                    '<span>Download</span>',
                    '</button>',
                    '</a>'
                ].join(' ')
            }, {
                name: 'delete',
                displayName: '',
                 cellClass: 'text-center grid-nav-button',
                width: '10%',
                enableFiltering: false,
                enableSorting: false,
                enableColumnMenu: false,
                cellTemplate: [
                    '<button class="btn btn-sm btn-danger" ng-disabled="checkPermission[\'EDIT_REPORT\']"',
                    'ng-click="grid.appScope.vm.remove(row.entity.id)">',
                    '<i class="fa fa-trash"></i>',
                    '<span>Delete</span>',
                    '</button>'
                ].join(' ')
            }
        ],
        onRegisterApi: function (gridApi) {
            vm.gridApi = gridApi;
            vm.gridApi.core.on.sortChanged($scope, sort);
        }
    };

    function read(options) {
        var fname = 'Read reports';
        var params = angular.extend({}, vm.sorting, vm.filter, vm.meta, options || {});
        if(params.type) params.type = params['type'].type;

        ReportsService.read(params).then(_onSuccess(fname), _onError(fname));

        function _onSuccess(fname) {
            return function _onSuccess(res) {
                $log.debug(fname + ' success: ', arguments);
                vm.gridOptions.data = res.items;
                vm.gridOptions.minRowsToShow = res.items.length;
                $scope.height = (vm.gridOptions.data ? vm.gridOptions.data.length + 1 : 1) * 34;
                angular.extend(vm.meta, res.meta);
            }
        }
    }

    function remove(id){
        var fname = 'DELETE upload report ';
        $log.debug(fname, arguments);
        var confirmed = confirm('Are you sure you want to delete this upload report ?');

        if (confirmed) {
            ReportsService.remove({ id: id }).then(_onSuccess(fname, id), _onError(fname));
        }
    }

    function _onSuccess(fname, id) {
        return function _onSuccess(res) {
            var msg = fname + ' success!';
            $log.debug(msg, arguments);
            toaster.pop('info', msg);
            $scope.submitted = false;

            $rootScope.$broadcast('actions.write', {
                entityId: res.id || id,
                entityType: 'REPORT',
                operationType: fname.split(' ')[0].toUpperCase()
            });

            vm.read();
        }
    }

    function _onError(fname) {
        return function _onError(res) {
            $log.info(fname + ' error: ', arguments);
            StatusHandler(res.status, res.statusText, {type: 'error'});
        }
    }

    function sort(grid, sortColumns) {
        sortColumns = _.sortBy(sortColumns, function (v) {
            return v.sort.priority;
        });

        if (sortColumns.length > 1) {
            sortColumns[0].unsort();
            sortColumns.shift();
        } else {
            var sorting = {by: 'id', at: 'asc'};
            if (sortColumns.length != 0 && sortColumns[0].sort.direction) {
                var sortColumn = sortColumns[0];
                sorting.by = sortColumn.field;
                sorting.at = sortColumn.sort.direction;
            }

            vm.sorting = sorting;
            vm.read();
        }
    }

    vm.read({ page: $state.params.pageNum || 1 });

    $scope.height = (vm.gridOptions.data ? vm.gridOptions.data.length + 1 : 1) * 34;
}
