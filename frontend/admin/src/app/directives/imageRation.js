/**
 * Correct rule for
 */
angular
    .module('app')
    .directive('ratio', [
        '$q',
        'toaster',
        function ($q, toaster) {
            return {
                require: 'ngModel',
                scope: {
                    ratio: '=ratio'
                },
                link: function (scope, element, attributes, ngModel) {
                    var DEFAULT_RATION = '1:1~0.1';

                    ngModel.$asyncValidators.ratio = validateRatio;

                    /**
                     * Parse ratio expressions
                     * @param rationExpressions
                     * @returns {{accuracy: Number, width: Number, height: Number, coefficient: Number}}
                     */
                    function parseRation(rationExpressions) {
                        rationExpressions = rationExpressions || DEFAULT_RATION;
                        var ratioArray = rationExpressions.split('~');
                        var ratioString = ratioArray[0];

                        var accuracy = parseFloat(ratioArray[1]) || 0;

                        var sizeArray = ratioString.split(':');
                        if (sizeArray.length === 1) sizeArray.push(sizeArray[0]);

                        var width = parseInt(sizeArray[0], 10);
                        var height = parseInt(sizeArray[1], 10);
                        var coefficient = height / width;

                        return {
                            accuracy: accuracy,

                            width: width,
                            height: height,
                            coefficient: coefficient
                        }

                    }

                    /**
                     * Validate ratio
                     * @param modelValue
                     * @returns {Promise}
                     */
                    function validateRatio(modelValue) {
                        if (!modelValue) return $q.resolve();

                        var ratioString = scope.ratio || DEFAULT_RATION;
                        var ratio = parseRation(ratioString);
                        var validator = buildValidator(ratio);

                        return loadImage(modelValue).then(validator);
                    }

                    /**
                     * Load image
                     * @param newValue
                     * @returns {Promise}
                     */
                    function loadImage(newValue) {
                        return new $q(function (resolve) {
                            var imageData = new Image();

                            imageData.src = URL.createObjectURL(newValue);
                            imageData.onload = function () {
                                resolve(imageData);
                            }
                        });
                    }

                    /**
                     * Build validator
                     * @param ratio
                     * @returns {Function}
                     */
                    function buildValidator(ratio) {
                        return function (image) {
                            if (!image) return $q.when();

                            var imageCoefficient = Math.abs(image.height / image.width);
                            var deltaCoefficient = Math.abs(imageCoefficient - ratio.coefficient);

                            var isValid = deltaCoefficient <= ratio.accuracy;
                            if (isValid) return $q.resolve();

                            var msg = [
                                'Please upload picture with a',
                                ratio.width,
                                ':',
                                ratio.height,
                                'aspect ratio'
                            ].join(' ');
                            toaster.pop('error', msg);
                            return $q.reject();
                        }
                    }
                }
            };
        }
    ]);

