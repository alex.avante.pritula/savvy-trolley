angular.module('app').directive('googlePlace', ['$rootScope', 'uiGmapGoogleMapApi',
    googlePlace
]);

function googlePlace($rootScope, GoogleMapApi) {
    return {
        require : 'ngModel',
        restrict: 'A',
        link    : function (scope, element, attrs, model) {
            var options = {
                types: ['address']
            };

            GoogleMapApi.then(function (maps) {
                scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

                scope.gPlace.addListener('place_changed', function () {
                    var address = element.val();
                    var place = scope.gPlace.getPlace();

                    //search-box data
                    scope.searchData = {
                        address: address,
                        place  : {
                            latitude : place.geometry.location.lat(),
                            longitude: place.geometry.location.lng()
                        }
                    };

                    $rootScope.$broadcast('google.place', scope.searchData);
                });
            });
        }
    }
}