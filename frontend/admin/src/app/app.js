angular.module('app', [])
    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('httpRequestInterceptor');
        $httpProvider.interceptors.push('status401Handler');
    })
    .service('status401Handler', ['$q', '$injector', '$rootScope', 'Storage',
        function ($q, $injector, $rootScope, Storage) {
            return {
                responseError: function (response) {
                    if (response.status === 401) {
                        var rootScope = $injector.get('$rootScope');
                        var toaster = $injector.get('toaster');
                        var $state = $injector.get('$state');
                        rootScope.currentAdmin = null;
                        Storage.clearAll();
                        $state.go('auth.login', {}, {reload: true});

                    }
                    return $q.reject(response);
                }
            };
        }])
    .service('httpRequestInterceptor', ['$rootScope', function ($rootScope) {
        return {
            request: function (config) {
                return config;
            }
        };
    }]);
