const config = require('./config');
const logger = require('./server/utils/logger');
const app = require('./server/api');
const util = require('util');
const http = require('http');
const kue = require('kue');
kue.app.listen(config.kue.port);

//run clustered instance in production and single for admin panel
const cluster = require('cluster');
const numCPUs = require('os').cpus().length;

const autoCleaner = require('./server/utils/autoCleaner');

logger.log('info', config.env);

if (config.env === 'development') {
  autoCleaner.start();
  app.listen(config.server.port, function () {
    //todo: Need normal logging system!
    console.log(
      new Date(),
      'info',
      util.format(
        'DEV Savvy Trolley | Web server successfully started at path http://%s:%s ',
        config.server.host,
        config.server.port
      )
    );
  });
}

if (config.env === 'production') {
  if (cluster.isMaster) {
    // Fork workers.

    autoCleaner.start();

    for (let i = 0; i < numCPUs; i++) {
      let worker = cluster.fork();

      logger.log('info', 'worker started: ', worker.id);

      worker.on('exit', function (code, signal) {
        if (signal) {
          logger.log('info', 'worker was killed by signal:' + signal + ' signal');
        } else if (code !== 0) {
          logger.log('info', 'worker exited with error code:' + code + ' code');
        } else {
          logger.log('info', 'worker success!');
        }
      });


    }
    cluster.on('exit', function (deadWorker) {
      // Restart the worker
      let worker = cluster.fork();

      // Note the process IDs
      let newPID = worker.process.pid;
      let oldPID = deadWorker.process.pid;

      // Log the event
      console.log('worker ' + oldPID + ' died.');
      console.log('worker ' + newPID + ' born.');
    });
  } else {
    let httpServer = http.createServer(app);
    httpServer.listen(config.server.port, function () {
      //todo: Need normal logging system!
      console.log(
        new Date(),
        'info',
        util.format('PROD Savvy Trolley | Web server successfully started at path http://%s:%s ',
        config.server.host,
        config.server.port)
      );
    });
  }
}
