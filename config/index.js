
var path = require('path');

var env = process.env.NODE_ENV || 'development';
var db = require('./database.json');
var logger = require('./logger.json');
var server = require('./server.json');
var socialAuth = require('./socialAuth.json');
var apn = require('./apn.json');

var root = path.join(__dirname, '/..');

var config = {
    env      : env,
    server   : {
        host: process.env.SERVER_HOST || server[env].host,
        port: process.env.SERVER_PORT || server[env].port
    },
    db       : {
        host                : process.env.DB_HOST || db[env].host,
        port                : parseInt(process.env.DB_PORT) || 3306,
        dbname              : process.env.DB_NAME || db[env].database,
        user                : process.env.DB_USER || db[env].user,
        password            : process.env.DB_PASSWORD || db[env].password,
        charset             : 'utf8mb4',
        connectionRetryCount: 5,
        delayBeforeReconnect: 3000,
        showErrors          : true
    },
    kue      : {
        port: '3240'
    },
    redis    : {
        server : {
            host: '127.0.0.1',
            port: '6379'
        },
        enabled: false
    },
    logger   : logger[env] || {},
    social   : {
        facebook: socialAuth.facebook,
        google  : socialAuth.google
    },
    mailgun  : {
        from  : 'support@savvytrolley.com.au',
        domain: 'savvytrolley.com.au',
        apiKey: 'key-7c025b52dab35cc1889ef0e3ac701694'
    },
    cleaner  : {
        time: '00 00 00 * * *'
    },
    session:  {
        secret: 'keyboard cat',
        maxAge: 1800000,
        prefix: 'sess:'
    },
    constants: {
        ONE_DAY: 1000 * 60 * 60 * 24,
        
        PAYMENT_ITEM_PRICE: 0.05,

        REQUEST_LIMIT: '50mb',

        ERROR_MESSAGES: {
            NOT_VERIFIED                    : 'Your account isn’t verified. Check your email, please.',
            NOT_REGISTERED                  : 'We can\'t find your email in the system. Please Sign up with your email. Alternatively, you can always use Facebook or Google authentication to use Savvy Trolley',
            INCORRECT_PASSWORD              : 'Password is incorrect. Please try again. If you can\'t remember it, select the \'Forgot?\' option to reset your password',
            EXIST_EMAIL_IF_USE_LOCAL_SIGN_IN: 'Our systems show this email address is linked to a Facebook or Google Account. Please signin using Facebook/Google on the home screen'
        },

        LIMIT       : 10,
        SINGLE_LIMIT: 1,
        STEP        : 0,

        JWT_SECRET: 'myFavoriteJwtSecret',

        ACCESS_TOKEN_LIFE_TIME : '7d',
        REFRESH_TOKEN_LIFE_TIME: '365d',

        ENTITY_TYPES: {
            SHOP        : 1,
            ITEM        : 2,
            ITEM_BY_SHOP: 3,
            CATEGORY    : 4,
            USER        : 5,
            ADMIN       : 6,
            ADVERTISING : 7,
            REPORT      : 8
        },

        DELIVERY_NOTIFICATION_ENTITY_TYPES: {
            SHOP        : 1,
            ITEM        : 2,
            ITEM_BY_SHOP: 3,
            CATEGORY    : 4,
            USER        : 5,
            ADMIN       : 6,
            ADVERTISING : 7,
            REPORT      : 8
        },

        BARCODE_MIN_LENGTH: 14,

        PRICE_MAX_LENGTH: {
           INTEGER   : 7,
           FRACTIONAL: 2
        },

        TEXT_MAX_LENGTH: 256,

        BULK_UPLOAD: {

            ITEMS: {
                BARCODE          : 0,
                NAME             : 1,
                BRAND            : 2,
                UNIT_TYPE        : 3,
                ITEM_SIZE        : 4,
                SERVING_SIZE     : 5,
                SERVING_TYPE     : 6,
                SERVING_PER_PACK : 7,
                IMAGE_URL        : 8,
                FIRST_SUBCATEGORY: 9
            },

            ITEMS_TO_SHOP: {
                BARCODE          : 0,
                NAME             : 1,
                BRAND            : 2,
                UNIT_TYPE        : 3,
                ITEM_SIZE        : 4,
                SERVING_SIZE     : 5,
                SERVING_TYPE     : 6,
                SERVING_PER_PACK : 7,
                IMAGE_URL        : 8,
                REGULAR_PRICE    : 9,
                PN_PRICE_REDUCED : 10,
                IS_OUT_OF_STOCK  : 11,
                PN_OUT_OF_STOCK  : 12,
                FIRST_SUBCATEGORY: 13
            },

            ADVERTISING: {
                ITEM_BARCODE         : 0,
                FROM                 : 1,
                TO                   : 2,
                FIRST_PRODUCT_BARCODE: 3
            },

            SALES: {
                BARCODE      : 0,
                SALE_PRICE   : 1,
                FROM         : 2,
                PN_FROM      : 3,
                TO           : 4,
                PN_TO        : 5
            }
        },

        BULK_DATE_TIME_PARSE: {
            DATE   : '/',
            TIME   : ':',
            BETWEEN: ' '
        },

        BULK_UPDATE_OFFICIAL_FIELD_NAMES: {
            brand           : 'Brand',
            name            : 'Name',
            unit_id         : 'Unit Type',
            servingSize     : 'Serving Size',
            itemSize        : 'Item Size',
            servingPerPack  : 'Serving Per Pack',
            regularPrice    : 'Regular&Unit Prices',
            path            : 'Image Url',
            is_out_of_stock : 'Is out of stock'
        },

        SETTING_ID: 1,

        PUSH_TYPES: {
            REGULAR_PRICE_REDUCED             : 1,
            ITEM_ENABLED_MODE_CHANGED         : 2,
            CONCRETE_ITEM_ENABLED_MODE_CHANGED: 3,
            ITEM_OUT_OF_STOCK                 : 4,
            SHOP_ADDED                        : 5,
            SALE_START                        : 6,
            SALE_FINISH                       : 7
        },

        SHOP_BLOCKING_PERMISSIONS: {
            SALES: 0,
            ADVERTISING: 1
        } ,

        REPORT_TYPES: {
            MONTH : 0,
            BULK  : 1
        },

        DEVICE_LIMIT: 100,

        SERVICE_ID: 0,

        RECOVERY_PASSWORD_ALPHABET: 'abcdefghijklmnopqrstuvwxyz1234567890',
        RECOVERY_PASSWORD_LENGTH  : 6,

        MAX_IMAGE_SIZE: 2 * 1024 * 1024,

        MONTH_DATE_RANGE_LIMITS: {
            SALES: 3,
            ADVERTISING: 3,
            PRODUCTS: 1
        },

        UNIT_TYPES: {
            "" : null,
            ltr: 1,
            ml : 2,
            kg : 3,
            gr : 4,
            pc : 5
        },

        DELIVERY_PUSH_HOURS_LIMITS: {
            START: 10,
            END:   18,
            STEP:  4
        },

        ROLES: {
            SAVVY_SHOP           : {
                permissions: [
                    'SHOP', 'REVIEW_SHOP', 'EDIT_SHOP', 'ONLY_OWN_SHOP', 'HAS_ONE_SHOP', // SHOP
                    'ITEM', 'CREATE_ITEM', 'REVIEW_ITEM', 'EDIT_ITEM', // ITEM
                    'ADVERTISING', 'CREATE_ADVERTISING', 'REVIEW_ADVERTISING', 'EDIT_ADVERTISING', // ADVERTISING
                    'REPORT','REVIEW_REPORT', // REPORT
                    'BULK_UPLOAD', // BULK_UPLOAD
                    'PROFILE', // PROFILE
                    'REVIEW_CATEGORY' // CATEGORY
                ],
                type       : 'Savvy admin'
            },
            ENTERPRISE_SAVVY_SHOP: {
                permissions: [
                    'SHOP', 'CREATE_SHOP', 'REVIEW_SHOP', 'EDIT_SHOP', 'ONLY_OWN_SHOP', // SHOP
                    'ITEM', 'CREATE_ITEM', 'REVIEW_ITEM', 'EDIT_ITEM', // ITEM
                    'ADVERTISING', 'CREATE_ADVERTISING', 'REVIEW_ADVERTISING', 'EDIT_ADVERTISING', // ADVERTISING
                    'REVIEW_ADMIN', 'ASSIGN_SHOP', 'REVIEW_CATEGORY', // CATEGORY
                    'REPORT','REVIEW_REPORT', 'EDIT_REPORT', // REPORT
                    'BULK_UPLOAD', // BULK_UPLOAD
                    'ACTIVITY_REVIEW', // ACTIVITY
                    'PROFILE' // PROFILE
                ],
                type       : 'Enterprise admin'
            },
            SHOP                 : {
                permissions: [
                    'SHOP', 'CREATE_SHOP', 'REVIEW_SHOP', 'EDIT_SHOP', 'NOT_HAS_SHOP', 'ASSIGN_SHOP', 'BLOCK_PERMISSIONS_TO_SHOP', // SHOP
                    'ITEM', 'CREATE_ITEM', 'REVIEW_ITEM', 'EDIT_ITEM',  // ITEM
                    'ADVERTISING', 'CREATE_ADVERTISING', 'REVIEW_ADVERTISING', 'EDIT_ADVERTISING', // ADVERTISING
                    'ACTIVITY_REVIEW','REVIEW_ADMIN', // ADMIN
                    'REPORT','REVIEW_REPORT', 'EDIT_REPORT', // REPORT
                    'PROFILE', // PROFILE
                    'REVIEW_CATEGORY' // CATEGORY
                ],
                type       : 'Shop admin'
            },
            USER                 : {
                permissions: [
                    'USER', 'CREATE_USER', 'REVIEW_USER', 'EDIT_USER', // USER
                    'ADMIN', 'CREATE_ADMIN', 'REVIEW_ADMIN', 'EDIT_ADMIN', 'ASSIGN_SHOP', 'EXCEPT_SUPER_ADMIN', // ADMIN
                    'ACTIVITY_REVIEW', // ACTIVITY
                    'PROFILE' // PROFILE
                ],
                type       : 'User admin'
            },
            STATISTIC            : {
                permissions: [
                    'STATISTIC', 'REVIEW_STATISTIC', // STATISTIC
                    'ACTIVITY_REVIEW', // ACTIVITY
                    'PROFILE' // PROFILE
                ],
                type       : 'Statistics admin'
            },
            SUPER_ADMIN          : {
                permissions: [
                    'SHOP', 'CREATE_SHOP', 'REVIEW_SHOP', 'EDIT_SHOP', 'HAS_ONE_SHOP', 'BLOCK_PERMISSIONS_TO_SHOP', // SHOP
                    'ITEM', 'CREATE_ITEM', 'REVIEW_ITEM', 'EDIT_ITEM',  // ITEM
                    'ADMIN', 'CREATE_ADMIN', 'REVIEW_ADMIN', 'EDIT_ADMIN', 'ASSIGN_SHOP', // ADMIN
                    'USER', 'CREATE_USER', 'REVIEW_USER', 'EDIT_USER', // USER
                    'STATISTIC', 'REVIEW_STATISTIC', // STATISTIC
                    'CATEGORY', 'CREATE_CATEGORY', 'REVIEW_CATEGORY', 'EDIT_CATEGORY', // CATEGORY
                    'ADVERTISING', 'CREATE_ADVERTISING', 'REVIEW_ADVERTISING', 'EDIT_ADVERTISING', 'EDIT_ADWORDS',// ADVETISING
                    'ACTIVITY_REVIEW', 'UPDATE_ADMIN_PASSWORD', // ACTIVITY
                    'REPORT','REVIEW_REPORT', 'EDIT_REPORT', // REPORT
                    'BULK_UPLOAD', // BULK_UPLOAD
                    'PROFILE' // PROFILE
                ],
                type       : 'Super admin'
            }
        }
    },

    root     : root,
    reports  : path.join(root, 'reports/'),

    push     : {
        apn: {
            cert       : path.join(root, apn[env].cert),
            key        : path.join(root, apn[env].cert),
            defaultData: {
                expiry: 24 * 3600,
                sound : 'ping.aiff'
            }
        },
        gcm: {
            id         : 'AIzaSyB196NE3X4n7rn84_1t5zl6r5SQQwv_iH8',
            options    : {},
            defaultData: {
                delayWhileIdle: false,
                timeToLive    : 24 * 3600,
                retries       : 1
            }
        }
    }

};

module.exports = config;
