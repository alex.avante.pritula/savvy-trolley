var config = require('../../config');
var models = require('../models');

var fs = require('fs');
var sThree = require('../utils/s-tree');
var request = require('request');
var uuid = require('uuid');
var path = require('path');

var kue = require('kue');
var redis = require('../utils/redisClient');
kue.redis.createClient = function(){
    return redis;
};

var Queue = kue.createQueue();

var Promise = require('bluebird');
var unlink = Promise.promisify(fs.unlink);

Queue.process('uploadImageWorker',function(job, done){
    var url = job.data.url;
    var itemId = job.data.itemId;
    var result = request(url);
    result.on('response', function (response) {

        var fileName = response.headers['content-type'].split('/');
        if (fileName[0] !== 'image'){
            done();
            return;
        };

        var filename = uuid.v1() + '.' + fileName[1];
        var filepath = path.join(config.root, '/tmp/', filename);
        var writeStream = fs.createWriteStream(filepath);

        result.pipe(writeStream).on('close', function () {
            var options = {
                fileName: filename,
                path    : filepath
            };
            sThree.uploadImage(options).then(function(result){
            	var options = {
		            url : result.Location,
		            name: result.Key
		        };
        		return models.images.create(options);
            }).then(function(image){
                return models.items.update({
                    image_id: image.id
                },{
                    where: {id: itemId}
                });
            }).catch(function(error){
                console.warn(error);
            }).finally(function(){
                done();
                unlink(filepath);
            });
        });
    });
});
