var pushService = require('../../server/utils/pushService');
var kue = require('kue');
var redis = require('../utils/redisClient');
var logger = require('../utils/logger');

kue.redis.createClient = function(){return redis.client};
var Queue = kue.createQueue();

Queue.process('deliveryNotificationWorker', function(job, done) {
  let {ids , title, token}   = job.data;
  logger.log('info', 'Sending ' + title + ' push = ', job.data);
  pushService.send({
    message: {
      title: title,
      ids  : ids
    }
  }, {
    recipients: [{token: token}]
  });
  return done();
});
