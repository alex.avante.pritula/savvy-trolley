module.exports = {
    admin                : require('./adminServices'),
    advertising          : require('./advertisingServices'),
    device               : require('./deviceServices'),
    history              : require('./historyServices'),
    item                 : require('./itemServices'),
    postCode             : require('./postCodeServices'),
    shop                 : require('./shopServices'),
    shopping             : require('./shoppingServices'),
    statistic            : require('./statisticService'),
    upload               : require('./uploadServices'),
    report               : require('./reportService'),
    user                 : require('./userServices'),
    wish                 : require('./wishServices'),
    bulkUpload           : require('./bulkUploadServices'),
    deliveryNotification : require('./deliveryNotificationService')
};