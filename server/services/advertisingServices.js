var db = require('../utils/sequelize');
var models = require('../models');

var Promise = db.Promise;

var CHECK_MAXIMUM_ADVERT_FOR_POSTCODE = [
	'SELECT',
	'COUNT(`adv`.`item_id`) >= 5 AS `hasMaximum`',
	'FROM advertising AS adv',
	'INNER JOIN `postCodes` as `postCode` ON `adv`.`post_code_id` = `postCode`.`id`',
	'WHERE `adv`.`item_id` = :itemId AND `postCode`.`code` = :postCode',
	'GROUP BY `adv`.`item_id`,`adv`.`post_code_id`'
].join(' ');

/**
 * Add recommends for item.
 *
 * @param {Object} options
 * @param {Number} options.itemId
 * @param {String} options.postCode
 * @param {Number}  options.productId
 * @param {Number} [options.from]
 * @param {Number} [options.to]
 * @return {Promise}
 */
function addAdvertising(options) {
	if(!options) {
		var error = new Error('invalid arguments');
		error.status = 400;
		return Promise.reject(error);
	}
	if(!options.itemId) {
		var error = new Error('itemId is required');
		error.status = 400;
		return Promise.reject(error);
	}
	if(!options.postCode) {
		var error = new Error('postCode is required');
		error.status = 400;
		return Promise.reject(error);
	}
	if(!options.productId) {
		var error = new Error('productId is required');
		error.status = 400;
		return Promise.reject(error);
	}

	return checkMaximum(options.itemId, options.postCode)
		.then(function () {
			var settings = {where: {code: options.postCode}};
			return models.postCodes.findOrCreate(settings);
		})
		.then(function (data) {
			var advertisingData = {
				item_id     : options.itemId,
				post_code_id: data[0].id,
				product_id  : options.productId,
				from        : options.from || Date.now(),
				to          : options.to || Date.now() + ONE_DAY
			};
			return models.advertising.create(advertisingData);
		});
}

/**
 * Get advertising.
 *
 * @param {Number} itemId
 * @param {Object} order
 * @param {Number} [limit]
 * @param {Number} [offset]
 * @returns {Promise}
 */
function getAdvertising(itemId, order, limit, offset, adminId) {
	var filterByAdmin = "";
	if(adminId) filterByAdmin = 'INNER JOIN shopByAdmins AS shopByAdmin ON shopByAdmin.shop_id = shop.id AND shopByAdmin.admin_id = :adminId';

	var sql = [
		'SELECT',
		'`item`.`id` AS `id`,',
		'`item`.`name` AS `name`,',
		'`shop`.`id` AS `choice.shopId`,',
		'`shop`.`name` AS `choice.shopName`,',
		'`shop`.`postCode` AS `choice.postCode`,',
        '`shop`.`hasAdvertising` AS `choice.hasAdvertising`,',
        '`shop`.`contactEmail` AS `choice.contactEmail`,',
		'`adv`.`product_id` AS `choice.productId`,',
		'`adv`.`from` AS `choice.from`,',
		'`adv`.`to` AS `choice.to`',
		'FROM advertising AS adv',
		'INNER JOIN itemByShops AS ibs ON ibs.id = adv.product_id AND adv.item_id = :itemId',
		'INNER JOIN items AS item ON ibs.item_id = item.id',
		'INNER JOIN shops AS shop ON ibs.shop_id = shop.id',
		filterByAdmin,
		'LEFT OUTER JOIN images AS image ON item.image_id = image.id',
		'GROUP BY item.id',
		'ORDER BY `' + order.by + '` ' + order.at,
		'LIMIT :limit',
		'OFFSET :offset'
	].join(' ');

	var options = {
		type        : db.QueryTypes.SELECT,
		replacements: {
			itemId: itemId,
			limit : limit || 25,
			offset: offset || 0,
			now   : Date.now(),
			adminId: adminId
		}
	};

	return db.query(sql, options);
}

/**
 * Return count of adverts
 *
 * @param {Number} itemId
 * @returns {Promise.<Number>}
 */
function getCountAdvertising(itemId, adminId) {
	if(!itemId) {
		var error = new Error('itemId is required');
		error.status = 400;
		return Promise.reject(error);
	}
	var options = {
		type        : db.QueryTypes.SELECT,
		replacements: {
			itemId: itemId,
			adminId: adminId
		}
	};
	var filterByAdmin = "";
	if(adminId) filterByAdmin = "INNER JOIN `itemByShops`  AS `itemByShop` ON `itemByShop`.`id` = `adv`.`product_id` "+
	"INNER JOIN `shopByAdmins` AS `shopByAdmin` ON `shopByAdmin`.`shop_id` = `itemByShop`.`shop_id` AND `shopByAdmin`.`admin_id` = :adminId";
	
	var sql = [
        'SELECT',
        'COUNT(`adv`.`id`) AS `totalCount`',
        'FROM `advertising` AS `adv`',
        filterByAdmin,
		'WHERE `adv`.`item_id` = :itemId;'
    ].join(' ');

	return db.query(sql, options)
		.then(function (rawLines) {
			var result = rawLines ? rawLines[0].totalCount : 0;
			return db.Promise.resolve(result);
		});
}

/**
 * Return recommends for item (you may also like).
 *
 * @param itemId
 * @param postCodes
 * @param [limit]
 * @param [offset]
 * @returns {Promise}
 */
function getRecommends(itemId, postCodes, limit, offset) {
	var sql = [
		'SELECT',
		'`item`.`id` AS `id`,',
		'`item`.`barCode` AS `barCode`,',
		'`item`.`name` AS `name`,',
		'`item`.`brand` AS `brand`,',
		'`image`.`url` AS `image`,',
		'`shop`.`id` AS `choice.shopId`,',
		'`shop`.`name` AS `choice.shopName`,',
		'`ibs`.`id` AS `choice.id`,',
		'`ibs`.`regularPrice` AS `choice.regularPrice`,',
		'`ibs`.`salePrice` AS `choice.salePrice`,',
		'`ibs`.`unitPrice` AS `choice.unitPrice`,',
		'IF(DATE(FROM_UNIXTIME(`ibs`.`from`/1000)) > CURDATE() OR `shop`.`hasSales` = FALSE, 0, `ibs` .`isSale`) as `choice.isSale`',
		'FROM advertising AS adv',
		'INNER JOIN itemByShops AS ibs ON ibs.id = adv.product_id AND adv.item_id = :itemId',
		postCodes ? 'AND `adv`.`post_code_id` IN (:postCodes)' : '',
		'INNER JOIN items AS item ON ibs.item_id = item.id',
		'INNER JOIN shops AS shop ON ibs.shop_id = shop.id', // AND shop.hasAdvertising = TRUE
		'LEFT OUTER JOIN images AS image ON item.image_id = image.id',
		'WHERE adv.from < :now AND adv.to > :now',
		'GROUP BY item.id',
		'ORDER BY ibs.regularPrice',
		'LIMIT :limit',
		'OFFSET :offset'
	].join(' ');
	var options = {
		type        : db.QueryTypes.SELECT,
		replacements: {
			itemId   : itemId,
			limit    : limit || 10,
			offset   : offset || 0,
			postCodes: Array.isArray(postCodes) ? postCodes : [postCodes],
			now      : Date.now()
		}
	};
	return db.query(sql, options);
}

/**
 * Check limitation for create adverts.
 *
 * @param {Number} itemId
 * @param {String} postCode
 * @returns {Promise}
 */
function checkMaximum(itemId, postCode) {
	var options = {
		type        : db.QueryTypes.SELECT,
		replacements: {
			itemId  : itemId,
			postCode: Array.isArray(postCode) ? postCode : [postCode],
			now     : Date.now()
		}
	};
	return db.query(CHECK_MAXIMUM_ADVERT_FOR_POSTCODE, options)
		.then(function (response) {
			var hasMaximum = response.length != 0 ? response[0]['hasMaximum'] : false;
			if (hasMaximum) {
				var error = new Error('Item has maximum recommendations for this region!');
				error.status = 417;
				throw error;
			}
			return false;
		});
}

function updateAdvertisingByShop(shopId){

	var postCodeId;
	return models.shops.findById(shopId)
		.then(function(shop){
			if (!shop) {
                var error = new Error('Shop not found!');
                error.status = 404; 
                throw error;
            }
			return models.postCodes.find({ where: { code: shop.postCode }})
		})
		.then(function(postCode){
			if (!postCode) {
                var error = new Error('postCode not found!');
                error.status = 404; 
                throw error;
            }
			postCodeId = postCode.id;
			var sql = [
				'SELECT `ibs`.`id` FROM itemByShops as `ibs`',
				'INNER JOIN advertising as `adv` ON `adv`.`product_id`= `ibs`.`id`',
				'WHERE `ibs`.`shop_id` = :shopId'
			].join(' ');

			var options = {
				type        : db.QueryTypes.SELECT,
				replacements: { shopId: shopId }
			};
			return db.query(sql, options);
		}).then(function(result){
			var productIds = result.map(function(item){ return item.id; });

			return models.advertising.update({
				post_code_id: postCodeId
			},{
				where: {
					product_id: {
						in : productIds
					}
				}
			});
		});
}

module.exports = {
	updateAdvertisingByShop : updateAdvertisingByShop,
	addAdvertising     		: addAdvertising,
	getAdvertising     		: getAdvertising,
	getCountAdvertising		: getCountAdvertising,
	getRecommends      		: getRecommends,
	checkMaximum       		: checkMaximum
};