var db = require('../utils/sequelize');
var config = require('../../config');

var GET_SHOPS_BY_ITEM_SQL = [
	'SELECT',
	'`shops`.`id` AS `id`,',
	'`shops`.`name` AS `name`,',
	'`image`.`url` AS `image`,',
	'`shops`.`owner` AS `owner`,',
	'`shops`.`abn` AS `abn`,',
	'`shops`.`acn` AS `acn`,',
	'`shops`.`address` AS `address`,',
	'`shops`.`contactName` AS `contactName`,',
	'`shops`.`contactPhone` AS `contactPhone`,',
	'`shops`.`contactEmail` AS `contactEmail`,',
	'`shops`.`postCode` AS `postCode`,',
	'`shops`.`longitude` AS `longitude`,',
	'`shops`.`latitude` AS `latitude`,',
	'`shops`.`isLocal` AS `isLocal`,',
	'`price`.`id` AS `price.id`,',
	'`price`.`regularPrice` AS `price.regularPrice`,',
	'`price`.`salePrice` AS `price.salePrice`,',
	'`price`.`unitPrice` AS `price.unitPrice`,',
	'IF(DATE(FROM_UNIXTIME(`price`.`from`/1000)) > CURDATE() OR `shops`.`hasSales` = FALSE, 0, `price` .`isSale`) as `price.isSale`,',
	'`price`.`enabled` AS `price.enabled`,',
	'`price`.`is_out_of_stock` AS `price.isOutOfStock`',
	'FROM `shops` AS `shops`',
	'INNER JOIN `itemByShops` AS `price` ON `shops`.`id` = `price`.`shop_id`',
	'AND `shops`.`postCode` IN (:postCodes)',
	'AND `price`.`item_id` = :id',
	'INNER JOIN `pricesForEnableItem` AS `bestPrice` ON `shops`.`id` = `bestPrice`.`shop_id`',
	'AND `price`.`item_id` = `bestPrice`.`item_id`',
	'LEFT JOIN `images` AS `image` ON `shops`.`image_id` = `image`.`id`',
	'ORDER BY bestPrice.bestPrice',
	'LIMIT :limit OFFSET :offset;'
].join(' ');

var GET_OPTIMAL_SHOPS_SQL = [
	'SELECT',
	'`shop`.id,',
	'`shop`.`name`,',
	'`shop`.`postCode`,',
	'`shop`.`latitude` AS `location.latitude`,',
	'`shop`.`longitude` AS `location.longitude`,',
	'`shop`.`isLocal` AS `isLocal`,',
	'`image`.`url` AS `image`,',
	'ROUND(SUM(`prices`.`bestPrice`), 2) AS `price`,',
	'COUNT(`ibs`.`id`) AS`itemCount`',
	'FROM `shops` AS`shop`',
	'INNER JOIN `itemByShops` AS`ibs` ON `shop`.`id` = `ibs`.`shop_id` and `ibs`.`item_id` in (:items)',
	'INNER JOIN `pricesForEnableItem` AS`prices` ON `shop`.`id` = `prices`.`shop_id` and `ibs`.`item_id` = `prices`.`item_id`',
	'LEFT JOIN `images` AS `image` ON `shop`.`image_id` = `image`.`id`',
	'WHERE `shop`.`postCode` IN (:postCodes)',
	'GROUP BY `shop`.`id`',
	'HAVING `itemCount` = :count',
	'ORDER BY `price` ASC',
	'LIMIT :limit',
	'OFFSET :offset;'
].join(' ');

var GET_OPTIMAL_SHOP_FOR_SINGLE_ITEM_SQL = [
	'SELECT',
	'`item`.`id` AS `itemId`,',
	'`shop`.`id` as `shopId`',
	'FROM `items` AS `item`',
	'INNER JOIN `itemByShops` AS `ibs` ON `ibs`.`item_id` = `item`.`id`',
	'INNER JOIN `shops` AS `shop` ON `shop`.`id` = `ibs`.`shop_id` AND `shop`.`postCode` IN (:postCodes)',
	'INNER JOIN `pricesForEnableItem` AS `price` ON `item`.`id` = `price`.`item_id` AND `price`.`shop_id` = `shop`.`id`',
    'LEFT JOIN `images` as `image` ON `item`.`image_id` = `image`.`id`',
    'WHERE item.id in (:items)',
	'GROUP BY `item`.`id`'
].join(' ');

/**
 * Get shops for item.
 *
 * @param {Object} options
 * @param {Number} options.id
 * @param {Array.<String>} options.postCodes
 * @param {Number} [options.limit = 10]
 * @param {Number} [options.offset = 0]
 * @returns {Promise}
 */
function getShopsForItem(options) {
	options = Object.assign({}, {
		limit : config.constants.LIMIT,
		offset: config.constants.STEP
	}, options);
	var settings = {
		type        : db.QueryTypes.SELECT,
		replacements: options
	};

	return db.query(GET_SHOPS_BY_ITEM_SQL, settings);
}

/**
 * Get optimal shop for items.
 *
 * @param {Object} data
 * @param {Array.<Number>} data.items
 * @param {Array.<String>} data.postCodes
 * @param {Object} [settings]
 * @param {Number} [settings.limit]
 * @param {Number} [settings.offset]
 * @returns {Promise}
 */
function getOptimalShops(data, settings) {
	settings = settings || {};
	var options = {
		type        : db.QueryTypes.SELECT,
		replacements: {
			count    : data.items.length,
			items    : data.items,
			limit    : settings.limit || config.constants.SINGLE_LIMIT,
			offset   : settings.offset || config.constants.STEP,
			postCodes: data.postCodes
		}
	};
	return db.query(GET_OPTIMAL_SHOPS_SQL, options);
}

/**
 * Get optimal shops for items. Best shop for one item.
 *
 * @param {Object} data
 * @param {Array.<Number>} data.items
 * @param {Array.<String>} data.postCodes
 * @param {Object} [settings]
 * @param {Number} [settings.limit]
 * @param {Number} [settings.offset]
 * @returns {Promise}
 */
function getOptimalShopForSingleItems(data, settings) {
	settings = settings || {};
	var options = {
		type        : db.QueryTypes.SELECT,
		replacements: {
			items    : data.items,
			postCodes: data.postCodes,
			limit    : settings.limit || config.constants.LIMIT,
			offset   : settings.offset || config.constants.STEP
		}
	};
	return db.query(GET_OPTIMAL_SHOP_FOR_SINGLE_ITEM_SQL, options);
}


function getShops(data, settings) {
  settings = settings || {};

  var ownShopsFilter = '';
  if (data.adminId){
      ownShopsFilter= 'INNER JOIN `shopByAdmins` as `saById` ON `saById`.`shop_id` = `shop`.`id`  AND `saById`.`admin_id` = :adminId';
  }

  var postCodesFilter = '';
  if (data.postCodes){
      postCodesFilter = 'AND `shop`.`postCode` IN (:postCodes)';
  }

  var sorting = '';
  if (data.sortBy){
      sorting = [
          'ORDER BY :sortBy ',
          data.sortAt === 'ASC' ? 'ASC': 'DESC'
      ].join(' ');
  }

  var adminEmailFilter = '';
  if (data.adminEmail){
      adminEmailFilter = [
          'INNER JOIN `admins` as `admin` ON `admin`.`email` LIKE :adminEmail',
          'INNER JOIN `shopByAdmins` as `saByEmail` ON `saByEmail`.`shop_id` = `shop`.`id` AND `saByEmail`.`admin_id` = `admin`.`id`'
      ].join(' ');
  }

  var GET_SHOPS = [
      'SELECT',
      '`shop`.`id` as `id`,',
      '`image`.`url` as `image`,',
      '`shop`.`name` as `name`,',
      '`shop`.`owner` as `owner`,',
      '`shop`.`postCode` as `postCode`,',
      '`shop`.`createdAt` as `createdAt`,',
      '`shop`.`hasSales` as `hasSales`,',
	  '`shop`.`hasAdvertising` as `hasAdvertising`,',
	  '`shop`.`updatedAt` as `updatedAt`',
      'FROM `shops` AS `shop`',
      'LEFT JOIN `images` AS `image` ON `shop`.`image_id` = `image`.`id`',
      ownShopsFilter,
      adminEmailFilter,
      'WHERE `shop`.`name` LIKE :name',
      postCodesFilter,
      'GROUP BY `shop`.`id`',
      sorting,
      'LIMIT :limit',
      'OFFSET :offset;'
  ].join(' ');

  var GET_SHOPS_COUNT = [
      'SELECT COUNT(*) as `count`',
      'FROM ( SELECT `shop`.`id` as `id`',
      'FROM `shops` AS `shop`',
       ownShopsFilter,
       adminEmailFilter,
      'WHERE `shop`.`name` LIKE :name',
      'GROUP BY `shop`.`id`',
      postCodesFilter,
      ') as `shops`'
  ].join(' ');

  var options = {
      type          : db.QueryTypes.SELECT,
      replacements: {
          sortBy    : data.sortBy,
          name      : '%' + data.query +'%',
          adminId   : data.adminId,
          adminEmail: '%' + data.adminEmail +'%',
          postCodes : data.postCodes,
          limit     : settings.limit || config.constants.LIMIT,
          offset    : settings.offset || config.constants.STEP
      }
  };

  var result = {
      shops: [],
      meta: {
        totalCount: 0
      }
  };


  return db.query(GET_SHOPS, options)
    .then(function (shops){
        result.shops = shops;
        return db.query(GET_SHOPS_COUNT, options);
    })
    .then(function (data) {
        result['meta'].totalCount = data[0].count;
        return result;
    });
}

module.exports = {
  getShops                    : getShops,
  getShopsForItem             : getShopsForItem,
  getOptimalShops             : getOptimalShops,
  getOptimalShopForSingleItems: getOptimalShopForSingleItems
};
