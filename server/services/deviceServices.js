var db = require('../utils/sequelize');

var GET_DEVICES_WHERE_PRODUCT_WISHES_SQL = [
    'SELECT device.token',
    'FROM postCodes AS postCode',
    'INNER JOIN postCodeByUsers AS pbu ON pbu.post_code_id = postCode.id AND postCode.code = :postCode',
    'INNER JOIN users AS user ON pbu.user_id = user.id AND user.hasNotifications= TRUE',
    'INNER JOIN wishes AS wish ON wish.user_id = user.id AND wish.item_id = :itemId',
    'INNER JOIN devices AS device ON user.id = device.user_id',
    'LIMIT :limit',
    'OFFSET :offset;'
].join(' ');

var COUNT_DEVICES_WHERE_PRODUCT_WISHES_SQL = [
    'SELECT COUNT(device.token) AS `count`',
    'FROM postCodes AS postCode',
    'INNER JOIN postCodeByUsers AS pbu ON pbu.post_code_id = postCode.id AND postCode.code = :postCode',
    'INNER JOIN users AS user ON pbu.user_id = user.id AND user.hasNotifications= TRUE',
    'INNER JOIN wishes AS wish ON wish.user_id = user.id AND wish.item_id = :itemId',
    'INNER JOIN devices AS device ON user.id = device.user_id;'
].join(' ');

var GET_DEVICES_WHERE_ITEM_DISABLED_SQL = [
    'SELECT',
    '`device`.`token` AS `token`,',
    '`wish`.`id` IS NOT NULL AS `isItemInWishes`,',
    '`shopping`.`id` IS NOT NULL AS `isItemInTrolley`',
    'FROM `devices` AS `device`',
    'INNER JOIN `users` AS `user` ON `user`.`id` = `device`.`user_id` AND user.hasNotifications= TRUE',
    'LEFT OUTER JOIN `wishes` AS `wish` ON `wish`.`user_id` = `user`.`id` AND `wish`.`item_id` = :itemId',
    'LEFT OUTER JOIN (`shopping` INNER JOIN `itemByShops` AS `shoppingList` ON `shoppingList`.`id` = `shopping`.`item_by_shop_id`)',
    'ON `shopping`.`user_id` = `user`.`id` AND `shoppingList`.`item_id` = :itemId',
    'WHERE (`wish`.`id` IS NOT NULL OR `shopping`.`id` IS NOT NULL)',
    'LIMIT :limit',
    'OFFSET :offset;'
].join(' ');


var COUNT_DEVICES_WHERE_ITEM_DISABLED_SQL = [
    'SELECT COUNT(`device`.`token`) AS `count`',
    'FROM `devices` AS `device`',
    'INNER JOIN `users` AS `user` ON `user`.`id` = `device`.`user_id` AND user.hasNotifications= TRUE',
    'LEFT OUTER JOIN `wishes` AS `wish` ON `wish`.`user_id` = `user`.`id` AND `wish`.`item_id` = :itemId',
    'LEFT OUTER JOIN (`shopping` INNER JOIN `itemByShops` AS `shoppingList` ON `shoppingList`.`id` = `shopping`.`item_by_shop_id`)',
    'ON `shopping`.`user_id` = `user`.`id` AND `shoppingList`.`item_id` = :itemId',
    'WHERE (`wish`.`id` IS NOT NULL OR `shopping`.`id` IS NOT NULL);'
].join(' ');

var GET_DEVICES_WHERE_PRODUCT_DISABLED_SQL = [
    'SELECT',
    '`device`.`token` AS `token`',
    'FROM `devices` AS `device`',
    'INNER JOIN `users` AS `user` ON `user`.`id` = `device`.`user_id` AND user.hasNotifications= TRUE',
    'LEFT OUTER JOIN (`shopping` INNER JOIN `itemByShops` AS `shoppingList` ON `shoppingList`.`id` = `shopping`.`item_by_shop_id`)',
    'ON `shopping`.`user_id` = `user`.`id` AND `shoppingList`.`id` = :productId',
    'WHERE (`shopping`.`id` IS NOT NULL)',
    'LIMIT :limit',
    'OFFSET :offset;'
].join(' ');

var COUNT_DEVICES_WHERE_PRODUCT_DISABLED_SQL = [
    'SELECT COUNT(`device`.`token`) AS `count`',
    'FROM `devices` AS `device`',
    'INNER JOIN `users` AS `user` ON `user`.`id` = `device`.`user_id` AND user.hasNotifications= TRUE',
    'LEFT OUTER JOIN (`shopping` INNER JOIN `itemByShops` AS `shoppingList` ON `shoppingList`.`id` = `shopping`.`item_by_shop_id`)',
    'ON `shopping`.`user_id` = `user`.`id` AND `shoppingList`.`id` = :productId',
    'WHERE (`shopping`.`id` IS NOT NULL);'
].join(' ');

var GET_DEVICES_WHERE_ITEM_OUT_OF_STOCK_SQL = [
    'SELECT',
    '`device`.`token` AS `token`,',
    '`wish`.`id` IS NOT NULL AS `isItemInWishes`,',
    '`shopping`.`id` IS NOT NULL AS `isItemInTrolley`',
    'FROM `postCodes` AS `postCode`',
    'INNER JOIN `postCodeByUsers` AS `pbu` ON `pbu`.`post_code_id` = `postCode`.`id` AND `postCode`.`code` = :postCode',
    'INNER JOIN `users` AS `user` ON `pbu`.`user_id` = `user`.`id` AND user.hasNotifications= TRUE',
    'INNER JOIN `devices` AS `device` ON `user`.`id` = `device`.`user_id`',
    'LEFT OUTER JOIN `wishes` AS `wish` ON `wish`.`user_id` = `user`.`id` AND `wish`.`item_id` = :itemId',
    'LEFT OUTER JOIN (`shopping` INNER JOIN `itemByShops` AS `shoppingList` ON `shoppingList`.`id` = `shopping`.`item_by_shop_id`)',
    'ON `shopping`.`user_id` = `user`.`id` AND `shoppingList`.`item_id` = :itemId AND `shoppingList`.`shop_id` = :shopId',
    'WHERE (`wish`.`id` IS NOT NULL OR `shopping`.`id` IS NOT NULL) ',
    'GROUP BY `device`.`token`',
    'LIMIT :limit',
    'OFFSET :offset;'
].join(' ');

var COUNT_DEVICES_WHERE_ITEM_OUT_OF_STOCK_SQL = [
    'SELECT',
    'COUNT(`device`.`token`) AS `count`',
    'FROM `postCodes` AS `postCode`',
    'INNER JOIN `postCodeByUsers` AS `pbu` ON `pbu`.`post_code_id` = `postCode`.`id` AND `postCode`.`code` = :postCode',
    'INNER JOIN `users` AS `user` ON `pbu`.`user_id` = `user`.`id` AND user.hasNotifications= TRUE',
    'INNER JOIN `devices` AS `device` ON `user`.`id` = `device`.`user_id`',
    'LEFT OUTER JOIN `wishes` AS `wish` ON `wish`.`user_id` = `user`.`id` AND `wish`.`item_id` = :itemId',
    'LEFT OUTER JOIN (`shopping` INNER JOIN `itemByShops` AS `shoppingList` ON `shoppingList`.`id` = `shopping`.`item_by_shop_id`)',
    'ON `shopping`.`user_id` = `user`.`id` AND `shoppingList`.`item_id` = :itemId AND `shoppingList`.`shop_id` = :shopId',
    'WHERE (`wish`.`id` IS NOT NULL OR `shopping`.`id` IS NOT NULL)',
    'GROUP BY `token`;'
].join(' ');

var GET_DEVICES_WHERE_SHOP_POSTCODE_ARE_SQL = [
    'SELECT device.token',
    'FROM postCodes AS postCode',
    'INNER JOIN postCodeByUsers AS pbu ON pbu.post_code_id = postCode.id AND postCode.code = :postCode',
    'INNER JOIN users AS user ON pbu.user_id = user.id AND user.hasNotifications= TRUE',
    'INNER JOIN devices AS device ON user.id = device.user_id',
    'LIMIT :limit',
    'OFFSET :offset;'
].join(' ');

var COUNT_DEVICES_WHERE_SHOP_POSTCODE_ARE_SQL = [
    'SELECT COUNT(device.token) AS `count`',
    'FROM postCodes AS postCode',
    'INNER JOIN postCodeByUsers AS pbu ON pbu.post_code_id = postCode.id AND postCode.code = :postCode',
    'INNER JOIN users AS user ON pbu.user_id = user.id AND user.hasNotifications= TRUE',
    'INNER JOIN devices AS device ON user.id = device.user_id;'
].join(' ');


/**
 * Return devices where item in wishes.
 * Uses for user wishes item changed.
 *
 * @param {Object} options
 * @param {Number} options.itemId
 * @param {String} options.postCode
 * @param {Number} options.limit
 * @param {Number} options.offset
 * @returns {Promise}
 */
function getDevicesPerWishes(options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return db.query(GET_DEVICES_WHERE_PRODUCT_WISHES_SQL, selectOptions);
}

/**
 * Return count of devices where item in wishes.
 * Uses for user wishes item changed.
 *
 * @param {Object} options
 * @param {Number} options.itemId
 * @param {String} options.postCode
 * @param {String} options.pushType
 * @returns {Promise}
 */
function countDevicesPerWishes(options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };

    return db.query(COUNT_DEVICES_WHERE_PRODUCT_WISHES_SQL, selectOptions)
        .then(function (data) {
            return data.length ? data[0].count : 0;
        });
}

/**
 * Return devices where item in wishes or shopping list.
 * Uses if item has been disabled.
 *
 * @param {Object} options
 * @param {Number} options.itemId
 * @param {Number} options.limit
 * @param {Number} options.offset
 * @returns {Promise}
 */
function getDevicesPerItem(options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return db.query(GET_DEVICES_WHERE_ITEM_DISABLED_SQL, selectOptions);
}

/**
 * Return devices where item in wishes or shopping list.
 * Uses if item has been disabled.
 *
 * @param {Object} options
 * @param {Number} options.itemId
 * @returns {Promise}
 */
function countDevicesPerItem(options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return db.query(COUNT_DEVICES_WHERE_ITEM_DISABLED_SQL, selectOptions)
        .then(function (data) {
            return data.length ? data[0].count : 0;
        });
}

/**
 * Return devices where item in shopping list.
 * Uses if product has been disabled.
 *
 * @param {Object} options
 * @param {Number} options.productId
 * @param {Number} options.limit
 * @param {Number} options.offset
 * @returns {Promise}
 */
function getDevicesPerConcreteItem(options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return db.query(GET_DEVICES_WHERE_PRODUCT_DISABLED_SQL, selectOptions);
}

/**
 * Return devices where item in shopping list.
 * Uses if product has been disabled.
 *
 * @param {Object} options
 * @param {Number} options.productId
 * @returns {Promise}
 */
function countDevicesPerConcreteItem(options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return db.query(COUNT_DEVICES_WHERE_PRODUCT_DISABLED_SQL, selectOptions)
        .then(function (data) {
            return data.length ? data[0].count : 0;
        });
}

/**
 * Return devices for item with shop.
 *
 * @param options
 * @param options.postCode
 * @param options.itemId
 * @param options.shopId
 * @param options.limit
 * @param options.offset
 * @returns {Promise}
 */
function getDevicesForItemWithShop(options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return db.query(GET_DEVICES_WHERE_ITEM_OUT_OF_STOCK_SQL, selectOptions);
}

/**
 * Return count of devices for item with shop.
 *
 * @param options
 * @param options.postCode
 * @param options.itemId
 * @param options.shopId
 * @returns {Promise}
 */
function countDevicesForItemWithShop(options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return db.query(COUNT_DEVICES_WHERE_ITEM_OUT_OF_STOCK_SQL, selectOptions)
        .then(function (data) {
            return data.length ? data[0].count : 0;
        });
}

/**
 * Return devices for shop with post code.
 *
 * @param options
 * @param options.postCode
 * @param options.shopId
 * @param options.limit
 * @param options.offset
 * @returns {Promise}
 */
function getDevicesForShopWithPostCode(options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return db.query(GET_DEVICES_WHERE_SHOP_POSTCODE_ARE_SQL, selectOptions);
}

/**
 * Return count of devices for shop with post code.
 *
 * @param options
 * @param options.postCode
 * @param options.shopId
 * @returns {Promise}
 */
function countDevicesForShopWithPostCode(options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return db.query(COUNT_DEVICES_WHERE_SHOP_POSTCODE_ARE_SQL, selectOptions)
        .then(function (data) {
            return data.length ? data[0].count : 0;
        });
}

module.exports = {
    getDevicesPerWishes  : getDevicesPerWishes,
    countDevicesPerWishes: countDevicesPerWishes,

    getDevicesPerItem  : getDevicesPerItem,
    countDevicesPerItem: countDevicesPerItem,

    getDevicesPerConcreteItem  : getDevicesPerConcreteItem,
    countDevicesPerConcreteItem: countDevicesPerConcreteItem,

    getDevicesForItemWithShop  : getDevicesForItemWithShop,
    countDevicesForItemWithShop: countDevicesForItemWithShop,

    getDevicesForShopWithPostCode  : getDevicesForShopWithPostCode,
    countDevicesForShopWithPostCode: countDevicesForShopWithPostCode
};
