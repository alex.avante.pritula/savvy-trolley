var models = require('../models');
const logger = require('../utils/logger');

(function () {
   var filter = {
      where: {
          to: {
              lt:  new Date().getTime()
          }
      }
   };

    logger.log('info', 'Disabled sales', new Date());
    models.itemByShop.update({ isSale: 0 }, filter)
      .then(function (result) {
          console.warn(result);
      }).catch(function (error) {
          console.warn(error);
      }).finally(function(){
      	process.exit();
      })
})();

