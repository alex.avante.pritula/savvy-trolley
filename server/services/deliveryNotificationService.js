const Promise = require('bluebird');
const db = require('../utils/sequelize');
const models = require('../models');

var GET_DEVICES_WHERE_PRODUCT_WISHES_SQL = [
    'SELECT device.token,',
    '`device`.`device_id` AS `device_id`,',
    '`device`.`user_id` AS `user_id`,',
    '`wish`.`id` IS NOT NULL AS `isItemInWishes`',
    'FROM postCodes AS postCode',
    'INNER JOIN postCodeByUsers AS pbu ON pbu.post_code_id = postCode.id AND postCode.code = :postCode',
    'INNER JOIN users AS user ON pbu.user_id = user.id AND user.hasNotifications= TRUE',
    'INNER JOIN wishes AS wish ON wish.user_id = user.id AND wish.item_id = :itemId',
    'INNER JOIN devices AS device ON user.id = device.user_id;'
].join(' ');

var GET_DEVICES_WHERE_ITEM_DISABLED_SQL = [
    'SELECT',
    '`device`.`token` AS `token`,',
    '`device`.`device_id` AS `device_id`,',
    '`device`.`user_id` AS `user_id`,',
    '`wish`.`id` IS NOT NULL AS `isItemInWishes`,',
    '`shopping`.`id` IS NOT NULL AS `isItemInTrolley`',
    'FROM `devices` AS `device`',
    'INNER JOIN `users` AS `user` ON `user`.`id` = `device`.`user_id` AND user.hasNotifications= TRUE',
    'LEFT OUTER JOIN `wishes` AS `wish` ON `wish`.`user_id` = `user`.`id` AND `wish`.`item_id` = :itemId',
    'LEFT OUTER JOIN (`shopping` INNER JOIN `itemByShops` AS `shoppingList` ON `shoppingList`.`id` = `shopping`.`item_by_shop_id`)',
    'ON `shopping`.`user_id` = `user`.`id` AND `shoppingList`.`item_id` = :itemId',
    'WHERE (`wish`.`id` IS NOT NULL OR `shopping`.`id` IS NOT NULL);'
].join(' ');

var GET_DEVICES_WHERE_PRODUCT_DISABLED_SQL = [
    'SELECT',
    '`device`.`token` AS `token`,',
    '`device`.`device_id` AS `device_id`,',
    '`device`.`user_id` AS `user_id`,',
    '`shopping`.`id` IS NOT NULL AS `isItemInTrolley`',
    'FROM `devices` AS `device`',
    'INNER JOIN `users` AS `user` ON `user`.`id` = `device`.`user_id` AND user.hasNotifications= TRUE',
    'LEFT OUTER JOIN (`shopping` INNER JOIN `itemByShops` AS `shoppingList` ON `shoppingList`.`id` = `shopping`.`item_by_shop_id`)',
    'ON `shopping`.`user_id` = `user`.`id` AND `shoppingList`.`id` = :productId',
    'WHERE (`shopping`.`id` IS NOT NULL);'
].join(' ');

var GET_DEVICES_WHERE_ITEM_OUT_OF_STOCK_SQL = [
    'SELECT',
    '`device`.`token` AS `token`,',
    '`device`.`device_id` AS `device_id`,',
    '`device`.`user_id` AS `user_id`,',
    '`wish`.`id` IS NOT NULL AS `isItemInWishes`,',
    '`shopping`.`id` IS NOT NULL AS `isItemInTrolley`',
    'FROM `postCodes` AS `postCode`',
    'INNER JOIN `postCodeByUsers` AS `pbu` ON `pbu`.`post_code_id` = `postCode`.`id` AND `postCode`.`code` = :postCode',
    'INNER JOIN `users` AS `user` ON `pbu`.`user_id` = `user`.`id` AND user.hasNotifications= TRUE',
    'INNER JOIN `devices` AS `device` ON `user`.`id` = `device`.`user_id`',
    'LEFT OUTER JOIN `wishes` AS `wish` ON `wish`.`user_id` = `user`.`id` AND `wish`.`item_id` = :itemId',
    'LEFT OUTER JOIN (`shopping` INNER JOIN `itemByShops` AS `shoppingList` ON `shoppingList`.`id` = `shopping`.`item_by_shop_id`)',
    'ON `shopping`.`user_id` = `user`.`id` AND `shoppingList`.`item_id` = :itemId AND `shoppingList`.`shop_id` = :shopId',
    'WHERE (`wish`.`id` IS NOT NULL OR `shopping`.`id` IS NOT NULL) ',
    'GROUP BY `device`.`token`;'
].join(' ');

var GET_DEVICES_WHERE_SHOP_POSTCODE_ARE_SQL = [
    'SELECT `device`.`token` as `token`,',
    '`device`.`device_id` AS `device_id`,',
    '`device`.`user_id` AS `user_id`',
    'FROM `devices` as `device`',
    'INNER JOIN `users` as `user` ON `device`.`user_id` = `user`.`id` AND `user`.`hasNotifications`= TRUE',
    'INNER JOIN `postCodeByUsers` as `pcu` ON  `pcu`.`user_id` = `user`.`id`',
    'INNER JOIN `postCodes` as `postCode` ON `postCode`.`id` = `pcu`.`post_code_id` AND `postCode`.`code` = :postCode'
].join(' ');


var GET_PUSH_NOTIFICATIONS_SQL = [
    'SELECT',
    'dpn.*, d.token, d.timezone',
    'FROM deliveryNotifications AS dpn',
    'INNER JOIN `devices` AS d ON d.user_id = dpn.user_id AND d.device_id = dpn.device_id',
    'WHERE `is_sent` = 0 AND delivery_date_time <= NOW() + INTERVAL (d.timezone * 60) MINUTE;'
].join(' ');

/**
 * Create notification
 *
 * @param {Object} settings
 * @param {String} settings.entityType
 * @param {Array}  settings.userData
 * @param {String} settings.pushType
 * @param {String} settings.entityId
 * @param {String} settings.data
 * @param {String} settings.date
 * @returns {Promise}
 */

function createNotification(settings) {
    if (!settings) {
        error = new Error('invalid arguments');
        error.status = 400;
        return Promise.reject(error);
    }

    if (!settings.entityId) {
        error = new Error('entityId is required');
        error.status = 400;
        return Promise.reject(error);
    }

    if (!settings.userData) {
        error = new Error('userData is required');
        error.status = 400;
        return Promise.reject(error);
    }

    if (!settings.entityType) {
        error = new Error('entityType is invalid');
        error.status = 400;
        return Promise.reject(error);
    }

    if (!settings.pushType) {
        error = new Error('pushType is invalid');
        error.status = 400;
        return Promise.reject(error);
    }

    var data = {
        entity_type       : settings.entityType,
        push_type         : settings.pushType,
        entity_id         : settings.entityId,
        shop_id           : settings.shopId,
        admin_id          : settings.adminId,
        delivery_date_time: settings.deliveryDateTime
    };
    var userData = settings.userData;
    var message =  settings['data'].message;
    var queryData = userData.map(function(user){

        var serializeData = Object.assign({}, message, {
            isItemInWishes:  (!!user['isItemInWishes']),
            isItemInTrolley: (!!user['isItemInTrolley'])
        });

        var tempData = Object.assign({
            user_id  : user["user_id"],
            device_id: user["device_id"],
            data: JSON.stringify(serializeData)
        }, data);
        return tempData;
    });

    return models.deliveryNotifications.bulkCreate(queryData,{});
}


/**
 * Return devices where item in wishes.
 * Uses for user wishes item changed.
 *
 * @param {Object} options
 * @param {Number} options.itemId
 * @param {String} options.postCode
 * @param {Number} options.limit
 * @param {Number} options.offset
 * @returns {Promise}
 */
function getDevicesPerWishes(options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return db.query(GET_DEVICES_WHERE_PRODUCT_WISHES_SQL, selectOptions);
}

/**
 * Return devices where item in wishes or shopping list.
 * Uses if item has been disabled.
 *
 * @param {Object} options
 * @param {Number} options.itemId
 * @param {Number} options.limit
 * @param {Number} options.offset
 * @returns {Promise}
 */
function getDevicesPerItem(options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return db.query(GET_DEVICES_WHERE_ITEM_DISABLED_SQL, selectOptions);
}

/**
 * Return devices where item in shopping list.
 * Uses if product has been disabled.
 *
 * @param {Object} options
 * @param {Number} options.productId
 * @param {Number} options.limit
 * @param {Number} options.offset
 * @returns {Promise}
 */
function getDevicesPerConcreteItem(options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return db.query(GET_DEVICES_WHERE_PRODUCT_DISABLED_SQL, selectOptions);
}

/**
 * Return devices for item with shop.
 *
 * @param options
 * @param options.postCode
 * @param options.itemId
 * @param options.shopId
 * @param options.limit
 * @param options.offset
 * @returns {Promise}
 */
function getDevicesForItemWithShop(options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return db.query(GET_DEVICES_WHERE_ITEM_OUT_OF_STOCK_SQL, selectOptions);
}

function getPushNotifaicationsData() {
    var selectOptions = {
        type        : db.QueryTypes.SELECT
    };

    return db.query(GET_PUSH_NOTIFICATIONS_SQL, selectOptions);
}

/**
 * Return devices for shop with post code.
 *
 * @param options
 * @param options.postCode
 * @param options.shopId
 * @param options.limit
 * @param options.offset
 * @returns {Promise}
 */
function getDevicesForShopWithPostCode(options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return db.query(GET_DEVICES_WHERE_SHOP_POSTCODE_ARE_SQL, selectOptions);
}

/**
 * set as is sent notification
 *
 * @param id
 * @returns {Promise}
 */
function setIsSentNotification(ids) {
    var isSent = 1;
    return models.deliveryNotifications.update({
        is_sent: isSent
    },{
        where: {
            id: {
                in: ids
            }
        }
    });
}

module.exports = {
    createNotification: createNotification,
    getPushNotifaicationsData: getPushNotifaicationsData,
    getDevicesPerWishes: getDevicesPerWishes,
    getDevicesPerItem: getDevicesPerItem,
    getDevicesPerConcreteItem: getDevicesPerConcreteItem,
    getDevicesForItemWithShop: getDevicesForItemWithShop,
    getDevicesForShopWithPostCode: getDevicesForShopWithPostCode,
    setIsSentNotification: setIsSentNotification
};
