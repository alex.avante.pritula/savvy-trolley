var Promise = require("bluebird");
var models = require('../models');
var config = require('../../config');
var db = require('../utils/sequelize');
var moment = require('moment');
var XLSX = require('xlsx');
var sThree = require('../utils/s-tree');


function saveReport(user, report){
    var filename = ( 'Report by ' + user.email + ' : ' + report.name + ' ' + moment().format('DD MMMM YYYY, hh:mm:ss A')).replace(',', ' ');
    var options = {
        bookType: 'xlsx',
        bookSST: false, 
        type: 'buffer'
    };
    var xlsxOut = XLSX.write(report.data, options);
    var settings = {fileName: filename.replace(/(\/)|(\s)|,/g,'_') + '.xlsx'};

    return sThree.upload(settings ,xlsxOut).then(function(result){
        var data = {
            name    : filename,
            admin_id: user.id,
            type    : report.type,
            shop_id : report.shopId,
            url     : result.Location
        };
        return models.reports.create(data);
    });
}

function getReports(options){

    var typeFilter = '';
    if (typeof options.type != 'undefined'){
        typeFilter = 'AND `reports`.`type` = :type';
    }

    var permissionFilter = ['' , ''];
    if (options.adminId){
        permissionFilter[0] = 'LEFT JOIN `shopByAdmins` as `shopByAdmin` ON `reports`.`shop_id` = `shopByAdmin`.`shop_id`';
        permissionFilter[1] = 'AND `shopByAdmin`.`admin_id` = :adminId OR (`reports`.`shop_id` IS NULL AND `reports`.`admin_id` = :adminId)';
    }

    var shopFilter = '';
    if (options.query){
        shopFilter = 'INNER JOIN `shops` ON `shops`.`id` = `reports`.`shop_id` AND `shops`.`name` LIKE :shopName';
    }

    var orderFilter = '';
    if (options.sortBy){
        orderFilter = 'ORDER BY `reports`.`' + options.sortBy  +'` ';
        if (options.sortAt) orderFilter +=  (options.sortAt === 'ASC' ? 'ASC' : 'DESC');
    }

    var adminFilter = '';
    if (options.email){
       adminFilter = 'AND `admins`.`email` LIKE :email';
    }

    var GET_REPORTS = [
        'SELECT ' +
        '`reports`.`id` as `id`,',
        '`reports`.`name` as `name`,',
        '`reports`.`url` as `url`,',
        '`reports`.`createdAt` as `createdAt`,',
        '`admins`.`email` as `email`',
        'FROM `reports`',
        'INNER JOIN `admins` ON `admins`.`id` = `reports`.`admin_id`',
        adminFilter,
        permissionFilter[0],
        shopFilter,
        'WHERE DATE(`reports`.`createdAt`) >= DATE(:from) AND DATE(`reports`.`createdAt`) <= DATE(:to)',
        typeFilter,
        permissionFilter[1],
        orderFilter,
        'LIMIT :limit',
        'OFFSET :offset;'
    ].join(' ');

   var settings = {
        type        : db.QueryTypes.SELECT,
        replacements: {
            limit   : options.limit,
            offset  : options.offset,
            type    : options.type,
            adminId : options.adminId,
            shopName: '%' + options.query + '%',
            from    : options.from,
            to      : options.to,
            email   : '%' + options.email + '%'
        }
    };

   var COUNT_ALL_REPORTS = [
        'SELECT COUNT(*) as `count`',
        'FROM `reports`',
        'INNER JOIN `admins` ON `admins`.`id` = `reports`.`admin_id`',
        adminFilter,
        permissionFilter[0],
        shopFilter,
        'WHERE DATE(`reports`.`createdAt`) >= DATE(:from) AND DATE(`reports`.`createdAt`) <= DATE(:to)',
        permissionFilter[1],
        typeFilter
   ].join(' ');

    var items = [];
    return db.query(GET_REPORTS, settings)
        .then(function(reports){
            items = reports;
            return db.query(COUNT_ALL_REPORTS, settings);
        }).then(function(totalCount){
            return {
                items: items,
                meta : {totalCount: totalCount[0].count}
            }
        });
}

module.exports  = {
    saveReport: saveReport,
    getReports: getReports
};
