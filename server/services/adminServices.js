var Promise = require('bluebird');
var db = require('../utils/sequelize');
var models = require('../models');
var validators = require('../validators');
var logger = require('../utils/logger');
var config = require('../../config');
var crypt = require('../utils/crypt');

var GET_FREE_ADMIN_SQL = [
    'SELECT ',
    '`admin`.`id`,',
    '`admin`.`email`,',
    '`admin`.`type`,',
    '`admin`.`role`,',
    '`admin`.`createdAt`,',
    '`admin`.`updatedAt`,',
    'count(`shops.shopByAdmin`.`id`) AS `shopCount`',
    'FROM `admins` AS `admin`',
    'LEFT OUTER JOIN (`shopByAdmins` AS `shops.shopByAdmin` INNER JOIN `shops` AS `shops` ON `shops`.`id` = `shops.shopByAdmin`.`shop_id`)',
    'ON `admin`.`id` = `shops.shopByAdmin`.`admin_id`',
    'WHERE `admin`.`role` in (:roles) AND `email` like :email',
    'GROUP BY `admin`.`id`',
    'HAVING `shopCount` = 0',
    'LIMIT :limit',
    'OFFSET :offset;'
].join(' ');

/**
 * Get free admins
 *
 * @param {Object} options
 * @param {String} options.email - filtration options
 * @param {Array<String>} options.roles - filtration options
 * @param {Number} [options.limit]
 * @param {Number} [options.offset]
 * @returns {Promise}
 */
function getFreeAdmin(options) {
    if(!options) {
        error = new Error('invalid arguments');
        error.status = 400;
        return Promise.reject(error);
    }
    if(!options.email) {
        error = new Error('email is required');
        error.status = 400;
        return Promise.reject(error);
    }
    if(!options.roles || options.roles.length == 0) {
        error = new Error('roles is required');
        error.status = 400;
        return Promise.reject(error);
    }
    options = Object.assign({}, options, {limit: config.constants.LIMIT, offset: config.constants.STEP});

    var settings = {
        type        : db.QueryTypes.SELECT,
        replacements: options,
        model       : db.models.admins
    };
    return db.query(GET_FREE_ADMIN_SQL, settings);
}

/**
 * Read permissions from db.
 *
 * @param {Array<String>} permissions
 * @returns {Promise}
 */
function getPermissions(permissions) {
    if (!permissions || permissions.length <= 0) {
        var error = new Error('Invalid permissions!');
        error.status = 400;
        return Promise.reject(error);
    }
    var options = {
        where     : {
            permission: {$in: permissions}
        },
        attributes: ['id', 'permission']
    };
    return models.permissions.all(options);
}

/**
 * Get admin
 *
 * @param {Number} id
 * @param {Object} [admin]
 * @returns {Promise}
 */
function getAdmin(id, admin) {
    if (admin && id == admin.id) {
        return Promise.resolve(admin);
    }
    var options = {
        include: [{
            model: models.permissions,
            as   : 'permissions'
        }]
    };
    return models.admins.findById(id, options);
}

/**
 * Create admin
 *
 * @param {Object} settings
 * @param {String} settings.email
 * @param {String} settings.password
 * @param {String} settings.role
 * @param {String} settings.type
 * @param {Object} [settings.transaction]
 * @returns {Promise}
 */
function createAdmin(settings) {
    if(!settings) {
        error = new Error('invalid arguments');
        error.status = 400;
        return Promise.reject(error);
    }
    if(!settings.email) {
        error = new Error('email is required');
        error.status = 400;
        return Promise.reject(error);
    }
    if(!settings.password) {
        var error = new Error('password is required');
        error.status = 400;
        return Promise.reject(error);
    }
    if(!settings.role) {
        error = new Error('role is required');
        error.status = 400;
        return Promise.reject(error);
    }
    if(!settings.type) {
        error = new Error('type is required');
        error.status = 400;
        return Promise.reject(error);
    }

    var role = config.constants.ROLES[settings.role];
    if (!role) {
        error = new Error('role is invalid');
        error.status = 400;
        return Promise.reject(error);
    }
    var options = {transaction: settings.transaction};
    var data = {
        email   : settings.email,
        password: crypt.cryptPassword(settings.password),
        type    : settings.type,
        role    : settings.role
    };
    var temp = {};
    return models
        .admins
        .create(data, options)
        .then(function (admin) {
            temp.admin = admin;
            return getPermissions(role.permissions)
        })
        .then(function (permissions) {
            var admin = temp.admin;
            temp.permissions = permissions;
            return admin.addPermissions(permissions, options);
        })
        .then(function () {
            var admin = temp.admin.toJSON();
            admin.permissions = temp.permissions.map(function (permission) {
                return permission.permission;
            });
            delete admin.password;

            return admin
        });
}

/**
 * Change admin role (permissions)
 *
 * @param {Object} settings
 * @param {Number} settings.adminId
 * @param {String} settings.role
 * @param {Object} [settings.transaction]
 * @returns {Promise}
 */
function updateAdmin(settings) {
    if(!settings) {
        error = new Error('invalid arguments');
        error.status = 400;
        return Promise.reject(error);
    }
    if(!settings.adminId) {
        error = new Error('adminId is required');
        error.status = 400;
        return Promise.reject(error);
    }
    if(!settings.role) {
        error = new Error('role is required');
        error.status = 400;
        return Promise.reject(error);
    }
    var role = config.constants.ROLES[settings.role];
    if (!role) {
        error = new Error('role is invalid');
        error.status = 400;
        return Promise.reject(error);
    }
    var adminId = settings.adminId;
    var options = {transaction: settings.transaction};
    var temp = {};

    return models
        .permissionByAdmins
        .destroy({where: {admin_id: adminId}}, options)
        .then(function () {
            return models.admins.findById(adminId, {attributes: {exclude: ['password']}})
        })
        .then(function (admin) {
            if (!admin) {
                var error = new Error('Admin not found!');
                error.status = 404;
                throw error;
            }
            admin.type = role.type;
            admin.role = settings.role;
            if(settings.password) admin.password = settings.password;
            return admin.save(options);
        })
        .then(function (admin) {
            temp.admin = admin;
            return getPermissions(role.permissions)
        })
        .then(function (permissions) {
            var admin = temp.admin;
            temp.permissions = permissions;
            return admin.addPermissions(permissions, options);
        })
        .then(function () {
            var admin = temp.admin.toJSON();
            admin.permissions = temp.permissions.map(function (permission) {
                return permission.permission;
            });

            return admin
        });
}

module.exports = {
    createAdmin     : createAdmin,
    getAdmin        : getAdmin,
    getFreeAdmin    : getFreeAdmin,
    getPermissions  : getPermissions,
    updateAdmin     : updateAdmin
};