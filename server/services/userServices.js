var uuid = require('uuid');
var util = require('util');
var async = require('async');
var Promise = require('bluebird');

var postCodeServices = require('./postCodeServices');
var models = require('../models');
var Cache = require('../utils/cache');
var logger = require('../utils/logger');
var config = require('../../config');
var crypt = require('../utils/crypt');
var sequelize = require('../utils/sequelize');
var mailService = require('../utils/mailService');

var cache = new Cache(config.redis);

/**
 * Resend email for confirm registration
 *
 * @throws Error
 * @param user
 * @returns {Promise}
 */
function reConfirm(user) {
    logger.log('info', 'reConfirm start');
    var now = Date.now();

    var data = { where: { user_id: user.id, isActive: false } };
    return models.confirmCodes.findOne(data)
        .then(function (code) {
            if (!code) {
                var error = new Error(config.constants.ERROR_MESSAGES.NOT_VERIFIED);
                error.status = 401;
                throw error;
            }
            logger.log('info', util.format('confirmCode', code.code));
            return mailService.verifyMessage(code.code, user.email);
        })
        .then(function () {
            logger.log('info', util.format('reConfirm', Date.now() - now));
            var error = new Error(config.constants.ERROR_MESSAGES.NOT_VERIFIED);
            error.status = 401;
            throw error;
        });
}

/**
 * Change user postcodes
 *
 * @param user
 * @param codes
 * @returns {Promise}
 */
function changePostCodes(user, codes) {
    if (!user || !user.id) {
        var error = new Error('User is required.');
        error.status = 400;
        return Promise.reject(error);
    }

    if (!codes) {
        error = new Error('Post codes is required.');
        error.status = 400;
        return Promise.reject(error);
    }

    for (var i = 0; i < codes.length; i++) {
        for (var j = 0; j < codes.length; j++) {
            if (i != j && codes[i].code === codes[j].code) {
                error = new Error('This postcode is already added');
                error.status = 400;
                return Promise.reject(error);
            }
        }
    }

    var hash = 'USER_' + user.id;
    cache.remove(hash, function () {
    });
    return sequelize.transaction(function (transaction) {
        return _changeCodes(transaction);
    });

    function _changeCodes(transaction) {
        if (!user) {
            error = new Error('User not found!');
            error.status = 404;
            throw error;
        }
        id = user.id;

        return models.postCodeByUser
            .destroy({where: {user_id: id}}, {transaction: transaction})
            .then(function () {
                var promises = [];
                codes.forEach(function (code) {
                    var postCodePromise = postCodeServices.addPostCode(user, code, transaction);
                    promises.push(postCodePromise);
                });
                return Promise.all(promises);
            })
    }
}

/**
 * Create tokens (access, refresh)
 *
 * @param user
 * @returns {Promise}
 */
function createTokens(user) {
    if (!user) {
        error = new Error('invalid arguments');
        error.status = 400;
        return Promise.reject(error);
    }
    if (!user.id) {
        error = new Error('id is required');
        error.status = 400;
        return Promise.reject(error);
    }
    if (!user.email) {
        var error = new Error('email is required');
        error.status = 400;
        return Promise.reject(error);
    }
    if (!user.accountType) {
        error = new Error('accountType is required');
        error.status = 400;
        return Promise.reject(error);
    }

    var data = {
        token  : uuid.v1(),
        user_id: user.id
    };
    return models.tokens.create(data)
        .then(function (token) {
            var accessData = {
                id   : user.id,
                email: user.email,
                type : user.accountType,
                token: token.token
            };
            if (accessData.type == 'email') {
                if (!user.password) {
                    error = new Error('password is required');
                    error.status = 400;
                    return Promise.reject(error);
                }
                accessData.checkSum = crypt.cryptPassword(user.password);
            }

            var refreshToken = {
                id   : user.id,
                token: token.token
            };
            logger.log('info', 'accessData', accessData, 'refreshToken', refreshToken);
            return {
                accessToken : crypt.createToken(accessData, config.constants.ACCESS_TOKEN_LIFE_TIME),
                refreshToken: crypt.createToken(refreshToken, config.constants.REFRESH_TOKEN_LIFE_TIME)
            };
        });
}

/**
 * Refresh user tokens
 *
 * @param access_token
 * @param refresh_token
 * @param done
 */
function refresh(access_token, refresh_token, done) {
    async.waterfall([
        _encryptTokens,
        _getTokenData,
        _refresh
    ], done);


    function _encryptTokens(done) {
        async.series({
            access : async.apply(crypt.encryptTokenAsync, access_token, true),
            refresh: async.apply(crypt.encryptTokenAsync, refresh_token, false)
        }, function (err, result) {
            if (err) {
                err.status = 403;
                return done(err);
            }
            logger.log('info', 'refresh', '_encryptTokens', result);
            var access = result.access;
            var refresh = result.refresh;
            if (access.id != refresh.id) {
                err = new Error('Tokens are not connected!');
                err.status = 400;
                return done(err);
            }

            done(null, refresh);
        });
    }

    function _getTokenData(refresh, done) {
        var query = {
            where  : {
                token  : refresh.token,
                user_id: refresh.id
            },
            include: [{model: models.users, as: 'owner'}]
        };
        models.tokens.findOne(query)
            .then(function (token) {
                if (!token) {
                    var err = new Error('Invalid refresh token!');
                    err.status = 400;
                    throw err;
                }
                logger.log('info', 'refresh', '_getTokenData', token);
                done(null, token);
                return null;
            })
            .catch(done);
    }

    function _refresh(info, done) {
        var user = info.owner;
        info.destroy()
            .then(function () {
                return createTokens(user);
            })
            .then(function (tokens) {
                done(null, tokens);
                return null;
            })
            .catch(done);
    }
}

/**
 * Format error for sign up.
 *
 * @param err
 * @returns {*}
 */
function errorFormatter(err, msg) {
    if (err.name === 'SequelizeUniqueConstraintError' && err.fields['email']) {
        msg = msg || 'Email already registered';
        var error = new Error(msg);
        error.status = 400;
        return error;
    }

    return err;
}

/**
 * Change notification mode (on/off).
 *
 * @param {Object} user
 * @return {Promise}
 */
function chengeNotificationMode(user, hasNotifications) {
    if (!user) {
        var error = new Error('User is requred!');
        error.status = 400;
        return Promise.reject(error);
    }
    user.hasNotifications = hasNotifications || false;
    return user.save();
}

module.exports = {
    reConfirm             : reConfirm,
    changePostCodes       : changePostCodes,
    chengeNotificationMode: chengeNotificationMode,
    createTokens          : createTokens,
    errorFormatter        : errorFormatter,
    refresh               : refresh
};