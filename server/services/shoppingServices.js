var models = require('../models');
var formatters = require('../formatters');
var db = require('../utils/sequelize');
var logger = require('../utils/logger');

var GET_SHOPPING_LIST_SQL = [
    'SELECT ',
    '`item`.`id` AS `id`,',
    '`item`.`barCode` AS `barCode`,',
    '`item`.`name` AS `name`,',
    '`item`.`brand` AS `brand`,',
    '`shopping`.`type` AS `type`,',
    '`ibs`.`id` AS `choice.id`,',
    '`ibs`.`regularPrice` AS `choice.regularPrice`,',
    '`ibs`.`salePrice` AS `choice.salePrice`,',
    '`ibs`.`unitPrice` AS `choice.unitPrice`,',
    'IF(DATE(FROM_UNIXTIME(`ibs`.`from`/1000)) > CURDATE() OR `shop`.`hasSales` = FALSE, 0, `ibs` .`isSale`) as `choice.isSale`,',
    '`ibs`.`is_out_of_stock` AS `choice.isOutOfStock`,',
    '`shop`.`id` AS `choice.shopId`,',
    '`shop`.`name` AS `choice.shopName`,',
    '`shop`.`postCode` AS `choice.postCode`,',
    '`shop`.`latitude` AS `location.latitude`,',
    '`shop`.`longitude` AS `location.longitude`,',
    '`shop`.`isLocal` AS `choice.isLocal`,',
    '`shopImage`.`url` AS `choice.shopImage`',
    'FROM `shopping` as `shopping`',
    'INNER JOIN `itemByShops` AS `ibs`  ON `shopping`.`item_by_shop_id` = `ibs`.`id` ',
    'AND `shopping`.`user_id` = :userId AND `ibs`.`enabled` = TRUE',
    'INNER JOIN `items` AS `item` ON `item`.`id` = `ibs`.`item_id` AND `item`.`enabled` = TRUE',
    'INNER JOIN `shops` AS `shop` ON `shop`.`id` = `ibs`.`shop_id`',
    'LEFT JOIN `images` AS `shopImage` ON `shop`.`image_id` = `shopImage`.`id`',
    'ORDER BY `shopping`.`createdAt` DESC',
    'LIMIT :limit',
    'OFFSET :offset;'
].join(' ');

var DELETE_INVALID_ITEM_FROM_SHOPPING_LIST_SQL = [
    'DELETE FROM `shopping`',
    'WHERE `shopping`.`user_id` = :userId',
    'AND `shopping`.`item_by_shop_id` NOT IN (',
    'SELECT `ibs`.`id`',
    'FROM `shops` AS `shop`',
    'INNER JOIN `itemByShops` AS `ibs` ON `ibs`.`shop_id` = `shop`.`id` AND `ibs`.`enabled` = TRUE',
    'INNER JOIN `items` AS `item` ON `ibs`.`item_id` = `item`.`id` AND `item`.`enabled` = TRUE',
    'WHERE `shop`.`postCode` IN (:postCodes));'
].join(' ');

var GET_SHOPPING_LIST_COUNT_SQL = [
    'SELECT',
    'COUNT(`shopping`.`id`) AS `totalCount`',
    'FROM `shopping`',
    'WHERE `shopping`.`user_id` = :userId;'
].join(' ');

/**
 * Get shopping list from db.
 * @param {Object} userData
 * @param {Number} userData.userId
 * @param {Array} userData.postCodes
 * @param {Object} options
 * @param {Number} options.userId
 * @param {Array} options.postCodes
 * @param {Number} options.limit
 * @param {Number} options.offset
 * @return {Promise}
 */
function getShoppingList(userData, options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };

    return removeInvalid(userData)
        .then(function () {
            return db.query(GET_SHOPPING_LIST_SQL, selectOptions)
        });
}

/**
 * Remove invalid data from shopping list
 * @param {Object} userData
 * @param {Number} userData.userId
 * @param {Array} userData.postCodes
 * @return {Promise}
 */
function removeInvalid(userData) {
    var options = {
        type        : db.QueryTypes.DELETE,
        replacements: userData
    };
    return db.query(DELETE_INVALID_ITEM_FROM_SHOPPING_LIST_SQL, options);
}

/**
 * Get shopping list count
 * @param {Object} userData
 * @param {Number} userData.userId
 * @param {Array} userData.postCodes
 * @return {Promise}
 */
function countUserShopping(userData) {
    var options = {
        type        : db.QueryTypes.SELECT,
        replacements: userData
    };
    return db.query(GET_SHOPPING_LIST_COUNT_SQL, options)
        .then(function (rawLines) {
            var result = rawLines ? rawLines[0] : null;
            return db.Promise.resolve(result);
        });
}

/**
 * Read shopping list
 * @param {Object} userData
 * @param {Number} userData.userId
 * @param {Array} userData.postCodes
 * @param {Object} options
 * @param {Number} options.userId
 * @param {Array} options.postCodes
 * @param {Number} options.limit
 * @param {Number} options.offset
 * @return {Promise}
 */
function read(userData, options) {
    var temp = {};
    return getShoppingList(userData, options)
        .then(function (rawResponse) {
            return formatters.item.shoppingList(rawResponse);
        })
        .then(function (result) {
            temp.shopping = result;
            return countUserShopping(userData);
        })
        .then(function (countShopping) {
            var response = {
                shopping: temp.shopping,
                meta    : {
                    totalCount: countShopping ? countShopping.totalCount : 0
                }
            };
            return response;
        });
}


/**
 * Update shopping list
 * @param {Object} user
 * @param {Number} user.id
 * @param {Array} addedItems
 * @param {Array} removedItems
 * @return {Promise}
 */
function update(user, addedItems, removedItems) {
    addedItems = addedItems || [];
    removedItems = removedItems || [];
    return getItems(removedItems)
        .then(function (items) {
            var removeData = {
                where: {
                    user_id        : user.id,
                    item_by_shop_id: {
                        $in: items.map(function (item) {
                            return item.id;
                        })
                    }
                }
            };
            return models.shopping.destroy(removeData);
        })
        .then(function () {
            return getItems(addedItems);
        })
        .then(function (items) {
            var addData = items.map(function (item) {
                var rawItem = addedItems.find(function (rawItem) {
                    return item.item_id === rawItem.itemId || item.shop_id === rawItem.shopId
                });
                return {
                    user_id        : user.id,
                    item_by_shop_id: item.id,
                    type           : rawItem.type
                };
            });
            return models.shopping.bulkCreate(addData, {ignoreDuplicates: true});
        });
}

/**
 * Return products
 * @param {Array} items
 * @return {Promise}
 */
function getItems(items) {
    items = items.map(function (item) {
        return {
            item_id: item.itemId,
            shop_id: item.shopId
        }
    });
    var options = {where: {$or: items}};
    return models.itemByShop.all(options);
}

module.exports = {
    read  : read,
    update: update
};