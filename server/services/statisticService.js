var util = require('util');
var db = require('../utils/sequelize');
var statisticFormatter = require('../formatters/statisticsFormatter');

var ONE_DAY = 1000 * 60 * 60 * 24;

/**
 * SQL queries
 * @type {string}
 */
var GET_USER_STATISTICS_SQL = [
    'SELECT',
    '`postCode`.`code` AS `postCode`,',
    'COUNT(`user`.`id`) AS `number_of_users`',
    'FROM `postCodes` AS `postCode`',
    'LEFT JOIN `postCodeByUsers` AS `pbu` ON `pbu`.`post_code_id` = `postCode`.`id`',
    'LEFT JOIN `users` AS `user` ON `pbu`.`user_id` = `user`.`id` AND `user`.`createdAt` BETWEEN :from AND :to',
    'WHERE `postCode`.`code` IN (:postCodes)',
    'GROUP BY `postCode`'
].join(' ');
var COUNT_USER_STATISTICS_SQL = [
    'SELECT',
    'count(`postCode`.`id`) AS `count`',
    'FROM `postCodes` AS `postCode`',
    'WHERE `postCode`.`code` IN (:postCodes);'
].join(' ');
var GET_WISHES_STATISTICS_SQL = [
    'SELECT',
    '`item`.`name`,',
    '`item`.`barCode`,',
    'COUNT(`wish`.`id`) AS `items_in_wish_list`',
    'FROM',
    '(SELECT `pbu`.`user_id` AS `id`',
    'FROM `postCodes` AS `postCode`',
    'INNER JOIN `postCodeByUsers` AS `pbu` ON `pbu`.`post_code_id` = `postCode`.`id` AND `postCode`.`code` IN (:postCodes)',
    'GROUP BY `pbu`.`user_id`) AS `user`',
    'INNER JOIN `wishes` AS `wish` ON `wish`.`user_id` = `user`.`id` and `wish`.`createdAt` BETWEEN :from AND :to',
    'INNER JOIN `items` AS `item` ON `item`.`id` = `wish`.`item_id`',
    'GROUP BY `item`.`id`'
].join(' ');
var COUNT_WISHES_STATISTICS_SQL = [
    'SELECT',
    'count(`id`) AS count',
    'FROM ',
    '(SELECT `item`.`id`',
    'FROM `postCodes` AS `postCode`',
    'INNER JOIN `postCodeByUsers` AS `pbu` ON `pbu`.`post_code_id` = `postCode`.`id` AND `postCode`.`code` IN (:postCodes)',
    'INNER JOIN `users` AS `user` ON `pbu`.`user_id` = `user`.`id`',
    'INNER JOIN `wishes` AS `wish` ON `wish`.`user_id` = `user`.`id` and `wish`.`createdAt` BETWEEN :from AND :to',
    'INNER JOIN `items` AS `item` ON `item`.`id` = `wish`.`item_id`',
    'GROUP BY `item`.`id`) AS `statistics`;'
].join(' ');
var GET_PURCHASED_STATISTICS_SQL = [
    'SELECT',
    '`item`.`name`,',
    '`item`.`barCode`,',
    'COUNT(`history`.`id`) AS `purchased_items`',
    'FROM ',
    '(SELECT `pbu`.`user_id` AS `id`',
    'FROM `postCodes` AS `postCode`',
    'INNER JOIN `postCodeByUsers` AS `pbu` ON `pbu`.`post_code_id` = `postCode`.`id` AND `postCode`.`code` IN (:postCodes)',
    'GROUP BY `pbu`.`user_id`) AS `user`',
    'INNER JOIN `histories` AS `history` ON `history`.`user_id` = `user`.`id` and `history`.`createdAt` BETWEEN :from AND :to',
    'INNER JOIN `items` AS `item` ON `item`.`id` = `history`.`item_id`',
    'GROUP BY `item`.`id`'
].join(' ');
var COUNT_PURCHASED_STATISTICS_SQL = [
    'SELECT',
    'count(`id`) AS count',
    'FROM ',
    '(SELECT `item`.`id`',
    'FROM `postCodes` AS `postCode`',
    'INNER JOIN `postCodeByUsers` AS `pbu` ON `pbu`.`post_code_id` = `postCode`.`id` AND `postCode`.`code` IN (:postCodes)',
    'INNER JOIN `users` AS `user` ON `pbu`.`user_id` = `user`.`id`',
    'INNER JOIN `histories` AS `history` ON `history`.`user_id` = `user`.`id` and `history`.`createdAt` BETWEEN :from AND :to',
    'INNER JOIN `items` AS `item` ON `item`.`id` = `history`.`item_id`',
    'GROUP BY `item`.`id`) AS `statistics`;'
].join(' ');

/**
 * Excel columns definitions
 *
 * @type {{POST_CODE: {header: string, key: string, width: number}, USER: {header: string, key: string, width: number}, ITEM: {header: string, key: string, width: number}, BAR_CODE: {header: string, key: string, width: number}, WISHES: {header: string, key: string, width: number}, HISTORY: {header: string, key: string, width: number}}}
 */
var COLUMNS_EXCEl = {
    POST_CODE: {header: 'Postcode', key: 'postCode', width: 10},
    USER     : {header: 'Number of users', key: 'numberOfUsers', width: 15},
    ITEM     : {header: 'Item name', key: 'name', width: 20},
    BAR_CODE : {header: 'Barcode', key: 'barCode', width: 15},
    WISHES   : {header: 'Items in wish list', key: 'itemsInWishList', width: 15},
    HISTORY  : {header: 'Purchased items', key: 'purchasedItems', width: 15}
};

/**
 * Statistics type definitions
 *
 * @type {{USERS: {GET_QUERY: string, HEADERS: string[], FORMATTER: users, COUNT_QUERY: string}, WISHES: {GET_QUERY: string, HEADERS: string[], FORMATTER: wishes, COUNT_QUERY: string}, HISTORY: {GET_QUERY: string, HEADERS: string[], FORMATTER: histories, COUNT_QUERY: string}}}
 */
var TYPES = {
    USERS  : {
        GET_QUERY  : GET_USER_STATISTICS_SQL,
        HEADERS    : ['POST_CODE', 'USER'],
        FORMATTER  : statisticFormatter.users,
        COUNT_QUERY: COUNT_USER_STATISTICS_SQL
    },
    WISHES : {
        GET_QUERY  : GET_WISHES_STATISTICS_SQL,
        HEADERS    : ['ITEM', 'BAR_CODE', 'WISHES'],
        FORMATTER  : statisticFormatter.wishes,
        COUNT_QUERY: COUNT_WISHES_STATISTICS_SQL
    },
    HISTORY: {
        GET_QUERY  : GET_PURCHASED_STATISTICS_SQL,
        HEADERS    : ['ITEM', 'BAR_CODE', 'HISTORY'],
        FORMATTER  : statisticFormatter.histories,
        COUNT_QUERY: COUNT_PURCHASED_STATISTICS_SQL
    }
};

/**
 * Sort definitions for statistics;
 *
 * @type {{POSTCODE: string, NUMBEROFUSERS: string, NAME: string, BARCODE: string, ITEMSINWISHLIST: string, PURCHASEDITEMS: string}}
 */
var ORDER = {
    POSTCODE       : 'postCode',
    NUMBEROFUSERS  : 'number_of_users',
    NAME           : 'name',
    BARCODE        : 'barCode',
    ITEMSINWISHLIST: 'items_in_wish_list',
    PURCHASEDITEMS : 'purchased_items'
};

function defaultOptions() {
    var now = Date.now();
    return {
        to    : new Date(now),
        from  : new Date(now - ONE_DAY),
        limit : 25,
        offset: 0
    };
}

/**
 *  Statistics sql query builder
 *
 * @param options
 * @returns {string}
 * @private
 */
function buildQuery(options) {
    var order = '';
    var type = options.type;
    var column = options.sortedBy || false;
    if (column) {
        var direction = options.sortedAt
            ? options.sortedAt
            : 'ASC';
        order = [
            'ORDER BY',
            column,
            direction
        ].join(' ');
    }
    return [
        TYPES[type].GET_QUERY,
        order,
        'LIMIT :limit',
        'OFFSET :offset'
    ].join(' ');
}

/**
 * Return statistics
 *
 * @param options
 * @param options.postCodes {Array | String}
 * @param options.type {String}
 * @param [options.to] {Date}
 * @param [options.from] {Date}
 * @param [options.limit] {Number}
 * @param [options.offset] {Number}
 * @param [options.sortedBy] {String}
 * @param [options.sortedAt] {String}
 */
function factory(options) {
    var settings = Object.assign({}, defaultOptions(), options);
    if (!settings.postCodes) {
        var error = new Error('Invalid post code');
        error.status = 400;
        throw error;
    }
    if (!settings.type) {
        error = new Error('Invalid type');
        error.status = 400;
        throw error;
    }

    settings.type = toUpperCase(settings.type);
    if (!TYPES[settings.type]) {
        error = new Error('Type not found!');
        error.status = 404;
        throw error;
    }

    settings.sortedBy = toUpperCase(settings.sortedBy);
    settings.postCodes = Array.isArray(settings.postCodes)
        ? settings.postCodes
        : [settings.postCodes];
    settings.sortedBy = ORDER[settings.sortedBy];

    var _sql = buildQuery(settings);
    return function (options) {
        var _options = {
            type        : db.QueryTypes.SELECT,
            replacements: Object.assign({}, settings, options)
        };
        return db
            .query(_sql, _options)
            .then(function (rawLines) {
                if (!rawLines || rawLines.length == 0) {
                    rawLines = [];
                }
                var type = TYPES[settings.type];
                return type.FORMATTER(rawLines);
            });
    }
}

/**
 * Count statistic entities
 *
 * @param settings
 * @param settings.type {String}
 * @returns {Promise.<Number>}
 */
function count(settings) {
    var options = Object.assign({}, settings);
    var type = toUpperCase(options.type);
    if (!type || !TYPES[type]) {
        var error = new Error('Type not found!');
        error.status = 404;
        return db.Promise.reject(error);
    }
    var _sql = TYPES[type].COUNT_QUERY;
    var _options = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return db
        .query(_sql, _options)
        .then(function (rawLines) {
            var result = rawLines
                ? rawLines[0].count
                : 0;
            return db.Promise.resolve(result);
        });
}

/**
 * Create columns for excel export
 * @param type {String}
 * @returns {Array}
 */
function buildExcelColumn(type) {
    type = toUpperCase(type);
    if (!type || !TYPES[type]) {
        var error = new Error('Type not found!');
        error.status = 404;
        throw error;
    }
    return TYPES[type].HEADERS.map(function (header) {
        return COLUMNS_EXCEl[header];
    });
}

/**
 *
 * @param string {String}
 * @returns {String|Boolean}
 */
function toUpperCase(string) {
    return string
        ? string.toUpperCase()
        : false;
}

module.exports = {
    factory         : factory,
    count           : count,
    types           : Object.keys(TYPES),
    buildExcelColumn: buildExcelColumn
};
