var fs = require('fs');
var path = require("path");
var models = require('../models');
var config = require('../../config');

function verifyItemsByTypes(items, report){
    var types = [];
    items.forEach(function(item, index){
        var reportItem = report[index];
        if (!item['unit_id'].length){
            reportItem['Unit Type'] = 'Unit Type is empty';
            delete item['unit_id'];
        } else {
            if (types.indexOf(item['unit_id']) === -1){
                types.push(item['unit_id']);
            }
        }

        if (!item['serving_unit_id'].length){
            reportItem['Serving Type'] = 'Serving Type is empty';
            delete item['serving_unit_id'];
        } else {
            if (items.indexOf(item['serving_unit_id']) === -1){
                types.push(item['serving_unit_id']);
            }
        }
    });

    var options = {where: {type: {$in: types}}};

    return models.units.all(options).then(function(types){
        items.forEach(function(item, index){
            var reportItem = report[index];

            if (!reportItem['Unit Type']) {
                var unitIndex = types.findIndex(function (unit) {
                    return unit['type'] === item['unit_id'];
                });
                if (unitIndex > -1){
                    item['unit_id'] = types[unitIndex].id;
                } else {
                    reportItem['Unit Type'] = 'Unit type: ' + item['unit_id'] + ' is not exist';
                    delete item['unit_id'];
                }
            }

            if (!reportItem['Serving Type']) {
                var unitIndex = types.findIndex(function (unit) {
                    return unit['type'] === item['serving_unit_id'];
                });
                if (unitIndex > -1){
                    item['serving_unit_id'] = types[unitIndex].id;
                } else {
                    reportItem['Serving Type'] = 'Serving unit type: ' + item['serving_unit_id'] + ' is not exist';
                    delete item['serving_unit_id'];
                }
            }

        });
    });
}


function verifyItemsByCategories(items, report, startingPoint){
	var categories = [];
	items.forEach(function(item ,index){
		var reportItem = report[index];
		var existCategories = item.categories.filter(function(cItem, cIndex){
			if (!cItem.length){
    			report[index].Categories.push('Category Field is empty (at column ' + (cIndex + startingPoint + 1) + ')');
				return false;
			}
			return true;
		});

		if (!existCategories.length){
			reportItem['Categories List'] = 'Valid categories list is empty';
			return;
		}

		categories.push.apply(categories, existCategories);
		item.categories = existCategories;
	});

	categories = Array.from(new Set(categories));

    var options = {
        where: {
            title: {$in: categories},
            parent_id: {$ne: null}
        }
    };

    return models.categories.all(options).then(function(categories){
    	items.forEach(function(item, index){
			var reportItem = report[index];
			if (reportItem['Categories List']){
                return;
            }
			var existCategories = item.categories;
			item.categories = [];

			existCategories.forEach(function(cItem){
				var cIndex = categories.findIndex(function(dbcItem){
					return dbcItem.title === cItem;
				});

				if (cIndex === -1){
                    report[index].Categories.push(cItem + ' is not exist from DB or is not subcategory');
                } else {
                    item.categories.push(categories[cIndex]);
                }
			});

			if (!item.categories.length){
                reportItem['Categories List'] = 'Valid categories list is empty';
            }
    	});
    });
}

function calculateUnitPrice(regularPrice, itemSize, unitId){
    const types = config.constants['UNIT_TYPES'];
    const priceLength = config.constants['PRICE_MAX_LENGTH'].INTEGER;

    var inc = 1;
    if (unitId === types['ml']   || unitId  === types['gr']){
       inc = 100;
    }
    if (unitId  === types['ltr'] || unitId  === types['kg']){
       inc = 0.1;
    }

    var unitPrice = parseFloat((Math.ceil(regularPrice/itemSize * inc * 100) / 100).toFixed(2));
    var unitPriceLength = unitPrice.toString().split('.')[0].length;

    if (unitPriceLength  > priceLength){
       return {result: "'Unit Price' field is out of range"};
    }
    return {result: unitPrice};
}


function getValidDate(date, controllDate) {
    var deliveryDate = new Date(date);
    var hour = deliveryDate.getHours();

    const limits = config.constants['DELIVERY_PUSH_HOURS_LIMITS'];

    if (hour < limits['START']) {
        deliveryDate.setHours(limits['START']);
    } else if (hour > limits['END']) {
        deliveryDate.setHours(limits['START']);
        deliveryDate.setDate(deliveryDate.getDate() + 1);
        var tempControllDate = new Date(controllDate);
        tempControllDate.setHours(limits['END'],0,0,0);
        if (tempControllDate < deliveryDate){
             return false;
        }
    } else {
        var remain = parseInt(hour - limits['START']) % parseInt(limits['STEP']);
        if (remain) {
            hour = (parseInt(hour - limits['START'] - remain) / limits['STEP'] + 1) * limits['STEP'] + limits['START'];
            deliveryDate.setHours(hour);
        }
    }
    return deliveryDate;
}


module.exports  = {
    getValidDate           : getValidDate,
    calculateUnitPrice     : calculateUnitPrice,
    verifyItemsByTypes	   : verifyItemsByTypes,
    verifyItemsByCategories: verifyItemsByCategories
};
