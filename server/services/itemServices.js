var Promise = require('bluebird');
var logger = require('../utils/logger');
var db = require('../utils/sequelize');
var formatters = require('../formatters');
var config = require('../../config');

var GET_ITEM_SQL = [
    'SELECT',
    '`item`.`id` AS `id`,',
    '`item`.`barCode` AS `barCode`,',
    '`item`.`name` AS `name`,',
    '`item`.`brand` AS `brand`,',
    '`item`.`itemSize` AS `itemSize`,',
    '`item`.`servingPerPack` AS `servingPerPack`,',
    '`item`.`servingSize` AS `servingSize`,',
    '`item`.`enabled` AS `enabled`,',
    '`item`.`createdAt` AS `createdAt`,',
    '`item`.`updatedAt` AS `updatedAt`,',
    '`unitType`.`type` AS `unitType`,',
    '`servingType`.`type` AS `servingType`,',
    '`itemImage`.`url` AS `image`,',
    '`hasSale`.`isSale` IS NOT NULL AS `hasSale`,',
    '`wish`.`id` IS NOT NULL AS `isItemInWishes`,',
    '`shopping`.`id` IS NOT NULL AS `isItemInTrolley`',
    'FROM `items` AS `item`',
    'LEFT OUTER JOIN `images` AS `itemImage` ON `item`.`image_id` = `itemImage`.`id`',
    'LEFT OUTER JOIN `units` AS `unitType`  ON `item`.`unit_id` = `unitType`.`id`',
    'LEFT OUTER JOIN `units` AS `servingType`  ON `item`.`serving_unit_id` = `servingType`.`id`',
    'LEFT OUTER JOIN `itemByShops` AS `hasSale` ON `hasSale`.`item_id` = `item`.`id`',
    'LEFT OUTER JOIN `wishes` AS `wish` ON `wish`.`item_id` = `item`.`id` AND `wish`.`user_id` = :userId',
    'LEFT OUTER JOIN (`shopping` INNER JOIN `itemByShops` as `shoppingList` ON `shoppingList`.`id` = `shopping`.`item_by_shop_id` AND `shopping`.`user_id` = :userId)',
    'ON `shoppingList`.`item_id` = `item`.`id`',
    'WHERE `item`.`enabled` = TRUE AND `item`.`id` = :itemId',
    'GROUP BY `item`.`id`',
    'LIMIT :limit',
    'OFFSET :offset;'
].join(' ');

var defaultSearchSettings = {
    limit : config.constants.LIMIT,
    offset: config.constants.STEP
};

var SEARCH_TYPES = {
    GLOBAL     : 1,
    CATEGORY   : 2,
    SUBCATEGORY: 3
};

/**
 * Search item
 *
 * @param {Object} options
 * @param {String} options.type
 * @param {Boolean} [options.hasQuery]
 * @param {Object} options.replacements
 * @param {Number} options.replacements.userId
 * @param {Array<String>} options.replacements.postCodes
 * @returns {Promise}
 */
function search(options) {
    var error = validateSearchOptions(options);
    if (error) {
        error.status = 400;
        return Promise.reject(error);
    }

    var sql = buildSearchSql(options.type, options.hasQuery);
    var settings = {
        type        : db.QueryTypes.SELECT,
        replacements: Object.assign({}, defaultSearchSettings, options.replacements)
    };
    return db.query(sql, settings)
        .then(function (rawResponse) {
            return formatters.item.rawExtended(rawResponse);
        });
}

/**
 * Build search query
 *
 * @param {String} type
 * @param {Boolean} hasQuery
 * @returns {string}
 */
function buildSearchSql(type, hasQuery) {
    logger.log('info', '_buildSearchQuery ' + JSON.stringify(arguments));
    var INJECTS = {
        CATEGORY   : [
            'FROM `categories` as `category`',
            'INNER JOIN `itemByCategories` as `ibc` ON `ibc`.`category_id` = `category`.`id` AND `category`.`parent_id` = :parent',
            'INNER JOIN `items` AS `item` ON `item`.`id` = `ibc`.`item_id`'
        ].join(' '),
        SUBCATEGORY: [
            'FROM `items` as `item`',
            'INNER JOIN `itemByCategories` as `ibc` ON `item`.`id` = `ibc`.`item_id` AND `ibc`.`category_id` = :category'
        ].join(' ')
    };
    var injectFROM = INJECTS[type] || 'FROM `items` as `item`';
    var injectWHERE = hasQuery ? 'AND (`item`.`barCode` = :barCode OR `item`.`name` like :name OR `item`.`brand` like :brand)' : '';

    var sql = [
        'SELECT',
        '`item`.`id` AS `id`,',
        '`item`.`barCode` AS `barCode`,',
        '`item`.`name` AS `name`,',
        '`item`.`brand` AS `brand`,',
        '`itemImage`.`url` AS `image`,',
        '`ibs`.`id` AS `bestChoice.id`,',
        '`ibs`.`regularPrice` AS `bestChoice.regularPrice`,',
        '`ibs`.`salePrice` AS `bestChoice.salePrice`,',
        'ROUND(`ibs`.`unitPrice`, 2) AS `bestChoice.unitPrice`,',
        '`ibs`.`shop_id` AS `bestChoice.shopId`,',
        'IF(DATE(FROM_UNIXTIME(`ibs`.`from`/1000)) > CURDATE() OR `shop`.`hasSales` = FALSE, 0, `ibs` .`isSale`) as `bestChoice.isSale`,',
        '`ibs`.`bestPrice` as `bestChoice.bestPrice`,',
        '`shop`.`name` AS `bestChoice.shopName`,',
        '`wish`.`id` IS NOT NULL AS `isItemInWishes`,',
        '`shopping`.`id` IS NOT NULL AS `isItemInTrolley`',
        injectFROM,
        'INNER JOIN `images` AS `itemImage` ON `item`.`image_id` = `itemImage`.`id`',
        'INNER JOIN (',
        	'SELECT `ibs`.* FROM (',
                'SELECT *, IF((`salePrice` = 0 OR `salePrice` IS NULL), `regularPrice`, LEAST(`salePrice`, `regularPrice`)) as `bestPrice`',
                'FROM `itemByShops`',
            ') `ibs`',
            'INNER JOIN (',
                'SELECT `item_id`, MIN(IF((`salePrice` = 0 OR `salePrice` IS NULL), `regularPrice`, LEAST(`salePrice`,`regularPrice`))) as `bestPrice`',
                'FROM `itemByShops`',
                'WHERE `enabled` = TRUE',
                'GROUP BY `item_id`',
            ') as `ibs_inner`',
            'ON `ibs`.`item_id` = `ibs_inner`.`item_id` AND `ibs`.`bestPrice` = `ibs_inner`.`bestPrice`',
        ') as `ibs` ON `item`.`id` = `ibs`.`item_id` AND `ibs`.`enabled` = TRUE',
        'INNER JOIN `shops` AS `shop` ON `shop`.`id` = `ibs`.`shop_id`', 
        'LEFT OUTER JOIN `wishes` AS `wish` ON `wish`.`item_id` = `item`.`id` AND `wish`.`user_id` = :userId',
        'LEFT OUTER JOIN (`shopping` INNER JOIN `itemByShops` as `shoppingList` ON `shoppingList`.`id` = `shopping`.`item_by_shop_id` AND `shopping`.`user_id` = :userId)',
        ' ON `shoppingList`.`item_id` = `item`.`id`',
        'WHERE `item`.`enabled` = TRUE',
        injectWHERE,
        'AND `shop`.`postCode` IN (:postCodes)', 
        'LIMIT :limit OFFSET :offset;'
    ].join(' ');

    return sql;
}

/**
 * Validate search options
 *
 * @param options
 * @returns {*}
 */
function validateSearchOptions(options) {
    var type = options.type;
    if (typeof type !== 'string' || !SEARCH_TYPES[type.toUpperCase()]) {
        var error = new Error('Invalid search type.');
        return error;
    }
    if (!options.replacements) {
        error = new Error('Invalid options, replacements is required.');
        return error;
    }

    var postCodes = options.replacements.postCodes;
    if (!postCodes || !Array.isArray(postCodes)) {
        error = new Error('Invalid postCodes.');
        return error;
    }

    var userId = options.replacements.userId;
    if (!userId || typeof userId !== 'number' || Number.isNaN(userId)) {
        error = new Error('Invalid user id.');
        return error;
    }

    return false;
}

/**
 * Get item for user
 *
 * @param options
 * @param options.userId
 * @param options.itemId
 * @returns {Promise}
 */
function getAbstractItems(options) {
    if (!options) {
        var error = new Error('Options is required!');
        error.status = 400;
        return Promise.reject(error);
    }
    if (!options.userId) {
        var error = new Error('User id is required!');
        error.status = 400;
        return Promise.reject(error);
    }
    if (!options.itemId) {
        var error = new Error('Item id is required!');
        error.status = 400;
        return Promise.reject(error);
    }

    var settings = {
        type        : db.QueryTypes.SELECT,
        replacements: Object.assign({
             limit: 1, 
             offset: 0 
        }, options)
    };
    return db.query(GET_ITEM_SQL, settings);
}


function itemsWithViewOnlyPermission(options){
    if (!options) {
        var error = new Error('Options is required!');
        error.status = 400;
        return Promise.reject(error);
    }

    var sorting = '';
    var filterWithPermission = '';

    if (options.adminId){
        filterWithPermission = [
            ',(',
                'SELECT(COUNT(*) > 0) as `status`',
                'FROM `itemByShops` as `itemByShop`',
                    'WHERE `itemByShop`.`item_id` = `item`.`id`',
                    'AND `itemByShop`.`shop_id` NOT IN (',
                        'SELECT `shop_id` FROM `shopByAdmins` as `shopByAdmin`',
                        'WHERE `shopByAdmin`.`admin_id` = :adminId',
                        'GROUP BY `shop_id`',
                    ')',
            ') as `viewOnly`'
        ].join(' ');
    }

    if (options.sortBy){
        sorting = 'ORDER BY `item`.`' + options.sortBy + '` ';
        if (options.sortAt) {
            sorting += options.sortAt;
        }
    }

    var ITEMS_WITH_DELETE_PERMISSION = [
        'SELECT `item`.`id`,',
        '`item`.`barCode`,',
        '`item`.`name`,',
        '`item`.`brand`,',
        '`item`.`itemSize`,',
        '`item`.`unit_id`,',
        '`image`.`url` as `image`,',
        '`item`.`createdAt`,',
        '`item`.`updatedAt`,',
        '`item`.`enabled`',
        filterWithPermission,
        'FROM `items` as `item`',
        'LEFT OUTER JOIN `images` as `image` ON `image`.`id` = `item`.`image_id`',
        'WHERE `item`.`barCode` LIKE :barCode OR `item`.`name` LIKE :name',
        sorting,
        'LIMIT :limit OFFSET :offset;'
    ].join(' ');

    var settings = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };

    return db.query(ITEMS_WITH_DELETE_PERMISSION, settings);
}

function itemViewOnlyPermission(adminId, itemId){

    var GET_VIEW_ONLY_PERMISSION_TO_ITEM = [
        'SELECT(COUNT(*) > 0) as `status`',
        'FROM `itemByShops` as `itemByShop`',
            'WHERE `itemByShop`.`item_id` = :itemId', 
            'AND `itemByShop`.`shop_id` NOT IN (',
                'SELECT `shop_id` FROM `shopByAdmins` as `shopByAdmin`',
                'WHERE `shopByAdmin`.`admin_id` = :adminId',
                'GROUP BY `shop_id`',
            ')'
    ].join(' ');

    var settings = {
        type        : db.QueryTypes.SELECT,
        replacements: {
            adminId: adminId,
            itemId:  itemId
        }
    };

    return db.query(GET_VIEW_ONLY_PERMISSION_TO_ITEM, settings);
}

function  getItemsByShopWithTimeLine(options){
   
   var GET_ITEMS_BY_SHOP = [
        '((`saleInRange`.count IS NOT NULL AND `saleInRange`.count != 0) OR `saleBefore`.product_id IS NOT NULL) AS `hasSale`,',
        '((`advInRange`.count IS NOT NULL AND `advInRange`.count != 0) OR `advBefore`.product_id IS NOT NULL) AS `hasAdverising`',
        'FROM items AS `item`', 
        'LEFT OUTER JOIN images AS `image` ON `image`.id = `item`.image_id',
        'INNER JOIN itemByShops AS `itemByShop` ON `itemByShop`.item_id = `item`.id AND `itemByShop`.shop_id = :shopId',
        'LEFT OUTER JOIN (',
            'SELECT',
                '(COUNT(*) > 0) AS `count`,',
                '`salesInRange`.product_id',
                'FROM salesActivityByAdmins AS `salesInRange`',
            'WHERE DATE(`salesInRange`.createdAt) BETWEEN DATE(:from) AND DATE(:to)',
              'AND DATE(FROM_UNIXTIME(`salesInRange`.to/1000)) >= DATE(:from)',
              'AND DATE(:to) >= DATE(FROM_UNIXTIME(`salesInRange`.from/1000))',
             'GROUP BY `salesInRange`.product_id',
        ') AS `saleInRange` ON `saleInRange`.product_id  = `itemByShop`.id',

        'LEFT OUTER JOIN(',
        	'SELECT `sale`.product_id',  
            'FROM (',
                'SELECT',
                    'MAX(createdAt) AS `lastTime`,',
                    'product_id',
                 'FROM salesActivityByAdmins',
                 'WHERE DATE(createdAt) < DATE(:from)',
                 'GROUP BY product_id',
            ') AS `lastSale`',
            'INNER JOIN salesActivityByAdmins  AS `sale` ON `sale`.createdAt = `lastSale`.`lastTime`',
            'AND `sale`.product_id = `lastSale`.product_id',
            'WHERE DATE(FROM_UNIXTIME(`sale`.to/1000)) >= DATE(:from)',
            'AND DATE(:to) >= DATE(FROM_UNIXTIME(`sale`.from/1000))',
        ') AS `saleBefore` ON `saleBefore`.product_id  = `itemByShop`.id', 

        'LEFT OUTER JOIN (',
            'SELECT',
            '(COUNT(*) > 0) AS `count`,',
            'advertising.product_id AS `product_id`',
            'FROM advActivityByAdmins AS `advInRange`',
            'INNER JOIN advertising ON advertising.id = `advInRange`.advertising_id',
            'WHERE DATE(`advInRange`.createdAt) BETWEEN DATE(:from) AND DATE(:to)',
              'AND DATE(FROM_UNIXTIME(`advInRange`.to/1000)) >= DATE(:from)',
              'AND DATE(:to) >= DATE(FROM_UNIXTIME(`advInRange`.from/1000))',
             'GROUP BY advertising.product_id',
        ') AS `advInRange` ON `advInRange`.product_id  = `itemByShop`.id',

        'LEFT OUTER JOIN(',
        	'SELECT advertising.product_id',  
            'FROM (',
                'SELECT',
                    'MAX(createdAt) AS `lastTime`,',
                    'advertising_id',
                 'FROM advActivityByAdmins',
                 'WHERE DATE(createdAt) < DATE(:from)',
                 'GROUP BY advertising_id',
            ') AS `lastAdv`',
            'INNER JOIN advActivityByAdmins  AS `adv` ON `adv`.createdAt = `lastAdv`.`lastTime`',
            'INNER JOIN advertising ON advertising.id = `adv`.advertising_id',
            'AND `adv`.advertising_id = `lastAdv`.advertising_id',
            'WHERE DATE(FROM_UNIXTIME(`adv`.to/1000)) >= DATE(:from)',
            'AND DATE(:to) >= DATE(FROM_UNIXTIME(`adv`.from/1000))',
        ') AS `advBefore` ON `advBefore`.product_id  = `itemByShop`.id', 

        'WHERE `item`.barCode LIKE :query OR `item`.name LIKE :query OR `item`.brand LIKE :query'
    ].join(' ');
   
    var GET_ITEMS = [
        'SELECT *',
        'FROM (',
            'SELECT', 
                '`item`.id AS `id`, ',
                '`item`.name AS `name`,', 
                '`item`.brand AS `brand`,',
                '`item`.barCode AS `barCode`,',
                '`itemByShop`.createdAt AS `createdAt`,', 
                '`itemByShop`.updatedAt AS `updatedAt`,',
                '`itemByShop`.enabled AS `enabled`,', 
                '`image`.url AS `image`,',
                GET_ITEMS_BY_SHOP,
            ') AS `item`',
        'WHERE `hasSale` != 0 OR `hasAdverising` != 0',  
        'ORDER BY `item`.`' + options.sortBy +'`',
        options.sortAt,
        'LIMIT :limit OFFSET :offset'
    ].join(' ');

    var GET_COUNT_ITEMS = [
       'SELECT COUNT(*) as `count`',
       'FROM (',
            'SELECT ',
                GET_ITEMS_BY_SHOP,
            ') AS `item`',
       'WHERE `hasSale` != 0 OR `hasAdverising` != 0'
    ].join(' ');

    var settings = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return Promise.all([
        db.query(GET_ITEMS, settings),
        db.query(GET_COUNT_ITEMS, settings)
            .then(function(result){
               return result[0].count;
            })
    ]);
}


function  getItemsByShop(options){
    var GET_ITEMS = [
        'SELECT', 
        '`item`.id AS `id`, ',
        '`item`.name AS `name`,', 
        '`item`.brand AS `brand`,',
        '`item`.barCode AS `barCode`,',
        '`itemByShop`.createdAt AS `createdAt`,', 
        '`itemByShop`.updatedAt AS `updatedAt`,',
        '`itemByShop`.enabled AS `enabled`,',     
        '`image`.url AS `image`,',
        '(`itemByShop`.isSale = TRUE AND DATE(FROM_UNIXTIME(`itemByShop`.to/1000)) >= DATE(NOW()) AND DATE(FROM_UNIXTIME(`itemByShop`.from/1000)) <= DATE(NOW()) ) AS `hasSale`,',
        '(`advertising`.count IS NOT NULL AND `advertising`.count != 0) AS `hasAdvertising`',
        'FROM items AS `item`', 
        'INNER JOIN itemByShops AS `itemByShop` ON `itemByShop`.item_id = `item`.id AND `itemByShop`.shop_id = :shopId',
        'LEFT OUTER JOIN images AS `image` ON `image`.id = `item`.image_id',
        'LEFT OUTER JOIN (',
            'SELECT',
                'COUNT(*) AS `count`,',
                'product_id AS `product_id`',
            'FROM advertising',
            'WHERE DATE(FROM_UNIXTIME(advertising.to/1000)) >= DATE(NOW())',
            'AND DATE(FROM_UNIXTIME(advertising.from/1000)) <= DATE(NOW())',
            'GROUP BY product_id',
        ') AS `advertising` ON `advertising`.product_id = `itemByShop`.id',
        'WHERE `item`.barCode LIKE :query OR `item`.name LIKE :query OR `item`.brand LIKE :query',
        'ORDER BY `' + options.sortBy +'`',
        options.sortAt,
        'LIMIT :limit OFFSET :offset'
    ].join(' ');

    var GET_COUNT_ITEMS = [
       'SELECT COUNT(*) as `count`',
       'FROM items AS `item`', 
       'INNER JOIN itemByShops AS `itemByShop` ON `itemByShop`.item_id = `item`.id AND `itemByShop`.shop_id = :shopId',
       'WHERE `item`.barCode LIKE :query OR `item`.name LIKE :query OR `item`.brand LIKE :query'
    ].join(' ');

    var settings = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };

    return Promise.all([
        db.query(GET_ITEMS, settings),
        db.query(GET_COUNT_ITEMS, settings)
          .then(function(result){
            return result[0].count;
          })
    ]);
}

module.exports = {
    itemViewOnlyPermission     : itemViewOnlyPermission,
    itemsWithViewOnlyPermission: itemsWithViewOnlyPermission,
    search                     : search,
    getAbstractItems           : getAbstractItems,
    buildSearchSql             : buildSearchSql,
    getItemsByShop             : getItemsByShop,
    getItemsByShopWithTimeLine : getItemsByShopWithTimeLine
};