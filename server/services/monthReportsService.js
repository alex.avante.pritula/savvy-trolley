var mailService = require('../utils/mailService');
var db = require('../utils/sequelize');
var config = require('../../config');
var models = require('../models');
var Promise = require('bluebird');
var moment = require('moment');
var XLSX = require('xlsx');
var reportService = require('./reportService');

var GET_ADVERTISING_ITEMS_BY_DATA_RANGE = [
    'SELECT `admin`.`email`, `item`.`barCode` as `itemBarCode` ,`productItem`.`barCode` as `productBarCode`,`adv`.`createdAt` as `createdAt`,`adv`.`from`,`adv`.`to`',
    'FROM (',
        'SELECT `admin_id`, `createdAt`, `from`, `to`, `advertising_id` FROM `advActivityByAdmins`',
        'WHERE `createdAt` < :rightLimit AND `createdAt` > :leftLimit',
    'UNION ALL',
        'SELECT `admin_id`,`createdAt`, `from`, `to`, `advActivityByAdmins`.`advertising_id`',
        'FROM `advActivityByAdmins`',
        'INNER JOIN(',
        '   SELECT `advertising_id`, MAX(`createdAt`) as  `maxDate`',
        '   FROM  `advActivityByAdmins`',
        '   WHERE `createdAt` <  :leftLimit',
        '   GROUP BY `advertising_id`',
        ') as `lastBeforeRange` ON `lastBeforeRange`.`maxDate`  = `advActivityByAdmins`.`createdAt`',
        '  AND `lastBeforeRange`.`advertising_id` = `advActivityByAdmins`.`advertising_id`',
    ') as `adv`',
    'INNER JOIN `admins` as `admin` ON `admin`.`id` = `adv`.`admin_id`',
    'INNER JOIN `advertising` ON `advertising`.`id` = `adv`.`advertising_id`',
    'INNER JOIN `itemByShops` ON `itemByShops`.`id` = `advertising`.`product_id` AND `itemByShops`.`shop_id` = :id',
    'INNER JOIN `items` as `item` ON `item`.`id` = `advertising`.`item_id`',
    'INNER JOIN `items` as `productItem` ON `productItem`.`id` = `itemByShops`.`item_id`',
    'WHERE `adv`.`from` <  :rightMSLimit AND `adv`.`to` > :leftMSLimit',
    'ORDER BY `productBarCode`, `createdAt` DESC'
].join(' ');

var GET_ADMINS_TO_SHOP = [
    'SELECT `admins`.`email`, `admins`.`id` FROM `shopByAdmins` ',
    'INNER JOIN `admins` ON `admins`.`id` = `shopByAdmins`.`admin_id` ' +
    'WHERE `shopByAdmins`.`shop_id` = :id'
].join(' ');


var GET_COUNT_USERS_BY_SHOP_POST_CODE = [
    'SELECT COUNT(postCodeByUsers.id) as `countUsers` FROM postCodeByUsers',
    'INNER JOIN postCodes ON postCodes.id = postCodeByUsers.post_code_id',
    'INNER JOIN shops ON shops.postCode = postCodes.code AND shops.id = :id'
].join(' ');

var GET_ITEMS_TO_SHOP = [
    'SELECT `admin`.`email`,`items`.`barCode` as `barCode`, `product`.`salePrice` as `salePrice`,`sales`.`createdAt` as `createdAt`, `sales`.`from`,`sales`.`to`',
    'FROM ( ',
        'SELECT `admin_id`, `createdAt`, `from`, `to`, `product_id`',
        'FROM `salesActivityByAdmins`',
        'WHERE `createdAt` < :rightLimit AND `createdAt` > :leftLimit',
    'UNION ALL',
        'SELECT `admin_id`,`createdAt`, `from`, `to`, `salesActivityByAdmins`.`product_id`',
        'FROM `salesActivityByAdmins`',
        'INNER JOIN(',
        '    SELECT `product_id`, MAX(`createdAt`) as `maxDate` ',
        '    FROM  `salesActivityByAdmins`',
        '    WHERE `createdAt` <  :leftLimit',
        '    GROUP BY `product_id`',
        ')  as `lastBeforeRange` ON `lastBeforeRange`.`maxDate`  = `salesActivityByAdmins`.`createdAt`',
        'AND `lastBeforeRange`.`product_id` = `salesActivityByAdmins`.`product_id`',
    ') as `sales`',
    'INNER JOIN `admins` as `admin` ON `admin`.`id` = `sales`.`admin_id`',
    'INNER JOIN `itemByShops` as `product` ON `product`.`id` = `sales`.`product_id` AND `product`.`shop_id` = :id',
    'INNER JOIN `items` ON `items`.`id` = `product`.`item_id`',
    'WHERE `sales`.`from` < :rightMSLimit  AND `sales`.`to` > :leftMSLimit',
    'ORDER BY `barCode`, `createdAt` DESC'
].join(' ');

function dateTimeFormatter(date) {
    return moment(date).format('DD MMMM YYYY hh:mm:ss A');
}

(function() {

    var dateNow = new Date();
    var currentMonth = dateNow.getMonth();
    var currentYear = dateNow.getFullYear();
    var currentDay = dateNow.getDate();

    var currentDate = new Date(currentYear, currentMonth, currentDay);
    var currentMSDate = currentDate.getTime();

    var previousDate = ((currentMonth == 0) ?
            new Date(currentYear - 1, 11, currentDay) :
            new Date(currentYear, currentMonth - 1, currentDay) );

    var previousMSDate = previousDate.getTime();
    var shopAdmins = [];
    var options = { where: { role: 'SHOP' }};

    models.admins.all(options).then(function(admins){
            shopAdmins = admins;
            var options = { where: ['DAY(createdAt) = ' + currentDay] };
            return models.shops.all(options);
        }).then(function (existShops) {
            return Promise.mapSeries(existShops,function (shop) {
                var options = {
                    type        : db['QueryTypes'].SELECT,
                    replacements: {  id: shop.id  }
                };

                var optionsByRange = {
                    type        : db['QueryTypes'].SELECT,
                    replacements: {
                        id           : shop.id ,
                        rightMSLimit : currentMSDate,
                        rightLimit   : currentDate,

                        leftMSLimit  : previousMSDate,
                        leftLimit    : previousDate
                    }
                }

                var itemPromises = [
                    db.query(GET_ITEMS_TO_SHOP, optionsByRange), //sales
                    db.query(GET_ADVERTISING_ITEMS_BY_DATA_RANGE, optionsByRange),// advertising
                    db.query(GET_ADMINS_TO_SHOP, options), // admins
                    db.query(GET_COUNT_USERS_BY_SHOP_POST_CODE, options) //count users to shop
                ];

                return Promise.all(itemPromises).then(function(dataToShop){

                    var owners = dataToShop[2];

                    if(!owners.length){
                        return "Admins list to shop (" + shop.name + ") is empty";
                    }

                    var report = [ ['Shop: ' + shop.name] ];
                    var sales = dataToShop[0];
                    var advertising = dataToShop[1];
                    var countUsers = dataToShop[3][0].countUsers;

                    report.push(['Count of users: ' + countUsers]);

                    if(sales.length){
                        //var salesSumToShop = 0;
                        report.push(['Sales']);
                        report.push(['Admin Email','Bar Code','Date start', 'Date Finish', 'Activated days', 'Sale Price']);

                        var tempBarCode = sales[0].barCode;
                        var to = sales[0].to;
                        var createdAt = sales[0].createdAt.getTime();

                        sales.forEach(function (item, index) {
                            var changed = (tempBarCode != item.barCode);

                            if(changed){
                                to = item.to;
                                createdAt = item.createdAt.getTime();
                                tempBarCode = item.barCode;
                            }

                            var from = item.from;

                            if(to < from) return;
                            if(to > item.to) to = item.to;

                            if(index > 0 && !changed){
                                if(createdAt < from) return;
                                else if(createdAt < item.to) to = createdAt;
                            }

                            var rightDateLimit = (to > currentDate) ? currentDate : to;
                            var leftDateLimit = (from < previousDate) ? previousDate : from;
                            var activeDays = Math.round((rightDateLimit - leftDateLimit) / config['constants'].ONE_DAY);

                            if(activeDays == 0) return;

                            report.push([item.email, item.barCode, dateTimeFormatter(from), dateTimeFormatter(to), activeDays, item.salePrice]);
                            to = from;
                            createdAt = item.createdAt.getTime();
                            //salesSumToShop += activeDays;
                        });

                        //salesSumToShop *= countUsers * config['constants'].PAYMENT_ITEM_PRICE;
                        //report.push(["Sum to payment: $ ", salesSumToShop.toFixed(2)]);

                    } else report.push(['Sales list is empty']);

                    if (advertising.length){

                        //var  advertisingSumToShop = 0;
                        report.push(['Advertising']);
                        report.push(['Admin Email' ,' Advertised Item Bar Code', 'Advertising Item Bar code', 'Date start', 'Date Finish', 'Activated days']);

                        var tempItemBarCode = advertising[0].itemBarCode;
                        var tempProductBarCode = advertising[0].productBarCode;
                        var to = advertising[0].to;
                        var createdAt = advertising[0].createdAt.getTime();

                        advertising.forEach(function (item, index) {

                            var changedItem = (tempItemBarCode != item.itemBarCode);
                            var changedProduct = (tempProductBarCode != item.productBarCode);

                            if (changedItem){
                                to = item.to;
                                createdAt = item.createdAt.getTime();
                                tempItemBarCode = item.itemBarCode;
                            }

                            if (changedProduct){
                                to = item.to;
                                createdAt = item.createdAt.getTime();
                                tempProductBarCode = item.productBarCode;
                            }

                            var from = item.from;

                            if (to < from) return;
                            if (to > item.to) to = item.to;

                            if (index > 0 && !changedItem && !changedProduct){
                                if(createdAt < from) return;
                                else if(createdAt < item.to) to = createdAt;
                            }

                            var rightDateLimit = (to > currentDate) ? currentDate : to;
                            var leftDateLimit = (from < previousDate) ? previousDate : from;
                            var activeDays = Math.round((rightDateLimit - leftDateLimit) / config['constants'].ONE_DAY);

                            if(activeDays == 0) return;

                            report.push([ item.email, item.itemBarCode, item.productBarCode, dateTimeFormatter(from), dateTimeFormatter(to), activeDays]);
                            to = from;
                            createdAt = item.createdAt.getTime();
                            //advertisingSumToShop  +=  activeDays;
                        });

                        //advertisingSumToShop *= countUsers * config['constants'].PAYMENT_ITEM_PRICE;
                        //report.push(["Sum to payment: $ ",advertisingSumToShop.toFixed(2)]);

                    } else {
                        report.push(['Advertising list is empty']);
                    }

                    var reportBook = XLSX.utils.book_new();
                    var admins = owners.concat(shopAdmins);
                    var reportSheet = XLSX.utils.aoa_to_sheet(report, { cellDates:true });
                    XLSX.utils.book_append_sheet(reportBook, reportSheet, 'Month Report');

                    var options = {
                        data  : reportBook,
                        shopId: shop.id,
                        type  : config.constants['REPORT_TYPES'].MONTH,
                        name  : 'Month report by ' + shop.name
                    };

                    return Promise.mapSeries(admins, function (admin) {
                        return reportService.saveReport(admin, options)
                            .then(function (result) {
                                return mailService.monthReportMessage(result.url, shop['name'], admin['email']);
                            }).then(function(){
                                return 'Sending shop(' + shop['name'] + ') month report to ' + admin['email'];
                            });
                    });
                });
            });
    })
    .then(function(result){
        console.log(result);
        process.exit(0);
    })
    .catch(function(error){
        console.log(error);
        process.exit(0);
    });
})();
