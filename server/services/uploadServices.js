var fs = require('fs');
var Promise = require('bluebird');

var models = require('../models');
var sThree = require('../utils/s-tree');


const unlink = Promise.promisify(fs.unlink);
/**
 * Upload files on S3.
 *
 * @param {Object} files
 * @returns {Promise}
 */
function uploadsTemp(files) {
    var promises = files.map(function (file) {
        var options = {
            fileName: file.filename,
            path    : file.path
        };
        return sThree.uploadImage(options);
    });
    return Promise.all(promises);
}

/**
 * Save image urls into db.
 *
 * @param files
 * @returns {Promise}
 */
function saveImageUrls(files) {
    return Promise.mapSeries(files,function (file) {
        var options = {
            url : file.Location,
            name: file.Key
        };
        return models.images.create(options);
    });
   // return Promise.all(promises);
}

/**
 * Remove temp files after upload on S3.
 *
 * @param files
 * @returns {Promise}
 */
function removeTemp(files) {
    var promises = files.map(function (file) {
        var path = ((typeof file == 'string') ? file : file.path);
        return unlink(path);
    });
    return Promise.all(promises);
}

function removeFromS3(urls) {
    var promises = urls.map(function (url) {
        return sThree.remove({ fileName: url });
    });
    return Promise.all(promises);
}

module.exports = {
    uploadsTemp  : uploadsTemp,
    saveImageUrls: saveImageUrls,
    removeTemp   : removeTemp,
    removeFromS3 : removeFromS3
};