var models = require('../models');
var db = require('../utils/sequelize');
var logger = require('../utils/logger');
var formatters = require('../formatters');

var GET_WISHES_SQL = [
    'SELECT ',
    '`item`.`id` AS `id`,',
    '`item`.`barCode` AS `barCode`,',
    '`item`.`name` AS `name`,',
    '`item`.`brand` AS `brand`,',
    '`image`.`url` AS `image`,',
    '`shop`.`name` AS `bestChoice.shopName`,',
    '`ibs`.`id` AS `bestChoice.id`,',
    '`ibs`.`regularPrice` AS `bestChoice.regularPrice`,',
    '`ibs`.`salePrice` AS `bestChoice.salePrice`,',
    'ROUND(`ibs`.`unitPrice`, 2) AS `bestChoice.unitPrice`,',
    'IF(DATE(FROM_UNIXTIME(`ibs`.`from`/1000)) > CURDATE() OR `shop`.`hasSales` = FALSE, 0, `ibs` .`isSale`) as `bestChoice.isSale`,',
    '`ibs`.`shop_id` AS `bestChoice.shopId`,',
    '`hasSale`.`isSale` as `hasSale`,',
    'true AS `isItemInWishes`,',
    '`shopping`.`id` IS NOT NULL AS `isItemInTrolley`',
    'FROM `items` AS `item`',
    'INNER JOIN `wishes` as `wish` ON `item`.`id` = `wish`.`item_id` AND `wish`.`user_id` = :userId AND `item`.`enabled` = TRUE',
    'INNER JOIN `images` as `image` ON `item`.`image_id` = `image`.`id`',
    'INNER JOIN `itemByShops` AS `ibs` ON `ibs`.`item_id` = `item`.`id`',
    'INNER JOIN `shops` AS `shop` ON `shop`.`id` = `ibs`.`shop_id` AND `shop`.`postCode` IN (:postCodes)',
    'INNER JOIN `pricesForEnableItem` AS `price` ON `item`.`id` = `price`.`item_id` AND `price`.`shop_id` = `shop`.`id`',
    'LEFT OUTER JOIN (`shopping` INNER JOIN `itemByShops` as `shoppingList` ON `shoppingList`.`id` = `shopping`.`item_by_shop_id` AND `shopping`.`user_id` = :userId)',
    ' ON `shoppingList`.`item_id` = `item`.`id`',
    'LEFT OUTER JOIN `itemByShops` AS `hasSale` ON `hasSale`.`item_id` = `item`.`id`',
    'AND `hasSale`.`shop_id` = `shop`.`id` AND `hasSale`.`isSale` = TRUE',
    'GROUP BY `item`.`id`',
    'ORDER BY `wish`.`createdAt` DESC',
    'LIMIT :limit',
    'OFFSET :offset;'
].join(' ');

var DELETE_INVALID_WISHES = [
    'DELETE FROM `wishes`',
    'WHERE `wishes`.`user_id` = :userId',
    'AND `wishes`.`item_id` NOT IN (',
    'SELECT `item`.`id`',
    'FROM `items` AS `item`',
    'INNER JOIN `itemByShops` AS `ibs` ON `ibs`.`item_id` = `item`.`id`',
    'INNER JOIN `shops` AS `shop` ON `ibs`.`shop_id` = `shop`.`id`',
    'AND `shop`.`postCode` IN (:postCodes) AND `item`.`enabled` = TRUE);'
].join(' ');

var GET_WISHES_COUNT_SQL = [
    'SELECT',
    'COUNT(`wishes`.`id`) AS `totalCount`',
    'FROM `wishes`',
    'WHERE `wishes`.`user_id` = :userId;'
].join(' ');

/**
 * Get wishes from db
 * @param {Object} userData
 * @param {Number} userData.userId
 * @param {Array} userData.postCodes
 * @param {Object} options
 * @param {Number} options.userId
 * @param {Array} options.postCodes
 * @param {Number} options.limit
 * @param {Number} options.offset
 * @return {Promise}
 */
function getWishes(userData, options) {
    var selectOptions = {
        type        : db.QueryTypes.SELECT,
        replacements: options
    };
    return removeInvalid(userData)
        .then(function () {
            return db.query(GET_WISHES_SQL, selectOptions)
        });
}

/**
 * Remove invalid wishes
 * @param {Object} userData
 * @param {Number} userData.userId
 * @param {Array} userData.postCodes
 * @return {Promise}
 */
function removeInvalid(userData) {
    var options = {
        type        : db.QueryTypes.DELETE,
        replacements: userData
    };
    return db.query(DELETE_INVALID_WISHES, options);
}

/**
 * Return wishes count
 * @param {Object} userData
 * @param {Number} userData.userId
 * @param {Array} userData.postCodes
 * @return {Promise}
 */
function countUserWishes(userData) {
    var options = {
        type        : db.QueryTypes.SELECT,
        replacements: userData
    };
    return db.query(GET_WISHES_COUNT_SQL, options)
        .then(function (rawLines) {
            var result = rawLines ? rawLines[0] : null;
            return db.Promise.resolve(result);
        });
}

/**
 * Read wishes
 * @param {Object} userData
 * @param {Number} userData.userId
 * @param {Array} userData.postCodes
 * @param {Object} options
 * @param {Number} options.userId
 * @param {Array} options.postCodes
 * @param {Number} options.limit
 * @param {Number} options.offset
 * @return {Promise}
 */
function read(userData, options) {
    var temp = {};
    return getWishes(userData, options)
        .then(function (rawResponse) {
            return formatters.item.rawExtended(rawResponse);
        })
        .then(function (result) {
            temp.wishes = result;
            return countUserWishes(userData);
        })
        .then(function (countWishes) {
            return {
                wishes: temp.wishes,
                meta  : {
                    totalCount: countWishes ? countWishes.totalCount : 0
                }
            };
        });
}

/**
 * Update wish list
 * @param {Object} user
 * @param {Number} user.id
 * @param {Array} addedItems
 * @param {Array} removedItems
 * @return {Promise}
 */
function update(user, addedItems, removedItems) {
    logger.log('info', 'add: ' + addedItems + ' remove: ' + removedItems);
    var options = {
        where: {
            user_id: user.id,
            item_id: {$in: removedItems}
        }
    };
    return models.wishes.destroy(options)
        .then(function () {
            var wishes = addedItems.map(function (item) {
                return {
                    user_id: user.id,
                    item_id: item
                }
            });
            return models.wishes.bulkCreate(wishes, {ignoreDuplicates: true});
        });
}

module.exports = {
    read  : read,
    update: update
};
