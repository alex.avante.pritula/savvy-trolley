var models = require('../models');
var formatters = require('../formatters');

/**
 * Read history
 *
 * @param {Object} [options]
 * @param {Object} [options.where]
 * @param {Number} [options.offset]
 * @param {Number} [options.limit]
 * @returns {Promise}
 */
function read(options) {

	var settings = Object.assign({}, {
		include: [{model: models.items, as: 'item'}],
		order  : [['purchaseDate', 'DESC']]
	}, options);

	var temp = {};

	return models.history.all(settings)
		.then(function (histories) {
			temp.history = formatters.history.view(histories);
			return models.history.count({ where: settings.where });
		})
		.then(function (totalCount) {
			return {
				histories: temp.history,
				meta: { totalCount: totalCount }
			};
		});
}

module.exports = {
	read: read
};