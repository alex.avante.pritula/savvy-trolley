var Promise = require('bluebird');

var models = require('../models');
var formatters = require('../formatters');
var logger = require('../utils/logger');
var config = require('../../config');

/**
 * Add postcode to user
 *
 * @param user
 * @param code
 * @param transaction
 * @returns {Promise}
 */
function addPostCode(user, code, transaction) {
    if(!user || !user.id) {
        var error = new Error('Invalid user.');
        error.status = 400;
        return Promise.reject(error);
    }
    if(!code || !code.code) {
        error = new Error('Invalid postcode.');
        error.status = 400;
        return Promise.reject(error);
    }

    logger.log('info', user + 'addPostCode' + code);
    return models.postCodes
        .findOrCreate({where: code, transaction: transaction})
        .spread(function (code) {
            return models.postCodeByUser.create({
                user_id     : user.id,
                post_code_id: code.id
            }, {transaction: transaction});
        });
}

/**
 * Get postcodes
 *
 * @param settings
 * @returns {Promise}
 */
function getPostCodes(settings) {
    settings = settings || {};
    var type = settings.type || 'user';
    var postCode = settings.postCode || '';
    if (type === 'user') {
        var options = {
            where : {code: {$like: '%' + postCode + '%'}},
            limit : settings.limit || 10,
            offset: settings.offset || 0
        };

        return models.postCodes.all(options)
            .then(function (postCodes) {
                return {
                    postCodes: formatters.postCode.toStrings(postCodes)
                };
            });
    } else if (type === 'shop') {
        options = {
            where     : {postCode: {$like: '%' + postCode + '%'}},
            attributes: ['postCode'],
            limit     : settings.limit || 10,
            offset    : settings.offset || 0,
            group     : ['postCode']
        };

        return models.shops.all(options)
            .then(function (shops) {
                return {
                    postCodes: formatters.postCode.toStringsOld(shops)
                };
            })
    }

    var err = new Error('Invalid type.');
    err.status = 400;
    return Promise.reject(err);
}

module.exports = {
    getPostCodes: getPostCodes,
    addPostCode : addPostCode
};