function view(datas) {
    return datas.map(function (data) {
        return {
            id          : data.item.id,
            barCode     : data.item.barCode,
            name        : data.item.name,
            brand       : data.item.brand,
            image       : data.item.image,
            enabled     : data.item.enabled,
            regularPrice: data.regularPrice,
            salePrice   : data.salePrice,
            unitPrice   : data.unitPrice,
            isSale      : data.isSale == '1',
            purchaseDate: data.purchaseDate
        };
    });
}

module.exports = {
    view: view
};