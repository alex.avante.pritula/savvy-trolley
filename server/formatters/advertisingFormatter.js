function deserializeForAdmin(rawLines) {
    return rawLines.map(function (rawLine) {
        return {
            id    : rawLine.id,
            name  : rawLine.name,
            choice: {
                shopId   : rawLine['choice.shopId'],
                shopName : rawLine['choice.shopName'],
                hasAdvertising : rawLine['choice.hasAdvertising'] == 1,
                contactEmail : rawLine['choice.contactEmail'],
                postCode : rawLine['choice.postCode'],
                productId: rawLine['choice.productId'],
                from     : rawLine['choice.from'],
                to       : rawLine['choice.to']
            }
        }
    });
}
function deserializeForUser(rawLines) {
    return rawLines.map(function (rawLine) {
        return {
            id     : rawLine.id,
            barCode: rawLine.barCode,
            name   : rawLine.name,
            brand  : rawLine.brand,
            image  : rawLine.image,
            choice : {
                id          : rawLine['choice.id'],
                shopId      : rawLine['choice.shopId'],
                shopName    : rawLine['choice.shopName'],
                regularPrice: rawLine['choice.regularPrice'],
                salePrice   : rawLine['choice.salePrice'],
                unitPrice   : rawLine['choice.unitPrice'],
                isSale      : rawLine['choice.isSale'] == 1
            }
        }
    });
}
function deserializeSimple(data) {
    return {
        id       : data.id,
        itemId   : data.item_id,
        productId: data.product_id,
        from     : data.from,
        to       : data.to,
        updatedAt: data.updatedAt,
        createdAt: data.createdAt
    }
}

module.exports = {
    forAdmin: deserializeForAdmin,
    forUser : deserializeForUser,
    simple  : deserializeSimple
};