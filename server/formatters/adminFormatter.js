function adminFull(admin) {
    var result = admin.toJSON();
    result.permissions = result.permissions.map(function (permission) {
        return permission.permission;
    });

    return result;
}

module.exports = {
    adminFull: adminFull
};