function deserializeOptimal(rawLines) {
	return rawLines.map(function (rawLine) {
		return {
			id      : rawLine.id,
			name    : rawLine.name,
			postCode: rawLine.postCode,
			image   : rawLine.image,
			price   : rawLine.price,
			isLocal : rawLine['isLocal'] == 1,
			location: {
				latitude : rawLine['location.latitude'],
				longitude: rawLine['location.longitude']
			}
		};
	});
}
function deserializeRawShops(rawLines) {
	return result = rawLines.map(function (line) {
		return {
			id       : line['id'],
			name     : line['name'],
			longitude: line['longitude'],
			latitude : line['latitude'],
      postCode:  line['postCode'],
			isLocal  : line['isLocal'] == 1,
			price    : {
				id          : line['price.id'],
				regularPrice: line['price.regularPrice'],
				salePrice   : line['price.salePrice'],
				unitPrice   : line['price.unitPrice'],
				isSale      : line['price.isSale'] == '1',
				isOutOfStock: line['price.isOutOfStock'] == '1'
			}
		}
	});
}
function deserializeShops(shops) {
	return shops.map(function (shop) {
		shop = shop.toJSON();
		shop.image = shop.image ? shop.image.url : '';
		return shop;
	});
}

module.exports = {
	optimal : deserializeOptimal,
	rawShops: deserializeRawShops,
	shops   : deserializeShops
};
