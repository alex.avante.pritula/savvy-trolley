function users(rawLines) {
    return rawLines.map(function (line) {
        return {
            postCode     : line['postCode'],
            numberOfUsers: line['number_of_users'] || 0
        };
    });
}

function wishes(rawLines) {
    return rawLines.map(function (line) {
        return {
            name           : line['name'],
            barCode        : line['barCode'],
            itemsInWishList: line['items_in_wish_list']
        };
    });
}

function histories(rawLines) {
    return rawLines.map(function (line) {
        return {
            name           : line['name'],
            barCode        : line['barCode'],
            purchasedItems: line['purchased_items']
        };
    });
}

module.exports = {
    users    : users,
    wishes   : wishes,
    histories: histories
};