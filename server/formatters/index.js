module.exports = {
    admin      : require('./adminFormatter'),
    advertising: require('./advertisingFormatter'),
    history    : require('./historyFormatter'),
    item       : require('./itemFormatter'),
    postCode   : require('./postCodeFormatter'),
    shop       : require('./shopFormatters')
};
