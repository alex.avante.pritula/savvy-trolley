function deserializeItemForExpandedView(line) {
	return {
		id             : line.id,
		barCode        : line.barCode,
		name           : line.name,
		image          : line.image,
		brand          : line.brand,
		unitType       : line.unitType,
		itemSize       : line.itemSize,
		servingPerPack : line.servingPerPack,
		servingSize    : line.servingSize,
		servingType    : line.servingType,
		enabled        : line.enabled,
		hasSale        : parseInt(line.hasSale) === 1,
		isItemInWishes : parseInt(line.isItemInWishes) === 1,
		isItemInTrolley: parseInt(line.isItemInTrolley) === 1,
		createdAt      : line.createdAt,
		updatedAt      : line.updatedAt
	};
}

function deserializeRawExtended(rawLines) {
	return rawLines.map(function (line) {
		return {
			id             : line.id,
			barCode        : line.barCode,
			name           : line.name,
			brand          : line.brand,
			image          : line.image,
			bestChoice     : {
				id          : line['bestChoice.id'],
				shopName    : line['bestChoice.shopName'],
				regularPrice: line['bestChoice.regularPrice'],
				salePrice   : line['bestChoice.salePrice'],
				unitPrice   : line['bestChoice.unitPrice'],
				bestPrice   : line['bestChoice.bestPrice'],
				isSale      : parseInt(line['bestChoice.isSale']) === 1,
				shopId      : line['bestChoice.shopId']
			},
			hasSale		   : parseInt(line['bestChoice.isSale']) === 1,
			isItemInWishes : parseInt(line.isItemInWishes) === 1,
			isItemInTrolley: parseInt(line.isItemInTrolley) === 1
		};
	});
}

function deserializeExtended(items) {
	return items.map(function (item) {
		return {
			id        : item.id,
			barCode   : item.barCode,
			name      : item.name,
			brand     : item.brand,
			image     : item.image,
			bestChoice: {
				id          : item['itemByShop'].id,
				regularPrice: item['itemByShop'].regularPrice,
				salePrice   : item['itemByShop'].salePrice,
				unitPrice   : item['itemByShop'].unitPrice,
				isSale      : parseInt(item['itemByShop'].isSale) === 1
			}
		};
	});
}

function deserializeItem(item){
    return {
        id            : item.id,
        barCode       : item.barCode,
        name          : item.name,
        brand         : item.brand,
        image         : item.image ? item.image.url : '',
        itemSize      : item.itemSize,
        servingPerPack: item.servingPerPack,
        servingSize   : item.servingSize,
        enabled       : item.enabled,
        createdAt     : item.createdAt,
        updatedAt     : item.updatedAt,
        unitType      : item.unitType ? item.unitType.type : '',
        servingType   : item.servingType ? item.servingType.type : ''
    };
}

function deserializeItemByShop(item) {
	return {
		id            : item.id,
		barCode       : item.barCode,
		name          : item.name,
		brand         : item.brand,
		image         : item.image ? item.image.url : '',
		itemSize      : item.itemSize,
		servingPerPack: item.servingPerPack,
		servingSize   : item.servingSize,
		enabled       : item.itemByShop.enabled,
		createdAt     : item.itemByShop.createdAt,
		updatedAt     : item.itemByShop.updatedAt,
		unitType      : item.unitType ? item.unitType.type : '',
		servingType   : item.servingType ? item.servingType.type : '',
		productId     : item.itemByShop.id,
		regularPrice  : item.itemByShop.regularPrice,
		salePrice     : item.itemByShop.salePrice,
		unitPrice     : item.itemByShop.unitPrice,
    	from     	  : (parseInt(item.itemByShop.from) === 0) ? null : item.itemByShop.from,
   		to		      : (parseInt(item.itemByShop.to) === 0) ? null : item.itemByShop.to,
		isSale        : parseInt(item.itemByShop.isSale) === 1,
		isOutOfStock  : parseInt(item.itemByShop.is_out_of_stock) === 1
	};
}

function deserializeItemsByShop(items) {
	return items.map(deserializeItemByShop);
}

function deserializeShoppingList(rawDataList) {
	return rawDataList.map(function (data) {
		return {
			id     : data.id,
			barCode: data.barCode,
			name   : data.name,
			brand  : data.brand,
			type   : data.type,
			choice : {
				id          : data['choice.id'],
				regularPrice: data['choice.regularPrice'],
				salePrice   : data['choice.salePrice'],
				unitPrice   : data['choice.unitPrice'],
				isSale      : parseInt(data['choice.isSale']) === 1,
				shopId      : data['choice.shopId'],
				shopName    : data['choice.shopName'],
				postCode    : data['choice.postCode'],
				shopImage   : data['choice.shopImage'],
				isLocal     : parseInt(data['choice.isLocal']) === 1,
				isOutOfStock: parseInt(data['choice.isOutOfStock']) === 1,
				location    : {
					latitude : data['location.latitude'],
					longitude: data['location.longitude']
				}
			}
		};
	});
}

function deserializeItemsForShopList(rawDataList){
	return rawDataList.map(function (data) {
		return {
			id    		  : data.id,
			barCode		  : data.barCode,
			name   		  : data.name,
			brand 		  : data.brand,
			type   		  : data.type,
			image   	  : data.image,
			hasSale		  : parseInt(data.hasSale) === 1,
			hasAdvertising: parseInt(data.hasAdvertising) === 1,
			enabled       : data.enabled,
			createdAt     : data.createdAt,
			updatedAt     : data.updatedAt
		};
	});
}

module.exports = {
  deserializeItem      : deserializeItem,
	itemForExpandedView: deserializeItemForExpandedView,
	rawExtended        : deserializeRawExtended,
	extended           : deserializeExtended,
	itemsByShop        : deserializeItemsByShop,
	itemByShop         : deserializeItemByShop,
	shoppingList       : deserializeShoppingList,
	itemsForShopList   : deserializeItemsForShopList
};
