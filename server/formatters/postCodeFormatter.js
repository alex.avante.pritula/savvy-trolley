function toStrings(codes) {
    return codes.map(function (code) {
        return code.code;
    })
}
function toStringsOld(codes) {
    return codes.map(function (code) {
        return code.postCode;
    })
}
function toIds(codes) {
    return codes.map(function (code) {
        return code.id;
    })
}

module.exports = {
    toStrings   : toStrings,
    toStringsOld: toStringsOld,
    toIds       : toIds
};