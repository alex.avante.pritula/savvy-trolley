var config = require('../../config');

module.exports = function (db, DataType) {
    var deliveryNotification = db.define('deliveryNotification', {
        id            : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        entity_id: {
            type: DataType.INTEGER,
            require   : true
        },
        device_id: {
            type      : DataType.STRING,
            allowEmpty: false,
            require   : true
        },
        user_id  : {
            type: DataType.INTEGER,
            allowEmpty: false,
            require   : true
        },
        admin_id  : {
            type: DataType.INTEGER,
            allowEmpty: false,
            require   : true
        },
        shop_id  : {
            type: DataType.INTEGER,
            defaultValue: null
        },
        entity_type   : {
            type      : DataType.INTEGER,
            allowEmpty: false,
            defaultValue: 1
        },
        push_type   : {
            type      : DataType.INTEGER,
            allowEmpty: false,
            defaultValue: 1
        },
        data       : {
            type        : DataType.TEXT,
            defaultValue: null
        },
        delivery_date_time: {
            type: DataType.DATE
        },
        is_sent : {
            type        : DataType.BOOLEAN,
            allowEmpty  : false,
            defaultValue: false
        }
    }, {
        classMethods: {
            associate: function (models) {
            },
            getEntityType: function (type) {
                type = type.toUpperCase();
                return config.constants.DELIVERY_NOTIFICATION_ENTITY_TYPES[type];
            }
        }
    });

    return deliveryNotification;
};
