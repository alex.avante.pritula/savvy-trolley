module.exports = function (db, DataType) {
    var shop = db.define('shop', {
        id          : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        name        : {
            type      : DataType.STRING,
            allowEmpty: false
        },
        image_id    : {
            type      : DataType.INTEGER,
            allowEmpty: false
        },
        owner       : {
            type      : DataType.STRING,
            allowEmpty: false
        },
        abn         : {
            type      : DataType.STRING(11),
            allowEmpty: false
        },
        acn         : {
            type      : DataType.STRING(9),
            allowEmpty: false
        },
        address     : {
            type      : DataType.STRING,
            allowEmpty: false
        },
        contactName : {
            type      : DataType.STRING,
            allowEmpty: false
        },
        contactPhone: {
            type      : DataType.STRING,
            allowEmpty: false
        },
        contactEmail: {
            type      : DataType.STRING,
            allowEmpty: false
        },
        postCode    : {
            type      : DataType.STRING,
            allowEmpty: false
        },
        longitude   : {
            type      : DataType.FLOAT,
            allowEmpty: false
        },
        latitude    : {
            type      : DataType.FLOAT,
            allowEmpty: false
        },
        hasSales     : {
            type        : DataType.INTEGER,
            defaultValue: 1
        },
        hasAdvertising    : {
            type        : DataType.INTEGER,
            defaultValue: 1
        },
        isLocal     : {
            type        : DataType.INTEGER,
            defaultValue: 0
        }
    }, {
        classMethods: {
            associate: function (models) {
                shop.belongsTo(models.images, {as: 'image', foreignKey: 'image_id'});
                shop.belongsToMany(models.items, {
                    as        : 'items',
                    through   : {model: models.itemByShop},
                    foreignKey: 'shop_id'
                });
                shop.belongsToMany(models.admins, {
                    as        : 'owners',
                    through   : {model: models.shopByAdmins},
                    foreignKey: 'shop_id'
                });
            }
        }
    });

    return shop;
};