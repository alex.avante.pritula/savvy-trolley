module.exports = function (db, DataType) {
    var history = db.define('history', {
        id          : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        user_id     : {
            type    : DataType.INTEGER,
            required: true
        },
        item_id     : {
            type    : DataType.INTEGER,
            required: true
        },
        regularPrice: {
            type      : DataType.FLOAT(9, 2),
            allowEmpty: false
        },
        salePrice   : {
            type      : DataType.FLOAT(9, 2),
            allowEmpty: false
        },
        unitPrice   : {
            type      : DataType.FLOAT(9, 2),
            allowEmpty: false
        },
        isSale      : {
            type      : DataType.STRING,
            allowEmpty: false
        },
        purchaseDate: {
            type    : DataType.BIGINT,
            required: true
        }
    }, {
        classMethods: {
            associate    : function (models) {
                history.belongsTo(models.users, {as: 'user', foreignKey: 'user_id'});
                history.belongsTo(models.items, {as: 'item', foreignKey: 'item_id'});
            }
        }
    });

    return history;
};
