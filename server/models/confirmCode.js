module.exports = function (db, DataType) {
    var confirmCode = db.define('confirmCode', {
        id      : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        code    : {
            type      : DataType.STRING,
            allowEmpty: false
        },
        isActive: {
            type        : DataType.BOOLEAN,
            defaultValue: false
        },
        user_id: {
            type      : DataType.INTEGER
        },
        payload:{
            type      : DataType.TEXT,
            defaultValue: null
        }
    }, {
        classMethods: {
            associate: function (models) {
                confirmCode.belongsTo(models.users, {as: 'user', foreignKey: 'user_id'});
            }
        }
    });

    return confirmCode;
};