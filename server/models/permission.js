module.exports = function (db, DataType) {
    var permission = db.define('permission', {
        id         : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        permission       : {
            type    : DataType.STRING(190),
            unique  : true,
            required: true
        },
        description: {
            type : DataType.TEXT,
            allowNull: true
        }
    }, {
        classMethods: {
            associate: function (models) {
                permission.belongsToMany(models.admins, {
                    as        : 'admins',
                    through   : {model: models.permissionByAdmins},
                    foreignKey: 'permission_id'
                });
            }
        }
    });

    return permission;
};
