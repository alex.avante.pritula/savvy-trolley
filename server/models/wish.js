module.exports = function (db, DataType) {
    var wish = db.define('wish', {
        id     : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        user_id: {
            type    : DataType.INTEGER,
            required: true
        },
        item_id: {
            type    : DataType.INTEGER,
            required: true
        }
    }, {
        classMethods: {
            associate    : function (models) {
            }
        }
    });

    return wish;
};
