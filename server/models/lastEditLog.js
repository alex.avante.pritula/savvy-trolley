var config = require('../../config');

module.exports = function (db, DataType) {
    var lastEditLog = db.define('lastEditLog', {
        id            : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        admin_id      : {
            type      : DataType.INTEGER,
            allowEmpty: false,
            require   : true
        },
        entity_type   : {
            type      : DataType.INTEGER,
            allowEmpty: false,
            require   : true
        },
        entity_id     : {
            type      : DataType.INTEGER,
            allowEmpty: false,
            require   : true
        },
        operation_type: {
            type      : DataType.ENUM('CREATE', 'EDIT', 'DELETE'),
            allowEmpty: false,
            require   : true
        },
        payload       : {
            type        : DataType.TEXT,
            defaultValue: null
        }
    }, {
        classMethods: {
            associate    : function (models) {
                lastEditLog.belongsTo(models.admins, {
                    as        : 'editor',
                    foreignKey: 'admin_id'
                });
            },
            getEntityType: function (type) {
                type = type.toUpperCase();
                return config.constants.ENTITY_TYPES[type];
            },
            format       : function (log) {
                return {
                    id           : log.id,
                    entityType   : log.admin_id,
                    entityId     : log.entity_type,
                    adminId      : log.entity_id,
                    operationType: log.operation_type,
                    createdAt    : log.createdAt,
                    updatedAt    : log.updatedAt,
                    editor       : log.editor
                };
            }
        }
    });

    return lastEditLog;
};