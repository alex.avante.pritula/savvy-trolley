module.exports = function (db, DataType) {
    var token = db.define('token', {
        id         : {
            type        : DataType.INTEGER,
            primaryKey  : true,
            autoIncrement: true
        },
        token: {
            type        : DataType.UUID,
            allowEmpty: false,
            require  : true
        },
        user_id: {
            type      : DataType.INTEGER
        }
    }, {
        classMethods: {
            associate: function(models) {
                token.belongsTo(models.users, {as:'owner', foreignKey: 'user_id'});
            }
        }
    });

    return token;
};