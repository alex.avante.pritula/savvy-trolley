module.exports = function (db, DataType) {
    var itemByCategory = db.define('itemByCategory', {
        id: {
            type        : DataType.INTEGER,
            primaryKey  : true,
            autoIncrement: true
        },
        item_id     : {
            type    : DataType.INTEGER,
            required: true
        },
        category_id: {
            type: DataType.INTEGER,
            required: true
        }
    },{
        classMethods: {
            associate: function(models) {}
        }
    });

    return itemByCategory;
};