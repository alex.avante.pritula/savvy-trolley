module.exports = function (db, DataType) {
    var shopByAdmin = db.define('shopByAdmin', {
        id          : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        admin_id     : {
            type    : DataType.INTEGER,
            required: true
        },
        shop_id: {
            type: DataType.INTEGER,
            required: true
        }
    }, {
        classMethods: {
            associate: function (models) {
            }
        }
    });

    return shopByAdmin;
};