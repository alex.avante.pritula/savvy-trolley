module.exports = function (db, DataType) {
    var shopping = db.define('shopping', {
        id             : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        user_id        : {
            type    : DataType.INTEGER,
            required: true
        },
        item_by_shop_id: {
            type    : DataType.INTEGER,
            required: true
        },
        type           : {
            type        : DataType.INTEGER,
            require     : true,
            defaultValue: 0
        }
    }, {
        tableName   : 'shopping',
        classMethods: {
            associate              : function (models) {
            }
        }
    });

    return shopping;
};
