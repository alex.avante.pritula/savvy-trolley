module.exports = function (db, DataType) {
    var user = db.define('user', {
        id         : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        email      : {
            type      : DataType.STRING(190),
            allowEmpty: false,
            unique    : true
        },
        password   : {
            type      : DataType.STRING,
            allowEmpty: false
        },
        verified   : {
            type        : DataType.BOOLEAN,
            defaultValue: false
        },
        accountType: {
            type        : DataType.STRING(50),
            defaultValue: 'email'
        },
        hasAdverts : {
            type        : DataType.BOOLEAN,
            defaultValue: true
        },
        hasNotifications: {
            type        : DataType.BOOLEAN,
            defaultValue: true
        }
    }, {
        classMethods: {
            associate: function (models) {
                user.hasMany(models.tokens, {as: 'tokens', foreignKey: 'user_id'});
                user.hasMany(models.devices, {as: 'devices', foreignKey: 'user_id'});
                user.hasMany(models.confirmCodes, {as: 'confirmCode', foreignKey: 'user_id'});
                user.belongsToMany(models.postCodes, {
                    as        : 'postCodes',
                    through   : {model: models.postCodeByUser},
                    foreignKey: 'user_id'
                });
                user.belongsToMany(models.items, {
                    as        : 'wishes',
                    through   : {model: models.wishes},
                    foreignKey: 'user_id'
                });
                user.belongsToMany(models.itemByShop, {
                    as        : 'shopping',
                    through   : {model: models.shopping},
                    foreignKey: 'user_id'
                });
            }
        }
    });

    return user;
};