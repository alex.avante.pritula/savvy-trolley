var logger = require('../utils/logger');

module.exports = function (db, DataType) {
	var itemByShop = db.define('itemByShop', {
		id             : {
			type         : DataType.INTEGER,
			primaryKey   : true,
			autoIncrement: true
		},
		regularPrice   : {
			type      : DataType.FLOAT,
			allowEmpty: false
		},
		salePrice      : {
			type      : DataType.FLOAT,
			required  : true
		},
		unitPrice      : {
			type      : DataType.FLOAT,
			allowEmpty: false
		},
		isSale         : {
			type      : DataType.STRING,
			allowEmpty: false
		},
		enabled        : {
			type        : DataType.BOOLEAN,
			defaultValue: true
		},
		item_id        : {
			type    : DataType.INTEGER,
			required: true
		},
		shop_id        : {
			type    : DataType.INTEGER,
			required: true
		},
		is_out_of_stock: {
			type        : DataType.INTEGER,
			defaultValue: 0
		},
        from        : {
            type    : DataType.BIGINT,
            required: true
        },
        to          : {
            type    : DataType.BIGINT,
            required: true
        }
	}, {
		classMethods: {
			associate: function (models) {
				itemByShop.belongsToMany(models.users, {
					as        : 'wishes',
					through   : {model: models.shopping},
					foreignKey: 'item_by_shop_id'
				});

                itemByShop.belongsTo(models.items, {
                	as: 'item',
					foreignKey: 'item_id'
                });

				itemByShop.belongsToMany(models.items, {
					as        : 'likeOf',
					through   : {model: models.advertising},
					foreignKey: 'product_id'
				});
			}
		}
	});

	return itemByShop;
};
