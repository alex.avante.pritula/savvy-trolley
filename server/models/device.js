module.exports = function (db, DataType) {
    var device = db.define('device', {
        id       : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        token    : {
            type      : DataType.STRING,
            allowEmpty: false,
            require   : true
        },
        device_id: {
            type      : DataType.STRING,
            allowEmpty: false,
            require   : true
        },
        user_id  : {
            type: DataType.INTEGER
        },
        timezone  : {
            type: DataType.FLOAT
        }
    }, {
        classMethods: {
            associate: function (models) {
            }
        }
    });

    return device;
};