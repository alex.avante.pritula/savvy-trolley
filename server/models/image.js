module.exports = function (db, DataType) {
    var image = db.define('image', {
        id : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        url: {
            type      : DataType.TEXT,
            allowEmpty: false
        },
        name: {
            type      : DataType.STRING,
            allowEmpty: false
        }
    }, {
        classMethods: {
            associate: function (models) {
            }
        }
    });

    return image;
};
