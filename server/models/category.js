module.exports = function (db, DataType) {
    var category = db.define('category', {
        id       : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        title    : {
            type      : DataType.STRING,
            allowEmpty: false
        },
        image_id : {
            type      : DataType.INTEGER,
            allowEmpty: false
        },
        parent_id: {
            type      : DataType.INTEGER,
            allowEmpty: false
        }
    }, {
        classMethods: {
            associate: function (models) {
                category.belongsTo(category, {as: 'parent', foreignKey: 'parent_id'});
                category.belongsTo(models.images, {as: 'image', foreignKey: 'image_id'});
                category.belongsToMany(models.items, {
                    as        : 'items',
                    through   : {model: models.itemByCategory},
                    foreignKey: 'category_id'
                });
            }
        }
    });

    return category
};
