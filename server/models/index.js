var db = require('../utils/sequelize');

var env = require('../../config').env;

var models = {};

models.users = db.import('./user');
models.postCodes = db.import('./postCode');
models.postCodeByUser = db.import('./postCodeByUser');
models.confirmCodes = db.import('./confirmCode');
models.categories = db.import('./category');
models.items = db.import('./item');
models.itemByCategory = db.import('./itemByCategory');
models.itemByShop = db.import('./itemByShop');
models.shops = db.import('./shop');
models.units = db.import('./unit');
models.tokens = db.import('./token');
models.wishes = db.import('./wish');
models.shopping = db.import('./shopping');
models.history = db.import('./history');
models.devices = db.import('./device');
models.images = db.import('./image');
models.advertising = db.import('./advertising');
models.admins = db.import('./admin');
models.permissions = db.import('./permission');
models.permissionByAdmins = db.import('./permissionByAdmin');
models.shopByAdmins = db.import('./shopByAdmin');
models.lastEditLogs = db.import('./lastEditLog');
models.settings = db.import('./setting');
models.reports = db.import('./report');
models.advActivityByAdmin = db.import('./advActivityByAdmin');
models.salesActivityByAdmin = db.import('./salesActivityByAdmin');
models.shopManagingPermissions = db.import('./shopManagingPermission');
models.deliveryNotifications = db.import('./deliveryNotification');

for (var key in models) {
    if (models.hasOwnProperty(key)) {
        models[key].associate(models);
    }
}

module.exports = models;