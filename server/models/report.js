module.exports = function (db, DataType) {
    var report = db.define('report', {
        id      : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        name: {
            type        : DataType.STRING,
            defaultValue: false
        },
        url: {
            type        : DataType.STRING,
            defaultValue: false
        },
        admin_id   : {
            type      : DataType.INTEGER,
            required  : true
        },
        shop_id   : {
            type      : DataType.INTEGER,
            required  : true
        },
        type   : {
            type    : DataType.INTEGER,
            required: true
        }
    }, {
        classMethods: {
            associate: function (models) {
                report.belongsTo(models.admins, {as: 'admin', foreignKey: 'admin_id'});
                report.belongsTo(models.shops,  {as: 'shop', foreignKey: 'shop_id'});
            }
        }
    });

    return report;
};