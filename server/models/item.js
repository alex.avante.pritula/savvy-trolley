module.exports = function (db, DataType) {
    var item = db.define('item', {
        id             : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        barCode        : {
            type      : DataType.STRING(36),
            unique    : true,
            allowEmpty: false
        },
        name           : {
            type      : DataType.STRING,
            allowEmpty: false
        },
        brand          : {
            type      : DataType.STRING,
            allowEmpty: false
        },
        image_id       : {
            type      : DataType.INTEGER,
            allowEmpty: false
        },
        itemSize       : {
            type      : DataType.FLOAT
        },
        servingPerPack : {
            type      : DataType.INTEGER
        },
        servingSize    : {
            type      : DataType.INTEGER
        },
        enabled        : {
            type        : DataType.BOOLEAN,
            defaultValue: true
        },
        unit_id        : {
            type: DataType.INTEGER
        },
        serving_unit_id: {
            type: DataType.INTEGER
        }
    }, {
        classMethods: {
            associate                     : function (models) {
                item.belongsToMany(models.shops, {
                    as        : 'shops',
                    through   : {model: models.itemByShop},
                    foreignKey: 'item_id'
                });
                item.belongsToMany(models.categories, {
                    as        : 'categories',
                    through   : {model: models.itemByCategory},
                    foreignKey: 'item_id'
                });
                item.belongsToMany(models.users, {
                    as        : 'liked',
                    through   : {model: models.wishes},
                    foreignKey: 'item_id'
                });
                item.belongsToMany(models.itemByShop, {
                    as        : 'recommends',
                    through   : {model: models.advertising},
                    foreignKey: 'item_id'
                });
                item.belongsTo(models.images, {as: 'image', foreignKey: 'image_id'});
                item.belongsTo(models.units, {as: 'unitType', foreignKey: 'unit_id'});
                item.belongsTo(models.units, {as: 'servingType', foreignKey: 'serving_unit_id'});
            }
        }
    });
    return item;
};
