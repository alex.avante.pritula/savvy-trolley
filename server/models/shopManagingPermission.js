module.exports = function (db, DataType) {
    var shopManagingPermission = db.define('shopManagingPermission', {
        id      : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        message: {
            type        : DataType.STRING,
            defaultValue: false
        },
        type: {
            type        : DataType.BOOLEAN,
            defaultValue: false
        },
        admin_id   : {
            type      : DataType.INTEGER,
            required  : true
        },
        shop_id   : {
            type      : DataType.INTEGER,
            required  : true
        }
    }, {
        classMethods: {
            associate: function (models) {
                shopManagingPermission.belongsTo(models.admins, {
                    as: 'admin',
                    foreignKey: 'admin_id'
                });
                shopManagingPermission.belongsTo(models.shops, {
                    as: 'shop',
                    foreignKey: 'shop_id'
                });
            }
        }
    });

    return shopManagingPermission;
};
