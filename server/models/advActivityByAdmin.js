module.exports = function (db, DataType) {
    var advActivityByAdmin = db.define('advActivityByAdmins', {
        id: {
            type: DataType.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        admin_id: {
            type: DataType.INTEGER,
            required: true
        },
        advertising_id: {
            type: DataType.INTEGER,
            required: true
        },
        from        : {
            type    : DataType.BIGINT,
            required: true
        },
        to          : {
            type    : DataType.BIGINT,
            required: true
        }
    }, {
        classMethods: {
            associate: function (models) {
            }
        }
    });

    return advActivityByAdmin;
}