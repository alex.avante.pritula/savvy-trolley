module.exports = function (db, DataType) {
    var postCode = db.define('postCode', {
        id  : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        code: {
            type      : DataType.STRING(4),
            allowEmpty: false,
            unique    : true
        }
    }, {
        classMethods: {
            associate     : function (models) {
                postCode.belongsToMany(models.users, {
                    as        : 'users',
                    through   : {model: models.postCodeByUser},
                    foreignKey: 'post_code_id'
                });
            }
        }
    });

    return postCode;
};