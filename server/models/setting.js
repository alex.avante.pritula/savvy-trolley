module.exports = function (db, DataType) {
    var setting = db.define('setting', {
        id      : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        hasAdverts : {
            type        : DataType.BOOLEAN,
            defaultValue: true
        }
    }, {
        classMethods: {
            associate: function (models) {
            }
        }
    });

    return setting;
};