module.exports = function (db, DataType) {
    var admin = db.define('admin', {
        id      : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        email   : {
            type      : DataType.STRING(190),
            allowEmpty: false,
            unique    : true
        },
        password: {
            type      : DataType.STRING,
            allowEmpty: false
        },
        type    : {
            type      : DataType.STRING,
            allowEmpty: false
        },
        role    : {
            type      : DataType.STRING,
            allowEmpty: false
        },
        enabled : {
            type        : DataType.BOOLEAN,
            allowEmpty  : false,
            defaultValue: true
        }
    }, {
        classMethods: {
            associate     : function (models) {
                admin.belongsToMany(models.permissions, {
                    as        : 'permissions',
                    through   : {model: models.permissionByAdmins},
                    foreignKey: 'admin_id'
                });
                admin.belongsToMany(models.shops, {
                    as        : 'shops',
                    through   : {model: models.shopByAdmins},
                    foreignKey: 'admin_id'
                });
            }
        }
    });

    return admin;
};