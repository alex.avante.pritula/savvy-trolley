module.exports = function (db, DataType) {
    var salesActivityByAdmin = db.define('salesActivityByAdmins', {
        id: {
            type: DataType.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        admin_id: {
            type: DataType.INTEGER,
            required: true
        },
        product_id: {
            type: DataType.INTEGER,
            required: true
        },
        from        : {
            type    : DataType.BIGINT,
            required: true
        },
        to          : {
            type    : DataType.BIGINT,
            required: true
        }
    }, {
        classMethods: {
            associate: function (models) {
            }
        }
    });

    return salesActivityByAdmin;
}