module.exports = function (db, DataType) {
    var unit = db.define('unit', {
        id  : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        type: {
            type      : DataType.STRING,
            allowEmpty: false
        }
    }, {
        classMethods: {
            associate: function (models) {
            }
        }
    });

    return unit;
};
