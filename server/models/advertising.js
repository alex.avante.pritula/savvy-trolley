module.exports = function (db, DataType) {
    var advertising = db.define('advertising', {
        id          : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        item_id     : {
            type    : DataType.INTEGER,
            required: true
        },
        product_id  : {
            type    : DataType.INTEGER,
            required: true
        },
        post_code_id: {
            type    : DataType.INTEGER,
            required: true
        },
        from        : {
            type    : DataType.BIGINT,
            required: true
        },
        to          : {
            type    : DataType.BIGINT,
            required: true
        }
    }, {
        tableName   : 'advertising',
        classMethods: {
            associate          : function (models) {
            }
        }
    });
    return advertising;
};
