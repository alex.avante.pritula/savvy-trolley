module.exports = function (db, DataType) {
    var postCodeByUser = db.define('postCodeByUser', {
        id          : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        user_id     : {
            type    : DataType.INTEGER,
            required: true
        },
        post_code_id: {
            type: DataType.INTEGER,
            required: true
        }
    }, {
        classMethods: {
            associate: function (models) {
            }
        }
    });

    return postCodeByUser;
};