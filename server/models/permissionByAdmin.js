module.exports = function (db, DataType) {
    var permissionByAdmin = db.define('permissionByAdmin', {
        id         : {
            type         : DataType.INTEGER,
            primaryKey   : true,
            autoIncrement: true
        },
        admin_id       : {
            type    : DataType.STRING(190),
            unique  : true,
            required: true
        },
        permission_id: {
            type : DataType.TEXT,
            allowNull: true
        }
    }, {
        classMethods: {
            associate: function (models) {
            }
        }
    });

    return permissionByAdmin;
};
