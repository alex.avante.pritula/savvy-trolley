var Logger = require('node-logger-web');
var path = require('path');

var config = require('../../config');
var sequelize = require('./sequelize');

var storage = false;

if(config.logger.db){
    storage = new Logger.SequelizeMySqlStorage({
        db: sequelize,
        handLogsModel: config.logger.models.handLogsModel,
        autoLogsModel: config.logger.models.autoLogsModel
    });
}

var fileName = path.join(config.root, config.logger.fileName || '');

var logger = Logger({
    properties: config.logger.properties,
    storage: storage,
    fileName: config.logger.fileName ? fileName : false,
    enable: config.logger.enable
});

module.exports = logger;