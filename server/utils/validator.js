var revalidator = require('revalidator');

module.exports = function (data, schema) {
    if (!data) {
        var error = new Error('Invalid argument.');
        error.status = 400;
        return error;
    }
    var result = revalidator.validate(data, schema);
    if (!result.valid) {
        var message = [
            result.errors[0].property,
            result.errors[0].message
        ].join(' ');
        error = new Error(message);
        error.status = 400;
        return error;

    }

    return false;
};