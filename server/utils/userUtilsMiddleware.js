function checkUserDataAccess(req, res, next) {
    var id = req.params.userId;
    if (id === 'me') {
        return next();
    }
    var user = req.user;
    if (!user) {
        var error = new Error();
        error.status = 401;
        return next(error);
    }
    if (user.id != id) {
        error = new Error();
        error.status = 403;
        return next(error);
    }
    next();
}

function checkPostCode(req, res, next) {
    if (req.user.postCodes.length == 0) {
        var error = new Error('Invalid postCodes!');
        error.status = 417;
        return next(error);
    }
    next();
}

module.exports = {
    checkUserDataAccess: checkUserDataAccess,
    checkPostCode      : checkPostCode
};