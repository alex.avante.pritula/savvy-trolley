var AWS = require('aws-sdk');
var fs = require('fs');
var uuid = require('uuid');
var Promise = require('bluebird');
var _ = require('lodash');

var logger = require('./logger');
var config = require('../../config');
var toJson = JSON.stringify;
var sizeOf = require('image-size');

AWS.config.loadFromPath(config.root + '/config/aws.json');

function createOptions(options) {
    var settings = {
        bucketName: 'savvy-trolley/images',
        fileName  : uuid.v1() + '.bin',
        path      : '/tmp/test'
    };
    return _.extend(settings, options);
}

/**
 * Upload file into S3 bucket
 *
 * @param options
 * @param options.path
 * @param options.fileName
 * @param done
 */

function uploadImage(options, done) {
    fs.readFile(options.path, function (err, body) {
        if (err) {
            done(err, null);
            return;
        }

        var imageSize = sizeOf(body);

        if (imageSize.width !== imageSize.height) {
            var error = new Error('Image has not ratio 1:1');
            error.status = 422;
            done(error, null);
            return;
        }

        upload(options, body, done);

    });
}

function upload(options, body, done){
    options = createOptions(options);
    var params = {
        Bucket: options.bucketName,
        Key:    options.fileName
    };

    var s3 = new AWS.S3({ params: params });
    s3.upload({
        Body: body,
        ACL: 'public-read'
    }).on('httpUploadProgress', function (progress) {
        var loaded = progress.loaded;
        var total  = progress.total || 1;
        logger.log('info', 'uploading ' + progress.key + ' file [' + (loaded * 100 / total).toFixed(2) + '%]');
    }).send(function (err, data) {
        logger.log('info', 'file uploaded ' + toJson(data));
        done(err, data);
    });
}

function remove(options, done){
    options = createOptions(options);
    var params = {
        Bucket: options.bucketName,
        Key:    options.fileName
    };

    var s3 = new AWS.S3();
    s3.deleteObject(params, function(err, data) {
        logger.log('info',toJson((err ? err : data)));
        done(err, data);
    });
}

module.exports = {
    uploadAsync: upload,
    upload     : Promise.promisify(upload),
    uploadImage: Promise.promisify(uploadImage),
    remove     : Promise.promisify(remove)
};

