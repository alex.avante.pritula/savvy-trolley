/*
 * Usage: /*return next(new Error('{"status": 400, "fields": [{"id": null}]}'));
 * For sequelize errors just pass sequelize error object like this:
 * users.create(req.body)
 .catch(function (error) {
 return next(error);
 });
 *
 */

module.exports = function (app) {

    var handleSequelizeErrors = function (err) {

        if (err.name === 'SequelizeValidationError' || err.name === 'SequelizeUniqueConstraintError' || err.name === 'SequelizeForeignKeyConstraintError') {
            err.status = 422;
            var fields = [];

            if (err.errors) {
                err.errors.forEach(function (error) {
                    fields.push({
                        field  : error.path,
                        message: error.message
                    })
                });
            }
            err.fields = fields;
        }
    };
    var handleMulterErrors = function (err) {
        var multerErrors = {
            'LIMIT_PART_COUNT'     : 'Too many parts',
            'LIMIT_FILE_SIZE'      : 'File too large',
            'LIMIT_FILE_COUNT'     : 'Too many files',
            'LIMIT_FIELD_KEY'      : 'Field name too long',
            'LIMIT_FIELD_VALUE'    : 'Field value too long',
            'LIMIT_FIELD_COUNT'    : 'Too many fields',
            'LIMIT_UNEXPECTED_FILE': 'Unexpected field'
        };
        if (multerErrors[err.code]) {
            err.status = 400;
        }
    };

    return function (err, req, res, next) {
        try {
            var error = JSON.parse(err.message);
        }
        catch (e) {
            handleSequelizeErrors(err);
            handleMulterErrors(err);
            return (next(err));
        }

        err.status = error.status;
        err.message = error.message || '';
        err.fields = error.fields || [];

        err.isError = 1;
        if (err)
            return next(err);

        return next();
    }
};