var jwt = require('jsonwebtoken');
var crypto = require('crypto');
var Promise = require('bluebird');
var secret = require('../../config').constants.JWT_SECRET;

function createHash(array) {
    var hash = crypto.createHash('sha256');
    var result = hash.update('savvy')
    array.forEach(function (item) {
        result.update('' + item);
    });
    result.update('trolley');
    return result.digest('base64');
}

function cryptPassword(password) {
    return createHash([password]);
}

function createToken(data, time, callback) {
    return jwt.sign(data, secret, {expiresIn: time}, callback);
}

function encryptToken(token, ignore, calback) {
    return jwt.verify(token, secret, {ignoreExpiration: ignore}, calback);
}

module.exports = {
    cryptPassword    : cryptPassword,
    createToken      : createToken,
    encryptTokenAsync: encryptToken,
    encryptToken     : Promise.promisify(encryptToken),
    createHash       : createHash
};