var Promise = require('bluebird');
var util = require('util');
var passport = require('passport');
var FacebookTokenStrategy = require('passport-facebook-token');
var GoogleTokenStrategy = require('passport-google-token').Strategy;
var JwtBearerStrategy = require('passport-http-jwt-bearer').Strategy;
var LocalStrategy = require('passport-local').Strategy;

var Cache = require('./cache');
var crypt = require('./crypt');
var logger = require('./logger');
var models = require('../models');
var services = require('../services');
var config = require('../../config');


var format = util.format;
var cache = new Cache(config.redis);

var errorFormatter = services.user.errorFormatter;

function findOrCreateUser(email, accountType) {
    var data = {email: email, accountType: accountType, verified: true};
    var query = {
        where  : data,
        include: [
            {model: models.postCodes, as: 'postCodes'},
            {model: models.tokens, as: 'tokens'}
        ]
    };
    return models.users.find(query)
        .then(function (user) {
            if (user) {
                return user;
            }
            return models.users.create(data);
        });
}

function getUser(id, email, token) {
    var options = {
        where  : {id: id, email: email},
        include: [
            {model: models.postCodes, as: 'postCodes', attributes: ['id', 'code'], through: {attributes: []}},
            {model: models.tokens, as: 'tokens', attributes: ['id', 'token'], where: {token: token}}
        ]
    };
    return models.users.findOne(options);
}

function getAdmin(email, password) {
    var options = {
        where     : {
            email   : email,
            password: crypt.cryptPassword(password)
        },
        attributes: {exclude: ['password', 'updatedAt']},
        include   : [{
            model     : models.permissions,
            as        : 'permissions',
            attributes: ['id', 'permission'],
            through   : {
                attributes: []
            }
        }]
    };

    return models.admins.findOne(options);
}

function init() {
    passport.serializeUser(function (admin, done) {
        logger.log('info', 'passport.serializeUser');
        done(null, admin.id);
    });

    passport.deserializeUser(function (id, done) {
        models.admins.findById(id, {
            attributes: {exclude: ['password', 'updatedAt']},
            include   : [{
                model     : models.permissions,
                as        : 'permissions',
                attributes: ['id', 'permission'],
                through   : {
                    attributes: []
                }
            }]
        })
            .then(function (admin) {
                logger.log('info', 'passport.deserializeUser');
                admin = admin || false;
                done(null, admin);
                return null;
            })
            .catch(done);
    });

    passport.use(new GoogleTokenStrategy({
        clientID    : config.social.google.clientID,
        clientSecret: config.social.google.clientSecret
    }, function (accessToken, refreshToken, profile, done) {
        process.nextTick(function () {
            logger.log('info', format('GoogleTokenStrategy', profile));
            var email = profile.emails[0].value || profile.id + '@gmail.com';
            var accountType = profile.provider;
            if (!email || !accountType) {
                var error = new Error('Invalid token');
                error.status = 401;
                done(error);
            }
            findOrCreateUser(email, accountType)
                .then(function (user) {
                    done(null, user);
                    return null;
                })
                .catch(function (err) {
                    done(errorFormatter(err));
                });
        });
    }));

    passport.use(new FacebookTokenStrategy({
        clientID    : config.social.facebook.clientID,
        clientSecret: config.social.facebook.clientSecret
    }, function (accessToken, refreshToken, profile, done) {
        process.nextTick(function () {
            logger.log('info', format('FacebookTokenStrategy', profile));
            var email = profile.emails[0].value || profile.id + '@facebook.com';
            var accountType = profile.provider;
            if (!email || !accountType) {
                var error = new Error('Invalid token');
                error.status = 401;
                done(error);
            }
            findOrCreateUser(email, accountType)
                .then(function (user) {
                    done(null, user);
                    return null;
                })
                .catch(function (err) {
                    done(errorFormatter(err));
                });
        });
    }));

    passport.use(new JwtBearerStrategy(config.constants.JWT_SECRET, function (token, done) {
        logger.log('info', format('JwtBearerStrategy', token));
        process.nextTick(function () {
            var hash = 'USER_' + token.id;
            cache.read(hash, token.token, function (err, user) {
                if (err) return done(err);
                if (user) return done(null, user, token);
                getUser(token.id, token.email, token.token)
                    .then(function (user) {
                        if (!user) {
                            var error = new Error('Not found');
                            error.status = 401;
                            return Promise.reject(error);
                        }
                        if (user.tokens.length === 0) {
                            error = new Error('Invalid token');
                            error.status = 401;
                            return Promise.reject(error);
                        }
                        if (user.accountType === 'email' && token.checkSum !== crypt.cryptPassword(user.password)) {
                            error = new Error('Checksum conflict');
                            error.status = 401;
                            return Promise.reject(error);
                        }
                        cache.write(hash, token.token, user, function (err) {
                            if (err) return done(err);
                            done(null, user, token);
                        });
                        return null;
                    })
                    .catch(done);
            });
        });
    }));

    passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    }, function (email, password, done) {
        process.nextTick(function () {
            getAdmin(email, password)
                .then(function (admin) {
                    if (!admin) {
                        done(null, false);
                        return null;
                    }
                    var permissions = admin.permissions.map(function (permission) {
                        return permission.permission;
                    });
                    done(null, admin, permissions);
                    return null;
                })
                .catch(done);
        });
    }));
}

function adminAuth(req, res, next) {
    passport.authenticate('local', {session: true})(req, res, next);
}

function logout(req, res, next) {
    req.logout();
    res.clearCookie('user');
    res.status(204);
    res.send();
}

function jwtAuth(req, res, next) {
    passport.authenticate('jwt-bearer', {session: false}, authErrorHandler(req, next, 'jwtAuth'))(req, res, next);
}

function facebookAuth(req, res, next) {
    passport.authenticate('facebook-token', {session: false}, authErrorHandler(req, next, 'facebookAuth'))(req, res, next);
}

function googleAuth(req, res, next) {
    passport.authenticate('google-token', {session: false}, authErrorHandler(req, next, 'googleAuth'))(req, res, next);
}

function authErrorHandler(req, next, handler) {
    return function (err, user, message) {
        if (err) {
            logger.log('error', format(handler, err));
            return next(err);
        }
        if (!user) {
            err = new Error('Unauthorized');
            err.status = 401;
            logger.log('error', format(handler, message, err));
            return next(err);
        }
        req.user = user;
        next();
    }
}

function fixUnderscore(req, res, next) {
    if (req.body.accessToken) {
        req.body.access_token = req.body.accessToken;
    }
    next();
}

module.exports = {
    passport: passport,
    init    : init,
    logout  : logout,
    admin   : adminAuth,
    auth    : jwtAuth,
    facebook: [fixUnderscore, facebookAuth],
    google  : [fixUnderscore, googleAuth]
};