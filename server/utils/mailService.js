
var Mailgun = require('mailgun-js');
var Promise = require('bluebird');
var _ = require('lodash');

var config = require('../../config');

var MESSAGES = {
    VERIFY  : [
        'Hello! <br>',
        'To confirm your Savvy Trolley account please click ',
        '<a href="http://{{host}}:{{port}}/api/v1/auth/confirm/{{id}}">here</a>'
    ].join(''),
    RECOVERY: [
        'Hello! <br>',
        'Your new password: {{password}}<br>',
        'To confirm recovery password for your Savvy Trolley account please click ',
        '<a href="http://{{host}}:{{port}}/api/v1/auth/recovery/approve?code={{id}}">here</a>'
    ].join(''),
    MONTH_REPORT: [
        'Hello! <br>',
        'Month report to shop: {{name}}<br>',
        '<a href="{{filename}}">Link to upload report</a>'
    ].join('')
};

var server = config.server;
var mailgun = new Mailgun({apiKey: config.mailgun.apiKey, domain: config.mailgun.domain});

function replacer(string, fields) {
    var result = string;
    Object.keys(fields).forEach(function (field) {
        result = result.replace('{{' + field + '}}', fields[field]);
    });
    return result;
}

function verifyMessage(id, mail) {
    var subject = 'Sign up';
    var fields = Object.assign({}, server, {id: id});
    var message = replacer(MESSAGES.VERIFY, fields);

    return sendMessage(mail, subject, message);
}

function recoveryMessage(id, mail, password) {
    var subject = 'Password recovery';
    var fields = Object.assign({}, server, {
        id: id,
        password: password
    });
    var message = replacer(MESSAGES.RECOVERY, fields);

    return sendMessage(mail, subject, message);
}

function monthReportMessage(filename, name, mail) {
    var subject = 'Month report by shop:'+ name;
    var fields = Object.assign({}, server, {
        name: name,
        filename: filename
    });
    var message = replacer(MESSAGES.MONTH_REPORT, fields);

    return sendMessage(mail, subject, message);
}

/**
 * Send message
 *
 * @param mail
 * @param subject
 * @param message
 * @return {Promise}
 */

function sendMessage(mail, subject, message) {
    return new Promise(function (resolve, reject) {
        var data = {
            from   : config.mailgun.from,
            to     : mail,
            subject: subject,
            html   : message
        };

        mailgun.messages().send(data, function (err, body) {
            if (err) {
                reject(err);
            } else {
                resolve(body);
            }
        });
    })

}

module.exports = {
    monthReportMessage: monthReportMessage,
    verifyMessage  : verifyMessage,
    recoveryMessage: recoveryMessage
};