var gcm = require('node-gcm');
var apn = require('apn');
var _ = require('lodash');
var async = require('async');
var Parallel = require('node-parallel');

var DEVICE_TYPES = {
    ANDROID: 1,
    IOS    : 2
};

var settings = {
    gcm: {
        id         : null,
        options    : {},
        msgcnt     : 1,
        defaultData: {
            delayWhileIdle: false,
            timeToLive    : 4 * 7 * 24 * 3600,
            retries       : 4
        }
    },
    apn: {
        gateway    : 'apn://gateway.sandbox.push.apple.com:2195',
        badge      : 1,
        defaultData: {
            expiry: 4 * 7 * 24 * 3600,
            sound : 'ping.aiff'
        }
    }
};

function PushNotifications(options) {
    this.settings = _.extend({}, settings, options);
}

/**
 * Send raw notification
 *
 * @param token
 * @param message
 * @param options
 * @callback done
 */
PushNotifications.prototype.send = function (message, token, options, done) {
    console.log('PushNotifications#send', arguments);
    if (typeof options == 'function') {
        done = options;
        options = {};
    }

    // Android GCM
    if ((options.type && options.type == DEVICE_TYPES.ANDROID) || token.length > 64) {
        return sendGCM(message, token, this.settings, done);
    }

    // iOS APN
    if ((options.type && options.type == options.type == DEVICE_TYPES.IOS) || token.length === 64) {
        return sendAPN(message, token, this.settings, done);
    }

    var error = new Error('Invalid recipient!');
    error.token = token;
    done(error);
};

/**
 * Send pushes.
 *
 * @callback before - call before sending push
 * @callback after - call after sending push
 * @returns {Function}
 */
PushNotifications.prototype.sendPushes = function(before, after) {
    var self = this;
    return function (message, recipients, callback) {
        console.log('PushNotifications#sendDecoratePush', arguments);
        async.map(recipients, function (recipient, next) {
            var tempMessage = Object.assign({}, message);
            if(before) before(tempMessage, recipient);

            self.sendPush(tempMessage, recipient, function (err, result) {
                if (err) return next(err);
                if(after) after(tempMessage, recipient, result, next);
                else next(null, result);
            });
        }, callback);
    };
};

/**
 * Send push method.
 * @param message
 * @param recipient
 * @param done
 * @param recipient.token
 * @callback done
 */
PushNotifications.prototype.sendPush = function(message, recipient, done) {
    console.log('PushNotifications#sendPush', arguments);
    recipient = recipient.token;
    this.send(message, recipient, function (err, result) {
        result = err || result;
        done(null, result);
    });
};

/**
 * Send notification for android (GCM).
 *
 * @param data
 * @param token
 * @param settings
 * @param done
 */
function sendGCM(data, token, settings, done) {
    var sender = new gcm.Sender(settings.gcm.id, settings.gcm.options);
    var note = new gcm.Message(settings.gcm.defaultData);
    Object.keys(data).forEach(function (property) {
        note.addData(property, data[property]);
    });

    console.log('GCMMessage data', data);
    console.log('GCMMessage Message', note);
    console.log('GCMMessage ids', token);

    sender.send(note, {registrationTokens: [token]}, function (err, result) {
        if (err) return done(err);
        if (result.failure !== 0) {
            var message = result.results[0].error;
            return done({device: 'android', token: token, message: message});
        }

        done(null, {device: 'android', token: token, message: 'completed'});
    });
}

/**
 * Send notification for iOS (APN).
 * @param data
 * @param token
 * @param settings
 * @param done
 */
function sendAPN(data, token, settings, done) {
    var parallel = new Parallel();
    var APNConnection = new apn.Connection(settings.apn);

    var note = new apn.Notification();
    note.expiry = Math.floor(Date.now() / 1000) + settings.apn.defaultData.expiry;
    note.badge = settings.apn.badge;
    note.sound = settings.apn.defaultData.sound;
    note.alert = data.title;
    delete data.title;
    note.payload = data;

    console.log('APNConnection data', data);
    console.log('APNConnection Message', note);
    console.log('APNConnection ids', token);

    APNConnection.pushNotification(note, [token]);

    parallel.add(function (done) {
        APNConnection.on('completed', function () {
            console.warn({device: 'ios', token: token, message: 'completed'});
            done(null, {device: 'ios', token: token, message: 'completed'});
        });
        APNConnection.on('error', function () {
            console.warn({device: 'ios', token: token, message: 'error'});
            done({device: 'ios', token: token, message: 'error'});
        });
        APNConnection.on('socketError', function () {
            done({device: 'ios', token: token, message: 'socketError'});
        });
        APNConnection.on('transmissionError', function () {
            done({device: 'ios', token: token, message: 'transmissionError'});
        });
        APNConnection.on('cacheTooSmall', function () {
            done({device: 'ios', token: token, message: 'cacheTooSmall'});
        });
    });

    parallel.done(function (err, results) {
        if (err) return done(err);
        done(null, results);
    });
}

PushNotifications.DEVICE_TYPES = DEVICE_TYPES;

module.exports = PushNotifications;
