var _permissions = [
    'PROFILE',
    'CREATE_SHOP',
    'REVIEW_SHOP',
    'EDIT_SHOP',
    'CREATE_ITEM',
    'REVIEW_ITEM',
    'EDIT_ITEM',
    'CREATE_ADMIN',
    'REVIEW_ADMIN',
    'EDIT_ADMIN',
    'CREATE_USER',
    'REVIEW_USER',
    'EDIT_USER',
    'ASSIGN_SHOP',
    'REVIEW_STATISTIC',
    'CREATE_CATEGORY',
    'REVIEW_CATEGORY',
    'EDIT_CATEGORY',
    'CREATE_ADVERTISING',
    'REVIEW_ADVERTISING',
    'EDIT_ADVERTISING',
    'UPLOAD_FROM_CSV',
    'BLOCK_PERMISSIONS_TO_SHOP'
];

var permissions = {};

function hasAdmin(req, res, next) {
    var permissions = req.permissions;
    if (!permissions) {
        var error = new Error();
        error.status = 403;
        return next(error);
    }
    next();
}

_permissions.forEach(function (permission) {
    permissions[permission] = [hasAdmin, createPermissionMiddleware(permission)];
    function createPermissionMiddleware(match) {
        return function (req, res, next) {
            var found = req.permissions[match];
            if (!found) {
                var error = new Error('You don`t have access to this API!');
                error.status = 403;
                return next(error);
            }

            next();
        }
    }
});

module.exports = permissions;