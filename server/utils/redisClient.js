var redis = require('redis');
var config = require('../../config');
var Promise = require('bluebird');
Promise.promisifyAll(redis.RedisClient.prototype);
var client = redis.createClient(config.redis.port, config.redis.host);

module.exports =  client;
