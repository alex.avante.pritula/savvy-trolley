var config = require('./../../config');
var Sequelize = require('sequelize');

var sequelize = new Sequelize(config.db.dbname, config.db.user, config.db.password, {
    host   : config.db.host,
    dialect: 'mysql',

    pool: {
        max : 40,
        min : 5,
        idle: 10000
    },
    logging: config.env == 'development'? console.log: null
});

module.exports = sequelize;
