var Multer = require('multer');
var uuid = require('uuid');
var mime = require('mime');
var path = require('path');

var config = require('../../config');

var storage = Multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(config.root, '/tmp/'));
    },
    filename   : function (req, file, cb) {
        var name = uuid.v1();
        var type = mime.extension(file.mimetype);
        cb(null, name + '.' + type);
    }
});

var multer = Multer({
    storage: storage,
    limits : { fileSize: config.constants.MAX_IMAGE_SIZE }
});

module.exports = multer;