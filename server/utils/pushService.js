var _ = require('lodash');

var Pusher =    require('./pusher');
var services = require('../services');
var config = require('../../config');
var async = require('async');

var pusher = new Pusher(config.push);

const deliveryNotificationService = require('../services/deliveryNotificationService');

/**
 * Send pushes when price reduced.
 * @param message
 * @param options
 * @returns {Promise}
 */
function sendWish(message, options) {
    console.log('sendWish', arguments);

    services.device.countDevicesPerWishes(options)
        .then(function (count) {
            var settings = {
                count       : count,
                queryOptions: options,
                query       : services.device.getDevicesPerWishes
            };
            _send(message, settings);
        });
}

/**
 * Send pushes when abstract item was disabled.
 * @param message
 * @param options
 */
function sendItemEnableChanged(message, options) {
    console.log('sendItemEnableChanged', arguments);
    services.device.countDevicesPerItem(options)
        .then(function (count) {
            var settings = {
                count       : count,
                queryOptions: options,
                query       : services.device.getDevicesPerItem,
                before      : decorator
            };
            _send(message, settings);
        });

    /**
     * Decorate message for concrete user (Before hook).
     * @param message
     * @param recipient
     * @returns {*}
     */
    function decorator(message, recipient) {
        console.log('Before send pushes hook', arguments);
        var title = message.title;
        if (recipient.isItemInTrolley && recipient.isItemInWishes) {
            message.title = 'Uh oh... ' + title + ' in your Shopping List is now out of stock. Please review your Shopping List';
        } else {
            message.title = recipient.isItemInWishes
                ? 'Uh oh... ' + title + ' in your Wish List is now out of stock. Please review your Wish List'
                : 'Uh oh... ' + title + ' in your Shopping List is now out of stock. Please review your Shopping List';
        }
    }
}

/**
 * Send pushes when product was disabled.
 * @param message
 * @param options
 */
function sendProductEnableChanged(message, options) {
    console.log('sendProductEnableChanged', arguments);
    services.device.countDevicesPerConcreteItem(options)
        .then(function (count) {
            console.log('countDevicesPerConcreteItem', arguments);
            var settings = {
                count       : count,
                queryOptions: options,
                query       : services.device.getDevicesPerConcreteItem
            };
            _send(message, settings);
        });
}

/**
 * Send pushes when product was marked "out of stock".
 * @param message
 * @param options
 */
function sendOutOfStockChanged(message, options) {
    console.log('sendOutOfStockChanged', arguments);
    services.device.countDevicesForItemWithShop(options)
        .then(function (count) {
            console.log('countDevicesForItemWithShop', arguments);
            var settings = {
                count       : count,
                queryOptions: options,
                query       : services.device.getDevicesForItemWithShop
            };
            _send(message, settings);
        })
        .catch(function(error){
            console.log(error);
        });
}

/**
 * Send pushes when new shop added.
 * @param message
 * @param options
 * @returns {Promise}
 */
function sendShopAdded(message, options) {
    console.log('shopAdded', arguments);
    services.device.countDevicesForShopWithPostCode(options)
        .then(function (count) {
            console.log('countDevicesForShopWithPostCode', arguments);
            var settings = {
                count       : count,
                queryOptions: options,
                query       : services.device.getDevicesForShopWithPostCode
            };
            _send(message, settings);
        })
        .catch(function(error){
            console.log(error);
        });
}

/**
 * Iterate device token and send pushes.
 * @param message
 * @param {Object} settings
 * @param {Function} settings.count
 * @param {Function} settings.query
 * @param {Object} settings.queryOptions
 * @param {Function} settings.before
 * @param {Function} settings.after
 */
function _send(message, settings) {
    console.warn("3333333333333333333333333333333333333333333333333333333333333", message, settings);

    var limit = config.constants.DEVICE_LIMIT;
    var _sendPush = pusher.sendPushes(settings.before, settings.after);

    var iterateCount = Math.ceil(settings.count / limit);

    async.times(iterateCount, iterate, complete);

    function iterate(iteration, next) {
        var options = _.extend({}, settings.queryOptions, {
            limit : limit,
            offset: iteration * limit
        });
        settings
            .query(options)
            .then(function (recipients) {
                _sendPush(message, recipients, next);
            })
            .catch(next);
    }
}

/**
 * Iterate device token and send pushes without query.
 * @param message
 * @param {Array} recipients
 */
function sendPushByTokens(message, recipients) {
    console.log('sendPushes', arguments);

    let _sendPush = pusher.sendPushes();
    let iterateCount = recipients.length;

    async.times(iterateCount, iterate, complete.bind(this, message));

    function iterate(iteration, next) {
        _sendPush(message, recipients, next);
    }
}

/**
 * Complete sending pushes handler.
 * @param message
 * @param err
 * @param results
 */
function complete(message, err, results) {
    if (err) {
        return console.log(new Date(), 'error : PushNotificationError', err);
    }
    console.log(new Date(), 'info : PushNotifications');

    if (typeof(message["ids"]) !== "undefined") {
        deliveryNotificationService.setIsSentNotification(message["ids"]);
    }

    results.forEach(function (result) {
        console.log('info : PushNotification ', result);
    });
}

function send(push, additionalData) {
    var message = push.message;
    var options = push.options;

    if (typeof (additionalData) != 'undefined') {
        if (typeof (additionalData["recipients"]) != 'undefined') {
            sendPushByTokens(message, additionalData["recipients"])
        }
        return;
    }

    if(!message) return;

    switch(message.pushType){
        case config.constants.PUSH_TYPES.REGULAR_PRICE_REDUCED:
            sendWish(message, options);
            break;
        case config.constants.PUSH_TYPES.SALE_START:
            sendWish(message, options);
            break;
        case config.constants.PUSH_TYPES.SALE_FINISH:
            getDevicesPerItem(message, options);
            break;
        case config.constants.PUSH_TYPES.ITEM_ENABLED_MODE_CHANGED:
            sendItemEnableChanged(message, options);
            break;
        case config.constants.PUSH_TYPES.CONCRETE_ITEM_ENABLED_MODE_CHANGED:
            sendProductEnableChanged(message, options);
            break;
        case config.constants.PUSH_TYPES.ITEM_OUT_OF_STOCK:
            sendOutOfStockChanged(message, options);
            break;
        case config.constants.PUSH_TYPES.SHOP_ADDED:
            sendShopAdded(message, options);
            break;
    }
}

module.exports = {
    send      : send
};
