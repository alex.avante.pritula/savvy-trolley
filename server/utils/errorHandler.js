var errorHandler = require('express-error-handler');
var logger = require('./logger');
var config = require('../../config');

module.exports = function (app) {
    return [
        function (err, req, res, next) {
            if (err) {
                err.status = err.status || 500;
                if (process.env.NODE_ENV === 'test') {
                    return next(err);
                }

                logger.error({
                    status : err.status,
                    message: err.message,
                    method : req.method,
                    url    : req.url,
                    body   : req.body,
                    ip     : req.ip,
                    stack  : err.stack
                }, err);
                next(err);
            } else {
                next();
            }
        },
        errorHandler({
            server  : app,
            handlers: {
                '500': function err500(err, req, res, next) {
                    res.status(500);
                    res.send({
                        error  : 500,
                        message: err.message || 'Something unexpected happened. The problem has been logged and we\'ll look into it!'
                    });
                },
                '503': function err503(err, req, res, next) {
                    res.status(503);
                    res.send({
                        error  : 503,
                        message: 'We\'re experiencing heavy load, please try again later'
                    });
                },
                '409': function err409(err, req, res, next) {
                    res.status(409);
                    res.send({
                        error  : 409,
                        message: err.message || "The specified resource already exists"
                    });
                },
                '405': function err405(err, req, res, next) {
                    res.status(405).send({
                        error  : 405,
                        message: "Method not allowed"
                    });
                },
                '404': function err404(err, req, res, next) {
                    res.status(404);
                    res.send({
                        error  : 404,
                        message: err.message || "Not Found"
                    });
                },
                '403': function err40(err, req, res, next) {
                    res.status(403);
                    res.send({
                        error  : 403,
                        message: err.message || "Forbidden"
                    });
                },
                '401': function err401(err, req, res, next) {
                    res.status(401);
                    res.send({
                        error  : 401,
                        message: err.message || "Unauthorized"
                    });

                },
                '400': function err400(err, req, res, next) {
                    res.status(400);
                    res.send({
                        error  : 400,
                        message: err.message || "Invalid request"
                    });
                },
                '422': function err422(err, req, res, next) {
                    res.status(422);
                    res.send({
                        error  : 422,
                        message: err.message || "Invalid fields",
                        fields : err.fields
                    });
                }
            }
        })]
};
