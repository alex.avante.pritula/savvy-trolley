var cron = require('cron');

var logger = require('./logger');
var models = require('../models');
var config = require('../../config');

var time = 7 * 24 * 60 * 60 * 1000;
//var time = 60 * 1000;

function removeNotConfirmedUser() {
    var timestamp = Date.now() - time;
    var date = new Date(timestamp);
    return models.users.destroy({where: {createdAt: {$lte: date}, verify: false}});
}

job = new cron.CronJob({
    cronTime: config.cleaner.time,
    onTick: function () {
        removeNotConfirmedUser()
            .then(function(body){
                logger.log('info', 'Automatically remove %s not confirmed users!', body)
            })
            .catch(function(err){
                logger.log('error', err)
            });
    }
});

module.exports = job;

