var redis = require('redis');

function Cache(config) {
    if (!config) {
        throw new Error('Config is required');
    }
    if(config.enabled) {
        this._client = redis.createClient(config.server);
        this._enabled = config.enabled;
    }
}

Cache.prototype.read = function (hash, key, callback) {
    if (!this._enabled) return callback(null, false);
    this._client.hgetall(hash, function (err, data) {
        if (err) return callback(err);
        data = data && data[key] ? JSON.parse(data[key]) : false;
        callback(null, data);
    });
};

Cache.prototype.write = function (hash, key, value, callback) {
    if (!this._enabled) return callback(null, false);
    var write = {};
    write[key] = JSON.stringify(value);
    this._client.hmset(hash, write, callback);
};

Cache.prototype.remove = function (key, callback) {
    if (!this._enabled) return callback(null, false);
    this._client.del(key, callback);
};

module.exports = Cache;