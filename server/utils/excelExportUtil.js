var async = require('async');
var Excel = require('exceljs');
var Promise = require('bluebird');

var keys = ['stream', 'sheetName', 'columns', 'iterator', 'iterationCount', 'limit'];

/**
 * Universal JSON to XLSX stream exporter
 * @param options
 * @param options.stream
 * @param options.sheetName
 * @param options.columns
 * @param options.iterator
 * @param options.iterationCount
 * @param options.limit
 * @param {Function} callback
 * @returns {*}
 */
function exportToXLSX(options, callback) {
    if (!options) {
        var error = new Error('invalid options');
        return callback(error);
    }

    for (var i = 0; i < keys.length; i++) {
        if (!options[keys[i]]) {
            error = new Error('options.' + keys[i] + ' has required');
            return callback(error);
        }
    }

    var limit = options.limit;
    var iterator = options.iterator;
    var workbookOptions = {
        stream          : options.stream,
        useStyles       : true,
        useSharedStrings: true
    };

    var workbook = new Excel.stream.xlsx.WorkbookWriter(workbookOptions);
    var sheet = workbook.addWorksheet(options.sheetName);

    sheet.columns = options.columns;
    async.timesSeries(options.iterationCount, iterate, end);

    function iterate(iteration, next) {
        var options = {
            limit : limit,
            offset: iteration * limit
        };
        iterator(options)
            .then(function (statistics) {
                statistics.forEach(function (row) {
                    sheet.addRow(row).commit();
                });
                next();
                return null;
            })
            .catch(next);
    }

    function end(err) {
        if (err) return callback(err);
        workbook.commit()
            .then(function () {
                callback();
            })
            .catch(callback);
    }
}

module.exports = {
    exportToXlsxAsync: exportToXLSX,
    exportToXLSX     : Promise.promisify(exportToXLSX)
};