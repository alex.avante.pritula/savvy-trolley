var cors = require('cors');
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var passport = require('./utils/passportWrapper').passport;
var urlManager = require('./routes');
var logger = require('./utils/logger');
var errorHandler = require('./utils/errorHandler');
var disallowMethods = require('./utils/methodsHandler');
var errorFormatter = require('./utils/errorFormatter');
var config = require('../config');
var RedisStore = require('connect-redis')(session);

var storage = new RedisStore({
	host: config['redis'].server['host'],
	port: config['redis'].server['port']
});

//initialize the app
var app = module.exports = express();
app.use(cors());
//set up static files directory
app.use(express.static(__dirname + '/../frontend/'));
app.use('/admin', express.static(__dirname + '/../frontend/admin/dist'));
app.use('/bower_components', express.static(__dirname + '/../frontend/admin/bower_components'));

app.use(cookieParser());
app.use(session({
	secret: config['session'].secret,
	resave: true,
	saveUninitialized: true,
	store: storage,
	cookie: {maxAge: config['session'].maxAge}
}));

app.use(passport.initialize());
app.use(passport.session());

//parse request body
app.use(bodyParser.json({limit: config.constants['REQUEST_LIMIT']}));
app.use(bodyParser.raw({limit: config.constants['REQUEST_LIMIT']}));

app.use(logger.requestHandler);

//configure routs
urlManager(app);

//format errors
app.use(errorFormatter(app));
app.use(errorHandler(app));

//disallow unused methods
disallowMethods(app);

process.on('uncaughtException', function (err) {
    console.log('error', 'process error:', err.stack);
});

process.on('SIGINT', function () {
    process.exit();
});


