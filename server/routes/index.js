var passport = require('../utils/passportWrapper');
var controllers = require('../controllers');

var userApi = require('./user');
var adminApi = require('./admin');

module.exports = function (app) {
    //passport init
    passport.init();

    //static links
    app.get('/', controllers.static.index);
    app.get('/index', controllers.static.index);
    app.get('/error', controllers.static.error);
    app.get('/success', controllers.static.success);
    app.get('/api/doc', controllers.static.docs);

    app.get('/api/templates/:name', controllers.static.templates);

    app.head('/api/check', function (req, res) {
        res.status(200).send();
    });

    adminApi(app, passport);
    userApi(app, passport);

};