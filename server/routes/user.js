var controllers = require('../controllers');

var utils = require('../utils/userUtilsMiddleware');

module.exports = function (app, passport) {

   app.get('/api/v1/auth/confirm/:code', controllers.v1.authController.verify);
    app.post('/api/v1/auth/signup', controllers.v1.authController.signUp);
    app.post('/api/v1/auth/signin', controllers.v1.authController.signIn);
    app.post('/api/v1/auth/facebook', passport.facebook, controllers.v1.authController.social);
    app.post('/api/v1/auth/google', passport.google, controllers.v1.authController.social);
    app.post('/api/v1/auth/refresh', controllers.v1.authController.refresh);
    app.post('/api/v1/auth/recovery', controllers.v1.authController.recovery);
    app.get('/api/v1/auth/recovery/approve', controllers.v1.authController.approve);

    app.use('/api/*', passport.auth);

    app.route('/api/v1/:userId/notifications')
      .get(controllers.v1.notificationController.notifications);

    app.route('/api/v1/auth/logout')
        .post(controllers.v1.authController.logout);

    app.route('/api/v1/users/:userId')
        .get(utils.checkUserDataAccess, controllers.v1.usersController.me);

    app.route('/api/v1/users/:userId/password')
        .put(utils.checkUserDataAccess, controllers.v1.usersController.changePassword);

    app.route('/api/v1/users/:userId/postcodes')
        .put(utils.checkUserDataAccess, controllers.v1.usersController.changeMyPostCodes);

    app.route('/api/v1/users/:userId/wishes')
        .get(utils.checkUserDataAccess, utils.checkPostCode, controllers.v1.wishController.read)
        .put(utils.checkUserDataAccess, utils.checkPostCode, controllers.v1.wishController.sync);

    app.route('/api/v1/users/:userId/shopping')
        .get(utils.checkUserDataAccess, utils.checkPostCode, controllers.v1.shoppingController.read)
        .post(utils.checkUserDataAccess, utils.checkPostCode, controllers.v1.shoppingController.restore)
        .put(utils.checkUserDataAccess, utils.checkPostCode, controllers.v1.shoppingController.sync);

    app.route('/api/v1/users/:userId/history')
        .get(utils.checkUserDataAccess, utils.checkPostCode, controllers.v1.historyController.read)
        .post(utils.checkUserDataAccess, utils.checkPostCode, controllers.v1.historyController.update);

    app.route('/api/v1/users/:userId/subscribe')
        .put(utils.checkUserDataAccess, controllers.v1.notificationController.update);

    app.route('/api/v1/users/:userId/notifications')
        .patch(utils.checkUserDataAccess, controllers.v1.usersController.changeNotificationMode);

    app.route('/api/v1/categories')
        .get(controllers.v1.categoriesController.index);

    app.route('/api/v1/categories/:categoryId')
        .get(controllers.v1.categoriesController.read);

    app.route('/api/v1/categories/:parentId/subcategories')
        .get(controllers.v1.categoriesController.subcategories);

    app.route('/api/v1/categories/:categoryId/items')
        .get(utils.checkPostCode, controllers.v1.itemsController.getItemsByCategory);

    app.route('/api/v1/items/search')
        .get(utils.checkPostCode, controllers.v1.itemsController.search);

    app.route('/api/v1/items/:itemId')
        .get(utils.checkPostCode, controllers.v1.itemsController.readExpanded);

    app.route('/api/v1/items/:itemId/recommends')
        .get(utils.checkPostCode, controllers.v1.itemsController.recommends);

    app.route('/api/v1/items/:itemId/shops')
        .get(utils.checkPostCode, controllers.v1.itemsController.shops);

    app.route('/api/v1/items/:itemId/like')
        .post(controllers.v1.wishController.like)
        .delete(controllers.v1.wishController.dislike);

    app.route('/api/v1/shops')
        .get(controllers.v1.shopsController.index);

    app.route('/api/v1/shopLocations')
    .get(controllers.v1.shopsController.shopsLocations);

    app.route('/api/v1/shops/optimal')
        .get(utils.checkPostCode, controllers.v1.shopsController.optimalShopsOld)
        .post(utils.checkPostCode, controllers.v1.shopsController.optimalShops);

    app.route('/api/v1/shops/:shopId')
        .get(controllers.v1.shopsController.read);

    app.route('/api/v1/:userId/notifications')
      .get(controllers.v1.notificationController.notifications);

    app.route('/api/v1/shops/:shopId/items')
        .get(controllers.v1.productController.getItemsByShop);

};
