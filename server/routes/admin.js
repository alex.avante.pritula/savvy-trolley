var controllers = require('../controllers');
var access = require('../utils/accessController');
var config = require('../../config');
var redis = require('../utils/redisClient');

function checkAuth(req, res, next) {

    var admin = req.user;
    if (!admin) {
        var error = new Error();
        error.status = 401;
        return next(error);
    }

    if (!admin.enabled) {
        var error = new Error('Admin was disabled');
        error.status = 401;
        return next(error);
    }
    next();
 
 
   /* var sessionId = config['session'].prefix + req.sessionID;
    redis.get(sessionId,function(error, result){
        if (error)  return next(error);
        var maxAge = config['session'].maxAge;
        var data = JSON.parse(result);
        data.cookie.originalMaxAge = maxAge;
        data.cookie.expires = new Date(Date.now() + maxAge);
        var newState = JSON.stringify(data);
        redis.set(sessionId, newState, function(err){
            if (!err) return next(err);
            next();
        });
    });*/
}


module.exports = function (app, passport) {
    app.route('/admin/api/v1/auth')
        .post(passport.admin, checkAuth, function (req, res, next) {
            var admin = req.user.toJSON();
            admin.permissions = admin.permissions.map(function (permission) {
                return permission.permission;
            });
            res.send(admin);
        });

    app.use('/admin/api/*', checkAuth);

    app.use('/admin/api/*', function (req, res, next) {
        var admin = req.user;
        if (!admin || !admin.permissions) {
            var error = new Error();
            error.status = 401;
            return next(error);
        }
        req.permissions = {};
        admin.toJSON().permissions.forEach(function (permission) {
            req.permissions[permission.permission] = true;
        });
        return next();
    });

    app.route('/admin/api/v1/auth/current')
        .get(controllers.v1.adminController.current);

    app.route('/admin/api/v1/auth/logout')
        .get(passport.logout);

    app.post('/admin/api/v1/upload/images', controllers.v1.uploadController.uploadImage);

    app.route('/admin/api/v1/admins/free/savvy')
        .get(access['REVIEW_ADMIN'], controllers.v1.adminController.freeAdmin);

    app.route('/admin/api/v1/admins')
        .get(access['REVIEW_ADMIN'], controllers.v1.adminController.index)
        .post(access['CREATE_ADMIN'], controllers.v1.adminController.create);

    app.route('/admin/api/v1/admins/:adminId')
        .get(access['REVIEW_ADMIN'], controllers.v1.adminController.read)
        .patch(access['EDIT_ADMIN'], controllers.v1.adminController.enable)
        .delete(access['EDIT_ADMIN'], controllers.v1.adminController.remove);

    app.route('/admin/api/v1/admins/me/password')
        .patch(access['PROFILE'], controllers.v1.adminController.changePassword);

    app.route('/admin/api/v1/admins/:adminId/password')
        .patch(access['EDIT_ADMIN'], controllers.v1.adminController.changePassword);

    app.route('/admin/api/v1/admins/:adminId/updateAdmin')
        .patch(access['EDIT_ADMIN'], controllers.v1.adminController.updateAdmin);

    app.route('/admin/api/v1/settings')
        .get(controllers.v1.settingController.index);

    app.route('/admin/api/v1/users')
        .get(access['REVIEW_USER'], controllers.v1.usersController.index)
        .patch(access['EDIT_USER'], controllers.v1.settingController.changeAdverts);

    app.route('/admin/api/v1/users/:userId')
        .get(access['REVIEW_USER'], controllers.v1.usersController.read)
        .patch(access['EDIT_USER'], controllers.v1.usersController.changeAdvert)
        .delete(access['EDIT_USER'], controllers.v1.usersController.remove);

    app.route('/admin/api/v1/users/:userId/credentials')
      .patch(access['EDIT_USER'], controllers.v1.usersController.changeCredentials)

    app.route('/admin/api/v1/users/:userId/postcodes')
        .put(access['EDIT_USER'], controllers.v1.usersController.changePostCodes);

    app.route('/admin/api/v1/users/:userId/wishes')
        .get(access['REVIEW_USER'], controllers.v1.wishController.index)
        .put(access['EDIT_USER'], controllers.v1.wishController.update);

    app.route('/admin/api/v1/users/:userId/shopping')
        .get(access['REVIEW_USER'], controllers.v1.shoppingController.index)
        .put(access['EDIT_USER'], controllers.v1.shoppingController.update);

    app.route('/admin/api/v1/users/:userId/history')
        .get(access['REVIEW_USER'], controllers.v1.historyController.index);

    app.route('/admin/api/v1/categories')
        .get(access['REVIEW_CATEGORY'], controllers.v1.categoriesController.index)
        .post(access['CREATE_CATEGORY'], controllers.v1.categoriesController.create);

    app.route('/admin/api/v1/categories/:categoryId')
        .get(access['REVIEW_CATEGORY'], controllers.v1.categoriesController.read)
        .put(access['EDIT_CATEGORY'], controllers.v1.categoriesController.update)
        .delete(access['EDIT_CATEGORY'], controllers.v1.categoriesController.destroy);

    app.route('/admin/api/v1/categories/:parentId/subcategories')
        .get(access['REVIEW_CATEGORY'], controllers.v1.categoriesController.subcategories);

    app.route('/admin/api/v1/categories/:categoryId/items')
        .get(access['REVIEW_CATEGORY'], controllers.v1.itemsController.getItemsByCategory)

    app.route('/admin/api/v1/items')
        .get(access['REVIEW_ITEM'], controllers.v1.itemsController.index)
        .post(access['CREATE_ITEM'], controllers.v1.itemsController.create);

    app.route('/admin/api/v1/items/:itemId')
        .get(access['REVIEW_ITEM'], controllers.v1.itemsController.read)
        .put(access['EDIT_ITEM'], controllers.v1.itemsController.update)
        .patch(access['EDIT_ITEM'], controllers.v1.itemsController.updateEnablingMode)
        .delete(access['EDIT_ITEM'], controllers.v1.itemsController.destroy);

    app.route('/admin/api/v1/items/:itemId/categories')
        .get(access['REVIEW_ITEM'], controllers.v1.itemsController.getCategoriesByItem);

    app.route('/admin/api/v1/items/:itemId/recommends')
        .get(access['REVIEW_ADVERTISING'], controllers.v1.recommendationController.read)
        .post(access['CREATE_ADVERTISING'], controllers.v1.recommendationController.create);

    app.route('/admin/api/v1/items/:itemId/recommends/:productId')
        .put(access['EDIT_ADVERTISING'], controllers.v1.recommendationController.update)
        .delete(access['EDIT_ADVERTISING'], controllers.v1.recommendationController.destroy);

    app.route('/admin/api/v1/shops')
        .get(access['REVIEW_SHOP'], controllers.v1.shopsController.index)
        .post(access['CREATE_SHOP'], controllers.v1.shopsController.create);

    app.route('/admin/api/v1/shops/:shopId/permissions/:type')
        .get(access['REVIEW_SHOP'], controllers.v1.shopsController.getShopPermissionByType)
        .put(access['EDIT_SHOP'],  controllers.v1.shopsController.changeShopPermissionByType);

    app.route('/admin/api/v1/shops/:shopId/blockingHistoryToShop')
        .get(access['REVIEW_SHOP'], controllers.v1.shopsController.getBlockingHistoryToShop)
        .post(access['EDIT_SHOP'],  controllers.v1.shopsController.addBlockingHistoryItemToShop);

    app.route('/admin/api/v1/shops/:shopId/blockingHistoryCountToShop')
        .get(access['REVIEW_SHOP'], controllers.v1.shopsController.getBlockingHistoryCountToShop);

    app.route('/admin/api/v1/shops/:shopId')
        .get(access['REVIEW_ITEM'], controllers.v1.shopsController.read)
        .put(access['EDIT_SHOP'], controllers.v1.shopsController.update)
        .delete(access['EDIT_SHOP'], controllers.v1.shopsController.destroy);

    app.route('/admin/api/v1/shops/:shopId/items')
        .get(access['REVIEW_SHOP'], controllers.v1.productController.getItemsByShop)
        .post(access['EDIT_SHOP'], controllers.v1.productController.addItemToShop);

    app.route('/admin/api/v1/shops/:shopId/reccomends')
        .get(access['REVIEW_SHOP'], controllers.v1.productController.getItemsByShopForReccomends);   

    app.route('/admin/api/v1/shops/:shopId/items/:itemId')
        .get(access['REVIEW_SHOP'], controllers.v1.productController.getItemByShop)
        .put(access['EDIT_SHOP'],   controllers.v1.productController.updateItemByShop)
        .patch(access['EDIT_SHOP'], controllers.v1.productController.updateInShopEnablingMode)
        .delete(access['EDIT_SHOP'], controllers.v1.productController.removeItemFromShop);

    app.route('/admin/api/v1/shops/:shopId/owners')
        .get(access['REVIEW_SHOP'], controllers.v1.adminController.owners);

    app.route('/admin/api/v1/shops/:shopId/owners/:adminId')
        .post(access['ASSIGN_SHOP'], controllers.v1.adminController.assign)
        .delete(access['ASSIGN_SHOP'], controllers.v1.adminController.unassign);

    app.route('/admin/api/v1/statistics/:type')
        .get(controllers.v1.statisticController.index);

    app.route('/admin/api/v1/statistics/:type/export')
        .get(controllers.v1.statisticController.exportToXLS);

    app.route('/admin/api/v1/postcodes')
        .get(controllers.v1.statisticController.getPostCodes);

    app.route('/admin/api/v1/log')
        .get(controllers.v1.lastEditController.read)
        .post(controllers.v1.lastEditController.log);

    app.route('/admin/api/v1/reports')
       .get( controllers.v1.reportController.reports); // access['REVIEW_REPORT'],

    app.route('/admin/api/v1/report/:id')
       .delete(controllers.v1.reportController.remove);

    app.route('/admin/api/v1/bulkUpload/:type')
        .post(access['CREATE_ITEM'], controllers.v1.bulkUploadController.upload);
};
