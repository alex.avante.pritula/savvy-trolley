var validator = require('../utils/bodyValidator');

function update(req, res, next) {
    return validator({
        properties: {
            addedItems  : {
                type      : 'array',
                items     : {
                    type      : 'object',
                    properties: {
                        itemId: {
                            type      : 'integer',
                            allowEmpty: false,
                            required  : true
                        },
                        shopId: {
                            type      : 'integer',
                            allowEmpty: false,
                            required  : true
                        },
                        type: {
                            type      : 'integer',
                            allowEmpty: false,
                            required  : true
                        }
                    }
                },
                allowEmpty: true,
                required  : false
            },
            removedItems: {
                type      : 'array',
                items     : {
                    type      : 'object',
                    properties: {
                        itemId: {
                            type      : 'integer',
                            allowEmpty: false,
                            required  : true
                        },
                        shopId: {
                            type      : 'integer',
                            allowEmpty: false,
                            required  : true
                        }
                    }
                },
                allowEmpty: true,
                required  : false
            }
        }
    })(req, res, next);
}

function restore(req, res, next) {
    return validator({
        properties: {
            addedItems: {
                type      : 'array',
                items     : {
                    type      : 'integer',
                    allowEmpty: false,
                    required  : true
                },
                allowEmpty: true,
                required  : false
            }
        }
    })(req, res, next);
}


module.exports = {
    update : update,
    restore: restore
};