var validator = require('../utils/bodyValidator');

function itemCreate(req, res, next) {
    return validator({
        properties: {
            barCode       : {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            name          : {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            brand         : {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            imageId       : {
                type      : 'integer',
                allowEmpty: false,
                required  : true
            },
            itemSize      : {
                type      : 'number',
                minimum   : 0.01,
                allowEmpty: false,
                required  : true
            },
            unitType      : {
                type   : "integer",
                minimum: 1,
                maximum: 5
            },
            servingPerPack: {
                type      : 'integer',
                allowEmpty: false
            },
            servingSize   : {
                type      : 'integer',
                allowEmpty: false
            },
            servingType   : {
                type   : "integer",
                minimum: 1,
                maximum: 5
            }
        }
    })(req, res, next)
}

function itemUpdate(req, res, next) {
    return validator({
        properties: {
            barCode       : {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            name          : {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            brand         : {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            imageId       : {
                type      : 'integer',
                allowEmpty: true,
                required  : false
            },
            itemSize      : {
                type      : 'number',
                minimum   : 0.01,
                allowEmpty: false,
                required  : true
            },
            unitType      : {
                type   : "integer",
                minimum: 1,
                maximum: 5
            },
            servingPerPack: {
                type      : 'integer',
                allowEmpty: false
            },
            servingSize   : {
                type      : 'integer',
                allowEmpty: false
            },
            servingType   : {
                type   : "integer",
                minimum: 1,
                maximum: 5
            }
        }
    })(req, res, next)
}

function shopItem(req, res, next) {
    var salePrice = {
        allowEmpty: true,
        required  : false
    };

    if(req.body.salePrice){
        salePrice = {
            type      : 'number',
            minimum   : 0.01,
            allowEmpty: true,
            required  : true
        };
    }

    return validator({ properties: {
            regularPrice: {
                type      : 'number',
                minimum   : 0.01,
                allowEmpty: false,
                required  : true
            },
            salePrice   : salePrice,
            unitPrice   : {
                type      : 'number',
                minimum   : 0.01,
                allowEmpty: false,
                required  : true
            },
            isSale      : {
                type      : 'boolean',
                allowEmpty: false,
                required  : true
            },
            from: {
                type      : 'integer',
                allowEmpty: true,
                required  : false
            },
            to  : {
                type      : 'integer',
                allowEmpty: true,
                required  : false
            }
        }
    })(req, res, next)
}

module.exports = {
    itemCreate: itemCreate,
    itemUpdate: itemUpdate,
    shopItem  : shopItem
};
