var validator = require('../utils/bodyValidator');
var validate = require('../utils/validator');
var config = require('../../config');

function createMiddleware(req, res, next) {
    return validator({
        properties: {
            email   : {
                type      : 'string',
                format    : 'email',
                allowEmpty: false,
                required  : true
            },
            password: {
                type      : 'string',
                minLength : 5,
                maxLength : 15,
                allowEmpty: false,
                required  : true
            },
            role    : {
                enum: Object.keys(config.constants.ROLES)
            }
        }
    })(req, res, next);
}

function roleMiddleware(req, res, next) {
    return validator({
        properties: {
            role: {
                enum: Object.keys(config.constants.ROLES)
            }
        }
    })(req, res, next);
}

function enable(req, res, next) {
    return validator({
        properties: {
            enabled: {
                type      : 'boolean',
                allowEmpty: false,
                required  : true
            }
        }
    })(req, res, next);
}

function password(req, res, next) {
    return validator({
        properties: {
            oldPassword: {
                type      : 'string',
                minLength : 5,
                maxLength : 15,
                allowEmpty: false,
                required  : true
            },
            newPassword: {
                type      : 'string',
                minLength : 5,
                maxLength : 15,
                allowEmpty: false,
                required  : true
            }
        }
    })(req, res, next);
}

module.exports = {
    createMiddleware: createMiddleware,
    roleMiddleware  : roleMiddleware,
    enable          : enable,
    password        : password
};