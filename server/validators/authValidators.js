var validator = require('../utils/bodyValidator');

function local(req, res, next) {
    return validator({
        properties: {
            email   : {
                type      : 'string',
                format    : 'email',
                allowEmpty: false,
                required  : true
            },
            password: {
                type      : 'string',
                minLength : 6,
                maxLength : 15,
                allowEmpty: false,
                required  : true
            }
        }
    })(req, res, next);
}

function refresh(req, res, next) {
    return validator({
        properties: {
            accessToken : {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            refreshToken: {
                type      : 'string',
                allowEmpty: false,
                required  : true
            }
        }
    })(req, res, next)
}

function logout(req, res, next) {
    return validator({
        properties: {
            refreshToken: {
                type      : 'string',
                allowEmpty: false,
                required  : true
            }
        }
    })(req, res, next)
}

function recovery(req, res, next) {
    return validator({
        properties: {
            email: {
                type      : 'string',
                format    : 'email',
                allowEmpty: false,
                required  : true
            }
        }
    })(req, res, next)
}

module.exports = {
    local   : local,
    refresh : refresh,
    logout  : logout,
    recovery: recovery
};