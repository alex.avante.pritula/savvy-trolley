var validator = require('../utils/bodyValidator');
var validate = require('../utils/validator');

function password(req, res, next) {
    return validator({
        properties: {
            oldPassword: {
                type      : 'string',
                minLength : 5,
                maxLength : 15,
                allowEmpty: false,
                required  : true
            },
            newPassword: {
                type      : 'string',
                minLength : 5,
                maxLength : 15,
                allowEmpty: false,
                required  : true
            }
        }
    })(req, res, next)
}

function adverts(req, res, next) {
    return validator({
        properties: {
            hasAdverts: {
                type      : 'boolean',
                allowEmpty: false,
                required  : true
            }
        }
    })(req, res, next)
}

function postcode(req, res, next) {
    return validator({
        properties: {
            postCodes: {
                type    : 'array',
                minItems: 1,
                maxItems: 5,
                items   : {
                    type      : 'object',
                    properties: {
                        code: {
                            type      : 'string',
                            allowEmpty: false,
                            required  : true,
                            minLength : 4,
                            maxLength : 4,
                            pattern   : /\d{4}/
                        }
                    }
                }
            }
        }
    })(req, res, next)
}

function changeCredentials(req, res, next) {
  return validator({
    properties: {
      email   : {
        type      : 'string',
        format    : 'email',
        allowEmpty: false,
        required  : true
      },
      password: {
        type      : 'string',
        minLength : 6,
        maxLength : 15,
        allowEmpty: false,
        required  : true
      }
    }
  })(req, res, next);
}


module.exports = {
    password : password,
    adverts  : adverts,
    postcode : postcode,
    changeCredentials: changeCredentials
};
