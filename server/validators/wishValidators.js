var validator = require('../utils/bodyValidator');

function validate(req, res, next) {
    return validator({
        properties: {
            addedItems  : {
                type      : 'array',
                items     : {
                    type: 'integer'
                },
                allowEmpty: true,
                required  : false
            },
            removedItems: {
                type      : 'array',
                items     : {
                    type: 'integer'
                },
                allowEmpty: true,
                required  : false
            }
        }
    })(req, res, next);
}

module.exports = {
    update: validate
};