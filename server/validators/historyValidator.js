var validator = require('../utils/bodyValidator');

function update(req, res, next) {
    return validator({
        properties: {
            newElements: {
                type     : 'array',
                minLength: 1,
                items    : {
                    itemId      : {
                        type      : 'number',
                        minimum   : 1,
                        allowEmpty: false,
                        required  : true
                    },
                    regularPrice: {
                        type      : 'number',
                        minimum   : 0.01,
                        allowEmpty: false,
                        required  : true
                    },
                    salePrice   : {
                        type      : 'number',
                        minimum   : 0.01,
                        allowEmpty: false,
                        required  : true
                    },
                    unitPrice   : {
                        type      : 'number',
                        minimum   : 0.01,
                        allowEmpty: false,
                        required  : true
                    },
                    isSale      : {
                        type      : 'boolean',
                        allowEmpty: false,
                        required  : true
                    },
                    purchaseDate: {
                        type      : 'number',
                        allowEmpty: false,
                        required  : true
                    }
                }
            }
        }
    })(req, res, next);
}

module.exports = {
    update: update
};