var validator = require('../utils/bodyValidator');
var config = require('../../config');

function validate(req, res, next) {
    return validator({
        properties: {
            entityType   : {
                enum      : Object.keys(config.constants.ENTITY_TYPES),
                allowEmpty: false,
                required  : true
            },
            entityId     : {
                type      : 'integer',
                allowEmpty: false,
                required  : true
            },
            adminId      : {
                type      : 'integer',
                allowEmpty: false,
                required  : true
            },
            operationType: {
                enum      : ['CREATE', 'EDIT', 'DELETE'],
                allowEmpty: false,
                required  : true
            }
        }
    })(req, res, next);
}

module.exports = {
    log: validate
};