var validator = require('../utils/bodyValidator');

function createAdvertising(req, res, next) {
    return validator({
        properties: {
            from     : {
                type      : 'integer',
                allowEmpty: false,
                required  : true
            },
            to       : {
                type      : 'integer',
                allowEmpty: false,
                required  : true
            },
            productId: {
                type      : 'integer',
                allowEmpty: false,
                required  : true
            },
            postCode : {
                type      : 'string',
                allowEmpty: false,
                required  : true,
                minLength : 4,
                maxLength : 4,
                pattern   : /\d{4}/
            }
        }
    })(req, res, next)
}

function updateAdvertising(req, res, next) {
    return validator({
        properties: {
            from: {
                type      : 'integer',
                allowEmpty: false,
                required  : true
            },
            to  : {
                type      : 'integer',
                allowEmpty: false,
                required  : true
            }
        }
    })(req, res, next)
}

module.exports = {
    create: createAdvertising,
    update: updateAdvertising
};