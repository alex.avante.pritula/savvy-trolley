var validator = require('../utils/bodyValidator');

function create(req, res, next) {
    return validator({
        properties: {
            title  : {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            imageId: {
                type      : 'integer',
                allowEmpty: true,
                required  : false
            }
        }
    })(req, res, next)
}

function update(req, res, next) {
    return validator({
        properties: {
            title  : {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            imageId: {
                type      : 'integer',
                allowEmpty: true,
                required  : false
            }
        }
    })(req, res, next)
}

module.exports = {
    create: create,
    update: update
};