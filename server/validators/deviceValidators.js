var validator = require('../utils/bodyValidator');

function subscribeNotification(req, res, next) {
    return validator({
        properties: {
            deviceToken: {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            deviceId   : {
                type      : 'string',
                allowEmpty: false,
                required  : true
            }
        }
    })(req, res, next)
}

module.exports = {
    subscribeNotification: subscribeNotification
};