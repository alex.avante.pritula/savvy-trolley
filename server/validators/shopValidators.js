var validator = require('../utils/bodyValidator');

function shop(req, res, next) {
    return validator({
        properties: {
            postCode    : {
                type      : 'string',
                allowEmpty: false,
                required  : true,
                minLength : 4,
                maxLength : 4,
                pattern   : /\d{4}/
            },
            name        : {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            owner       : {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            imageId     : {
                type      : 'integer',
                allowEmpty: true,
                required  : false
            },
            abn         : {
                type      : 'string',
                allowEmpty: false,
                required  : true,
                minLength : 11,
                maxLength : 11
            },
            acn         : {
                type      : 'string',
                allowEmpty: false,
                required  : true,
                minLength : 9,
                maxLength : 9
            },
            address     : {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            contactName : {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            contactPhone: {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            contactEmail: {
                type      : 'string',
                allowEmpty: false,
                required  : true
            },
            longitude   : {
                type      : 'number',
                minimum   : -180,
                maximum   : 180,
                allowEmpty: false,
                required  : true
            },
            latitude    : {
                type      : 'number',
                minimum   : -85.05112878,
                maximum   : 85.05112878,
                allowEmpty: false,
                required  : true
            }
        }
    })(req, res, next)
}

function optimal(req, res, next) {
    return validator({
        properties: {
            items: {
                type      : 'array',
                minItems  : 1,
                items     : {
                    type      : 'integer',
                    allowEmpty: false,
                    required  : true
                },
                allowEmpty: false,
                required  : true
            }
        }
    })(req, res, next)
}

module.exports = {
    shop   : shop,
    optimal: optimal
};