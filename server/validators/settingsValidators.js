var validator = require('../utils/bodyValidator');

function adverts(req, res, next) {
    return validator({
        properties: {
            hasAdverts: {
                type      : 'boolean',
                allowEmpty: false,
                required  : true
            }
        }
    })(req, res, next);
}

module.exports = {
    adverts: adverts
};