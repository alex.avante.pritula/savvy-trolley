module.exports = {
    admin      : require('./adminValidators'),
    advertising: require('./advertisingValidators'),
    auth       : require('./authValidators'),
    category   : require('./categoryValidators'),
    device     : require('./deviceValidators'),
    history    : require('./historyValidator'),
    item       : require('./itemValidators'),
    lastEdit   : require('./lastEditValidator'),
    settings   : require('./settingsValidators'),
    shopping   : require('./shoppingValidators'),
    shop       : require('./shopValidators'),
    user       : require('./userValidators'),
    wish       : require('./wishValidators')
};