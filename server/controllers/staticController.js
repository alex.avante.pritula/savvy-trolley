var fs = require('fs');
var path = require('path');
var config = require('../../config');

function renderStaticHtmlFile(path) {
    return function (req, res, next) {
        fs.createReadStream(__dirname + '/../../frontend/' + path).pipe(res);
    }
}

function renderStaticTemplateFile(){
	return function (req, res, next) {
        res.setHeader('Content-disposition', 'attachment; filename=' + req.params.name + '.xlsx');
        res.set('Content-Type', 'text/csv');
        res.sendFile(config.root + '/templates/' + req.params.name + '.xlsx');
    }
}

module.exports = {
    index:    renderStaticHtmlFile('index.html'),
    success:  renderStaticHtmlFile('success.html'),
    error:    renderStaticHtmlFile('error.html'),
    docs :    renderStaticHtmlFile('swagger/index.html'),
    templates:renderStaticTemplateFile()
};
