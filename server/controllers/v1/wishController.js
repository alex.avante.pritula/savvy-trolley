var models = require('../../models');
var formatters = require('../../formatters');
var services = require('../../services');
var logger = require('../../utils/logger');
var validators = require('../../validators');

var config = require('../../../config');
/**
 * Read wish list
 *
 * @param req
 * @param res
 * @param next
 */
function index(req, res, next) {
    var id = req.params.userId;

    var options = {
        include: [{model: models.postCodes, as: 'postCodes'}]
    };
    models.users.findById(id, options)
        .then(function (user) {
            if (!user) {
                var error = new Error();
                error.status = 401;
                throw error;
            }
            var postCodes = user.postCodes.length != 0 ? formatters.postCode.toStrings(user.postCodes) : null;

            var limit = parseInt(req.query.limit, 10) || 1000;
            var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
            var offset = parseInt(req.query.offset, 10) || step * limit;

            var userData = {
                userId   : user.id,
                postCodes: postCodes
            };
            var options = {
                userId   : user.id,
                limit    : limit,
                offset   : offset,
                postCodes: postCodes
            };

            return services.wish.read(userData, options);
        })
        .then(function (response) {
            res.status(200).json(response);
        })
        .catch(next);
}

/**
 * Update wish list
 *
 * @param req
 * @param res
 * @param next
 */
function update(req, res, next) {
    var id = req.params.userId;

    var options = {
        include: [{model: models.postCodes, as: 'postCodes'}]
    };
    models.users.findById(id, options)
        .then(function (user) {
            if (!user) {
                var error = new Error();
                error.status = 401;
                throw error;
            }

            var addedItems = req.body.addedItems || [];
            var removedItems = req.body.removedItems || [];
            return services.wish.update(user, addedItems, removedItems)
        })
        .then(function () {
            res.status(204).send();
        })
        .catch(next);
}

/**
 * Add item to wish list.
 * Deprecated api.
 *
 * @param req
 * @param res
 * @param next
 */
function like(req, res, next) {
    var itemId = req.params.itemId;
    var user = req.user;
    var postCodes = formatters.postCode.toStrings(user.postCodes);
    var options = {
        include: [{
            model: models.shops,
            as   : 'shops',
            where: {
                postCode: {
                    $in: postCodes
                }
            }
        }]
    };
    models.items.findById(itemId, options)
        .then(function (item) {
            logger.log('info', 'like ', item);
            if (!item) {
                var error = new Error('Item not found!');
                error.status = 404;
                throw error;
            }
            return user.addWishes(item);
        })
        .then(function () {
            res.status(204).send();
        })
        .catch(next);
}

/**
 * Remove item from wish list.
 * Deprecated api.
 *
 * @param req
 * @param res
 * @param next
 */
function dislike(req, res, next) {
    var user = req.user;
    var itemId = req.params.itemId;
    models.items.findById(itemId)
        .then(function (item) {
            logger.log('info', 'dislike ' + JSON.stringify(item));
            if (!item) {
                var error = new Error('Item not found!');
                error.status = 404;
                throw error;
            }

            return user.removeWishes(item);
        })
        .then(function () {
            res.status(204).send();
        })
        .catch(next);
}

/**
 * Get wish list.
 *
 * @param req
 * @param res
 * @param next
 * @returns {*|Promise.<T>}
 */
function me(req, res, next) {
    var user = req.user;
    if (!user) {
        var error = new Error();
        error.status = 401;
        throw error;
    }
    var postCodes = formatters.postCode.toStrings(user.postCodes);

    var limit = parseInt(req.query.limit, 10) || 1000;
    var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;

    var userData = {
        userId   : user.id,
        postCodes: postCodes
    };
    var options = {
        userId   : user.id,
        limit    : limit,
        offset   : offset,
        postCodes: postCodes
    };

    return services.wish.read(userData, options)
        .then(function (response) {
            res.status(200).json(response);
        })
        .catch(next);
}

/**
 * Sync wish list
 *
 * @param req
 * @param res
 * @param next
 */
function sync(req, res, next) {
    var user = req.user;
    if (!user) {
        var error = new Error();
        error.status = 401;
        throw error;
    }
    var addedItems = req.body.addedItems || [];
    var removedItems = req.body.removedItems || [];
    services.wish.update(user, addedItems, removedItems)
        .then(function () {
            next();
            return null;
        })
        .catch(next);
}

module.exports = {
    read   : me,
    sync   : [validators.wish.update, sync, me],
    like   : like,
    dislike: dislike,

    index : index,
    update: [validators.wish.update, update]
};