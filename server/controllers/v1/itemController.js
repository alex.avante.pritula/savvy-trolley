var Promise = require('bluebird');
var models = require('../../models');
var formatters = require('../../formatters');
var services = require('../../services');
var logger = require('../../utils/logger');
var validators = require('../../validators');
var config = require('../../../config');

/**
 * Read all items
 *
 * @param req
 * @param res
 * @param next
 */
function index(req, res, next) {

    var query = req.query.query || '';
    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step =  parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;
    var barCode = query + '%';
    var name = '%' + query + '%';
    var user = req.user;

    var options = {
        barCode : barCode,
        name    : name,
        sortAt  : req.query.at || 'DESC',
        sortBy  : req.query.by || 'createdAt',
        limit   : limit,
        step    : step,
        offset  : offset
    };

    if (user.role === 'ENTERPRISE_SAVVY_SHOP' || user.role === 'SAVVY_SHOP'){
        options['adminId'] =  user.id;
    }

    services.item.itemsWithViewOnlyPermission(options)
        .then(function(items){
            req.temp = {items: items};
            options = {
                where: {
                    $or: [
                        {barCode: {$like: barCode}},
                        {name: {$like: name}}
                    ]
                }
            };
            return models.items.count(options);
        }).then(function(totalCount){
            var result = {
                items: req.temp.items,
                meta : {
                    totalCount: totalCount
                }
            };
            res.status(200).send(result);
        }).catch(next);
}

/**
 * Search item
 *
 * @param req
 * @param res
 * @param next
 */
function search(req, res, next) {
    var user = req.user;
    if (!user) {
        var error = new Error();
        error.status = 401;
        return next(error);
    }

    var query = req.query.query;
    if (typeof query !== 'string' || query.length < 3) {
        var error = new Error('Invalid query parameter!');
        error.status = 400;
        return next(error);
    }
    var type = req.query.type || 'GLOBAL';
    type = type.toUpperCase();

    var category = parseInt(req.query.category, 10) || 1;
    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;

    var options = {
        type: type,
        hasQuery: true,
        replacements: {
            barCode: query,
            name: '%' + query + '%',
            brand: '%' + query + '%',
            limit: limit,
            offset: offset,
            postCodes: formatters.postCode.toStrings(user.postCodes),
            category: category,
            parent: category,
            userId: user.id
        }
    };

    services.item.search(options).then(function (items) {
        var result = {query: query, items: items};
        res.status(200).send(result);
    }).catch(next);
}

/**
 * Create item
 *
 * @param req
 * @param res
 * @param next
 */

function create(req, res, next) {
    var data = {
        barCode: req.body.barCode,
        name: req.body.name,
        brand: req.body.brand,
        image_id: req.body.imageId,
        unit_id: req.body.unitType,
        servingPerPack: req.body.servingPerPack,
        servingSize: req.body.servingSize,
        serving_unit_id: req.body.servingType,
        itemSize: req.body.itemSize
    };
    var categories = req.body.categories;
    models.items.create(data)
        .then(function (item) {
            req.temp = {item: item};
            return Promise.map(categories,function(category){
                var options = {
                    item_id: item.id,
                    category_id: category.id
                };
                return models.itemByCategory.create(options);
            });
        })
        .then(function(){
            var item = req.temp['item'];
            return Promise.all([
                item.getImage(),
                item.getCategories()
            ]);
        })
        .then(function (data) {
            var item = req.temp['item'].toJSON();
            item.image = data[0] ? data[0].url : null;
            item.categories = data[1] ? data[1] : [];
            res.status(200).send(item);
        })
        .catch(function (err) {
            next(err);
        });
}

/**
 * Read item
 *
 * @param req
 * @param res
 * @param next
 */
function read(req, res, next) {
    var user = req.user;
    var id = parseInt(req.params.itemId);
    var options = {
        attributes: {exclude: ['unit_id', 'serving_unit_id', 'image_id']},
        include   : [
            {model: models.units, as: 'unitType'},
            {model: models.units, as: 'servingType'},
            {model: models.images, as: 'image'}]
    };

    models.items.findById(id, options)
        .then(function (item) {
            if (!item) {
                var error = new Error('Not found!');
                error.status = 404;
                throw error;
            }
            var result = item.toJSON();
            result.unitType = result.unitType ? result.unitType.type : null;
            result.servingType = result.servingType ? result.servingType.type : null;
            result.image = ((result.image) ? result.image.url : null);
            req.temp = {item: result};
            return item.getCategories();

        }).then(function(categories){
            req['temp'].item.categories = categories;
            if (user.role === 'ENTERPRISE_SAVVY_SHOP' || user.role === 'SAVVY_SHOP'){
                return services.item.itemViewOnlyPermission(user.id, id);
            }
        }).then(function(viewOnly){
            var item = req['temp'].item;
            item.viewOnly = false;
            if (viewOnly){
                item.viewOnly = viewOnly[0].status;
            }
            res.status(200).send(item);
        }).catch(next);
}

/**
 * Read item information for expanded view
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function readExpanded(req, res, next) {
    var user = req.user;
    if (!user) {
        var error = new Error('Unauthorized');
        error.status = 401;
        return next(error);
    }

    var itemId = req.params.itemId;
    if (!itemId) {
        var error = new Error('Unauthorized');
        error.status = 401;
        return next(error);
    }

    var options = {
        userId: user.id,
        itemId: itemId
    };
    services.item.getAbstractItems(options)
        .then(function (items) {
            if (!items.length) {
                var error = new Error('This item is no longer listed as available!');
                error.status = 404;
                throw error;
            }
            var result = formatters.item.itemForExpandedView(items[0]);

            res.status(200).send(result);
        })
        .catch(next);
}

/**
 * Get shops info for item in expanded view
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function getShops(req, res, next) {
    if (!req.user) {
        var err = new Error();
        err.status = 401;
        return next(err);
    }
    var id = parseInt(req.params.itemId);

    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;
    var options = {
        id: id,
        limit: limit,
        offset: offset,
        postCodes: formatters.postCode.toStrings(req.user.postCodes)
    };

    services.shop.getShopsForItem(options)
        .then(function (rawLines) {
            return formatters.shop.rawShops(rawLines);
        })
        .then(function (shops) {
            var result = {
                shops: shops
            };
            res.status(200).send(result);
        })
        .catch(next);
}

/**
 * Get recommends for item in expanded view
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function getRecommends(req, res, next) {
    var itemId = req.params.itemId;
    var user = req.user;
    if (!user) {
        var error = new Error();
        error.status = 401;
        return next(error);
    }

    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;

    var postCodes = formatters.postCode.toIds(user.postCodes);
    services.advertising.getRecommends(itemId, postCodes, limit, offset)
        .then(function (rawLines) {
            var response = {
                advertising: formatters.advertising.forUser(rawLines)
            };
            res.status(200).send(response);
        })
        .catch(next);
}

/**
 * Update item
 *
 * @param req
 * @param res
 * @param next
 */
function update(req, res, next) {
    var id = req.params.itemId;
    var newCategories = req.body.categories;

    var oldItemSize = null;
    var oldUnitType = null;
    var newItemSize = req.body.itemSize;
    var newUnitType = req.body.unitType;

    models.items.findById(id)
        .then(function (item) {

            if (!item) {
                var error = new Error('Not found!');
                error.status = 404;
                throw error;
            }
            oldItemSize = item.itemSize;
            oldUnitType = item.unit_id;

            item.barCode         = req.body.barCode;
            item.name            = req.body.name;
            item.brand           = req.body.brand;
            item.image_id        = req.body.imageId ? req.body.imageId : item.image_id;
            item.unit_id         = newUnitType;
            item.servingPerPack  = req.body.servingPerPack || null;
            item.servingSize     = req.body.servingSize    || null;
            item.serving_unit_id = req.body.servingType    || null;
            item.itemSize        = newItemSize;

            return item.save();
        })
        .then(function (item) {
            req.temp = {item: item};
            return item.getCategories();
        })
        .then(function(oldCategories){
            return Promise.all([
                Promise.map(newCategories, function(newCategory){
                    var newCategoryId = newCategory.id;
                    var index = oldCategories.findIndex(function(oldCat){
                        return oldCat.id === newCategoryId;
                    });
                    if (index === -1){
                       return models.itemByCategory.create({
                            item_id     : id,
                            category_id : newCategoryId
                       });
                    }
                    return null;
                }),
                Promise.map(oldCategories, function(oldCategory){
                    var index = newCategories.findIndex(function(newCat){
                        return newCat.id === oldCategory.id;
                    });

                    if (index === -1){
                        return models.itemByCategory.destroy({
                        where: {
                          item_id     : id,
                          category_id :  oldCategory.id
                        }
                     });
                    }
                    return null;
                })
            ]);
        })
        .then(function(){
            return models.itemByShop.all({where:{item_id: id}});
        })
        .then(function (itemByShops) {
            if (newItemSize !== oldItemSize || newUnitType !== oldUnitType) {
                Promise.mapSeries(itemByShops, function (itemByShop) {
                    var types = config.constants['UNIT_TYPES'],  inc = 1;
                    if (newUnitType === types['ml']   || newUnitType === types['gr'])   inc = 100;
                    if (newUnitType === types['ltr']  || newUnitType === types['kg'])   inc = 0.1;
                    itemByShop.unitPrice = parseFloat((Math.ceil(itemByShop.regularPrice / newItemSize * inc * 100) / 100).toFixed(2));
                    return itemByShop.save();
                });
            }
        })
        .then(function(){
            return req['temp'].item.getImage();
        })
        .then(function (image) {
            var item = req['temp'].item.toJSON();
            item.image = image ? image.url : null;
            res.status(200).send(item);
        })
        .catch(function (err) {
            next(err);
        });
}

/**
 * Delete item
 *
 * @param req
 * @param res
 * @param next
 */
function destroy(req, res, next) {
    var id = req.params.itemId;
    models.items.findById(id)
        .then(function (item) {
            if (!item) {
                var error = new Error('Not found!');
                error.status = 404;
                throw error;
            }

            return item.destroy()
        })
        .then(function () {
            res.status(204).send();
        })
        .catch(function (err) {
            next(err);
        });
}

/**
 * Update enabling mode for item
 *
 * @param req
 * @param res
 * @param next
 */
function updateEnablingMode(req, res, next) {
    var id = req.params.itemId;
    var push = null;

    models.items.findById(id)
        .then(function (item) {
            if (!item) {
                var error = new Error('Not found!');
                error.status = 404;
                throw error;
            }

            item.enabled = req.body.enabled;
            if (!item.enabled) {
                push = {
                    options: { itemId: id },
                    message: { itemId: id }
                };
            }

            return item.save();
        })
        .then(function () {
            var deliveryDateTime = req.body.deliveryDateTime;
            if (push && deliveryDateTime) {
                logger.log('info', 'ITEM_ENABLED_MODE_CHANGED = ', push);

                var pushData = {
                    data            : push,
                    entityType      : config.constants.DELIVERY_NOTIFICATION_ENTITY_TYPES.ITEM,
                    pushType        : config.constants.PUSH_TYPES.ITEM_ENABLED_MODE_CHANGED,
                    entityId        : id,
                    userData        : [],
                    deliveryDateTime: deliveryDateTime,
                    adminId         : req.user.id
                };

                return services.deliveryNotification.getDevicesPerItem(push.options)
                    .then(function (data) {
                        pushData.userData = data;
                        return services.deliveryNotification.createNotification(pushData);
                    });
            }
        })
        .then(function(){
            res.status(204).send();
        })
        .catch(next);
}

/**
 * Read categories for item.
 *
 * @param req
 * @param res
 * @param next
 */
function getCategoriesByItem(req, res, next) {
    var itemId = req.params.itemId;
    models.items.findById(itemId)
        .then(function (item) {
            if (!item) {
                var error = new Error('Item not found');
                error.status = 404;
                throw error;
            }
            var categories = item.getCategories();
            var totalCount = item.countCategories();

            return Promise.all([categories, totalCount]);
        })
        .then(function (data) {
            var response = {
                categories: data[0],
                meta      : {
                    totalCount: data[1]
                }
            };
            res.status(200).send(response);
        })
        .catch(next);

}

/**
 * Read items by category
 *
 * @param req
 * @param res
 * @param next
 */
function getItemsByCategory(req, res, next) {
    var user = req.user;
    if (!user) {
        var error = new Error();
        error.status = 401;
        return next(error);
    }

    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step =  parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;

    var category = req.params.categoryId;

    var options = {
        type        : 'SUBCATEGORY',
        replacements: {
            limit    : limit,
            offset   : offset,
            postCodes: formatters.postCode.toStrings(user.postCodes),
            category : category,
            userId   : user.id
        }
    };

    services.item.search(options)
        .then(function (items) {
            var result = {items: items};
            res.status(200).send(result);
        })
        .catch(next);
}

module.exports = {
    index             : index,
    create            : [validators.item.itemCreate, create],
    read              : read,
    readExpanded      : readExpanded,
    update            : [validators.item.itemUpdate, update],
    destroy           : destroy,
    updateEnablingMode: updateEnablingMode,

    getCategoriesByItem   : getCategoriesByItem,
    getItemsByCategory    : getItemsByCategory,

    search: search,

    recommends: getRecommends,
    shops     : getShops
};
