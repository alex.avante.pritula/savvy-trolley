var Promise = require('bluebird');
var models = require('../../models');
var formatters = require('../../formatters');
var validators = require('../../validators');
var logger = require('../../utils/logger');
var services = require('../../services');
var config = require('../../../config');
var sequelize = require('sequelize');

/**
 * Add item to shop
 *
 * @param req
 * @param res
 * @param next
 */
function addItemToShop(req, res, next) {
    var shopId = req.params.shopId;
    var itemId = req.body.itemId;
    var isOutOfStock = req.body.isOutOfStock;
    var user = req.user;

    var data = {
        regularPrice: req.body.regularPrice,
        unitPrice: req.body.unitPrice,
        salePrice: req.body.salePrice || false,
        isSale: req.body.isSale,
        from: parseInt(req.body.from) || false,
        to: parseInt(req.body.to) || false,
        is_out_of_stock: isOutOfStock
    };

    models.shops.findById(shopId)
        .then(function (shop) {
            if (!shop) {
                var error = new Error('Shop not found!');
                error.status = 404;
                throw error;
            }
            req.temp = {shop: shop};
            return models.items.findById(itemId);
        })
        .then(function (item) {
            if (!item) {
                var error = new Error('Item not found!');
                error.status = 404;
                throw error;
            }

            req.temp.item = item;

            var options = Object.assign({
                shop_id: shopId,
                item_id: itemId
            }, data);

            return models.itemByShop.create(options);
        })
        .then(function(itemByShop){
            req['temp'].itemByShop = itemByShop;

            if (data.from && data.to){
                var options = {
                    admin_id: user.id,
                    product_id: itemByShop.id,
                    from: data.from,
                    to: data.to
                };
                logger.log('info', 'Sales activity', options );
                return models.salesActivityByAdmin.create(options);
            }
        })
        .then(function(){
            var pushes = [];
            var shop = req['temp'].shop;
            var itemByShop = req['temp'].itemByShop;
            var deliveryDateTime = req.body.deliveryDateTime;

            if (isOutOfStock && deliveryDateTime['ITEM_OUT_OF_STOCK']) {
                pushes.push({
                    action: 'getDevicesForItemWithShop',
                    deliveryDateTime: deliveryDateTime['ITEM_OUT_OF_STOCK'],
                    options: {
                        itemId  : itemId,
                        shopId  : shopId,
                        postCode: shop.postCode,
                        pushType: config.constants.PUSH_TYPES.ITEM_OUT_OF_STOCK
                    },
                    message: {
                        itemId: itemId,
                        shopId: shopId
                    }
                });
            }

            if (data.isSale) {
                if (deliveryDateTime['SALE_START']) {
                    pushes.push({
                        action          : 'getDevicesPerItem',
                        deliveryDateTime: deliveryDateTime['SALE_START'],
                        options: {
                            itemId  : itemId,
                            postCode: shop.postCode,
                            pushType: config.constants.PUSH_TYPES.SALE_START
                        },
                        message: {
                            itemId      : itemId,
                            shopId      : shopId,
                            regularPrice: itemByShop.regularPrice,
                            salePrice   : itemByShop.salePrice
                        }
                    });
                }

                if (data.isSale && deliveryDateTime['SALE_FINISH']) {
                    pushes.push({
                        action          : 'getDevicesPerItem',
                        deliveryDateTime: deliveryDateTime['SALE_FINISH'],
                        options: {
                            itemId  : itemId,
                            postCode: shop.postCode,
                            pushType: config.constants.PUSH_TYPES.SALE_FINISH
                        },
                        message: {
                            itemId      : itemId,
                            shopId      : shopId,
                            regularPrice: itemByShop.regularPrice,
                            salePrice   : itemByShop.salePrice
                        }
                    });
                }
            }

            return Promise.map(pushes, function(pushItem){
                logger.log('info', pushItem.title + ' push = ', pushItem);
                return services.deliveryNotification[pushItem.action](pushItem.options).then(function (data) {
                    var pushData = {
                        data: pushItem,
                        entityType: config.constants.DELIVERY_NOTIFICATION_ENTITY_TYPES.ITEM_BY_SHOP,
                        pushType: pushItem.options.pushType,
                        entityId: itemByShop.id,
                        userData: data,
                        deliveryDateTime: pushItem.deliveryDateTime,
                        shopId: shopId,
                        adminId: user.id
                    };
                    return services.deliveryNotification.createNotification(pushData);
                });
            });
        })
        .then(function () {
            var result = Object.assign({}, data, {
                shopId: shopId,
                itemId: itemId
            });
            res.status(200).send(result);
        })
        .catch(next);
}

/**
 * Read items by shop
 *
 * @param req
 * @param res
 * @param next
 */
function getItemsByShop(req, res, next) {
    var shopId = req.params.shopId;
    var sortBy = req.query.by || 'createdAt';
    var sortAt = req.query.at;
    var query = req.query.query || '';
    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;
    var timeLine = parseInt(req.query.timeLine) === 1;
    var to = (req.query.to ? new Date(parseInt(req.query.to)) : new Date());
    var from = (req.query.from ? new Date(parseInt(req.query.from)) : new Date());

    if (sortAt.toLowerCase() !== 'desc') {
        sortAt = 'asc';
    }

    var options = {
        query: '%' + query + '%',
        from: from,
        to: to,
        limit: limit,
        offset: offset,
        shopId: shopId,
        sortBy: sortBy,
        sortAt: sortAt
    };

    return services.item[(timeLine ? 'getItemsByShopWithTimeLine' : 'getItemsByShop')](options)
        .then(function(data){
            var result = { 
                items: formatters.item.itemsForShopList(data[0]),
                meta: {totalCount: data[1]}
            };
            res.status(200).send(result);
        }).catch(next); 
}

function getItemsByShopForReccomends(req, res, next) {
    var shopId = req.params.shopId;
    var sortBy = req.query.by || 'createdAt';
    var sortAt = req.query.at || 'DESC';
    var query = req.query.query || '';
    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;

    if (sortBy === 'updatedAt' || sortBy === 'createdAt') {
        sortBy = sequelize.col('itemByShop.' + sortBy);
    }

    var where = {
        $or: [
            {barCode: query},
            {name: {$like: '%' + query + '%'}}
        ]
    };

    var getOptions = {
        where: where,
        offset: offset,
        limit: limit,
        attributes: {exclude: ['unit_id', 'serving_unit_id', 'image_id']},
        include: [
            {model: models.units, as: 'unitType'},
            {model: models.units, as: 'servingType'},
            {model: models.images, as: 'image'}],
        order: [[sortBy, sortAt]]
    };
    var countOptions = {
        where: where
    };

    models.shops.findById(shopId)
        .then(function (shop) {
            if (!shop) {
                var error = new Error('Shop not found!');
                error.status = 404;
                throw error;
            }

            return Promise.all([shop.getItems(getOptions), shop.countItems(countOptions)]);
        })
        .then(function (data) {
            var result = {
                items: formatters.item.itemsByShop(data[0]),
                meta: {
                    totalCount: data[1]
                }
            };
            res.send(result);
        })
        .catch(next);
}


/**
 * Read item by shop
 *
 * @param req
 * @param res
 * @param next
 */
function getItemByShop(req, res, next) {
    var shopId = req.params.shopId;
    var itemId = req.params.itemId;

    var query = {
        where: {id: itemId},
        attributes: {exclude: ['unit_id', 'serving_unit_id', 'image_id']},
        include: [{
            model: models.units, 
            as: 'unitType' 
        },{ 
            model: models.units, 
            as: 'servingType' 
        },{ 
            model: models.images, 
            as: 'image' 
        }]
    };
    models.shops.findById(shopId)
        .then(function (shop) {
            if (!shop) {
                var error = new Error('Shop not found!');
                error.status = 404;
                throw error;
            }
            return shop.getItems(query);
        })
        .then(function (items) {
            if (!items || !items.length) {
                return models.items.find(query)
                    .then(function (existItem) {
                        if (!existItem) {
                            var error = new Error('Item not found!');
                            error.status = 404;
                            throw error;
                        }
                        var item = Object.assign({
                            exist : false
                        }, formatters.item.deserializeItem(existItem));
                        return item;
                    });
            }
            var itemByShop = Object.assign({
                exist : true
            }, formatters.item.itemByShop(items[0]));
            return itemByShop;
        }).then(function (result) {
            res.send(result);
        })
        .catch(next);
}

/**
 * Remove item from shop
 *
 * @param req
 * @param res
 * @param next
 */
function removeItemFromShop(req, res, next) {
    var shop = req.params.shopId;
    var item = req.params.itemId;
    models.itemByShop.findOne({
        where: {
            shop_id: shop,
            item_id: item
        }
    })
        .then(function (item) {
            if (!item) {
                var error = new Error('Item not found!');
                error.status = 404;
                throw error;
            }

            return item.destroy();
        })
        .then(function () {
            res.status(204).send();
        })
        .catch(next);
}

/**
 * Update item by shop
 *
 * @param req
 * @param res
 * @param next
 */
function updateItemByShop(req, res, next) {
    var user = req.user;
    var shopId = req.params.shopId;
    var itemId = req.params.itemId;

    var options = {
        include: [{
            model: models.items,
            as: 'items',
            where: {id: itemId}
        }]
    };

    var shop = null;
    var item = null;
    var itemByShop = null;
    var oldFrom = null;
    var oldTo = null;
    var pushes = [];
    var stockPush = false;
    var newFrom = parseInt(req.body.from) || false;
    var newTo =   parseInt(req.body.to)   || false;

    models.shops.findById(shopId, options)
        .then(function (existShop) {
            if (!existShop) {
                var error = new Error('Not found!');
                error.status = 404;
                throw error;
            }

            shop = existShop;
            item = shop.items[0];
            itemByShop = item.itemByShop;

            var limit = config.constants.MONTH_DATE_RANGE_LIMITS['PRODUCTS'];
            var currentDate = new Date(), lastUpdation = new Date(itemByShop.updatedAt);
            lastUpdation.setMonth(lastUpdation.getMonth() + limit);

            if (lastUpdation > currentDate && user.role === 'SAVVY_SHOP_ADMIN'){
                return shop.countOwners({where: {role: 'ENTERPRISE_SAVVY_SHOP'}});
            }
            return true;
        }).then(function(isValid){

            if (!isValid) {
                var error = new Error('Price changing is possible only once a month!');
                error.status = 403;
                throw error;
            }

            logger.log('info', 'updateItemByShop itemByShop = ' + JSON.stringify(itemByShop));

            var newPrice = req.body.regularPrice;
            var oldPrice = itemByShop.regularPrice;
            itemByShop.regularPrice = newPrice;

            var newIsSale = req.body.isSale ? 1 : 0;
            itemByShop.isSale = newIsSale;
            
            var newOutOfStock = req.body.isOutOfStock;
            var oldOutOfStock = itemByShop.is_out_of_stock === 1;
            itemByShop.is_out_of_stock = newOutOfStock;

            itemByShop.unitPrice = req.body.unitPrice;
            itemByShop.salePrice = req.body.salePrice || null;

            oldFrom = itemByShop.from;
            oldTo = itemByShop.to;

            itemByShop.from = newFrom;
            itemByShop.to =   newTo;

            var pricePush = oldPrice > newPrice;
            stockPush = newOutOfStock && !oldOutOfStock;

            var deliveryDateTime = req.body.deliveryDateTime;

            if (stockPush && deliveryDateTime['ITEM_OUT_OF_STOCK']) {
                pushes.push({
                    action: 'getDevicesForItemWithShop',
                    deliveryDateTime: deliveryDateTime['ITEM_OUT_OF_STOCK'],
                    options: {
                        itemId: item.id,
                        shopId: shop.id,
                        postCode: shop.postCode,
                        pushType: config.constants.PUSH_TYPES.ITEM_OUT_OF_STOCK
                    },
                    message: {
                        itemId: item.id,
                        shopId: shop.id
                    }
                });
            }

            if (newIsSale  && deliveryDateTime['SALE_START'] && (oldFrom !== newFrom)) {
                pushes.push({
                    action: 'getDevicesPerItem',
                    deliveryDateTime: deliveryDateTime['SALE_START'],
                    options: {
                        itemId: item.id,
                        postCode: shop.postCode,
                        pushType: config.constants.PUSH_TYPES.SALE_START
                    },
                    message: {
                        itemId: item.id,
                        shopId: shop.id,
                        regularPrice: itemByShop.regularPrice,
                        salePrice: itemByShop.salePrice
                    }
                });
            }

            if (newIsSale  && deliveryDateTime['SALE_FINISH'] && (oldTo !== newTo)) {
                pushes.push({
                    action: 'getDevicesPerItem',
                    deliveryDateTime: deliveryDateTime['SALE_FINISH'],
                    options: {
                        itemId: item.id,
                        postCode: shop.postCode,
                        pushType: config.constants.PUSH_TYPES.SALE_FINISH
                    },
                    message: {
                        itemId: item.id,
                        shopId: shop.id,
                        regularPrice: itemByShop.regularPrice,
                        salePrice: itemByShop.salePrice
                    }
                });
            }

            if (pricePush && deliveryDateTime['REGULAR_PRICE_REDUCED']) {
                pushes.push({
                    action: 'getDevicesPerWishes',
                    deliveryDateTime: deliveryDateTime['REGULAR_PRICE_REDUCED'],
                    options: {
                        itemId: item.id,
                        postCode: shop.postCode,
                        pushType: config.constants.PUSH_TYPES.REGULAR_PRICE_REDUCED
                    },
                    message: {
                        itemId: item.id,
                        shopId: shop.id,
                        oldPrice: oldPrice,
                        newPrice: newPrice
                    }
                });
            }
            return itemByShop.save();

        }).then(function(itemByShop){
            if (itemByShop.isSale && (oldFrom !== newFrom || oldTo !== newTo)){
                var options = {
                    admin_id:  user.id,
                    product_id: itemByShop.id,
                    from: newFrom,
                    to: newTo
                };
                logger.log('info', 'Sales activity', options );
                return models.salesActivityByAdmin.create(options);
            }
        }).then(function(){
            return Promise.map(pushes, function(pushItem){
                logger.log('info', pushItem.title + ' push = ', pushItem);
                return services.deliveryNotification[pushItem.action](pushItem.options).then(function (data) {
                    var pushData = {
                        data: pushItem,
                        entityType: config.constants.DELIVERY_NOTIFICATION_ENTITY_TYPES.ITEM_BY_SHOP,
                        pushType: pushItem.options.pushType,
                        entityId: itemByShop.id,
                        userData: data,
                        deliveryDateTime: pushItem.deliveryDateTime,
                        shopId: shopId,
                        adminId: user.id
                    };
                    return services.deliveryNotification.createNotification(pushData);
                });
            });
        }).then(function(){
           if (stockPush) {
             var options = {where: {item_by_shop_id: itemByShop.id}};
             return models.shopping.destroy(options);
           }
        }).then(function(){
            res.status(200).send(item);
        }).catch(next);
}

/**
 * Update product enabling mode
 *
 * @param req
 * @param res
 * @param next
 */
function updateInShopEnablingMode(req, res, next) {
    var shopId = req.params.shopId;
    var itemId = req.params.itemId;

    var options = {
        include: [{
            model: models.items,
            as: 'items',
            where: {id: itemId}
        }]
    };
    var push = null;
    models.shops.findById(shopId, options)
        .then(function (shop) {
            if (!shop) {
                var error = new Error('Not found!');
                error.status = 404;
                throw error;
            }
            var item = shop.items[0];
            var itemByShop = item.itemByShop;

            itemByShop.enabled = req.body.enable;

            if (!itemByShop.enabled) {
                push = {
                    options: {
                      productId: itemByShop.id
                    },
                    message: {
                      itemId  : item.id,
                      shopId  : shop.id
                    }
                };
            }
            return itemByShop.save();
        })
        .then(function (item) {
            req.temp = {item: item};
            var deliveryDateTime = req.body.deliveryDateTime;

            if (push && deliveryDateTime) {
                logger.log('info', 'Product update enabling mode push = ', push);

                return services.deliveryNotification.getDevicesPerConcreteItem(push.options)
                  .then(function (data) {
                    var pushData = {
                        data: push,
                        entityType: config.constants.DELIVERY_NOTIFICATION_ENTITY_TYPES.ITEM_BY_SHOP,
                        pushType: config.constants.PUSH_TYPES.CONCRETE_ITEM_ENABLED_MODE_CHANGED,
                        entityId: push.options.productId,
                        userData: data,
                        deliveryDateTime: deliveryDateTime,
                        shopId: shopId,
                        adminId: req.user.id
                    };
                    return services.deliveryNotification.createNotification(pushData);
                });
            }
        }).then(function(){
           if (push) {
               var id = req['temp'].item['id'];
               var options = {where: {item_by_shop_id: id}};
               return models.shopping.destroy(options);
           }
        }).then(function(){
            res.status(200).send(req['temp'].item);
        }).catch(next);
}

module.exports = {
    addItemToShop: [validators.item.shopItem, addItemToShop],
    getItemsByShop: getItemsByShop,
    getItemByShop: getItemByShop,
    updateItemByShop: [validators.item.shopItem, updateItemByShop],
    updateInShopEnablingMode: updateInShopEnablingMode,
    removeItemFromShop: removeItemFromShop,
    getItemsByShopForReccomends: getItemsByShopForReccomends
};
