var Promise = require('bluebird');

var models = require('../../models');
var logger = require('../../utils/logger');
var services = require('../../services');
var validators = require('../../validators');

var config = require('../../../config');

/**
 * Get history
 *
 * @param req
 * @param res
 * @param next
 */
function index(req, res, next) {
    var id = req.params.userId;

    var options = {
        include: [{model: models.postCodes, as: 'postCodes'}]
    };
    models.users.findById(id, options)
        .then(function (user) {
            if (!user) {
                var error = new Error();
                error.status = 404;
                return next(error);
            }

            var limit = parseInt(req.query.limit, 10) || 50;
            var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
            var offset = parseInt(req.query.offset, 10) || step * limit;

            var where = {user_id: user.id};


            var options = {
                where : where,
                offset: offset,
                limit : limit
            };

            return services.history.read(options)
        })
        .then(function (response) {
            res.status(200).send(response);
        });
}

/**
 * Get my history
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function read(req, res, next) {
    var user = req.user;
    if (!user) {
        var error = new Error();
        error.status = 401;
        return next(error);
    }

    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;

    var options = {
        where :  { user_id: user.id },
        offset: offset,
        limit : limit
    };

    services.history.read(options)
        .then(function (response) {
            res.status(200).send(response);
        });
}

/**
 * Sync my history
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function update(req, res, next) {
    var user = req.user;
    if (!user) {
        var error = new Error();
        error.status = 401;
        return next(error);
    }
    logger.log('info', 'update update body: ' + JSON.stringify(req.body));
    var createHistory = req.body.newElements.map(function (element) {
        var data = {
            user_id     : user.id,
            item_id     : element.itemId,
            regularPrice: element.regularPrice,
            salePrice   : element.salePrice,
            unitPrice   : element.unitPrice,
            isSale      : element.isSale,
            purchaseDate: element.purchaseDate
        };
        return models.history.create(data);
    });
    Promise.all(createHistory)
        .then(function (history) {
            logger.log('info', 'update history: ' + JSON.stringify(history));
            res.status(204).send();
        });
}

module.exports = {
    index: index,
    read  : read,
    update: [validators.history.update, update]
};