var models = require('../../models');
var validators = require('../../validators');
var config = require('../../../config');
var settingValues = ['hasAdverts'];

/**
 * Get application settings
 *
 * @param req
 * @param res
 * @param next
 */
function getSettings(req, res, next) {
    models.settings.findById(config.constants.SETTING_ID, {attributes: settingValues})
        .then(function (setting) {
            res.status(200).send(setting);
        })
        .catch(next);
}

/**
 * Enable/disable google Adwords.
 *
 * @param req
 * @param res
 * @param next
 */
function changeAdverts(req, res, next) {
    models.settings.update({hasAdverts: req.body.hasAdverts}, {where: {id: config.constants.SETTING_ID}})
        .then(function () {
            res.status(204).send();
        })
        .catch(next);
}

module.exports = {
    index        : getSettings,
    changeAdverts: [validators.settings.adverts, changeAdverts]
};