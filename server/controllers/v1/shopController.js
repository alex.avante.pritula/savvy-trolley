var models = require('../../models');
var formatters = require('../../formatters');
var logger = require('../../utils/logger');
var sequelize = require('../../utils/sequelize');
var services = require('../../services');
var validators = require('../../validators');
var Promise = require('bluebird');
var config = require('../../../config');
/**
 * read all shops
 *
 * @param req
 * @param res
 * @param next
 */
function index(req, res, next) {
  var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
  var step =  parseInt(req.query.page, 10) - 1 || config.constants.STEP;

  var data = {
      sortBy:     req.query.by || 'createdAt',
      sortAt:     req.query.at || 'DESC',
      query:      req.query.query || '',
      adminEmail: req.query.adminEmail || '',
      postCodes:  (req.query.postcodes ? req.query.postcodes.split(',') : null),
      adminId:    ((req.permissions && req.permissions['ONLY_OWN_SHOP']) ? req.user.id: null)
  };

  var settings = {
      offset:     parseInt(req.query.offset, 10) || step * limit,
      limit:      limit
  };

  services.shop.getShops(data, settings)
      .then(function (result) {
          res.status(200).send(result);
      }).catch(next);
}

/**
 * create shop
 *
 * @param req
 * @param res
 * @param next
 */
function create(req, res, next) {
    var data = {
        name: req.body.name,
        owner: req.body.owner,
        abn: req.body.abn,
        acn: req.body.acn,
        address: req.body.address,
        contactName: req.body.contactName,
        contactPhone: req.body.contactPhone,
        contactEmail: req.body.contactEmail,
        postCode: req.body.postCode,
        longitude: req.body.longitude,
        latitude: req.body.latitude,
        isLocal: req.body.isLocal || 0,
        hasSales: req.body.hasSales || 1,
        hasAdvertising: req.body.hasAdvertising || 1
    };

    if (req.body.imageId){
         data.image_id = req.body.imageId;
    }

    models.shops.create(data)
        .then(function (shop) {
            req.temp = {shop: shop.toJSON(), instance: shop};
            var owners = req.body.owners || [];
            return shop.addOwners(owners);
        }).then(function (){
            return req.temp.instance.getImage();
        })
        .then(function (image) {
            var shop = req.temp.shop;
            shop.image = image ? image.url : null;
            var deliveryDateTime = req.body.deliveryDateTime;

            if (deliveryDateTime) {
                var push = {
                    options: {postCode: shop.postCode},
                    message: {shopId: shop.id}
                };

                return services.deliveryNotification.getDevicesForShopWithPostCode(push.options).then(function (data) {
                    var pushData = {
                        data            : push,
                        entityType      : config.constants.DELIVERY_NOTIFICATION_ENTITY_TYPES.SHOP,
                        pushType        : config.constants.PUSH_TYPES.SHOP_ADDED,
                        entityId        : shop.id,
                        shopId          : shop.id,
                        userData        : data,
                        adminId         : req.user.id,
                        deliveryDateTime: deliveryDateTime
                    };
                    return services.deliveryNotification.createNotification(pushData);
                }).catch(next);
            }
        })
        .then(function(){
            res.status(200).send(req.temp.shop);
        })
        .catch(next);
}

/**
 * read shop
 *
 * @param req
 * @param res
 * @param next
 */
function read(req, res, next) {
    var shopId = req.params.shopId;
    var options = {
        attributes: {exclude: ['image_id']},
        include: [{model: models.images, as: 'image'}]
    };

    if (req.permissions && req.permissions['ONLY_OWN_SHOP']) {
        var admin = req.user;
        options.include.push({
            model: models.admins,
            as: 'owners',
            where: {id: admin.id}
        });
    }

    models.shops.findById(shopId, options)
        .then(function (shop) {
            if (!shop) {
                var error = new Error('Shop not found!');
                error.status = 404;
                throw error;
            }

            shop = shop.toJSON();
            shop.image = shop.image ? shop.image.url : null;
            shop.isLocal = shop.isLocal === 1;
            shop.hasSales = shop.hasSales === 1;
            shop.hasAdvertising = shop.hasAdvertising === 1;

            delete shop.admins;
            res.status(200).send(shop);
        })
        .catch(next);
}

/**
 * Get optimal shop old version (work with HTTP GET method)
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function findOptimalShopsOld(req, res, next) {
    var user = req.user;
    if (!user) {
        var error = new Error();
        error.status = 401;
        return next(error);
    }

    var items = req.query.items;
    items = items.split(',');
    if (!items || items.length === 0) {
        var err = new Error('Invalid items');
        err.status = 400;
        return next(err);
    }

    var data = {
        items: items,
        postCodes: formatters.postCode.toStrings(user.postCodes)
    };

    services.shop.getOptimalShops(data)
        .then(function (rawShops) {
            return formatters.shop.optimal(rawShops);
        })
        .then(function (shops) {
            if (shops.length === 0) {
                var err = new Error('Shop not found');
                err.status = 404;
                throw err;
            }
            var shop = shops[0];
            req.temp = {shop: shop};
            var query = {
                where: {id: shop.id},
                include: [
                    {
                        model: models.items,
                        as: 'items',
                        attributes: ['id', 'barCode', 'name', 'brand'],
                        where: {id: {$in: items}}
                    }
                ]
            };
            return models.shops.find(query);
        })
        .then(function (shop) {
            var result = {
                shop: req.temp.shop,
                items: formatters.item.extended(shop.items)
            };
            res.status(200).json(result);
        })
        .catch(next);
}

/**
 * Get optimal shop current version (work with HTTP POST method)
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function findOptimalShops(req, res, next) {
    var user = req.user;
    if (!user) {
        var error = new Error();
        error.status = 401;
        return next(error);
    }

    logger.log('info', JSON.stringify(req.body));
    var items = req.body.items;
    var data = {
        items: items,
        postCodes: formatters.postCode.toStrings(user.postCodes)
    };

    services.shop.getOptimalShops(data)
        .then(function (rawShops) {
            return formatters.shop.optimal(rawShops);
        })
        .then(function (shops) {
            if (shops.length === 0) {
                var err = new Error('Shop not found');
                err.status = 404;
                throw err;
            }
            var shop = shops[0];
            req.temp = {shop: shop};
            var query = {
                where: {id: shop.id},
                include: [
                    {
                        model: models.items,
                        as: 'items',
                        attributes: ['id', 'barCode', 'name', 'brand'],
                        where: {id: {$in: items}}
                    }
                ]
            };
            return models.shops.find(query);
        })
        .then(function (shop) {
            var result = {
                shop: req.temp.shop,
                items: formatters.item.extended(shop.items)
            };
            res.status(200).json(result);
        })
        .catch(next);
}

/**
 * update shop
 *
 * @param req
 * @param res
 * @param next
 */
function update(req, res, next) {
    var shopId = req.params.shopId;
    var newOwners = req.body.owners;
    var postCode =  req.body.postCode;
    var isPostCodeChanged;

    models.shops.findById(shopId)
        .then(function (shop) {
            if (!shop) {
                var error = new Error('Shop not found!');
                error.status = 404;
                throw error;
            }

            isPostCodeChanged = (shop.postCode !== postCode);
            shop.name = req.body.name;
            shop.image_id = req.body.imageId;
            shop.owner = req.body.owner;
            shop.abn = req.body.abn;
            shop.acn = req.body.acn;
            shop.address = req.body.address;
            shop.contactName = req.body.contactName;
            shop.contactPhone = req.body.contactPhone;
            shop.contactEmail = req.body.contactEmail;
            shop.postCode = postCode;
            shop.longitude = req.body.longitude;
            shop.latitude = req.body.latitude;
            shop.isLocal = req.body.isLocal;
            shop.hasSales = req.body.hasSales;
            shop.hasAdvertising = req.body.hasAdvertising;
            return shop.save();
        })
        .then(function(shop){
            req.temp = {shop: shop};
            return shop.getOwners();
        })
        .then(function(oldOwners){
            return Promise.mapSeries(oldOwners, function(oldOwner){
                var oldOwnerId = oldOwner;
                var index = newOwners.findIndex(function(newOwnerId){
                    return (oldOwnerId === newOwnerId);
                });
                if (index === -1){
                    return oldOwner.removeShops(shopId);
                }
                newOwners.splice(index, 1);
                return null;
            });
        })
        .then(function(){
            var shop = req.temp.shop;
            return shop.addOwners(newOwners);
        })
        .then(function(){
            if (isPostCodeChanged){
                return services.advertising.updateAdvertisingByShop(shopId);
            }
        })
        .then(function(){
            var shop = req.temp.shop;
            return shop.getImage();
        })
        .then(function (image) {
            var shop = req.temp.shop.toJSON();
            shop.image = image ? image.url : null;
            shop.isLocal = shop.isLocal === 1;
            shop.hasSales = shop.hasSales === 1;
            shop.hasAdvertising = shop.hasAdvertising === 1;
            res.status(200).json(shop);
        })
        .catch(next);
}
/**
 * remove shop
 *
 * @param req
 * @param res
 * @param next
 */
function destroy(req, res, next) {
    var shopId = req.params.shopId;
    models.shops.findById(shopId)
        .then(function (shop) {
            if (!shop) {
                var error = new Error('Shop not found!');
                error.status = 404;
                throw error;
            }
            return shop.destroy();
        })
        .then(function () {
            res.status(204).send();
        })
        .catch(next);
}


function getBlockingHistoryCountToShop(req, res, next){

    var options = {
        attributes: [
            'type',
            [sequelize.fn('count', sequelize.col('type')), 'count']
        ],
        where: {
            shop_id: req.params.shopId
        },
        group: ['type']
     };

    models.shopManagingPermissions.all(options).then(function(result){
        if (!result) {
            var error = new Error('Blocking comments history to this shop is not exist!');
            error.status = 404;
            throw error;
        }
        var countByTypes = {};
        result.forEach(function (item) {
            var temp  = item.toJSON();
            countByTypes[temp.type] = temp.count;
        });
        res.send({countByTypes: countByTypes});
    })
    .catch(next);

}

function addBlockingHistoryItemToShop(req, res, next){
    var shopId = req.params.shopId;
    var message = req.body.message;
    var type = req.body.type;
    var user = req.user;

    models.shops.findById(shopId)
        .then(function (shop) {
            if (!shop) {
                var error = new Error('Shop not found!');
                error.status = 404;
                throw error;
            }
            var options = {
                admin_id: user.id,
                shop_id:  shopId,
                type:     type,
                message:  message
            };

            return models.shopManagingPermissions.create(options);
        })
        .then(function () {
            res.status(200).send();
        })
        .catch(next);
}

function getBlockingHistoryToShop(req, res, next){

    var shopId = req.params.shopId;
    var sortBy = req.query.by || 'createdAt';
    var sortAt = req.query.at || 'DESC';
    var type =   req.query.type;
    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;

    var options = {
        offset    : offset,
        limit     : limit,
        attributes: ['id', 'message', 'createdAt'],
        order     : [[sortBy, sortAt]],
        where     : {shop_id: shopId}
    };

    if (type){
        options['where'].type = type;
    }

    models.shopManagingPermissions.all(options)
        .then(function (items) {
            req.temp = {
                items: items.map(function (item) {
                    var result = item.toJSON();
                    return result;
                })
            };
            options = {where: options['where']};
            return models.shopManagingPermissions.count(options);
        })
        .then(function (totalCount) {
            var result = {
                items: req.temp.items,
                meta : {totalCount: totalCount}
            };
            res.status(200).send(result);
        })
        .catch(next);
}

function changeShopPermissionByType(req, res, next){

    var shopId = parseInt(req.params.shopId, 10);
    var type   = parseInt(req.params.type, 10);
    var value  = parseInt(req.body.value, 10);
    var types  = config.constants['SHOP_BLOCKING_PERMISSIONS'];

    models.shops.findById(shopId)
        .then(function(shop){

            if (!shop) {
                var error = new Error('Shop not found!');
                error.status = 404;
                throw error;
            }

            switch(type){
                case types['SALES']:
                    shop.hasSales = value;
                    break;
                case types['ADVERTISING']:
                    shop.hasAdvertising = value;
                    break;
                default:
                    var error = new Error('Invalid type!');
                    error.status = 422;
                    throw error;
            }

            return shop.save();
        }).then(function (shop) {
            req.temp = { shop: shop.toJSON() };
            return shop.getImage();
        })
        .then(function (image) {
            var shop = req.temp.shop;
            shop.image = image ? image.url : null;
            shop.isLocal = shop.isLocal == 1;
            shop.hasSales = shop.hasSales == 1;
            shop.hasAdvertising = shop.hasAdvertising == 1;
            res.status(200).send(shop);
        })
        .catch(next);
}


function getShopPermissionByType(req, res , next){
   var shopId = parseInt(req.params.shopId, 10);
   var type   = parseInt(req.params.type, 10);
   var types  = config.constants['SHOP_BLOCKING_PERMISSIONS'];

   models.shops.findById(shopId)
        .then(function(shop){
            if (!shop) {
                var error = new Error('Shop not found!');
                error.status = 404;
                throw error;
            }

            switch (type){
                case types['SALES']:
                    res.status(200).send({type: shop.hasSales === 1});
                    break;
                case types['ADVERTISING']:
                    res.status(200).send({type: shop.hasAdvertising === 1});
                    break;
                default:
                    var error = new Error('Invalid type!');
                    error.status = 422;
                    throw error;
            }
        })
        .catch(next);
}


function getShopsLocations(req, res, next) {
    models.shops.all().then(function (shops) {
        var result = shops.map(function (shop) {
             var data = {
                  id       : shop.id,
                  postCode : shop.postCode,
                  owner    : shop.owner,
                  name     : shop.name,
                  longitude: shop.longitude,
                  latitude : shop.latitude
             };
            return data;
        });
        res.status(200).send(result);
    }).catch(next);
}

module.exports = {
    index: index,
    create: [validators.shop.shop, create],
    read: read,
    update: [validators.shop.shop, update],
    destroy: destroy,
    shopsLocations: getShopsLocations,

    optimalShopsOld: findOptimalShopsOld,
    optimalShops: [validators.shop.optimal, findOptimalShops],

    changeShopPermissionByType : changeShopPermissionByType,
    getShopPermissionByType    : getShopPermissionByType,

    getBlockingHistoryToShop:      getBlockingHistoryToShop,
    addBlockingHistoryItemToShop : addBlockingHistoryItemToShop,
    getBlockingHistoryCountToShop: getBlockingHistoryCountToShop
};
