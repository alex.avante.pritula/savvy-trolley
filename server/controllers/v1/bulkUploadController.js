var services = require('../../services');
var config = require('../../../config');
var models = require('../../models');
var Promise = require('bluebird');
var logger = require('../../utils/logger');
var kue = require('kue');
var Queue = kue.createQueue();
var XLSX = require('xlsx');
var fs = require('fs');
var path = require('path');
var unlink = Promise.promisify(fs.unlink);
var multer = require('../../utils/multer');

/** Items To Shop
 * @param req
 * @param res
 * @param next
 */
function itemsToShop(req,res,next){
    var data = req.temp.data;
    var structure = config.constants['BULK_UPLOAD'].ITEMS_TO_SHOP;
    var length = Object.keys(structure).length;

    if (data[0].length < length){
        var error = new Error('Ivalid file structure');
        error.status = 422;
        return next(error);
    }

    data.splice(0, 1);

    var user = req.user;
    var items = [];
    var pushes = [];
    var itemsForUpdate = {};
    var reportStructure = [];
    var indexUpdatedRows = [];
    var shopId = req.body.shopId;   
    var shoppingItems = [];
    var pushConfig  = config.constants['DELIVERY_PUSH_HOURS_LIMITS'];
    var separator   = config.constants['BULK_DATE_TIME_PARSE'].TIME;
    var priceLength = config.constants['PRICE_MAX_LENGTH'];
    var updateNames = config.constants['BULK_UPDATE_OFFICIAL_FIELD_NAMES'];

    models.shops.findById(shopId).then(function(shop) {

        if (!shop) {
            var error = new Error('Shop is not exist!');
            error.status = 404;
            throw error;
        }

        req.temp = {
            shop: shop,
            createdItems: {},
            updatedItemsHasImage: {}
        };

        data.forEach(function (row, index) {

            var item = [];
            for (var i  = 0; i < length; i++){
               var field = (row[i] ? row[i].trim() : '');
               item.push(field);
            }

            var tempItem = {
                barCode         : item[structure['BARCODE']],
                name            : item[structure['NAME']],
                brand           : item[structure['BRAND']],
                unit_id         : item[structure['UNIT_TYPE']].toLowerCase(),
                itemSize        : parseFloat(item[structure['ITEM_SIZE']]),
                servingSize     : parseInt(item[structure['SERVING_SIZE']]),
                serving_unit_id : item[structure['SERVING_TYPE']].toLowerCase(),
                servingPerPack  : parseInt(item[structure['SERVING_PER_PACK']]),
                path            : item[structure['IMAGE_URL']],
                regularPrice    : item[structure['REGULAR_PRICE']],
                pnPriceReduced  : item[structure['PN_PRICE_REDUCED']],
                is_out_of_stock : item[structure['IS_OUT_OF_STOCK']].trim(),
                pnOutOfStock    : item[structure['PN_OUT_OF_STOCK']],
                categories      : item.splice(structure['FIRST_SUBCATEGORY'], item.length),
                // default
                image_id        : null
            };

            var tempItemForUpload = {};
            var isValidBarCode = false;

            var errorItem = {
                'Image': '',
                'Serving Size': '',
                'Serving Per Pack': '',
                'Serving Type': '',
                'PN(Regular Price)': '',
                'PN(Out of stock)': '',
                'Categories': []
            };

            // Barcode
            if (tempItem['barCode'].length < config.constants['BARCODE_MIN_LENGTH']){
                errorItem['BarCode'] = 'Barcode Length less than ' + config.constants['BARCODE_MIN_LENGTH'];
            } else if (tempItem['barCode'].length > config.constants['TEXT_MAX_LENGTH']) {
                errorItem['BarCode'] = "Field 'BarCode'Length is longer than " + config.constants['TEXT_MAX_LENGTH'];
            } else if (!/^\d+$/.test(tempItem['barCode'])) {
                errorItem['BarCode'] = 'Barcode must consist only of digits';
            } else {
                isValidBarCode = true;
            }

            // Name
            if (!tempItem['name'].length) {
                errorItem['Name'] = "Field 'Name' is empty";
            } else if (tempItem['name'].length > config.constants['TEXT_MAX_LENGTH']){
                errorItem['Name'] = "Field 'Name' Length is longer than " + config.constants['TEXT_MAX_LENGTH'];
            } else {
                tempItemForUpload['name'] = tempItem['name'];
            }

            // Brand
            if (!tempItem['brand'].length) {
                errorItem['Brand'] = "Field 'Brand' is empty";
            } else if (tempItem['brand'].length > config.constants['TEXT_MAX_LENGTH']){
                errorItem['Brand'] = "Field 'Brand' Length is longer than " + config.constants['TEXT_MAX_LENGTH'];
            } else {
                tempItemForUpload['brand'] = tempItem['brand'];
            }

            // Item Size
            if (isNaN(tempItem['itemSize'])) {
                errorItem['Item Size'] = "Invalid 'Item size' field";
            } else if (tempItem['itemSize'] < 0) {
                errorItem['Item Size'] = "Invalid 'Item size' field";
            } else {
                tempItemForUpload['itemSize'] = tempItem['itemSize'];
            }

            // Serving Size
            if (isNaN(tempItem['servingSize'])) {
                errorItem['Serving Size'] = "'Serving size' field is empty";
            } else if (tempItem['servingSize'] < 0) {
                errorItem['Serving Size'] = "Invalid 'Serving size' field";
            } else {
                tempItemForUpload['servingSize'] = tempItem['servingSize'];
            }

            if (errorItem['Serving Size']) {
                tempItem['servingSize'] = null;
            }

            // ServingPerPack
            if (isNaN(tempItem['servingPerPack'])) {
                errorItem['Serving Per Pack'] = "'Serving Per Pack' field is empty";
            } else if (tempItem['servingPerPack'] < 0) {
                errorItem['Serving Per Pack'] = "Invalid 'Serving Per Pack' field";
            } else {
                tempItemForUpload['servingPerPack'] = tempItem['servingPerPack'];
            }

            if (errorItem['Serving Per Pack']) {
                tempItem['servingPerPack'] = null;
            }

            // Regular Price
            if (tempItem['regularPrice'].length) {
                var regularPrice = parseFloat(tempItem['regularPrice']);
                if (!isNaN(regularPrice)){
                    var priceMatches = tempItem['regularPrice'].split('.');
                    if (priceMatches[0].length >  priceLength['INTEGER']) {
                        errorItem['Regular Price'] = "'Regular Price' field is out of range";
                    } else if (priceMatches[1] && priceMatches[1].length > priceLength['FRACTIONAL']) {
                        errorItem['Regular Price'] = "Fractional part of 'Regular Price' field is out of range";
                    } else if (regularPrice < 0){
                        errorItem['Regular Price'] = "'Regular Price' must be greater than '0'";
                    } else {
                        tempItem['regularPrice'] = parseFloat(tempItem['regularPrice']).toFixed(2);
                    }
                } else {
                    errorItem['Regular Price'] = "Invalid 'Regular Price' field";
                }
            } else {
                errorItem['Regular Price'] = "Invalid 'Regular Price' field";
            }

            // Is out of stock
            if (!tempItem['is_out_of_stock'].length) {
                errorItem['Is out of stock'] =  "Invalid 'Is out of stock' field";
            } else if (item[structure['IS_OUT_OF_STOCK']] === 'yes'){
                tempItem['is_out_of_stock'] = true;
            } else if (item[structure['IS_OUT_OF_STOCK']] === 'no') {
                tempItem['is_out_of_stock'] = false;
            } else {
                errorItem['Is out of stock'] = "Invalid 'Is out of stock' field";
            }

            tempItemForUpload['path'] = '';

            // Image url
            if (!tempItem['path'].length) {
                errorItem['Image'] = 'Image url field is empty. Item was created without image';
            } else if (tempItem['path'].indexOf('http') === -1) {
                errorItem['Image'] = "Invalid 'image url' field";
            } else {
                tempItemForUpload['path'] = tempItem['path'];
            }

            if (isValidBarCode && typeof (itemsForUpdate[tempItem['barCode']]) == 'undefined') {
                itemsForUpdate[tempItem['barCode']] = tempItemForUpload;
            }

            // Push Notification (Regular Price Reduced)
            if (!tempItem['pnPriceReduced'].length) {
                errorItem['PN(Regular Price)'] = "'PN(Regular Price)' field is empty";
            } else {
                if (tempItem['pnPriceReduced'].length > 1){
                    var pnTime = parseInt(tempItem['pnPriceReduced'].split(separator)[0], 10);
                    if (pnTime > pushConfig['END'] || pnTime < pushConfig['START']){
                        errorItem['PN(Regular Price)'] = 'Inappropriate time format';
                    } else {
                        var remain = parseInt(pnTime - pushConfig['START']) % parseInt(pushConfig['STEP']);
                        if (remain){
                            errorItem['PN(Regular Price)'] = 'Inappropriate time format';
                        } else {
                            tempItem['pnPriceReduced'] = pnTime;
                        }
                    }
                } else {
                    var inc = parseInt(tempItem['pnPriceReduced'], 10) - 1;
                    var pnTime = pushConfig['START'] + inc * pushConfig['STEP'];
                    if (pnTime > pushConfig['END']){
                        errorItem['PN(Regular Price)'] = 'Inappropriate time format';
                    } else {
                        tempItem['pnPriceReduced'] = pnTime;
                    }
                }
            }

            // Push Notification (Is out of stock)
            if (!tempItem['pnOutOfStock'].length) {
                errorItem['PN(Out of stock)'] = "'PN(Out of stock)' field is empty";
            } else {
                if (tempItem['pnOutOfStock'].length > 1){
                    var pnTime = parseInt(tempItem['pnOutOfStock'].split(separator)[0], 10);
                    if(pnTime > pushConfig['END'] || pnTime < pushConfig['START']){
                        errorItem['PN(Out of stock)'] = 'Inappropriate time format';
                    } else {
                        var remain = parseInt(pnTime - pushConfig['START']) % parseInt(pushConfig['STEP']);
                        if(remain){
                          errorItem['PN(Out of stock)'] = "Inappropriate time format";
                        } else {
                          tempItem['pnOutOfStock'] = pnTime;
                        }
                    }
                } else {
                    var inc = parseInt(tempItem['pnOutOfStock'], 10) - 1;
                    var pnTime = pushConfig['START'] + inc * pushConfig['STEP'];
                    if (pnTime > pushConfig['END']){
                        errorItem['PN(pnOutOfStock)'] = 'Inappropriate time format';
                    } else {
                        tempItem['pnOutOfStock'] = pnTime;
                    }
                }
            }

             if (tempItem['categories'].length === 1 & !tempItem['categories'][0].length){
                 tempItem['categories'] = [];
             }

            indexUpdatedRows[index] = [];
            items.push(tempItem);
            reportStructure.push(errorItem);
        });
        return services.bulkUpload.verifyItemsByTypes(items, reportStructure);
    }).then(function(){
        return services.bulkUpload.verifyItemsByCategories(items, reportStructure, length);
    }).then(function(){

        return Promise.mapSeries(items, function(item, index){
            var reportItem = reportStructure[index];

            var findOptions = {
                returning: true,
                where: {barCode: item['barCode']}
            };

            var savvyEnterpriseOptions = {
                limit     : 1,
                offset    : 0,
                barCode   : item['barCode'],
                name      : '',
                attributes: {
                    exclude: ['parent_id', 'image_id']
                },
                include   : [{
                    model: models.images,
                    as   : 'image'
                }]
            };

            var updateItems = {};
            if (itemsForUpdate[item['barCode']]) {
                if (itemsForUpdate[item['barCode']]['regularPrice']) {
                    updateItems['regularPrice'] = itemsForUpdate[item['barCode']]['regularPrice'];
                }
            }

            if (user.role === 'ENTERPRISE_SAVVY_SHOP' || user.role === 'SAVVY_SHOP'){
                savvyEnterpriseOptions['adminId'] =  user.id;
            }

            return services.item.itemsWithViewOnlyPermission(savvyEnterpriseOptions)
                .then(function(itemsWithViewOnlyPermission) {
                    if (itemsWithViewOnlyPermission.length === 0) {

                         if (!reportItem['regularPrice'] && !reportItem['itemSize'] && !reportItem['unit_id']) {
                             var unitPrice = services.bulkUpload.calculateUnitPrice(item['regularPrice'], item['itemSize'], item['unit_id']);
                             if (typeof unitPrice.result == 'string') {
                                reportItem['unitPrice'] = unitPrice.result;
                             } else {
                                item.unitPrice = unitPrice.result;
                             }
                         }

                         if (Object.keys(reportItem).length > 7){
                             return null;
                         }
                         var options = {
                            where: {barCode: item['barCode']},
                            defaults: item
                         };

                         return models.items.findOrCreate(options)
                            .spread(function (existItem, created) {
                                if (created){
                                    req['temp'].createdItems[index] = existItem;
                                }

                                var options = {
                                    where: {
                                        item_id: existItem.id,
                                        shop_id: shopId
                                    },
                                    defaults: {
                                        is_out_of_stock: item['is_out_of_stock'],
                                        regularPrice   : item['regularPrice'],
                                        unitPrice      : item['unitPrice'],
                                        item_id        : existItem.id,
                                        shop_id        : shopId
                                    }
                                };

                                return models.itemByShop.findOrCreate(options);
                            }).spread(function(existItemToShop, created){
                                if (created){
                                    return;
                                }
                                if (reportItem['Is out of stock']) {
                                    if (!existItemToShop['is_out_of_stock'] && item['is_out_of_stock']) {
                                        if (!reportItem['PN(Out of stock)']) {
                                            var pushData = {
                                                action: 'getDevicesForItemWithShop',
                                                deliveryDateTime: item['pnOutOfStock'],
                                                options: {
                                                    itemId:   existItemToShop.item_id,
                                                    entityId: existItemToShop.id,
                                                    shopId:   shopId,
                                                    postCode: req['temp'].shop.postCode,
                                                    pushType: config.constants.PUSH_TYPES.ITEM_OUT_OF_STOCK
                                                },
                                                message: {
                                                    itemId: item.id,
                                                    shopId: shopId
                                                }
                                            };
                                            shoppingItems.push(existItemToShop.id);
                                            pushes.push(pushData);
                                        } else {
                                            if (!reportItem['PN(Out of stock)']) {
                                                reportItem['PN(Out of stock)'] = 'PN cannot be sent because this event is not in list of events which run PN';
                                            }
                                        }
                                    }
                                    existItemToShop['is_out_of_stock'] = item['is_out_of_stock'];
                                }

                                if (!reportItem['regularPrice']) {
                                    if (existItemToShop.regularPrice > item['regularPrice']) {
                                        if (!reportItem['PN(Regular Price)']) {
                                            var pushData = {
                                                action: 'getDevicesPerWishes',
                                                deliveryDateTime: item['pnPriceReduced'],
                                                options: {
                                                    itemId: existItemToShop.item_id,
                                                    entityId: existItemToShop.id,
                                                    postCode: req['temp'].shop.postCode,
                                                    pushType: config.constants.PUSH_TYPES.REGULAR_PRICE_REDUCED
                                                },
                                                message: {
                                                    shopId: shopId,
                                                    newPrice: item.regularPrice,
                                                    itemId: existItemToShop.item_id,
                                                    oldPrice: existItemToShop.regularPrice
                                                }
                                            };
                                            pushes.push(pushData);
                                        } else {
                                            if (!reportItem['PN(Regular Price)']) {
                                                reportItem['PN(Regular Price)'] = 'PN cannot be sent because this event is not in list of events which run PN';
                                            }
                                        }
                                    }
                                    existItemToShop.regularPrice = item['regularPrice'];
                                    existItemToShop.unitPrice = item['unitPrice'];
                                }

                                return existItemToShop.save();
                            }).then(function(){
                                var createdItem = req.temp.createdItems[index];
                                if (!createdItem){
                                    return;
                                }
                                return Promise.mapSeries(item.categories, function(category){
                                    return category.addItem(createdItem);
                                });
                            }).catch(next);
                    }

                    var foundItem = itemsWithViewOnlyPermission[0];

                    if (typeof (foundItem.viewOnly) != 'undefined' && foundItem.viewOnly) {
                        updateItems['item_id'] = foundItem.id;
                        updateItems['shop_id'] = shopId;
                        var options = {
                            where: {
                                item_id: foundItem.id,
                                shop_id: shopId
                            },
                            defaults: updateItems
                        };

                        return models.itemByShop.findOrCreate(options)
                            .spread(function(existItemToShop, created){
                                if (created){
                                    return;
                                }

                                if (!reportItem['Is out of stock']) {
                                    if (!existItemToShop['is_out_of_stock'] && item['is_out_of_stock']) {
                                        if (!reportItem['PN(Out of stock)']) {
                                            var pushData = {
                                                action: 'getDevicesForItemWithShop',
                                                deliveryDateTime: item['pnOutOfStock'],
                                                options: {
                                                    itemId  : foundItem.id,
                                                    entityId: existItemToShop.id,
                                                    shopId  : shopId,
                                                    postCode: req['temp'].shop.postCode,
                                                    pushType: config.constants.PUSH_TYPES.ITEM_OUT_OF_STOCK
                                                },
                                                message: {
                                                    itemId: foundItem.id,
                                                    shopId: shopId
                                                }
                                            };
                                            shoppingItems.push(existItemToShop.id);
                                            pushes.push(pushData);
                                        } else {
                                            if (!reportItem['PN(Out of stock)']) {
                                                reportItem['PN(Out of stock)'] = 'PN cannot be sent because this event is not in list of events which run PN';
                                            }
                                        }
                                    }
                                    existItemToShop['is_out_of_stock'] = item['is_out_of_stock'];
                                }
                                if (!reportItem['Regular Price']) {
                                    if (existItemToShop.regularPrice > item['regularPrice']){
                                        if (!reportItem['PN(Regular Price)']) {
                                            var pushData = {
                                                action: 'getDevicesPerWishes',
                                                deliveryDateTime: item['pnPriceReduced'],
                                                options: {
                                                    itemId  : foundItem.id,
                                                    entityId: existItemToShop.id,
                                                    postCode: req['temp'].shop.postCode,
                                                    pushType: config.constants.PUSH_TYPES.REGULAR_PRICE_REDUCED
                                                },
                                                message: {
                                                    itemId  : foundItem.id,
                                                    shopId  : shopId,
                                                    oldPrice: existItemToShop.regularPrice,
                                                    newPrice: item.regularPrice
                                                }
                                            };
                                            pushes.push(pushData);
                                        } else {
                                            if (!reportItem['PN(Regular Price)']){
                                                reportItem['PN(Regular Price)'] = 'PN cannot be sent because this event is not in list of events which run PN';
                                            }
                                        }
                                    }
                                    existItemToShop.regularPrice = item['regularPrice'];
                                }

                                if (!reportItem['regularPrice'] && !reportItem['itemSize'] && !reportItem['unit_id']) {
                                    var unitPrice = services.bulkUpload.calculateUnitPrice(item['regularPrice'],  foundItem['itemSize'], item['unit_id']);
                                    if (typeof unitPrice.result == 'string') {
                                        reportItem['unitPrice'] = unitPrice.result;
                                        return null;
                                    } else {
                                        existItemToShop.unitPrice = unitPrice.result;
                                    }
                                }
                                return existItemToShop.save();
                        }).then(function(existItemToShop){
                            if (!existItemToShop){
                                return;
                            }

                            var priceUpdateKeys = Object.keys(updateItems);
                            for (var i = 0; i < priceUpdateKeys.length; i++) {
                                indexUpdatedRows[index].push(priceUpdateKeys[i]);
                            }

                            if (foundItem.image !== itemsForUpdate[item['barCode']]['path']) {
                                itemsForUpdate[item['barCode']]['id'] = foundItem.id;
                                req.temp.updatedItemsHasImage[index] = itemsForUpdate[item['barCode']];
                            }

                            return Promise.mapSeries(item.categories, function(category){
                                if (!category){
                                     return;
                                }
                                return category.hasItem(foundItem.id).then(function(thereIs) {
                                    if (!thereIs){
                                         return category.addItem(foundItem.id);
                                    }
                                });
                            });
                        }).catch(next);

                    }

                    var tempItemForUpdate = itemsForUpdate[item['barCode']];
                    if (item['serving_unit_id']) {
                        tempItemForUpdate['serving_unit_id'] = item['serving_unit_id'];
                    }
                    if (item['unit_id']) {
                        tempItemForUpdate['unit_id'] = item['unit_id'];
                    }

                    if ((!reportItem['Item Size'] && item['itemSize']) || (!reportItem['Unit Type'] && item['unit_id']) ) {
                        var options = {where: {item_id: foundItem.id }};
                        var indexItemInOwnShop = -1;
                        var itemByShopsForUpdate =  [];
                        var itemSize = item['itemSize'] ? item['itemSize'] : foundItem['itemSize'];
                        var unitId   = item['unit_id']  ? item['unit_id']  : foundItem['unit_id'];

                        return models.itemByShop.all(options).then(function (shopItems) {
                            return Promise.mapSeries(shopItems, function (itemToShop, index) {
                                var regularPrice = itemToShop.regularPrice;

                                if (itemToShop['shop_id'] === shopId){
                                    indexItemInOwnShop = index;
                                    if (!reportItem['Regular Price']){
                                        regularPrice = item['regularPrice'];
                                    }
                                }

                                var unitPrice = services.bulkUpload.calculateUnitPrice(regularPrice, itemSize, unitId);

                                if (typeof unitPrice.result == 'string') {
                                    reportItem['Unit Price'] = '....';
                                    return;
                                }
                                itemToShop.unitPrice = unitPrice.result;
                                return itemToShop;
                            });
                        }).then(function (shopItems) {
                            if (reportItem['Unit Price']){
                                return;
                            }
                            itemByShopsForUpdate = shopItems;

                            if (indexItemInOwnShop !== -1){
                                var existItemToShop = itemByShopsForUpdate[indexItemInOwnShop];
                                updateItems['item_id'] = foundItem.id;
                                updateItems['shop_id'] = shopId;

                                if (!reportItem['Is out of stock']){
                                    updateItems['is_out_of_stock'] = item['is_out_of_stock'];
                                    existItemToShop['is_out_of_stock'] = item['is_out_of_stock'];

                                    if (!existItemToShop['is_out_of_stock'] && item['is_out_of_stock']) {
                                        if (!reportItem['PN(Out of stock)']) {
                                            var pushData = {
                                                action: 'getDevicesForItemWithShop',
                                                deliveryDateTime: item['pnOutOfStock'],
                                                options: {
                                                    itemId  : foundItem.id,
                                                    entityId: existItemToShop.id,
                                                    shopId  : shopId,
                                                    postCode: req['temp'].shop.postCode,
                                                    pushType: config.constants.PUSH_TYPES.ITEM_OUT_OF_STOCK
                                                },
                                                message: {
                                                    itemId: foundItem.id,
                                                    shopId: shopId
                                                }
                                            };
                                            shoppingItems.push(existItemToShop.id);
                                            pushes.push(pushData);
                                        } else {
                                            if (!reportItem['PN(Out of stock)']) {
                                                reportItem['PN(Out of stock)'] = 'PN cannot be sent because this event is not in list of events which run PN';
                                            }
                                        }
                                    }
                                }

                                if (!reportItem['Regular Price']){
                                    updateItems['regularPrice'] = item['regularPrice'];
                                    existItemToShop['regularPrice'] = item['regularPrice'];

                                    if (existItemToShop.regularPrice > item['regularPrice']) {
                                        if (!reportItem['PN(Regular Price)']) {
                                            var pushData = {
                                                action: 'getDevicesPerWishes',
                                                deliveryDateTime: item['pnPriceReduced'],
                                                options: {
                                                    itemId  : foundItem.id,
                                                    entityId: existItemToShop.id,
                                                    postCode: req['temp'].shop.postCode,
                                                    pushType: config.constants.PUSH_TYPES.REGULAR_PRICE_REDUCED
                                                },
                                                message: {
                                                    itemId  : foundItem.id,
                                                    shopId  : shopId,
                                                    oldPrice: existItemToShop.regularPrice,
                                                    newPrice: item.regularPrice
                                                }
                                            };
                                            pushes.push(pushData);
                                        } else {
                                            if (!reportItem['PN(Regular Price)']) {
                                                reportItem['PN(Regular Price)'] = 'PN cannot be sent because this event is not in list of events which run PN';
                                            }
                                        }
                                    }
                                }
                                return existItemToShop.save();
                            }

                            var options = {
                                item_id: foundItem.id,
                                shop_id: shopId
                            };

                            if (!reportItem['Regular Price']) {
                                var unitPrice = services.bulkUpload.calculateUnitPrice(item['regularPrice'], itemSize, unitId);
                                if (typeof unitPrice.result == 'string') {
                                    reportItem['Unit Price'] = unitPrice.result;
                                    return;
                                }
                                options['regularPrice'] = item['regularPrice'];
                                options['unitPrice'] = unitPrice['result'];
                            }

                            if (!reportItem['Is out of stock']){
                                options['is_out_of_stock'] = item['is_out_of_stock'];
                            }

                            return models.itemByShop.create(options);
                        }).then(function () {
                            if (reportItem['Unit Price']){
                                 return;
                            }
                            return models.items.update(tempItemForUpdate, findOptions);
                        }).then(function () {
                            if (reportItem['Unit Price']){
                                return;
                            }
                            return Promise.mapSeries(itemByShopsForUpdate, function (itemToShop) {
                                itemToShop.save();
                            });
                        }).then(function(){
                            if (reportItem['Unit Price']){
                                 return;
                            }
                            var baseUpdateKeys = Object.keys(itemsForUpdate[item['barCode']]);
                            var priceUpdateKeys = Object.keys(updateItems);
                            for (var i = 0; i < baseUpdateKeys.length; i++) {
                                indexUpdatedRows[index].push(baseUpdateKeys[i]);
                            }
                            for (var i = 0; i < priceUpdateKeys.length; i++) {
                                indexUpdatedRows[index].push(priceUpdateKeys[i]);
                            }
                            if (foundItem.image !== itemsForUpdate[item['barCode']]['path']) {
                                itemsForUpdate[item['barCode']]['id'] = foundItem.id;
                                req['temp'].updatedItemsHasImage[index] = itemsForUpdate[item['barCode']];
                            }
                            return Promise.mapSeries(item.categories, function(category){
                                if (!category){
                                     return;
                                }
                                return category.hasItem(foundItem.id).then(function(thereIs) {
                                    if (!thereIs) {
                                       return category.addItem(foundItem.id);
                                    }
                                });
                            });
                        }).catch(next);
                    }

                    return models.items.update(tempItemForUpdate, findOptions).then(function () {
                        updateItems['item_id'] = foundItem.id;
                        updateItems['shop_id'] = shopId;
                        if (!reportItem['Is out of stock']){
                            updateItems['is_out_of_stock'] = item['is_out_of_stock'];
                        }
                        if (!reportItem['Regular Price']){
                            updateItems['regularPrice'] = item['regularPrice'];
                        }

                        var options = {
                            where: {
                                item_id: foundItem.id,
                                shop_id: shopId
                            },
                            defaults: updateItems
                        };

                        return models.itemByShop.findOrCreate(options)
                            .spread(function(existItemToShop, created){
                                if (created){ 
                                    return;
                                }

                                if (!reportItem['Is out of stock']) {
                                    if (!existItemToShop['is_out_of_stock'] && item['is_out_of_stock']) {
                                        if (!reportItem['PN(Out of stock)']) {
                                            var pushData = {
                                                action: 'getDevicesForItemWithShop',
                                                deliveryDateTime: item['pnOutOfStock'],
                                                options: {
                                                    itemId  : foundItem.id,
                                                    entityId: existItemToShop.id,
                                                    shopId  : shopId,
                                                    postCode: req['temp'].shop.postCode,
                                                    pushType: config.constants.PUSH_TYPES.ITEM_OUT_OF_STOCK
                                                },
                                                message: {
                                                    itemId: foundItem.id,
                                                    shopId: shopId
                                                }
                                            };
                                            shoppingItems.push(existItemToShop.id);
                                            pushes.push(pushData);
                                        } else {
                                            if (!reportItem['PN(Out of stock)']) {
                                                reportItem['PN(Out of stock)'] = 'PN cannot be sent because this event is not in list of events which run PN';
                                            }
                                        }
                                    }
                                    existItemToShop['is_out_of_stock'] = item['is_out_of_stock'];
                                }

                                if (!reportItem['Regular Price']) {
                                    if (existItemToShop['regularPrice'] > item['regularPrice']) {
                                        if (!reportItem['PN(Regular Price)']) {
                                            var pushData = {
                                                action: 'getDevicesPerWishes',
                                                deliveryDateTime: item['pnPriceReduced'],
                                                options: {
                                                    itemId  : foundItem.id,
                                                    entityId: existItemToShop.id,
                                                    postCode: req['temp'].shop.postCode,
                                                    pushType: config.constants.PUSH_TYPES.REGULAR_PRICE_REDUCED
                                                },
                                                message: {
                                                    itemId  : foundItem.id,
                                                    shopId  : shopId,
                                                    oldPrice: existItemToShop.regularPrice,
                                                    newPrice: item.regularPrice
                                                }
                                            };
                                            pushes.push(pushData);
                                        } else {
                                            if (!reportItem['PN(Regular Price)']) {
                                                reportItem['PN(Regular Price)'] = 'PN cannot be sent because this event is not in list of events which run PN';
                                            }
                                        }
                                    }
                                    existItemToShop.regularPrice = item['regularPrice'];
                                }

                                return existItemToShop.save();
                       }).then(function(){
                          var baseUpdateKeys = Object.keys(itemsForUpdate[item['barCode']]);
                          var priceUpdateKeys = Object.keys(updateItems);
                          for (var i = 0; i < baseUpdateKeys.length; i++) {
                              indexUpdatedRows[index].push(baseUpdateKeys[i]);
                          }

                          for (var i = 0; i < priceUpdateKeys.length; i++) {
                              indexUpdatedRows[index].push(priceUpdateKeys[i]);
                          }

                          if (foundItem.image !== itemsForUpdate[item['barCode']]['path']) {
                              itemsForUpdate[item['barCode']]['id'] = foundItem.id;
                              req['temp'].updatedItemsHasImage[index] = itemsForUpdate[item['barCode']];
                          }

                          return Promise.mapSeries(item.categories, function(category){
                              if (!category){
                                   return;
                              }
                              return category.hasItem(foundItem.id).then(function(thereIs) {
                                  if (!thereIs){
                                       return category.addItem(foundItem.id);
                                  }
                              });
                          });
                   }).catch(next);
                });
            }).catch(next);
        });
    }).then(function(){
        var createdItems = req.temp.createdItems;
        var updatedItemsHasImage = req.temp.updatedItemsHasImage;

        for (var i in createdItems){
            if (reportStructure[i].Image){
                continue;
            }
            var options = {
                url   : items[i].path,
                itemId: createdItems[i].id
            };
            Queue.create('uploadImageWorker', options).save();
        }

        for (var i in updatedItemsHasImage){
            if (!updatedItemsHasImage[i].path.length){
                continue;
            }
            var options = {
                url   : updatedItemsHasImage[i].path,
                itemId: updatedItemsHasImage[i].id
            };

            Queue.create('uploadImageWorker', options).save();
        }
    }).then(function() {
        return Promise.map(pushes, function(pushItem){
            logger.log('info', pushItem.title + ' push = ', pushItem);
            return services.deliveryNotification[pushItem.action](pushItem.options).then(function (data) {
                var pushData = {
                    data: pushItem,
                    entityType: config.constants.DELIVERY_NOTIFICATION_ENTITY_TYPES.ITEM_BY_SHOP,
                    pushType: pushItem.options.pushType,
                    entityId: pushItem.options.entityId,
                    userData: data,
                    deliveryDateTime: pushItem.deliveryDateTime,
                    shopId: shopId,
                    adminId: user.id
                };
                return services.deliveryNotification.createNotification(pushData);
            });
        });
    }).then(function(){
        if (shoppingItems.length){
            var options = {
                where: {
                    item_by_shop_id: {
                        $in:  Array.from(new Set(shoppingItems))
                    }
                }
            };
            return models.shopping.destroy(options);
        }
    }).then(function() {
        var report = [];
        var countUpdated = 0;
        var updateReport = '';
        var countOfCreated = 0;    
        var reportBook = XLSX.utils.book_new();
        var name = 'Associate multiple items to your store';

        reportStructure.forEach(function(item, index){
            var tempIndex =  index + 2;
            var categories = item['Categories'];
            
            if (categories.length){
                report.push(['Warnings (Categories) at row: '+ tempIndex]);
                for (var i in categories){
                    report.push(['At column: '+ i, categories[i]]);
                }
            }
            delete item['Categories'];

            if (Object.keys(item).length < 7 && indexUpdatedRows[index].length === 0) {
                countOfCreated++;

                if (item['Image']){
                    report.push(
                        ['Warning (Image Url)'],
                        ['At row: '+ tempIndex, item['Image'] ]
                    );
                }
                
                if (item['Serving Size']){
                    report.push(
                        ['Warning (Serving Size)'],
                        ['At row: '+ tempIndex, item['Serving Size'] ]
                    );
                }

                if (item['Serving Per Pack']){
                    report.push(
                        ['Warning (Serving Per Pack)'],
                        ['At row: '+ tempIndex, item['Serving Per Pack'] ]
                    );
                }

                if (item['Serving Type']){
                    report.push(
                        ['Warning (Serving Type)'],
                        ['At row: '+ tempIndex, item['Serving Type'] ]
                    );
                }

                if (item['PN(Regular Price)']){
                    report.push(
                        ['Warning (PN(Regular Price))'],
                        ['At row: '+ tempIndex, item['PN(Regular Price)'] ]
                    );
                }

                if (item['PN(Out of stock)']){
                    report.push(
                        ['Warning (PN(Out of stock))'],
                        ['At row: '+ tempIndex, item['PN(Out of stock)'] ]
                    );
                }
                
                return;
            }

            delete item['Image'];
            delete item['Serving Type'];
            delete item['Serving Size'];
            delete item['Serving Per Pack'];
            delete item['PN(Regular Price)'];
            delete item['PN(Out of stock)'];

            if (indexUpdatedRows[index].length > 0) {
                var number = 0;
                countUpdated++;
                report.push(['Was updated at row ' + tempIndex]);
                for (var i in indexUpdatedRows[index]) {
                    var name = updateNames[indexUpdatedRows[index][i]];
                    if (!name){
                        continue;
                    }
                    number++;
                    report.push(['At column: ' + number, name]);
                }
                return;
            }

            report.push(['Errors at row: ' + tempIndex]);
            for (var i in item){
                 report.push([i, item[i]]);
            }
        });

        report.unshift(['Successfully added : ' + countOfCreated  + ' items']);
        report.unshift(['Successfully updated : ' + countUpdated  + ' items']);
        var reportSheet = XLSX.utils.aoa_to_sheet(report, { cellDates:true });
        XLSX.utils.book_append_sheet(reportBook, reportSheet, 'Report');
        
        var options = {
            data  : reportBook,
            shopId: shopId,
            type  : config.constants['REPORT_TYPES'].BULK,
            name  : name
        };

        return services.report.saveReport(user,options);
    }).then(function(result){
        req.temp = {result: result};
        return unlink(req.file.path);
    }).then(function(){
        res.status(200).send(req.temp.result);
    }).catch(next);
}


/** Items
 * @param req
 * @param res
 * @param next
 */
function itemsUpload(req, res, next){
    var data = req.temp.data;
    var structure = config.constants['BULK_UPLOAD'].ITEMS;
    var length = Object.keys(structure).length;
    
    data.splice(0, 1);
    if (data[0].length < length){
        var error = new Error('Invalid file structure');
        error.status = 422;
        return next(error);
    }

    var user = req.user;
    var items = [];
    var reportStructure = [];

    data.forEach(function(row){
        var item = [];
        for (var i  = 0; i < row.length; i++){
            var field = (row[i] ? row[i].trim() : '');
            item.push(field);
        }

        var tempItem = {
            barCode         : item[structure['BARCODE']],
            name            : item[structure['NAME']],
            brand           : item[structure['BRAND']],
            unit_id         : item[structure['UNIT_TYPE']].toLowerCase(),
            itemSize        : parseFloat(item[structure['ITEM_SIZE']]),
            servingSize     : parseInt(item[structure['SERVING_SIZE']]),
            serving_unit_id : item[structure['SERVING_TYPE']].toLowerCase(),
            servingPerPack  : parseInt(item[structure['SERVING_PER_PACK']]),
            path            : item[structure['IMAGE_URL']],
            categories      : item.splice(structure['FIRST_SUBCATEGORY'], item.length),
            // default
            image_id        : null
        };


        var errorItem = {
            "Image"           : "",
            "Serving Size"    : "",
            "Serving Per Pack": "",
            "Serving Type"    : "",
            "Categories"      : []
        };

        // Barcode
        if (tempItem['barCode'].length < config.constants['BARCODE_MIN_LENGTH']) {
            errorItem['BarCode'] = "Field 'BarCode' Length is less than " + config.constants['BARCODE_MIN_LENGTH'];
        } else if (tempItem['barCode'].length > config.constants['TEXT_MAX_LENGTH']) {
            errorItem['BarCode'] = "Field 'BarCode'Length is longer than " + config.constants['TEXT_MAX_LENGTH'];
        } else if (!/^\d+$/.test(tempItem['barCode'])){
            errorItem['BarCode'] = "Field 'BarCode' must consist only of digits";
        }

        // Name
        if (!tempItem['name'].length){
            errorItem['Name'] = "Field 'Name' is empty";
        } else if(tempItem['name'].length > config.constants['TEXT_MAX_LENGTH']){
            errorItem['Name'] = "Field 'Name' Length is longer than " + config.constants['TEXT_MAX_LENGTH'];
        }
        // Brand
        if (!tempItem['brand'].length){
            errorItem['Brand'] = "Field 'Brand' is empty";
        } else if(tempItem['name'].length > config.constants['TEXT_MAX_LENGTH']){
            errorItem['Brand'] = "Field 'Brand' Length is longer than " + config.constants['TEXT_MAX_LENGTH'];
        }

        // Item Size
        if (isNaN(tempItem['itemSize'])){
            errorItem['Item Size'] = "Invalid 'Item size' field";
        }else if (tempItem['itemSize'] < 0){
            errorItem['Item Size'] = "Invalid 'Item size' field";
        }

        // Serving Size
        if (isNaN(tempItem['servingSize'])) {
            errorItem['Serving Size'] = "'Serving size' field is empty";
        } else if (tempItem['servingSize'] < 0){
            errorItem['Serving Size'] = "Invalid 'Serving size' field";
        }

        if (errorItem['Serving Size']){
            tempItem['servingSize'] = null;
        }

        //Serving Per Pack
        if (isNaN(tempItem['servingPerPack'])){
            errorItem['Serving Per Pack'] = "'Serving Per Pack' field is empty";
        } else if (tempItem['servingPerPack'] < 0){
            errorItem['Serving Per Pack'] = "Invalid 'Serving Per Pack' field";
        }

        if (errorItem['Serving Per Pack']){
            tempItem['servingPerPack'] = null;
        }

        // Image Url
        if (!tempItem['path'].length){
            errorItem['Image'] = "Image url field is empty. Item was created without image";
        } else if (tempItem['path'].indexOf("http") == -1){
            errorItem['Image'] = "Invalid 'image url' field";
        }

        items.push(tempItem);
        reportStructure.push(errorItem);
    });

    services.bulkUpload.verifyItemsByTypes(items, reportStructure)
        .then(function(){
            return services.bulkUpload.verifyItemsByCategories(items, reportStructure, length);
        }).then(function(){

            return Promise.mapSeries(items, function(item, index){
                var reportItem = reportStructure[index];
                if (Object.keys(reportItem).length > 5){
                    return null;
                }
                var options = {
                    where: { barCode: item['barCode'] },
                    defaults: item
                };
                return models.items.findOrCreate(options).spread(function(existItem, created){
                    if (!created){
                        reportItem['DataBase'] = "Item with Bar Code (" + item['barCode'] + ") has already existed";
                        return;
                    }

                    if (!reportItem['Image']){
                        var options = {
                            url   : item['path'],
                            itemId: existItem.id
                        };
                        Queue.create('uploadImageWorker', options).save();
                    }

                    return Promise.mapSeries(item.categories, function(category){
                        return category.addItem(existItem);
                    });
                });
            });
        }).then(function(){
            var countAll = 0;
            var reportBook = XLSX.utils.book_new();
            var report = [];
            var name = "Bulk upload new items";

            reportStructure.forEach(function(item, index){
                var tempIndex = index + 2;

                var categories = item['Categories'];
                if(categories.length){
                    report.push(['Warnings (Categories) at row: '+ tempIndex]);
                    for(var i in categories) report.push(['At column: '+ i, categories[i]]);
                }
                delete item['Categories'];

                if(Object.keys(item).length < 5){
                    countAll++;

                    if(item['Image']){
                        report.push(
                            ['Warning (Image Url)'],
                            ['At row: '+ tempIndex, item['Image'] ]
                        );
                    }
                    
                    if(item['Serving Size']){
                        report.push(
                            ['Warning (Serving Size)'],
                            ['At row: '+ tempIndex, item['Serving Size'] ]
                        );
                    }

                    if(item['Serving Per Pack']){
                        report.push(
                            ['Warning (Serving Per Pack)'],
                            ['At row: '+ tempIndex, item['Serving Per Pack'] ]
                        );
                    }

                    if(item['Serving Type']){
                        report.push(
                            ['Warning (Serving Type)'],
                            ['At row: '+ tempIndex, item['Serving Type'] ]
                        );
                    }
                    return;
                }

                delete item['Image'];
                delete item['Serving Type'];
                delete item['Serving Size'];
                delete item['Serving Per Pack'];

                if (!Object.keys(item).length) return;
                report.push(['Errors at row: ' + tempIndex]);

                for(var i in item) report.push([i, item[i]]);
            });

            report.unshift(["Successfully added: " + countAll  + " items to MasterDB"]);

            var reportSheet = XLSX.utils.aoa_to_sheet(report, { cellDates:true });
            XLSX.utils.book_append_sheet(reportBook, reportSheet,"Report");

            var options = {
                data: reportBook,
                type: config.constants['REPORT_TYPES'].BULK,
                name: name
            };
            return services.report.saveReport(user,options);

        }).then(function(result){
            req.temp = { result: result};
            return unlink(req.file.path);
        }).then(function(){
             res.status(200).send(req.temp.result);
        }).catch(next);
}

/**
 * Upload sales
 *
 * @param req
 * @param res
 * @param next
 */
function sales(req, res, next) {
    var data = req.temp.data;
    var structure = config.constants['BULK_UPLOAD'].SALES;

    if(data[0].length < Object.keys(structure).length){
        var error = new Error('Ivalid file structure');
        error.status = 422;
        return next(error);
    }
    data.splice(0, 1);

    var user = req.user;
    var pushes = [];
    var itemsByShop = {};
    var hasSaleLimit = false;
    var reportStructure = null;
    var shopId = req.body.shopId;
    var separators = config.constants['BULK_DATE_TIME_PARSE'];
    var priceLength = config.constants['PRICE_MAX_LENGTH'];

    models.shops.findById(shopId).then(function(shop) {
        if (!shop) {
            var error = new Error('Shop is not exist!');
            error.status = 404;
            throw error;
        }

        req.temp = { shop: shop };
        if(user.role != "SAVVY_SHOP_ADMIN") return true;
        return shop.countOwners({ where: { role: "ENTERPRISE_SAVVY_SHOP" } });

    }).then(function(count){
        hasSaleLimit = (!count);

        reportStructure = data.map(function(row, index){

            var item = [];
            for(var i  = 0; i < row.length; i++){
               var field = (row[i] ? row[i].trim() : '');
               item.push(field);
            }

            var tempItem = {
                barCode      : item[structure['BARCODE']],
                salePrice    : item[structure['SALE_PRICE']],
                from         : item[structure['FROM']],
                pnFrom       : item[structure['PN_FROM']],
                to           : item[structure['TO']],
                pnTo         : item[structure['PN_TO']]
            };

            var errorItem = {};

            // Barcode
            if(tempItem['barCode'].length < config.constants['BARCODE_MIN_LENGTH']) {
                errorItem['BarCode'] = "Field 'BarCode' Length is less than " + config.constants['BARCODE_MIN_LENGTH'];
            } else if(tempItem['barCode'].length > config.constants['TEXT_MAX_LENGTH']) {
                errorItem['BarCode'] = "Field 'BarCode'Length is longer than " + config.constants['TEXT_MAX_LENGTH'];
            } else if(!/^\d+$/.test(tempItem['barCode'])){
                errorItem['BarCode'] = "Field 'BarCode' must consist only of digits";
            }

            if (tempItem['salePrice'].length) {
                var salePrice = parseFloat(tempItem['salePrice']);
                if(!isNaN(salePrice)){

                    var priceMatches = tempItem['salePrice'].split('.');

                    if(priceMatches[0].length >  priceLength['INTEGER']) {
                        errorItem['Sale Price'] = "'Sale Price' field is out of range";
                    } else if (priceMatches[1] && priceMatches[1].length > priceLength['FRACTIONAL']) {
                        errorItem['Sale Price'] = " Fractional part of 'Sale Price' field is out of range";
                    } else if (salePrice < 0){
                        errorItem['Sale Price'] = "'Sale Price' must be greater than '0'";
                    } else {
                        tempItem['salePrice'] = parseFloat(tempItem['salePrice']).toFixed(2);
                    }
                } else {
                    errorItem['Sale Price'] = "Invalid 'Sale Price' field";
                }
            } else {
                errorItem['Sale Price'] = "Invalid 'Sale Price' field";
            }

            // From
            if(tempItem['from'].length) {
                var dParams = tempItem['from'].split(separators['DATE']);
                var from = new Date(dParams[2], dParams[1] - 1, dParams[0], 0, 0, 0, 0);
                if (from.toString() != 'Invalid Date') {
                    if (from < new Date()){
                        errorItem['Start Date'] = "Field 'Start Date' is earlier than current date";
                    } else {
                        tempItem['from'] = from;
                    }
                } else {
                    errorItem['Start Date'] = "Invalid 'Start Date' field";
                }
            } else {
                errorItem['Start Date'] = "Invalid 'Start Date' field";
            }

            //To
            if(tempItem['to'].length) {
                var dParams = tempItem['to'].split(separators['DATE']);
                var to = new Date(dParams[2], dParams[1] - 1, dParams[0], 0, 0, 0, 0);
                if (to.toString() != 'Invalid Date') {
                    if (to < new Date()){
                        errorItem['Finish Date'] = "Field 'Finish Date' is earlier than current date";
                    } else {
                        tempItem['to'] = to;
                    }
                } else {
                    errorItem['Finish Date'] = "Invalid 'Finish Date' field";
                }
            } else {
                errorItem['Finish Date'] = "Invalid 'Finish Date' field";
            }


            // Date
            if (!errorItem['Start Date'] && !errorItem['Finish Date']) {
                if (tempItem['to'] <= tempItem['from']){
                    errorItem['Date'] = 'Date Start is earlier than Finish date';
                }
                if (hasSaleLimit){
                    var limitDate = new Date(tempItem['from']);
                    limitDate.setMonth(limitDate.getMonth() + config['constants'].MONTH_DATE_RANGE_LIMITS['SALES']);
                    if (limitDate < tempItem['to']){
                        errorItem['Date'] = 'End date must be 3 months or less than the Start date';
                    }
                }
            }

            // Push Notification (From)
            if (tempItem['pnFrom'].length){
                var dtParams   = tempItem['pnFrom'].split(separators['BETWEEN']);
                if (dtParams.length > 1){
                    var dateParams = dtParams[0].split(separators['DATE']);
                    var timeParams = dtParams[1].split(separators['TIME']);
                   
                    if (dateParams.length > 2 && timeParams.length > 1){ 
                        var pnFrom = new Date(dateParams[2], dateParams[1] - 1, dateParams[0], timeParams[0], timeParams[1], 0, 0);
                        if (pnFrom.toString() == 'Invalid Date'){
                            errorItem['PN(Sale start)'] = "Invalid PN(Sale start) field1";
                        }
                        if(!errorItem['PN(Sale start)'] && !errorItem['Start Date'] && !errorItem['Date']) {
                            pnFrom = services.bulkUpload.getValidDate(pnFrom, tempItem['from']);
                            if(!pnFrom){
                                errorItem['PN(Sale start)'] = "Push notification about 'Sale Start' must be early than 'Sale Start' field";
                            } else tempItem['pnFrom'] = pnFrom;
                        }
                    } else errorItem['PN(Sale start)'] = "Invalid PN(Sale start) field2";
                } else errorItem['PN(Sale start)'] = "Invalid PN(Sale start) field3";
            }

            // Push Notification (To)
            if(tempItem['pnTo'].length){
                var dtParams   = tempItem['pnTo'].split(separators['BETWEEN']);
                if(dtParams.length > 1){
                    var dateParams = dtParams[0].split(separators['DATE']);
                    var timeParams = dtParams[1].split(separators['TIME']);

                    if(dateParams.length > 2 && timeParams.length > 1){ 
                        var pnTo = new Date(dateParams[2], dateParams[1] - 1, dateParams[0], timeParams[0], timeParams[1], 0, 0);

                        if (pnTo.toString() == 'Invalid Date'){
                            errorItem['PN(Sale finish)'] = "Invalid PN(Sale finish) field1";
                        }
                        if(!errorItem['PN(Sale finish)'] && !errorItem['Finish Date'] && !errorItem['Date']) {
                            pnTo = services.bulkUpload.getValidDate(pnTo, tempItem['to']);
                            if(!pnTo) errorItem['PN(Sale finish)'] = "Push notification about 'Sale Finish' must be early than 'Sale Finish' field";
                            else tempItem['pnTo'] = pnTo;
                        }
                    } else errorItem['PN(Sale finish)'] = "Invalid PN(Sale finish) field2";
                } else errorItem['PN(Sale finish)'] = "Invalid PN(Sale finish) field3";    
            }

            if(!Object.keys(errorItem).length){
                tempItem['index'] = index;
                tempItem['to'] = tempItem['to'].getTime();
                tempItem['from'] = tempItem['from'].getTime();
                itemsByShop[tempItem['barCode']] = tempItem;
            }
            return errorItem;
        });

        var options = { where: { barCode : { in: Object.keys(itemsByShop) }} };
        return req.temp['shop'].getItems(options);
    }).then(function(existItems){
        req['temp'].existItems = existItems;
        var shop = req.temp['shop'];

        return Promise.mapSeries(existItems, function(item){
            var data = itemsByShop[item['barCode']];

            if(item['itemByShop'].regularPrice <= data.salePrice){
                reportStructure[data.index].salePrice = "Sale price needs to be a lower price point than the regular price. Please review your sales price to continue";
                return null;
            }

            var activity = {
                admin_id   : user.id,
                product_id : item['itemByShop'].id,
                from       : data.from,
                to         : data.to
            };

            var product = {
                filter: {
                    where: {
                        item_id: item.id,
                        shop_id: shopId
                    }
                },
                data: {
                    salePrice    : data.salePrice,
                    from         : data.from,
                    to           : data.to,
                    isSale       : true
                }
            };


            if((item.from != data.from) && data.pnFrom) {
                pushes.push({
                    deliveryDateTime: services.bulkUpload.getValidDate(data.pnFrom),
                    options: {
                        itemId  : item.id,
                        postCode: req['temp'].shop.postCode,
                        pushType: config.constants.PUSH_TYPES.SALE_START
                    },
                    message: {
                        itemId      : item.id,
                        shopId      : shopId,
                        regularPrice: item.regularPrice,
                        salePrice   : data.salePrice
                    }
                });
            }

            if((item.to != data.to) && data.pnTo){
                pushes.push({
                    deliveryDateTime: services.bulkUpload.getValidDate(data.pnTo),
                    options: {
                        itemId      : item.id,
                        postCode    : req['temp'].shop.postCode,
                        pushType    : config.constants.PUSH_TYPES.SALE_FINISH
                    },
                    message: {
                        itemId      : item.id,
                        shopId      : shopId,
                        regularPrice: item.regularPrice,
                        salePrice   : data.salePrice
                    }
                });
            }

            return Promise.all([
                models.itemByShop.update(product.data, product.filter),
                models.salesActivityByAdmin.create(activity)
            ]);
        });
    }).then(function (result) {
        req['temp'].result = result;

        return Promise.mapSeries(pushes, function(pushItem){
            logger.log('info', pushItem['message'].title + ' push = ', pushItem);
            return services.deliveryNotification[
                (pushItem.options.pushType == config.constants.PUSH_TYPES.SALE_START) ?
                'getDevicesPerWishes' : 'getDevicesPerItem'
            ](pushItem.options).then(function (data) {
                var pushData = {
                    data            : pushItem,
                    entityType      : config.constants.DELIVERY_NOTIFICATION_ENTITY_TYPES.ITEM_BY_SHOP,
                    pushType        : pushItem.options.pushType,
                    entityId        : pushItem.options.itemId,
                    userData        : data,
                    deliveryDateTime: pushItem.deliveryDateTime,
                    shopId          : shopId,
                    adminId         : user.id
                };
                return services.deliveryNotification.createNotification(pushData);
            });
        });
    }).then(function(){
        var existItems = req['temp'].existItems;
        var result     = req['temp'].result;
        var reportBook = XLSX.utils.book_new();
        var report = [];
        var count = 0;
        var name = "Set up multiple sales for your items";

        result.forEach(function(item, index){
            if(!item) return;
            var barCode = existItems[index].barCode;
            var reportIndex = itemsByShop[barCode].index;
            reportStructure[reportIndex].isUpdated = true;
        });

        reportStructure.forEach(function(item, index){
            if(!Object.keys(item).length) item['barCode'] = "Item with this 'BarCode' does not exist in your shop/all db";
            if(item.isUpdated){
                count++;
                return;
            }
            report.push(['Error at row: '+ (index + 2), ]);
            for(var i in item) report.push(['At column: ' + i, item[i]]);
        });
        report.unshift(['Successfully setted sales to ' + count +  'items']);

        var reportSheet = XLSX.utils.aoa_to_sheet(report, { cellDates:true });
        XLSX.utils.book_append_sheet(reportBook, reportSheet, "Report");

        var options = {
            data  :  reportBook,
            shopId:  shopId,
            type  :  config.constants['REPORT_TYPES'].BULK,
            name  :  name
        };
        return services.report.saveReport(user,options);

    }).then(function(result){
        req.temp = { result: result};
        return unlink(req.file.path);
    }).then(function(){
        res.status(200).send(req.temp.result);
    }).catch(next);
}

/**
 * Upload advertising
 *
 * @param req
 * @param res
 * @param next
 */
function advertising(req, res, next){
    var user = req.user;
    var data = req.temp.data;
    var structure = config.constants['BULK_UPLOAD'].ADVERTISING;
    var length = Object.keys(structure).length;

    if(data[0].length < length){
        var error = new Error('Ivalid file structure');
        error.status = 422;
        return next(error);
    }

    data.splice(0, 1);

    var postCodeId;
    var reportStructure = [];
    var shopId = req.body.shopId;   
    var hasAdvertisingLimit = false;
    var separator = config.constants['BULK_DATE_TIME_PARSE'].DATE;

    models.shops.findById(shopId)
        .then(function(shop) {

            if (!shop) {
                var error = new Error('Shop is not exist!');
                error.status = 404;
                throw error;
            }

            req.temp = { shop: shop };
            return models.postCodes.find({ where: { code: shop.postCode} });

        }).then(function(postCode){

            if (!postCode) {
                var error = new Error('Invalid post code!');
                error.status = 422;
                throw error;
            }

            postCodeId  = postCode.id;
            if(user.role != "SAVVY_SHOP_ADMIN") return true;
            var options = { where: { role: "ENTERPRISE_SAVVY_SHOP" } };
            var shop = req['temp'].shop;
            return shop.countOwners(options);

        }).then(function(count){
            hasAdvertisingLimit = (!count);

            return Promise.mapSeries(data, function (row) {
                var item = [];
                for(var i  = 0; i < row.length; i++){
                var field = (row[i] ? row[i].trim() : '');
                item.push(field);
                }

                var tempItem = {
                    barCode     : item[structure['ITEM_BARCODE']],
                    from        : item[structure['FROM']],
                    to          : item[structure['TO']],
                    advBarCodes : {}
                };
                var errorItem = {};

                // Barcode
                if(tempItem['barCode'].length < config.constants['BARCODE_MIN_LENGTH']) {
                    errorItem['BarCode'] = "Field 'BarCode' Length is less than " + config.constants['BARCODE_MIN_LENGTH'];
                } else if(tempItem['barCode'].length > config.constants['TEXT_MAX_LENGTH']) {
                    errorItem['BarCode'] = "Field 'BarCode'Length is longer than " + config.constants['TEXT_MAX_LENGTH'];
                } else if(!/^\d+$/.test(tempItem['barCode'])){
                    errorItem['BarCode'] = "Field 'BarCode' must consist only of digits";
                }

                if(tempItem['from'].length) {
                    var dParams = tempItem['from'].split(separator);
                    var from = new Date(dParams[2], dParams[1] - 1, dParams[0], 0, 0, 0, 0);
                    if (from.toString() != 'Invalid Date') {
                        if (from < new Date()){
                            errorItem['Start Date'] = "Field 'Start Date' is earlier than current date";
                        } else {
                            tempItem['from'] = from;
                        }
                    } else {
                        errorItem['Start Date'] = "Invalid 'Start Date' field";
                    }
                } else {
                    errorItem['Start Date'] = "Invalid 'Start Date' field";
                }

                if(tempItem['to'].length) {
                    var dParams = tempItem['to'].split(separator);
                    var to = new Date(dParams[2], dParams[1] - 1, dParams[0], 0, 0, 0, 0);

                    if (to.toString() != 'Invalid Date') {
                        if (to < new Date()){
                            errorItem['Finish Date'] = "Field 'Finish Date' is earlier than current date";
                        } else {
                            tempItem['to'] = to;
                        }
                    } else {
                        errorItem['Finish Date'] = "Invalid 'Finish Date' field";
                    }
                } else {
                    errorItem['Finish Date'] = "Invalid 'Finish Date' field";
                }

                if (!errorItem['Start Date'] && !errorItem['Finish Date']) {
                    if (tempItem['to'] < tempItem['from']) {
                        errorItem['Date'] = "Date Start is earlier than Finish date";
                    }
                    if (hasAdvertisingLimit){
                        var limitDate = new Date(tempItem['from']);
                        limitDate.setMonth(limitDate.getMonth() + config['constants'].MONTH_DATE_RANGE_LIMITS['ADVERTISING']);
                        if(limitDate < tempItem['to']) errorItem['Date'] = " End date must be 3 months or less than the Start date";
                    }
                }

                var advBarCodes = item.splice(structure['FIRST_PRODUCT_BARCODE'], item.length);
                if(!advBarCodes.length){
                    errorItem['Advertising List'] = "Advertising List is empty";
                }

                advBarCodes.forEach(function(barCode, bIndex){
                    var error = "";

                    if(barCode.length < config.constants['BARCODE_MIN_LENGTH']) {
                        error = "Field 'Product Item BarCode' Length is less than " + config.constants['BARCODE_MIN_LENGTH'];
                    } else if(barCode.length > config.constants['TEXT_MAX_LENGTH']) {
                        error = "Field 'Product Item BarCode' Length is longer than " + config.constants['TEXT_MAX_LENGTH'];
                    } else if(!/^\d+$/.test(barCode)) {
                        error = "Field 'Product Item BarCode'  must consist only of digits";
                    }

                    if(error){
                        if(!errorItem['Advertising List']){
                            errorItem['Advertising List'] = {};
                        }
                        errorItem['Advertising List'][bIndex] = error;
                    }
                });

                reportStructure.push(errorItem);
                if(Object.keys(errorItem).length){
                    return null;
                }

                tempItem.advBarCodes = advBarCodes;
                var options = { where: { barCode: tempItem['barCode'] } };

                return models.items.find(options).then(function(existItem){

                    if(!existItem){
                        return "Field 'Bar Code' ("+ tempItem['barCode'] +") is not exist";
                    }

                    return Promise.mapSeries(advBarCodes, function(advBarCode){

                        var options = { where: { barCode: advBarCode } };
                        var from = tempItem['from'].getTime();
                        var to = tempItem['to'].getTime();

                        return models.items.find(options).then(function(existAdvItem) {

                            if (!existAdvItem){
                                return "Field 'Product Item BarCode' (" + advBarCode + ") is not exist";
                            }
                            var options = {
                                where: {
                                    item_id: existAdvItem.id,
                                    shop_id: shopId
                                }
                            };
                            return models.itemByShop.find(options).then(function(existProduct){
                                 if (!existProduct){
                                    return "Field 'Product Item BarCode' ("+ advBarCode +") is not exist in your shop";
                                 }

                                 var options = {
                                     where: {
                                         item_id     : existItem.id,
                                         product_id  : existProduct.id
                                     },
                                     defaults: {
                                         from        : from,
                                         to          : to,
                                         post_code_id: postCodeId
                                     }
                                 };

                                return models.advertising.findOrCreate(options)
                                   .spread(function(advertising, created){
                                       if(!created) {
                                           advertising.from = from;
                                           advertising.to = to;
                                           return advertising.save();
                                       }
                                       return advertising;
                                   }).then(function(advertising){
                                       var options = {
                                           admin_id       : user.id,
                                           advertising_id : advertising.id,
                                           from           : from,
                                           to             : to
                                       };
                                       logger.log('info', 'Sales activity', options );
                                       return models.advActivityByAdmin.create(options);
                                   }).then(function () {
                                      return "success";
                                   }).catch(next);
                            });
                        });
                    });
                });
            });
        }).then(function(mainResult){
            var reportBook = XLSX.utils.book_new();
            var report = [];
            var count = 0;
            var name = "Set targeted recommendations for your shop";

            mainResult.forEach(function (result, row) {
                var reportItem = reportStructure[row];
                if(result){
                    if(!Array.isArray(result)){
                        reportItem['Bar Code'] = result;
                    } else {
                        result.forEach(function (item, column) {
                            if(item == "success"){
                                count++;
                                return;
                            }
                            if(!reportItem['Advertising List']){
                                reportItem['Advertising List'] = {};
                            }
                            reportItem['Advertising List'][column] = item;
                        });
                    }
                }

                if(Object.keys(reportItem).length){
                    report.push(['Error at row: '+ (row + 2)]);
                    var advertisingList  = reportItem['Advertising List'];
                    for(var i in advertisingList){
                        report.push(['At column: ' + (parseInt(i) + length), advertisingList[i]]);
                    }

                    delete reportItem['Advertising List'];
                    for(var i in reportItem) report.push(['At column: ' + i, reportItem[i]]);
                }

            });

            report.unshift(["Successfully setted " + count + " advertising items"]);
            var reportSheet = XLSX.utils.aoa_to_sheet(report, { cellDates:true });
            XLSX.utils.book_append_sheet(reportBook, reportSheet, "Report");

            var options = {
                data  : reportBook,
                shopId: shopId,
                type  : config.constants['REPORT_TYPES'].BULK,
                name  : name
            };
            return services.report.saveReport(user, options);
        }).then(function(result){
            req.temp = { result: result };
            return unlink(req.file.path);  
        }).then(function(){
             res.status(200).send(req.temp.result);
        }).catch(next);
}

/**
 * @param req
 * @param res
 * @param next
 */

function itemsUpdate(req, res, next){
   var data = req.temp.data;
   var structure = config.constants['BULK_UPLOAD'].ITEMS;
   var length = Object.keys(structure).length;
   if(data[0].length < length){
     var error = new Error('Invalid file structure');
     error.status = 422;
     return next(error);
   }

    data.splice(0, 1);

    var user = req.user;
    var items = {};
    var categories = [];
    var unitTypes = [];
    var reportStructure = [];
    var updateNames = config.constants['BULK_UPDATE_OFFICIAL_FIELD_NAMES'];

    return Promise.mapSeries(data, function(row, index){
        var item = [];
        for(var i  = 0; i < length; i++){
            var field = (row[i] ? row[i].trim() : '');
            item.push(field);
        }

        var tempItem = {
            barCode         : item[structure['BARCODE']],
            name            : item[structure['NAME']],
            brand           : item[structure['BRAND']],
            unit_id         : item[structure['UNIT_TYPE']].toLowerCase(),
            itemSize        : item[structure['ITEM_SIZE']],
            servingSize     : item[structure['SERVING_SIZE']],
            serving_unit_id : item[structure['SERVING_TYPE']].toLowerCase(),
            servingPerPack  : item[structure['SERVING_PER_PACK']],
            path            : item[structure['IMAGE_URL']],
            categories      : item.splice(structure['FIRST_SUBCATEGORY'], item.length)
        }, errorItem = {};

         // Barcode
        if(tempItem['barCode'].length < config.constants['BARCODE_MIN_LENGTH']) {
            errorItem['BarCode'] = "Field 'BarCode' Length is less than " + config.constants['BARCODE_MIN_LENGTH'];
        } else if(tempItem['barCode'].length > config.constants['TEXT_MAX_LENGTH']) {
            errorItem['BarCode'] = "Field 'BarCode'Length is longer than " + config.constants['TEXT_MAX_LENGTH'];
        } else if(!/^\d+$/.test(tempItem['barCode'])){
            errorItem['BarCode'] = "Field 'BarCode' must consist only of digits";
        }

        // Name
        if(tempItem['name'].length > config.constants['TEXT_MAX_LENGTH']){
           errorItem['Name'] = "Field 'Name' Length is longer than " + config.constants['TEXT_MAX_LENGTH'];
        }
        if(!tempItem['name'] || errorItem['Name']){
            delete tempItem['name'];
        }

        // Brand
        if(tempItem['brand'].length > config.constants['TEXT_MAX_LENGTH']){
           errorItem['Brand'] = "Field 'Brand' Length is longer than " + config.constants['TEXT_MAX_LENGTH'];
        }
        if(!tempItem['brand'] || errorItem['Brand']){
            delete tempItem['brand'];
        }

        // Item Size
        if(tempItem['itemSize']) {
            tempItem['itemSize'] = parseFloat(tempItem['itemSize']);
            if(isNaN(tempItem['itemSize']) || tempItem['itemSize'] < 0){
                errorItem['Item Size'] = "Invalid 'Item size' field";
            }
        }
        if(!tempItem['itemSize'] || errorItem['Item Size']){
            delete tempItem['itemSize'];
        }

        // Serving Size
        if(tempItem['servingSize']) {
            tempItem['servingSize'] = parseInt(tempItem['servingSize']);
            if(isNaN(tempItem['servingSize']) || tempItem['servingSize'] < 0){
                errorItem['Serving Size'] = "Invalid 'Serving size' field";
            }
        }
        if(!tempItem['servingSize'] || errorItem['Serving Size']){
            delete tempItem['servingSize'];
        }

        //Serving Per Pack
        if(tempItem['servingPerPack']){
            tempItem['servingPerPack'] = parseInt(tempItem['servingPerPack']);
            if(isNaN(tempItem['servingPerPack']) || tempItem['servingPerPack'] < 0){
                errorItem['Serving Per Pack'] = "Invalid 'Serving Per Pack' field";
            }
        }
        if(!tempItem['servingPerPack'] || errorItem['Serving Per Pack']){
            delete tempItem['servingPerPack'];
        } 

        // Image Url
        if (tempItem['path'].length && tempItem['path'].indexOf("http") == -1){
            errorItem['Image'] = "Invalid 'image url' field";
        }
        if(!tempItem['path'] || errorItem['Image']){
            delete tempItem['path'];
        }  

        // Unit Type
        if(!tempItem['unit_id']){
            delete tempItem['unit_id'];
        }
        // Serving Unit Type
        if(!tempItem['serving_unit_id']){
            delete tempItem['serving_unit_id'];
        }

        // Categories
        if(!tempItem['categories'] || (tempItem['categories'].length === 1 & !tempItem['categories'][0].length)){
            delete tempItem['categories'];
        }

        reportStructure.push(errorItem);
        if(errorItem['BarCode']) return;
        var options = { where: { barCode: tempItem['barCode'] } };

        return models.items.find(options).then(function(item){
               if(!item){
                 errorItem['BarCode'] = "Field 'Bar Code' ("+ tempItem['barCode'] +") is not exist";
                 return;
               }

               items[tempItem['barCode']] = {
                  data: tempItem,  
                  dbItem: item,
                  index: index
               }

               if(tempItem['path']){
                  var options = {
                    url   : tempItem['path'],
                    itemId: item.id
                 };
                 Queue.create('uploadImageWorker', options).save();
               }

               if(tempItem['unit_id']) {
                 unitTypes.push(tempItem['unit_id']);
               }

               if(tempItem['serving_unit_id']) {
                 unitTypes.push(tempItem['serving_unit_id']);   
               }

               if(tempItem['categories']) {
                 return item.getCategories();
               }
            }).then(function(existCategories){
                if(!existCategories) return;
                tempItem['categories'] = tempItem['categories'].filter(function(category){
                    var existIndex = existCategories.findIndex(function(existCategory){
                        return existCategory.title === category;
                    });
                    return existIndex === -1;
                });
                categories.push.apply(categories,tempItem['categories']);
            });

    }).then(function(){
        var options = {
            units: { where: { type: { 
                $in: Array.from(new Set(unitTypes))
            } } },
            categories: { where: { title: { 
               $in: Array.from(new Set(categories)) 
            } } }
        };

        return Promise.all([
            models.units.all(options['units']),
            models.categories.all(options['categories'])
        ]);

    }).then(function(result){
        var dbUnits = result[0];
        var dbCategories = result[1]
        return Promise.mapSeries(Object.keys(items), function(key){
            var item = items[key];
            var data = item['data'];
            var dbItem = item['dbItem'];
            var reportItem = reportStructure[item['index']];

            // Unit Type
            if(data['unit_id']){
                var unitIndex = dbUnits.findIndex(function(unit, index){
                    return data['unit_id'] === unit['type'];
                });
                if(unitIndex === -1) reportItem['Unit Type'] = "Unit type: " + data['unit_id'] + " is not exist";
                else data['unit_id'] = dbUnits[unitIndex].id;
            }

            // Serving Unit Type
            if(data['serving_unit_id']){
                var servingUnitIndex = dbUnits.findIndex(function(unit, index){
                    return data['serving_unit_id'] === unit['type'];
                });
                if(servingUnitIndex === -1)   reportItem['Serving Type'] = "Serving unit type: " + data['serving_unit_id'] + " is not exist";
                else data['serving_unit_id'] = dbUnits[servingUnitIndex].id; 
            }

            // Categories
            var existCategories = data['categories'];
            var bulkCategories = [];

            if(existCategories) {
                existCategories.forEach(function(category){
                    var index = dbCategories.findIndex(function(dbCategory){
                        return dbCategory['title'] === category;
                    });

                    if(index === -1){
                        if(!reportItem['Categories']) reportItem['Categories'] = [];
                        reportItem['Categories'].push(category + ' is not exist from DB or is not subcategory');
                        return;
                    } 

                    bulkCategories.push({
                        item_id: dbItem.id,
                        category_id: dbCategories[index].id 
                    });
                });

                delete data['categories'];
            }

            if(Object.keys(reportItem).length){
                delete items[key];
                return;
            }

            Object.keys(data).forEach(function(key){ dbItem[key] = data[key]; });
            return dbItem.save().then(function(){
                if(bulkCategories.length){
                    return models.itemByCategory.bulkCreate(bulkCategories);
                }
            });
        });
    }).then(function(){
        var reportBook = XLSX.utils.book_new();
        var report = [];

        var name = 'Bulk update master DB items';
        
        var barCodes = Object.keys(items);
        report.push(['Successfully updated ' + barCodes.length + ' items']);

        barCodes.forEach(function(barCode){
           var item = items[barCode];
           report.push(['Was updated at row: ', item['index'] + 2]);
           for(var key in item['data']) report.push([updateNames[key]]);
        });

        reportStructure.map(function (result, index) {
            if(Object.keys(result).length){
                report.push(['Errors at row: ', index + 2]);
                if(result['Categories']){
                    report.push(['Categories']);
                    for(var i in result['Categories']){
                        report.push(['', result['Categories'][i] ]);
                    }
                    delete result['Categories'];
                }
                for(var key in result){
                    report.push([key, result[key]]);
                };
            }
        });

        var reportSheet = XLSX.utils.aoa_to_sheet(report, { cellDates:true });
        XLSX.utils.book_append_sheet(reportBook, reportSheet, "Report");

        var options = {
            data  : reportBook,
            type  : config.constants['REPORT_TYPES'].BULK,
            name  : name
        };
        return services.report.saveReport(user, options);
    }).then(function(result){
        req.temp = { result: result };
        return unlink(req.file.path);  
    }).then(function(){
        res.status(200).send(req.temp.result);
    }).catch(next);
}

/**
 * @param req
 * @param res
 * @param next
 */
function upload(req, res, next){

    var file = req.file;
    if (!file) {
        var error = new Error('File don`t upload!');
        error.status = 400;
        return next(error);
    }

    var readBook  = XLSX.readFile(file['path']);
    var readSheet = readBook.Sheets[Object.keys(readBook.Sheets)[0]];

    Object.keys(readSheet).forEach(function(row) {
        if(readSheet[row].w) readSheet[row].w = readSheet[row].v.toString();
    });

    var data = XLSX.utils.sheet_to_json(readSheet, { header: 1 });

    if(data.length < 2){
        var error = new Error('File is empty');
        error.status = 422;
        return next(error);
    }
    
    req.temp = {
        data: data.filter(function(row){
          return row.length;
        }) 
    };
    var type = req.params.type;

    switch(type){
        case 'itemsUpdate': itemsUpdate(req, res, next);
        break;
        case 'itemsUpload': itemsUpload(req, res, next);
        break;       
        case 'itemsToShop': itemsToShop(req, res, next);
        break;
        case 'sales': sales(req, res, next);
        break;
        case 'advertising': advertising(req, res, next);
        break;
        default: 
            var error = new Error('Invalid type!');
            error.status = 422;
            return next(error);
    }
}

module.exports = {
    upload: [ multer.single('bulk') , upload]
};