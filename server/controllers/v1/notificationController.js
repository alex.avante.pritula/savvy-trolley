var models = require('../../models');
var validators = require('../../validators');
var config = require('../../../config');
var Promise = require('bluebird');

/**
 * Subscribe device for push notification
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function update(req, res, next) {
    var user = req.user;
    if (!user) {
        var err = new Error();
        err.status = 401;
        return next(err);
    }

    var options = {
        user_id  : user.id,
        device_id: req.body.deviceId,
        token    : req.body.deviceToken,
        timezone : req.body.timeZone
    };
    
    models.devices.upsert(options)
        .then(function () {
            res.status(204).send();
        }).catch(next);
}

function notifications(req, res, next) {

    var limit  = parseInt(req.query.limit, 10)    || config.constants['LIMIT'];
    var step   = parseInt(req.query.page, 10) - 1 || config.constants['STEP'];
    var offset = parseInt(req.query.offset, 10)   || step * limit;
    var userId = req.user.id;

    var options = {
        where : {
           user_id: userId,
           is_sent: true
        },
        offset: offset,
        limit : limit,
        order : [['delivery_date_time', 'DESC']]
    };

    var pushTypes = config.constants['PUSH_TYPES'];

    models.deliveryNotifications.all(options).then(function(notifications){

        var options = {
            include: [{
                model: models.images,
                as: 'image'
            }]
        };

        return Promise.mapSeries(notifications, function(pushItem){
             var data = JSON.parse(pushItem['data']);
             return Promise.all([
                 ((data.itemId) ? models.items.findById(data.itemId, options) : null),
                 ((data.shopId) ? models.shops.findById(data.shopId, options) : null)
             ]).then(function(additionals){
                 var shop = additionals[1];
                 var item = additionals[0];

                 if (!shop && pushItem.push_type !== pushTypes['ITEM_ENABLED_MODE_CHANGED']){
                     return null;
                 }

                 if (!item && pushItem.push_type !== pushTypes['SHOP_ADDED']){
                     return null;
                 }

                 var notification  = {};

                 switch (pushItem.push_type){
                     case pushTypes['REGULAR_PRICE_REDUCED']:
                         notification = {
                            title   : 'Price reduced go and check it out!',
                            postCode: shop.postCode,
                            shopName: shop.name,
                            itemName: item.name,
                            imageUrl: ((item.image) ? item.image.url : null)
                         };
                         break;
                     case pushTypes['ITEM_ENABLED_MODE_CHANGED']:
                         notification = {
                             title   : 'Out of store',
                             itemName: item.name,
                             imageUrl: ((item.image) ? item.image.url : null)
                         };
                          break;
                     case pushTypes['CONCRETE_ITEM_ENABLED_MODE_CHANGED']:
                         notification = {
                             title   : 'Out of store',
                             postCode: shop.postCode,
                             shopName: shop.name,
                             itemName: item.name,
                             imageUrl: ((item.image) ? item.image.url : null)
                         };
                          break;
                     case pushTypes['ITEM_OUT_OF_STOCK']:
                         notification = {
                             title   : 'Out of store',
                             postCode: shop.postCode,
                             shopName: shop.name,
                             itemName: item.name,
                             imageUrl: ((item.image) ? item.image.url : null)
                         };
                          break;
                     case pushTypes['SHOP_ADDED']:
                         notification = {
                             title   : 'New shop was added!',
                             postCode: shop.postCode,
                             shopName: shop.name,
                             imageUrl: ((shop.image) ? shop.image.url : null)
                         };
                          break;
                     case pushTypes['SALE_START']:
                          notification = {
                              title   : 'Sales start today',
                              postCode: shop.postCode,
                              shopName: shop.name,
                              itemName: item.name,
                              imageUrl: ((item.image) ? item.image.url : null)
                          };
                          break;
                     case pushTypes['SALE_FINISH']:
                           notification = {
                               title   :  'Sales finishes today',
                               postCode:  shop.postCode,
                               shopName:  shop.name,
                               itemName:  item.name,
                               imageUrl:  ((item.image) ? item.image.url : null)
                           };
                           break;
                     default: return null;
                 }
                 notification['id'] = pushItem['id'];
                 notification['pushType'] = pushItem['push_type'];
                 notification['pushDate'] = pushItem['delivery_date_time'];
                 return Object.assign({}, data, notification);
             }).catch(next);
        });
    }).then(function(result){
        var pushes = result.filter(function (pushItem) {
            return (!!pushItem);
        });
        res.status(200).send(pushes);
    }).catch(next);
}

module.exports = {
    notifications: notifications,
    update: [validators.device.subscribeNotification, update]
};
