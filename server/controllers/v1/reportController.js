var services = require('../../services');
var config = require('../../../config');
var models = require('../../models');

/**
 * Remove report
 *
 * @param req
 * @param res
 * @param next
 */
function remove(req, res, next){
    var id = req.params.id;
    models.reports.findById(id)
        .then(function(report){
            if (!report){
                var error = new Error('Report is not exist');
                error.status = 404;
                throw error;
            }
            req.temp = {report: report};
            return services.upload.removeFromS3([report.url]);
        }).then(function(){
            return req.temp.report.destroy();
        }).then(function(){
            res.status(204).send();
        }).catch(next);
}


/**
 * Get reports by admin
 *
 * @param req
 * @param res
 * @param next
 */
function reports(req, res, next){
    var user = req.user;
    var step =  parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var offset = parseInt(req.query.offset, 10) || step * limit;
    var adminId = ((user.role === 'SAVVY_SHOP' || user.role === 'ENTERPRISE_SAVVY_SHOP') ? user.id : null ) ;

    var options = {
        sortBy: req.query.by || 'id',
        sortAt: req.query.at || 'ASC',
        query:  req.query.query || '',
        limit:  limit,
        offset: offset,
        adminId: adminId,
        from:   req.query.from ? new Date(req.query.from) : new Date(),
        to:     req.query.to ? new Date(req.query.to) : new Date(),
        email:  req.query.email || '',
        type:   config['constants'].REPORT_TYPES[req.query.type]
    };

    services.report.getReports(options).then(function (result) {
        res.status(200).send(result);
    }).catch(next);
}


module.exports = {
    reports: reports,
    remove: remove
};
