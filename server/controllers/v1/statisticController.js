var config = require('../../../config');
var services = require('../../services');
var exporter = require('../../utils/excelExportUtil');

/**
 * Get statistics
 *
 * @param req
 * @param res
 * @param next
 */
function index(req, res, next) {
    var type = req.params.type.toUpperCase();
    var postCodes = req.query.postcode ? req.query.postcode.split(',') : null;
    var from = parseInt(req.query.from, 10);
    var to = parseInt(req.query.to, 10);

    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;

    var options = {
        type     : type,
        limit    : limit,
        offset   : offset,
        postCodes: postCodes,
        from     : new Date(from),
        to       : new Date(to),
        sortedBy : req.query.by,
        sortedAt : req.query.at
    };

    var statistics = services.statistic.factory(options)({});
    var count = services.statistic.count(options);
    Promise
        .all([statistics, count])
        .then(function (results) {

            var response = {
                statistics: results[0],
                meta      : {totalCount: results[1]}
            };
            res.status(200).send(response);
        })
        .catch(next);
}

/**
 * Export statistic to xls.
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function exportToXLS(req, res, next) {
    var type = req.params.type.toUpperCase();
    var postCodes = req.query.postcode ? req.query.postcode.split(',') : null;
    var from = parseInt(req.query.from, 10);
    var to = parseInt(req.query.to, 10);
    var options = {
        type     : type,
        postCodes: postCodes,
        from     : new Date(from),
        to       : new Date(to),
        sortedBy : req.query.by,
        sortedAt : req.query.at
    };
    services
        .statistic
        .count(options)
        .then(function (count) {
            var limit = 100;
            var iterationCount = Math.ceil(count / limit);
            var sheetName = 'Statistics';
            var iterator = services.statistic.factory(options);
            var columns = services.statistic.buildExcelColumn(type);
            res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            res.setHeader("Content-Disposition", "attachment; filename=" + sheetName + ".xlsx");

            var exporterOption = {
                stream        : res,
                sheetName     : sheetName,
                columns       : columns,
                iterator      : iterator,
                iterationCount: iterationCount,
                limit         : limit
            };
            return exporter
                .exportToXLSX(exporterOption);
        })
        .then(function () {
            res.status(200).end();
        })
        .catch(next);
}

/**
 * Get post codes.
 *
 * @param req
 * @param res
 * @param next
 */
function getPostCodes(req, res, next) {
    var query = req.query.postcode || '';

    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;

    var options = {
        limit   : limit,
        offset  : offset,
        type    : req.query.type,
        postCode: query
    };

    services
        .postCode
        .getPostCodes(options)
        .then(function (response) {
            res.status(200).send(response);
        })
        .catch(next);
}

module.exports = {
    index       : index,
    getPostCodes: getPostCodes,
    exportToXLS : exportToXLS
};