var util = require('util');

var models = require('../../models');
var logger = require('../../utils/logger');
var validators = require('../../validators');

var config = require('../../../config');

var PASSWORD_EXCLUDE = {exclude: ['password']};

/**
 * Read last activity information
 *
 * @param req
 * @param res
 * @param next
 */
function readOne(req, res, next) {
    var entityType = req.query.entity_type || '';
    var entityId = typeof parseInt(req.query.entity_id, 10) === 'number'
        ? parseInt(req.query.entity_id, 10)
        : null;

    var where = {
        entity_type: models.lastEditLogs.getEntityType(entityType)
    };

    if (typeof entityId === 'number' && !isNaN(entityId)) {
        where.entity_id = entityId;
    } else {
        where.entity_id = {$ne: config.constants.SERVICE_ID}; // service - all entity with id equal zero
    }

    var options = {
        where  : where,
        include: [{model: models.admins, as: 'editor', attributes: PASSWORD_EXCLUDE}],
        limit  : 1,
        order  : [['createdAt', 'DESC']]
    };
    logger.log('info', util.format('lastEditApi#log', options));

    models.lastEditLogs.all(options)
        .then(function (logs) {
            var result = {logExists: false, log: null};
            if (logs && logs.length !== 0) {
                result.logExists = true;
                result.log = models.lastEditLogs.format(logs[0]);
            }
            res.status(200).send(result);
        })
        .catch(next);
}

/**
 * Add activity information
 *
 * @param req
 * @param res
 * @param next
 */
function log(req, res, next) {
    var entity_type = req.body.entityType || '';
    var entity_id = req.body.entityId;
    var admin_id = req.body.adminId;
    var payload = req.body.payload || null;
    var operation_type = req.body.operationType;
    entity_type = entity_type.toUpperCase();
    var data = {
        entity_type   : config.constants.ENTITY_TYPES[entity_type],
        entity_id     : entity_id,
        admin_id      : admin_id,
        operation_type: operation_type,
        payload       : payload
    };
    logger.log('info', util.format('lastEditApi#log', data));
    models
        .lastEditLogs
        .create(data)
        .then(function (log) {
            req.temp = {log: log.toJSON()};
            var options = {attributes: PASSWORD_EXCLUDE};
            return log.getEditor(options);
        })
        .then(function (editor) {
            var result = {logExists: false, log: null};
            var log = req.temp.log;
            log.editor = editor;
            if (log) {
                result.logExists = true;
                result.log = models.lastEditLogs.format(log);
            }
            res.status(200).send(result);
        })
        .catch(next);
}

module.exports = {
    read: readOne,
    log : [validators.lastEdit.log, log]
};