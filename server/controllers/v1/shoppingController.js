var models = require('../../models');
var logger = require('../../utils/logger');
var formatters = require('../../formatters');
var services = require('../../services');
var validators = require('../../validators');

/**
 * Read shopping list
 *
 * @param req
 * @param res
 * @param next
 */
function index(req, res, next) {
    var id = req.params.userId;

    var options = {
        include: [{model: models.postCodes, as: 'postCodes'}]
    };
    models.users.findById(id, options)
        .then(function (user) {
            if (!user) {
                var error = new Error();
                error.status = 401;
                throw error;
            }
            var postCodes = user.postCodes.length != 0 ? formatters.postCode.toStrings(user.postCodes) : null;

            var limit = parseInt(req.query.limit, 10) || 1000;
            var step = parseInt(req.query.page, 10) - 1 || 0;
            var offset = parseInt(req.query.offset, 10) || step * limit;

            var userData = {
                userId   : user.id,
                postCodes: postCodes
            };
            var options = {
                userId   : user.id,
                limit    : limit,
                offset   : offset,
                postCodes: postCodes
            };

            return services.shopping.read(userData, options)
        })
        .then(function (result) {
            res.status(200).json(result);
        })
        .catch(next);
}

/**
 * Update shopping list
 *
 * @param req
 * @param res
 * @param next
 */
function update(req, res, next) {
    var id = req.params.userId;

    var options = {
        include: [{model: models.postCodes, as: 'postCodes'}]
    };
    models.users.findById(id, options)
        .then(function (user) {
            if (!user) {
                var error = new Error();
                error.status = 401;
                throw error;
            }

            var addedItems = req.body.addedItems;
            var removedItems = req.body.removedItems;
            return services.shopping.update(user, addedItems, removedItems)
        })
        .then(function () {
            res.status(204).send();
        })
        .catch(next);
}

/**
 * Get shopping list
 *
 * @param req
 * @param res
 * @param next
 */
function read(req, res, next) {
    var user = req.user;
    if (!user) {
        var error = new Error();
        error.status = 401;
        throw error;
    }
    var postCodes = formatters.postCode.toStrings(user.postCodes);

    var limit = parseInt(req.query.limit, 10) || 1000;
    var step = parseInt(req.query.page, 10) - 1 || 0;
    var offset = parseInt(req.query.offset, 10) || step * limit;

    var userData = {
        userId   : user.id,
        postCodes: postCodes
    };
    var options = {
        userId   : user.id,
        limit    : limit,
        offset   : offset,
        postCodes: postCodes
    };

    services.shopping.read(userData, options)
        .then(function (result) {
            res.status(200).json(result);
        })
        .catch(next);
}

/**
 * Sync shopping list
 *
 * @param req
 * @param res
 * @param next
 */
function sync(req, res, next) {
    var user = req.user;
    if (!user) {
        var error = new Error();
        error.status = 401;
        throw error;
    }
    var addedItems = req.body.addedItems;
    var removedItems = req.body.removedItems;
    logger.log('info', 'add: ' + addedItems + ' remove: ' + removedItems);

    services
        .shopping
        .update(user, addedItems, removedItems)
        .then(function () {
            next();
            return null;
        })
        .catch(next);
}

/**
 * Restore item from history
 *
 * @param req
 * @param res
 * @param next
 */
function restore(req, res, next) {
    var user = req.user;
    if (!user) {
        var error = new Error();
        error.status = 401;
        throw error;
    }
    var items = req.body.addedItems;
    var data = {
        postCodes: formatters.postCode.toStrings(user.postCodes),
        items    : items
    };
    logger.log('info', 'restore: ' + data);
    services.shop.getOptimalShopForSingleItems(data)
        .then(function (items) {
            return services.shopping.update(user, items);
        })
        .then(function () {
            next();
            return null;
        })
        .catch(next);
}

module.exports = {
    read   : read,
    sync   : [validators.shopping.update, sync, read],
    restore: [validators.shopping.restore, restore, read],

    index : index,
    update: [validators.shopping.update, update]
};