var uuid = require('uuid');
var Promise = require('bluebird');

var Cache = require('../../utils/cache');
var models = require('../../models');
var services = require('../../services');
var sequelize = require('../../utils/sequelize');
var crypt = require('../../utils/crypt');
var validators = require('../../validators');
const logger = require('../../utils/logger');
const mailService = require('../../utils/mailService');
const util = require('util');

var config = require('../../../config');

var USER_MODEL = ['id', 'email', 'accountType', 'verified', 'hasAdverts', 'createdAt'];

var cache = new Cache(config.redis);

/**
 * read all users
 *
 * @param req
 * @param res
 * @param next
 */
function index(req, res, next) {
    var sortBy = req.query.by || 'cretedAt';
    var sortAt = req.query.at || 'DESC';
    var query = req.query.query || '';
    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;

    var where = { email: { $like: '%' + query + '%' }};

    var options = {
        where     : where,
        offset    : offset,
        limit     : limit,
        attributes: USER_MODEL,
        order     : [[sortBy, sortAt]]
    };
    models.users.all(options)
        .then(function (users) {
            req.temp = { users: users };
            return models.users.count({where: where});
        })
        .then(function (totalCount) {
            var response = {
                users: req.temp.users,
                meta : {
                    totalCount: totalCount
                }

            };
            res.status(200).send(response)
        })
        .catch(next);
}

/**
 * read user
 *
 * @param req
 * @param res
 * @param next
 */
function me(req, res, next) {
    var user = req.user;
    if (!user) {
        var error = new Error('User not found!');
        error.status = 404;
        return next(error);
    }

    models.settings.findById(config.constants.SETTING_ID)
        .then(function (setting) {
            var data = {
                email           : user.email,
                postCodes       : user.postCodes.map(function (postCode) {
                    return {code: postCode.code};
                }),
                accountType     : user.accountType,
                hasAdverts      : setting.hasAdverts,
                hasNotifications: user.hasNotifications
            };

            res.status(200).send(data);
        }).catch(next);
}

/**
 * Change password
 *
 * @param req
 * @param res
 * @param next
 */
function changePassword(req, res, next) {
    var user = req.user;
    if (!user) {
        var error = new Error('User not found!');
        error.status = 404;
        throw error;
    }
    if (user.accountType != 'email') {
        var error = new Error('This account type not support password changing');
        error.status = 403;
        throw error;
    }

    var oldPassword = crypt.cryptPassword(req.body.oldPassword);

    if (user.password !== oldPassword) {
        var error = new Error('Old password incorrect');
        error.status = 400;
        throw error;
    }

    user.password = crypt.cryptPassword(req.body.newPassword);

    user.save().then(function () {
            var hash = 'USER_' + user.id;
            cache.remove(hash, function () {});
            res.status(204).send({});
        })
        .catch(next);
}

/**
 * Change my postcodes
 * @param req
 * @param res
 * @param next
 */
function changeMyPostCodes(req, res, next) {
    var codes = req.body.postCodes;
    var user = req.user;
    services.user.changePostCodes(user, codes)
        .then(function () {
            res.status(204).send();
        })
        .catch(next);
}

/**
 * Change postcodes
 *
 * @param req
 * @param res
 * @param next
 */
function changePostCodes(req, res, next) {
    var id = req.params.userId;
    var codes = req.body.postCodes;
    models.users.findById(id)
        .then(function (user) {
            return services.user.changePostCodes(user, codes);
        })
        .then(function () {
            res.status(204).send();
        })
        .catch(next);
}

/**
 * Read user profile
 *
 * @param req
 * @param res
 * @param next
 */
function read(req, res, next) {
    var id = req.params.userId;
    var options = {
        include: [{model: models.postCodes, as: 'postCodes', attributes: ['code'], through: {attributes: []}}]
    };
    var tasks = [models.users.findById(id, options), models.settings.findById(config.constants.SETTING_ID)];
    Promise.all(tasks)
        .then(function (data) {
            if (!data[0]) {
                var error = new Error('Not found!');
                error.status = 404;
                throw error;
            }
            var result = {
                id         : data[0].id,
                email      : data[0].email,
                accountType: data[0].accountType,
                postCodes  : data[0].postCodes.map(function (postCode) {
                    return {code: postCode.code};
                }),
                verified   : data[0].verified,
                createdAt  : data[0].createdAt,
                hasAdverts : data[1].hasAdverts
            };
            res.send(result);
        })
        .catch(next)
}

/**
 * Enable/disable google adwords for concrete user.
 * Not used
 *
 * @param req
 * @param res
 * @param next
 */
function changeAdvert(req, res, next) {
    var userId = req.params.userId;
    models.users.findById(userId)
        .then(function (user) {
            if (!user) {
                var error = new Error('User not found!');
                error.status = 404;
                throw error;
            }
            user.hasAdverts = req.body.hasAdverts;
            return user.save();
        })
        .then(function (user) {
            res.status(200).send(user);
        })
        .catch(next);
}

/**
 * Enable/disable push notification for concrete user.
 *
 * @param req
 * @param res
 * @param next
 */
function changeNotificationMode(req, res, next) {
    var user = req.user;
    var hasNotification = req.body.hasNotification;
    console.log(req.body.hasNotification);
    services.user.chengeNotificationMode(user, hasNotification)
        .then(function () {
            res.status(204).send();
        })
        .catch(next);
}

/**
 * Remove user
 *
 * @param req
 * @param res
 * @param next
 */
function remove(req, res, next) {
    var userId = parseInt(req.params.userId);
    models.users.findById(userId)
        .then(function (user) {
            if (!user) {
                var error = new Error('User not found!');
                error.status = 404;
                throw error;
            }
            return user.destroy();
        })
        .then(function () {
            res.status(200).send({ id: userId });
        })
        .catch(next);
}

/**
 * Change credentials of user
 *
 * @param req
 * @param res
 * @param next
 */
function changeCredentials(req, res, next) {
  let userId = req.params.userId;
  let email = req.body.email;
  let password = crypt.cryptPassword(req.body.password);
  logger.log('info', util.format('Change credentials with mail = %s, password = %s', email, password));

  let verified = 0;

  let data = {
    email   : email,
    password: password,
    verified: verified
  };

  let options = {
    where: {
      id: userId
    }
  };

  models.users.update(data, options)
    .then(function (user) {
      logger.log('info', util.format('user updated', user));
      let data = {
        code   : uuid.v1(),
        user_id: userId
      };
      return models.confirmCodes.create(data)
    })
    .then(function (code) {
      logger.log('info', util.format('confirmCode', code.code));
      return mailService.verifyMessage(code.code, data.email);
    })
    .then(function (body) {
      logger.log('info', body);
      res.status(204).send();
      return null;
    })
    .catch(function (err) {
      next(services.user.errorFormatter(err, config.constants.ERROR_MESSAGES.EXIST_EMAIL_IF_USE_LOCAL_SIGN_IN));
    });
}

module.exports = {
    index: index,

    me                    : me,
    changePassword        : [validators.user.password, changePassword],
    changeMyPostCodes     : [validators.user.postcode, changeMyPostCodes],
    changeNotificationMode: changeNotificationMode,

    read           : read,
    changePostCodes: [validators.user.postcode, changePostCodes],
    changeAdvert   : [validators.user.adverts, changeAdvert],
    remove         : remove,
    changeCredentials: [validators.user.changeCredentials, changeCredentials]
};
