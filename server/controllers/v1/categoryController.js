var models = require('../../models');
var logger = require('../../utils/logger');
var validators = require('../../validators');
var config = require('../../../config');

/**
 * read all root categories
 *
 * @param req
 * @param res
 * @param next
 */
function index(req, res, next) {
    var query = req.query.query || '';

    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;

    var where = {title: {$like: '%' + query + '%'}, parent_id: null};

    var options = {
        where     : where,
        offset    : offset,
        limit     : limit,
        attributes: {exclude: ['parent_id', 'image_id']},
        include   : [{model: models.images, as: 'image'}]
    };

    models.categories.all(options)
        .then(function (categories) {
            req.temp = {
                categories: categories.map(function (category) {
                    category = category.toJSON();
                    category.image = category.image ? category.image.url : null;
                    return category;
                })
            };
            return models.categories.count({where: where});
        })
        .then(function (totalCount) {
            var result = {
                categories: req.temp.categories,
                meta      : {
                    totalCount: totalCount
                }
            };
            res.status(200).send(result);
        })
        .catch(next);
}

/**
 * read category
 *
 * @param req
 * @param res
 * @param next
 */
function read(req, res, next) {
    var id = req.params.categoryId;
    if (!id) {
        var error = new Error('Bad request');
        error.status = 400;
        return next(error);
    }
    var options = {
        attributes: {exclude: ['image_id']},
        include   : [{model: models.images, as: 'image'}]
    };
    models.categories.findById(id, options)
        .then(function (category) {
            if (!category) {
                var error = new Error('Not found');
                error.status = 404;
                throw error;
            }
            category = category.toJSON();
            category.image = category.image ? category.image.url : null;
            res.status(200).send(category);
        })
        .catch(next);
}

/**
 * create category
 *
 * @param req
 * @param res
 * @param next
 */
function create(req, res, next) {
    var data = {
        title    : req.body.title,
        parent_id: req.body.parentId || null,
        image_id : req.body.imageId || null
    };
    logger.log('info', 'create', data);
    models.categories.create(data)
        .then(function (category) {
            req.temp = {category: category};
            return category.getImage();
        })
        .then(function (image) {
            var category = req.temp.category.toJSON();
            category.image = image ? image.url : null;
            delete category.image_id;
            res.status(200).send(category);
        })
        .catch(next);
}

/**
 * update category
 *
 * @param req
 * @param res
 * @param next
 */
function update(req, res, next) {
    var id = req.params.categoryId;
    if (!id) {
        var error = new Error('Bad request');
        error.status = 400;
        return next(error);
    }

    models.categories.findById(id)
        .then(function (category) {
            if (!category) {
                var error = new Error('Category not found!');
                error.status = 404;
                throw error;
            }
            category.title = req.body.title;
            category.image_id = req.body.imageId;
            return category.save();
        })
        .then(function (category) {
            req.temp = {category: category};
            return category.getImage();
        })
        .then(function (image) {
            var category = req.temp.category.toJSON();
            category.image = image ? image.url : null;
            res.status(200).send(category);
        })
        .catch(next);
}

/**
 * delete category
 *
 * @param req
 * @param res
 * @param next
 */
function destroy(req, res, next) {
    var id = req.params.categoryId;
    if (!id) {
        var error = new Error('Bad request');
        error.status = 400;
        return next(error);
    }

    models.categories.destroy({where: {id: id}})
        .then(function () {
            res.status(204).send();
        })
        .catch(next);
}

/**
 * read subcategories for category
 *
 * @param req
 * @param res
 * @param next
 */
function readSubcategories(req, res, next) {
    var parent = req.params.parentId;
    if (!parent) {
        var error = new Error('Bad request');
        error.status = 400;
        return next(error);
    }
    logger.log('info', 'readSubcategories', parent);

    var query = req.query.query || '';

    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;

    var where = {title: {$like: '%' + query + '%'}, parent_id: parent};

    var options = {
        where     : where,
        offset    : offset,
        limit     : limit,
        attributes: {exclude: ['parent_id', 'image_id']},
        include   : [{model: models.images, as: 'image'}]
    };

    logger.log('info', 'readSubcategories', query);
    models.categories.findById(parent)
        .then(function (category) {
            logger.log('info', 'readSubcategories', category);
            if (!category) {
                var error = new Error('Not found');
                error.status = 404;
                throw error;
            }
            return models.categories.all(options)
        })
        .then(function (categories) {
            req.temp = {
                categories: categories.map(function (category) {
                    category = category.toJSON();
                    category.image = category.image ? category.image.url : null;
                    return category;
                })
            };
            return models.categories.count({where: where});
        })
        .then(function (totalCount) {
            var result = {
                categories: req.temp.categories,
                meta      : {
                    totalCount: totalCount
                }
            };
            res.status(200).send(result);
        })
        .catch(next);

}

module.exports = {
    index        : index,
    read         : read,
    create       : [validators.category.create, create],
    update       : [validators.category.update, update],
    destroy      : destroy,
    subcategories: readSubcategories
};