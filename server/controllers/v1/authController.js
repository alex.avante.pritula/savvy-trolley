var uuid = require('uuid');
var util = require('util');
var async = require('async');
var Promise = require('bluebird');

var Chance = require('chance');

var Cache = require('../../utils/cache');
var models = require('../../models');
var services = require('../../services');
var crypt = require('../../utils/crypt');
var logger = require('../../utils/logger');
var validators = require('../../validators');
var mailService = require('../../utils/mailService');

var config = require('../../../config');

var chance = new Chance();
var cache = new Cache(config.redis);

/**
 * Create user profile.
 *
 * @param req
 * @param res
 * @param next
 */
function signUp(req, res, next) {
    var email = req.body.email;
    var password = crypt.cryptPassword(req.body.password);
    logger.log('info', util.format('Sign up with mail = %s, password = %s', email, password));

    var data = {
        email   : email,
        password: password
    };

    models.users.create(data)
        .then(function (user) {
            logger.log('info', util.format('user saved', user));
            var data = {
                code   : uuid.v1(),
                user_id: user.id
            };
            return models.confirmCodes.create(data)
        })
        .then(function (code) {
            logger.log('info', util.format('confirmCode', code.code));
            return mailService.verifyMessage(code.code, data.email);
        })
        .then(function (body) {
            logger.log('info', body);
            res.status(204).send();
            return null;
        })
        .catch(function (err) {
            next(services.user.errorFormatter(err, config.constants.ERROR_MESSAGES.EXIST_EMAIL_IF_USE_LOCAL_SIGN_IN));
        });
}

/**
 * Verified user after registration
 *
 * @param req
 * @param res
 * @param next
 */
function verifiedUser(req, res, next) {
    var code = req.params.code;
    models.confirmCodes
        .findOne({where: {code: code, isActive: false}})
        .then(function (code) {
            if (!code) {
                var error = new Error('Code not found!');
                error.status = 404;
                throw error;
            }
            code.isActive = true;
            return code.save();
        })
        .then(function (code) {
            return code.getUser();
        })
        .then(function (user) {
            user.verified = true;
            return user.save();
        })
        .then(function () {
            res.redirect('/success');
            res.end();
        })
        .catch(function (err) {
            res.redirect('/error');
            res.end();
            logger.log('error', 'verifiedUser ' + JSON.stringify(err));
        });
}

/**
 * Local sign in api
 * Return user profile and credentials
 *
 * @param req
 * @param res
 * @param next
 */
function localSignIn(req, res, next) {
    var email = req.body.email;
    var password = crypt.cryptPassword(req.body.password);

    logger.log('info', util.format('Sign in with mail = %s, password = %s', email, password));
    models.users.findOne({
        where  : {email: email, accountType: 'email'},
        include: [{model: models.postCodes, as: 'postCodes'}]
    }).then(function (user) {
            logger.log('info', JSON.stringify(user));
            if (!user) {
                logger.log('info', util.format('user not found', user));
                var err = new Error(config.constants.ERROR_MESSAGES.NOT_REGISTERED);
                err.status = 404;
                throw err;
            }

            if (!user.verified) {
                logger.log('info', 'not Verify');
                return services.user.reConfirm(user);
            }

            if (user.password != password) {
                var err = new Error(config.constants.ERROR_MESSAGES.INCORRECT_PASSWORD);
                err.status = 403;
                throw err;
            }

            req.temp = {user: user};
            var tasks = [services.user.createTokens(user), models.settings.findById(config.constants.SETTING_ID)];
            return Promise.all(tasks);
        })
        .then(function (data) {
            var user = req.temp.user;
            var result = {
                email           : user.email,
                postCodes       : user.postCodes ? user.postCodes.map(function (postCode) {
                    return {code: postCode.code};
                }) : [],
                tokens          : data[0],
                hasAdverts      : data[1].hasAdverts,
                hasNotifications: user.hasNotifications,
                accountType     : user.accountType
            };
            res.status(200).json(result);
        })
        .catch(next);
}

/**
 * Sign in from social network.
 * Return profile and credentials.
 *
 * @param req
 * @param res
 * @param next
 */
function socialSignIn(req, res, next) {
    var user = req.user;
    if (!user) {
        var err = new Error();
        err.status = 401;
        throw err;
    }
    logger.log('info', 'social', user);
    var tasks = [services.user.createTokens(user), models.settings.findById(config.constants.SETTING_ID)];
    Promise.all(tasks)
        .then(function (data) {
            var result = {
                email           : user.email,
                postCodes       : user.postCodes ? user.postCodes.map(function (postCode) {
                    return {code: postCode.code};
                }) : [],
                tokens          : data[0],
                hasAdverts      : data[1].hasAdverts,
                hasNotifications: user.hasNotifications,
                accountType     : user.accountType
            };
            res.status(200).json(result);
            return null;
        })
        .catch(next);
}

/**
 * Refresh credentials.
 *
 * @param req
 * @param res
 * @param next
 */
function refresh(req, res, next) {
    var access_token = req.body.accessToken;
    var refresh_token = req.body.refreshToken;
    logger.log('info', 'refresh', 'start', 'access_token', access_token, 'refresh_token', refresh_token);

    services.user.refresh(
        access_token,
        refresh_token,
        function (err, result) {
            logger.log('info', 'refresh', 'finish', result);
            if (err) return next(err);
            res.status(200).json(result);
        });
}

/**
 * Remove credentials
 *
 * @param req
 * @param res
 * @param next
 */
function logout(req, res, next) {
    var refreshToken = req.body.refreshToken;
    var deviceId = req.body.deviceId;
    
    logger.log('info', util.format('logout', 'start', 'refresh_token', refreshToken));

    crypt.encryptToken(refreshToken, true)
        .then(function (refresh) {
            logger.log('info', util.format('logout', 'encryptToken', 'refresh', refresh));
            var hash = 'USER_' + refresh.id;
            cache.remove(hash, function () {
            });
            var query = {
                where: {
                    token  : refresh.token,
                    user_id: refresh.id
                }
            };
            return models.tokens.find(query);
        })
        .then(function (token) {
            logger.log('info', util.format('logout', 'tokens', 'find', token));
            token = token.destroy();
            var device = models.devices.destroy({where: {device_id: deviceId}});

            return Promise.all([token, device]);
        })
        .then(function () {
            logger.log('info', util.format('logout', 'delete all', arguments));
            res.status(204).send();
        })
        .catch(next);
}

/**
 * Recovery password (if exist).
 *
 * @param req
 * @param res
 * @param next
 */
function recovery(req, res, next) {
    var body = req.body;
    var options = {
        where: {email: body.email}
    };
    models.users.find(options)
        .then(function (user) {
            if (!user) {
                var error = new Error('Invalid email');
                error.status = 404;
                throw error;
            }
            if (user.accountType !== 'email') {
                error = new Error('Invalid account type');
                error.status = 404;
                throw error;
            }

            var newPassword = chance.string({
                length: config.constants.RECOVERY_PASSWORD_LENGTH,
                pool  : config.constants.RECOVERY_PASSWORD_ALPHABET
            });

            var data = {
                code   : uuid.v1(),
                user_id: user.id,
                payload: newPassword
            };
            return models.confirmCodes.create(data)
        })
        .then(function (code) {
            logger.log('info', util.format('confirmCode', code.toJSON()));
            return mailService.recoveryMessage(code.code, body.email, code.payload);
        })
        .then(function (body) {
            logger.log('info', body);
            res.status(204).send();
            return null;
        })
        .catch(next);
}

/**
 * Approve password recovery
 *
 * @param req
 * @param res
 * @param next
 */
function approve(req, res, next) {
    var code = req.query.code;
    models.confirmCodes
        .find({where: {code: code, isActive: false}})
        .then(function (code) {
            if (!code) {
                var error = new Error('Code not found!');
                error.status = 404;
                throw error;
            }
            code.isActive = true;
            req.temp = { password: code.payload };
            return code.save()
        })
        .then(function (code) {
            return code.getUser();
        })
        .then(function (user) {
            user.password = crypt.cryptPassword(req.temp.password);
            return user.save();
        })
        .then(function () {
            res.redirect('/success');
            res.end();
        })
        .catch(function (err) {
            res.redirect('/error');
            res.end();
            logger.log('error', 'approve  ' + JSON.stringify(err));
        });
}

module.exports = {
    signUp: [validators.auth.local, signUp],
    verify: verifiedUser,
    signIn: [validators.auth.local, localSignIn],
    social: socialSignIn,

    refresh: [validators.auth.refresh, refresh],
    logout : [validators.auth.logout, logout],

    recovery: [validators.auth.recovery, recovery],
    approve : approve
};
