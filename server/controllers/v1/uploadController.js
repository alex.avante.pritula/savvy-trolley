var services = require('../../services');
var logger = require('../../utils/logger');
var multer = require('../../utils/multer');

/**
 * Upload 9images to Amazon S3
 * 
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */

function uploadImage(req, res, next) {
    if (!req.files) {
        var error = new Error('File don`t upload!');
        error.satatus = 422;
        return next(error);
    }
    var files = req.files;
    services.upload.uploadsTemp(files)
        .then(function (s3Files) {
            logger.log('info', 'Files uploaded:', s3Files);

            return services.upload.saveImageUrls(s3Files);
        })
        .then(function (images) {
            var result = {
                images: images.map(function (image) {
                    return {
                        id: image.id,
                        url: image.url
                    };
                })
            };
            res.send(result);
            return services.upload.removeTemp(files);
        })
        .then(function () {
            logger.log('info', 'Drop temp files', arguments);
            res.end();
            return false;
        })
        .catch(next);
}

module.exports = {
    uploadImage: [multer.array('images'), uploadImage]
};