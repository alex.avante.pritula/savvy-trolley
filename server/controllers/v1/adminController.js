var Promise = require('bluebird');

var models = require('../../models');

var crypt = require('../../utils/crypt');
var services = require('../../services');
var formatters = require('../../formatters');
var validators = require('../../validators');
var sequelize = require('../../utils/sequelize');

var config = require('../../../config');

/**
 * Get admins
 *
 * @param req
 * @param res
 * @param next
 */
function index(req, res, next) {
    var roles = req.permission.access;
    var sortBy = req.query.by || 'createdAt';
    var sortAt = req.query.at || 'DESC';
    var filtrationRole = req.query.role ? req.query.role.toUpperCase() : false;
    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;

    var email = req.query.email || '';
    if (filtrationRole) {
        roles = roles.filter(function (accessRole) {
            return accessRole == filtrationRole;
        });
    }
    var where = {
        role : {$in: roles},
        email: {$like: '%' + email + '%'}
    };

    var options = {
        where     : where,
        limit     : limit,
        offset    : offset,
        attributes: {exclude: 'password'},
        order     : [[sortBy, sortAt]]
    };
    models.admins
        .findAndCount(options)
        .then(function (result) {
            var response = {
                admins: result.rows,
                meta  : {
                    totalCount: result.count
                }
            };
            res.status(200).send(response);
        })
        .catch(next);
}

/**
 * Get only free (does not have shops) admins
 *
 * @param req
 * @param res
 * @param next
 */
function getFreeAdmin(req, res, next) {
    var roles = req.permission.access || ['SAVVY_SHOP'];

    var filtrationRole = req.query.role || 'SAVVY_SHOP';
    var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
    var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
    var offset = parseInt(req.query.offset, 10) || step * limit;

    var email = req.query.email || '';
    if (filtrationRole) {
        roles = roles.filter(function (accessRole) {
            return accessRole == filtrationRole;
        });
    }

    var options = {
        email : '%' + email + '%',
        roles : roles,
        limit : limit,
        offset: offset
    };
    services
        .admin
        .getFreeAdmin(options)
        .then(function (admins) {
            var response = {
                admins: admins
            };
            res.status(200).send(response);
        })
        .catch(next);
}

/**
 * Create new admin
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function create(req, res, next) {
    var access = req.permission.access;
    if (access.indexOf(req.body.role) == -1) {
        var error = new Error();
        error.status = 403;
        return next(error);
    }

    var role = config.constants.ROLES[req.body.role];
    if (!role) {
        error = new Error('Invalid role!');
        error.status = 400;
        return next(error);
    }

    sequelize.transaction(function (transaction) {
        var data = {
            email      : req.body.email,
            password   : req.body.password,
            role       : req.body.role,
            type       : role.type,
            transaction: transaction
        };
        return services
            .admin
            .createAdmin(data)
            .then(function (admin) {
                res.status(200).send(admin);
            });
    }).catch(next);


}

/**
 * Ger admin for current session
 *
 * @param req
 * @param res
 * @param next
 */
function current(req, res, next) {
    var admin = formatters
        .admin
        .adminFull(req.user);
    res.status(200).send(admin);
}

/**
 * Read admin info
 *
 * @param req
 * @param res
 * @param next
 */
function read(req, res, next) {
    var id = req.params.adminId;
    var options = {
        attributes: {exclude: ['password', 'updatedAt']},
        include   : [{
            model     : models.permissions,
            as        : 'permissions',
            attributes: ['permission'],
            through   : {
                attributes: []
            }
        }]
    };
    models
        .admins
        .findById(id, options)
        .then(function (admin) {
            admin = formatters
                .admin
                .adminFull(admin);
            res.status(200).send(admin);
        })
        .catch(next);
}

/**
 * Change password for admin
 *
 * @param req
 * @param res
 * @param next
 */
function changePassword(req, res, next) {
    var adminId = req.params.adminId || req.user.id;

    services.admin.getAdmin(adminId)
        .then(function (admin) {
            if (!admin) {
                var error = new Error('Admin not found!');
                error.status = 404;
                throw error;
            }
            var oldPassword = crypt.cryptPassword(req.body.oldPassword);
            if (admin.password !== oldPassword) {
                error = new Error('Old password incorrect.');
                error.status = 400;
                throw error;
            }

            admin.password = crypt.cryptPassword(req.body.newPassword);
            return admin.save();
        })
        .then(function () {
            res.status(204).send({});
        })
        .catch(next);
}

/**
 * Change role(permissions) for admin
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */

function updateAdmin(req, res, next) {
    var access = req.permission.access;
    if (access.indexOf(req.body.role) == -1) {
        var error = new Error();
        error.status = 403;
        return next(error);
    }

    var role = config.constants.ROLES[req.body.role];
    if (!role) {
        error = new Error('Invalid role!');
        error.status = 400;
        return next(error);
    }

    sequelize.transaction(function (transaction) {
        var options = {
            adminId: req.params.adminId,
            transaction: transaction,
            role: req.body.role
        };

        if(req.body.password && req.user.role == 'SUPER_ADMIN'){
            options['password'] = crypt.cryptPassword(req.body.password);
        }

        return services.admin.updateAdmin(options)
            .then(function (admin) {
                res.status(200).send(admin);
            });
    }).catch(next);
}

/**
 * Remove admin
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function remove(req, res, next) {
    var adminId = req.params.adminId || 0;
    return models
        .admins
        .findById(adminId)
        .then(function (admin) {
            if (!admin) {
                var error = new Error('Admin not found!');
                error.status = 404;
                throw error;
            }

            var access = req.permission.access;
            if (access.indexOf(admin.role) == -1) {
                error = new Error();
                error.status = 403;
                throw error;
            }

            return admin.destroy();
        })
        .then(function () {
            res.status(204).send()
        })
        .catch(next);
}

/**
 * Update admin enabled mode
 *
 * @param req
 * @param res
 * @param next
 */
function enable(req, res, next) {
    var adminId = req.params.adminId;

    models
        .admins
        .findById(adminId)
        .then(function (admin) {
            if (!admin) {
                var error = new Error('Admin not found!');
                error.status = 404;
                throw error;
            }
            var access = req.permission.access;
            if (access.indexOf(admin.role) == -1) {
                error = new Error();
                error.status = 403;
                throw error;
            }

            admin.enabled = req.body.enabled;
            return admin.save();
        })
        .then(function () {
            res.status(204).send({});
        })
        .catch(next);
}

/**
 * Get shop owners
 *
 * @param req
 * @param res
 * @param next
 */
function owners(req, res, next) {
    var shopId = req.params.shopId;
    var options = {attributes: {exclude: ['password']}};

    models.shops.findById(shopId)
        .then(function (shop) {
            if (!shop) {
                var error = new Error('Shop not found!');
                error.status = 404;
                throw error;
            }
            return Promise.all([shop.getOwners(options), shop.countOwners()]);
        })
        .then(function (data) {
            var result = {
                owners: data[0],
                meta  : {
                    totalCount: data[1]
                }
            };
            res.send(result);
        })
        .catch(next);
}

/**
 * Assign admin to shop
 * @param req
 * @param res
 * @param next
 */
function assign(req, res, next) {
    var adminId = req.params.adminId;
    var shopId = req.params.shopId;
    models
        .admins
        .findById(adminId)
        .then(function (admin) {
            if (!admin) {
                var error = new Error('Admin not found!');
                error.sataus = 404;
                throw error;
            }

            return admin.addShops(shopId);
        })
        .then(function () {
            res.status(204).send();
        })
        .catch(next);
}

/**
 * Unassign admin from shop
 *
 * @param req
 * @param res
 * @param next
 */
function unassign(req, res, next) {
    var adminId = req.params.adminId;
    var shopId = req.params.shopId;
    models
        .admins
        .findById(adminId)
        .then(function (admin) {
            if (!admin) {
                var error = new Error('Admin not found!');
                error.sataus = 404;
                throw error;
            }

            return admin.removeShops(shopId);
        })
        .then(function () {
            res.status(204).send();
        })
        .catch(next);
}

/**
 * Check admin permissions before assign to shop. middleware
 *
 * @param req
 * @param res
 * @param next
 */
function checkPermissionForAssignShop(req, res, next) {
    var adminId = req.params.adminId;
    services
        .admin
        .getAdmin(adminId, req.user)
        .then(function (admin) {
            if (!admin) {
                var error = new Error('Admin not found!');
                error.sataus = 404;
                throw error;
            }
            req.temp = {admin: admin};

            var notHasShop = admin.permissions.find(function (permission) {
                return permission.permission === 'NOT_HAS_SHOP';
            });
            if (notHasShop) {
                error = new Error('this user can not has shops');
                error.status = 417;
                throw error;
            }

            var hasOne = admin.permissions.find(function (permission) {
                return permission.permission === 'HAS_ONE_SHOP';
            });
            if (hasOne) {
                return admin.countShops();
            }

            return 0;
        })
        .then(function (count) {
            if (count !== 0) {
                var error = new Error('This user already has the shop');
                error.status = 417;
                throw error;
            }

            next();
        })
        .catch(next);
}

/**
 * Build restriction middleware.
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function checkRestriction(req, res, next) {
    var permissions = req.permissions;
    req.permission = {};
    if (!permissions) {
        var error = new Error();
        error.status = 403;
        return next(error);
    }
    if (permissions['ONLY_ENTERPRISE_SAVVY_SHOP_ADMIN']) {
        req.permission.access = ['ENTERPRISE_SAVVY_SHOP'];
        return next();
    }

    if (permissions['EXCEPT_SUPER_ADMIN']) {
        req.permission.access = Object.keys(config.constants.ROLES).filter(function (role) {
            return role !== 'SUPER_ADMIN';
        });
        return next();
    }


    req.permission.access = Object.keys(config.constants.ROLES);
    return next();
}

module.exports = {
    index         : [checkRestriction, index],
    create        : [validators.admin.createMiddleware, checkRestriction, create],
    read          : read,
    current       : current,
    updateAdmin   : [validators.admin.roleMiddleware, checkRestriction, updateAdmin],
    changePassword: [validators.admin.password, changePassword],
    enable        : [validators.admin.enable, checkRestriction, enable],
    remove        : [checkRestriction, remove],

    owners  : owners,
    assign  : [checkPermissionForAssignShop, assign],
    unassign: unassign,

    freeAdmin: [checkRestriction, getFreeAdmin]
};
