var models = require('../../models');
var services = require('../../services');
var formatters = require('../../formatters');
var validators = require('../../validators');
var config = require('../../../config');

/**
 * Create advertising (recommendations) for item
 *
 * @param req
 * @param res
 * @param next
 */
function create(req, res, next) {

	var from = parseInt(req.body.from) || false;
	var to = parseInt(req.body.to) || false;

	var options = {
		itemId: req.params.itemId,
		postCode: req.body.postCode,
		productId: req.body.productId,
		from: from,
		to: to
	};
	var user = req.user;

	req.temp = {};

	services.advertising.addAdvertising(options)
		.then(function (advertising) {
            req['temp'].advertising = advertising;
			var options = {
				admin_id:   user.id,
				advertising_id: advertising.id,
				from: from,
				to: to
			};
			return models.advActivityByAdmin.create(options);
		})
		.then(function(){
            var response = formatters.advertising.simple(req['temp'].advertising);
            res.status(200).send(response);
		}).catch(next);
}

/**
 * Read recommendations for item
 *
 * @param req
 * @param res
 * @param next
 */
function read(req, res, next) {

	var itemId = req.params.itemId;

	var limit = parseInt(req.query.limit, 10) || config.constants.LIMIT;
	var step = parseInt(req.query.page, 10) - 1 || config.constants.STEP;
	var offset = parseInt(req.query.offset, 10) || step * limit;

	var order = {
		by: req.query.by || 'id',
		at: req.query.at || 'DESC'
	};
	var user = req['user'];

	if (!user){
        var error = new Error('User is not exist!');
        error.status = 403;
        return next(error);
	}

	var adminId = ((user.role === 'SUPER_ADMIN') ? null : user.id);

	services.advertising.getAdvertising(itemId, order, limit, offset,  adminId)
		.then(function (rawLines) {
			req.temp = {
				advertising: formatters.advertising.forAdmin(rawLines)
			};
			return services.advertising.getCountAdvertising(itemId, adminId);
		})
		.then(function (countAdvertising) {
			var response = {
				advertising: req.temp.advertising,
				meta       : {
					totalCount: countAdvertising
				}
			};
			res.status(200).send(response);
		})
		.catch(next);
}

/**
 * Update recommendation
 *
 * @param req
 * @param res
 * @param next
 */
function update(req, res, next) {
	var user = req.user;
	var itemId = req.params.itemId;
	var productId = req.params.productId;

	var newFrom = parseInt(req.body.from) || Date.now();
	var newTo = parseInt(req.body.to) || Date.now() + config.constants.ONE_DAY;

	var oldTo = null, oldFrom = null;

	var options = {
		include: [{
			model: models.itemByShop,
			as: 'recommends',
			where: {id: productId}
		}]
	};

    req.temp = {};

	models.items.findById(itemId, options)
		.then(function (item) {
			if (!item) {
				var error = new Error('Not found!');
				error.status = 404;
				throw error;
			}

			var recommends = item.recommends;
			var advertising = recommends[0].advertising;
			oldFrom = advertising.from;
			oldTo = advertising.to;

			advertising.from = newFrom;
			advertising.to = newTo;

			return advertising.save();
		})
		.then(function (advertising) {
            req['temp'].advertising = advertising;

            if (newFrom !== oldFrom || oldTo !== newTo){
                var options = {
                    admin_id:   user.id,
                    advertising_id: advertising.id,
                    from: newFrom,
                    to: newTo
                };
                return models.advActivityByAdmin.create(options);
			}
		}).then(function(){
        	res.status(200).send(req['temp'].advertising);
		}).catch(next);
}

/**
 * Remove recommendation
 *
 * @param req
 * @param res
 * @param next
 */
function destroy(req, res, next) {
	var itemId = req.params.itemId;
	var productId = req.params.productId;

	models.items.findById(itemId)
		.then(function (item) {
			if (!item) {
				var error = new Error('Item not found!');
				error.status = 404;
				throw error;
			}
			return item.removeRecommends(productId);
		})
		.then(function () {
			res.status(204).send();
		})
		.catch(next);
}


module.exports = {
	create : [validators.advertising.create, create],
	read   : read,
	update : [validators.advertising.update, update],
	destroy: destroy
};